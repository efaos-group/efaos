package it.polimi.network;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ProtocolParserTest {

    ProtocolParser parser;

    @Before
    public void initTest(){
        parser = new ProtocolParser();
    }

    @Test
    public void parsingTest(){
        parser.setTopic("topic");
        parser.setPlayerID("player");
        parser.setCommands("move");
        String col = "A";
        String row = "3";
        String []arguments = new String[] {col,row};
        parser.setMessageArguments(arguments);
        assertEquals("#topic##player##move#[A][3]",parser.getParsedMessage());
        parser.parseMessage("#topic##player##move#[A][3]");
        String []commands = parser.getCommands();
        assertEquals("topic",commands[0]);
        assertEquals("player",commands[1]);
        assertEquals("move",commands[2]);
        String []arg = parser.getMessageArguments();
        assertEquals("A", arg[0]);
        assertEquals("3", arg[1]);
    }
}
