/**
 *
 */
package it.polimi.network;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class TopicTest {

    private Topic topic1, topic2;

    @Before
    public void setUp() {
        topic1 = new Topic("galilei");
        topic2 = new Topic("galilei");
    }

    @Test
    public void testGetter() {

        assertThat(topic1.getMinimalTopicCode(), instanceOf(String.class));
        assertThat(topic1.getCompleteTopicCode(), instanceOf(String.class));
    }

    @Test
    public void testEqualityOfCode() {

        assertThat(topic1.getMinimalTopicCode(), not(topic2.getMinimalTopicCode()));
        assertThat(topic1.getCompleteTopicCode(), not(topic2.getCompleteTopicCode()));
        assertThat(topic1, not(topic2));
    }

}
