/**
 *
 */
package it.polimi.server.controller.fake;

import it.polimi.server.model.GameLogic;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.ItemDeck.ItemType;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.card.item.ItemFactory;
import it.polimi.server.model.fake.FakeNotifier;
import it.polimi.server.model.fake.FakeTableMap;
import it.polimi.server.model.sector.walkable.SecureSector;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class FakeGameLogic extends GameLogic {

    public enum EVENT {PASS, MOVE, USE, ATTACK, SELECT, DRAWSECTOR, DRAWITEM, DISCARD, DISCONNECT}

    public final Queue<EVENT> buffer;
    public final Queue<Sector> bufferSector;
    public final Queue<ItemCard> bufferCard;

    private String username;

    public FakeGameLogic() throws IOException {
        super(new FakeTableMap(), new HashMap<String, Player>(), new FakeNotifier());
        buffer = new LinkedList<EVENT>();
        bufferSector = new LinkedList<Sector>();
        bufferCard = new LinkedList<ItemCard>();
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public void passTurn(){
        buffer.add(EVENT.PASS);
    }

    @Override
    public void move(Sector to){
        buffer.add(EVENT.MOVE);
        bufferSector.add(to);
    }

    @Override
    public void useCard(ItemCard card, Sector sector) {
        buffer.add(EVENT.USE);
        bufferSector.add(sector);
        bufferCard.add(card);
    }

    @Override
    public void attack() {
        buffer.add(EVENT.ATTACK);
    }

    @Override
    public void selectSectorForNoise(Sector sector){
        buffer.add(EVENT.SELECT);
        bufferSector.add(sector);
    }

    @Override
    public String getGamerIdentifier() {
        return username;
    }

    @Override
    public Sector getSector(Character col, Integer row) {
        return new SecureSector(col, row, null);
    }

    @Override
    public void movedIntoEscapeSector(Sector sector) { }

    @Override
    public void movedIntoDangerousSector(Sector sector) { }

    @Override
    public void movedIntoSecureSector(Sector sector) { }

    @Override
    public void deleteDeadPlayer(String playerID) { }

    @Override
    public void deleteEscapedPlayer(String playerID) { }

    @Override
    public ItemCard getItemCard(String cardType) {
        return ItemFactory.factory(ItemType.parseInput(cardType), null);
    }

    @Override
    public void drawSector() {
        buffer.add(EVENT.DRAWSECTOR);
    }

    @Override
    public void drawItemCard() {
        buffer.add(EVENT.DRAWITEM);
    }

    @Override
    public void discardCard(ItemCard card) {
        buffer.add(EVENT.DISCARD);
        bufferCard.add(card);
    }

    @Override
    public void removeDisconnectedPlayerFromGame(String playerID) {
        buffer.add(EVENT.DISCONNECT);
    }

}
