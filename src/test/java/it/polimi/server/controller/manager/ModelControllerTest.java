/**
 *
 */
package it.polimi.server.controller.manager;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import it.polimi.server.controller.Controller;
import it.polimi.server.controller.exception.GamerCheatException;
import it.polimi.server.controller.fake.FakeGameLogic;
import it.polimi.server.model.Sector;
import it.polimi.server.model.card.item.DefenseCard;
import it.polimi.server.model.card.item.SpotlightCard;
import it.polimi.server.model.exception.BadSectorException;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class ModelControllerTest {

    private Controller controller;
    private FakeGameLogic fakeLogic;

    @Before
    public void setUp() throws IOException {
        fakeLogic = new FakeGameLogic();
        controller = new ModelController(fakeLogic);
    }

    @Test
    public void testMove() {
        final String pg = "player1";
        fakeLogic.setUsername(pg);

        controller.move(pg, "A", "1");

        assertEquals(FakeGameLogic.EVENT.MOVE, fakeLogic.buffer.poll());
        Sector sector = fakeLogic.bufferSector.poll();
        assertTrue("The destination sector should be different", sector.getCol() == 'A' && sector.getRow() == 1);
    }

    @Test
    public void testPassTurn() {
        final String pg = "player1";
        fakeLogic.setUsername(pg);

        controller.passTurn(pg);

        assertEquals(FakeGameLogic.EVENT.PASS, fakeLogic.buffer.poll());
    }

    @Test
    public void testUseCard() {
        final String pg = "player1";
        fakeLogic.setUsername(pg);


        controller.useCard(pg, "spotlight", "B", "3");

        assertEquals(FakeGameLogic.EVENT.USE, fakeLogic.buffer.poll());
        assertThat(fakeLogic.bufferCard.poll(), instanceOf(SpotlightCard.class));

        Sector sector = fakeLogic.bufferSector.poll();
        assertTrue("The destination sector should be different", sector.getCol() == 'B' && sector.getRow() == 3);

        controller.useCard(pg, "defense", "B", "3");

        assertEquals(FakeGameLogic.EVENT.USE, fakeLogic.buffer.poll());
        assertThat(fakeLogic.bufferCard.poll(), instanceOf(DefenseCard.class));
    }

    @Test
    public void testDiscardCard() {
        final String pg = "player1";
        fakeLogic.setUsername(pg);

        controller.discardCard(pg, "spotlight");

        assertEquals(FakeGameLogic.EVENT.DISCARD, fakeLogic.buffer.poll());
        assertThat(fakeLogic.bufferCard.poll(), instanceOf(SpotlightCard.class));
    }

    @Test
    public void testDrawSectorCard() {
        final String pg = "player1";
        fakeLogic.setUsername(pg);

        controller.drawSectorCard(pg);

        assertEquals(FakeGameLogic.EVENT.DRAWSECTOR, fakeLogic.buffer.poll());
    }

    @Test
    public void testDrawItemCard() {
        final String pg = "player1";
        fakeLogic.setUsername(pg);

        controller.drawItemCard(pg);

        assertEquals(FakeGameLogic.EVENT.DRAWITEM, fakeLogic.buffer.poll());
    }

    @Test
    public void testAttack() {
        final String pg = "player1";
        fakeLogic.setUsername(pg);

        controller.attack(pg);

        assertEquals(FakeGameLogic.EVENT.ATTACK, fakeLogic.buffer.poll());
    }

    @Test
    public void testSelectNoise() {
        final String pg = "player1";
        fakeLogic.setUsername(pg);

        controller.selectSectorForNoise(pg, "D", "7");

        assertEquals(FakeGameLogic.EVENT.SELECT, fakeLogic.buffer.poll());
        Sector sector = fakeLogic.bufferSector.poll();
        assertTrue("The destination sector should be different", sector.getCol() == 'D' && sector.getRow() == 7);
    }

    @Test
    public void testDisconnect() {
        final String pg = "player1";
        fakeLogic.setUsername(pg);

        controller.removeDisconnectedPlayerFromGame(pg);

        assertEquals(FakeGameLogic.EVENT.DISCONNECT, fakeLogic.buffer.poll());
    }

    @Test(expected = GamerCheatException.class)
    public void testCheatException() {
        final String pg = "player1";
        fakeLogic.setUsername(pg);

        controller.attack("player2");
    }

    @Test(expected = BadSectorException.class)
    public void testSectorException() {
        final String pg = "player1";
        fakeLogic.setUsername(pg);

        controller.useCard(pg, "spotlight", "17", "A");

        assertEquals(FakeGameLogic.EVENT.USE, fakeLogic.buffer.poll());
        assertThat(fakeLogic.bufferCard.poll(), instanceOf(SpotlightCard.class));
    }
}
