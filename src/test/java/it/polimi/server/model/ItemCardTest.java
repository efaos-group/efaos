/**
 *
 */
package it.polimi.server.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.server.model.ItemDeck.ItemType;
import it.polimi.server.model.card.item.ItemFactory;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 *
 */
public class ItemCardTest {

    private ItemCard adl, atk, dfs, sdv, spt, tlp;

    @Before
    public void setUp() throws Exception {
        adl = ItemFactory.factory(ItemType.ADRENALINE, null);
        atk = ItemFactory.factory(ItemType.ATTACK, null);
        dfs = ItemFactory.factory(ItemType.DEFENSE, null);
        sdv = ItemFactory.factory(ItemType.SEDATIVES, null);
        spt = ItemFactory.factory(ItemType.SPOTLIGHT, null);
        tlp = ItemFactory.factory(ItemType.TELEPORT, null);
    }

    /**
     * Test method for {@link it.polimi.server.model.ItemCard#hashCode()}.
     */
    @Test
    public void testHashCode() {
        assertTrue("", adl.hashCode() == ItemFactory.factory(ItemType.ADRENALINE, null).hashCode());
        assertTrue("", atk.hashCode() == ItemFactory.factory(ItemType.ATTACK, null).hashCode());
        assertTrue("", dfs.hashCode() == ItemFactory.factory(ItemType.DEFENSE, null).hashCode());
        assertTrue("", sdv.hashCode() == ItemFactory.factory(ItemType.SEDATIVES, null).hashCode());
        assertTrue("", spt.hashCode() == ItemFactory.factory(ItemType.SPOTLIGHT, null).hashCode());
        assertTrue("", tlp.hashCode() == ItemFactory.factory(ItemType.TELEPORT, null).hashCode());
    }


    @Test
    public void testEqualsObject() {
        assertTrue("", adl.equals(adl));

        assertFalse("", tlp.equals(null));
    }

    @Test
    public void testIsCard() {
        assertFalse("", adl.isAttackCard());
        assertFalse("", atk.isDefenseCard());
        assertFalse("", sdv.isSpotlightCard());
    }

}
