package it.polimi.server.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.server.model.card.escape.RedCard;
import it.polimi.server.model.card.item.AdrenalineCard;
import it.polimi.server.model.card.sector.BaseSectorDeck;
import it.polimi.server.model.card.sector.NoiseInAnySectorCard;
import it.polimi.server.model.card.sector.NoiseInAnySectorItem;
import it.polimi.server.model.card.sector.NoiseInYourSectorCard;
import it.polimi.server.model.card.sector.NoiseInYourSectorItem;
import it.polimi.server.model.card.sector.SilenceCard;
import it.polimi.server.model.fake.FakeBaseSectorDeck;

import org.junit.Before;
import org.junit.Test;

public class SectorCardTest {

    private NoiseInAnySectorCard noiseAny;
    private NoiseInAnySectorItem noiseAnyItem;
    private NoiseInYourSectorCard noiseYour;
    private NoiseInYourSectorItem noiseYourItem;
    private SilenceCard silence;
    private FakeBaseSectorDeck deck;

    @Before
    public void initTest() {
        deck = new FakeBaseSectorDeck();
        noiseAny = new NoiseInAnySectorCard(deck);
        noiseAnyItem = new NoiseInAnySectorItem(deck);
        noiseYour = new NoiseInYourSectorCard(deck);
        noiseYourItem = new NoiseInYourSectorItem(deck);
        silence = new SilenceCard(deck);
    }

    @Test
    public void discardAnyTest() {
        noiseAny.discard();
        deck.caller.remove(0);
    }

    @Test
    public void discardAnyItemTest() {
        noiseAnyItem.discard();
        deck.caller.remove(0);
    }

    @Test
    public void discardYourTest() {
        noiseYour.discard();
        deck.caller.remove(0);
    }

    @Test
    public void discardYourItemTest() {
        noiseYourItem.discard();
        deck.caller.remove(0);
    }

    @Test
    public void discardSilenceTest() {
        silence.discard();
        deck.caller.remove(0);
    }

    @Test
    public void equalsAndHashcodeTest() {
        assertTrue(noiseAny.equals(new NoiseInAnySectorCard(null)) &&
                noiseAny.hashCode() == new NoiseInAnySectorCard(null).hashCode());

        assertTrue(noiseAnyItem.equals(new NoiseInAnySectorItem(null)) &&
                noiseAnyItem.hashCode() == new NoiseInAnySectorItem(null).hashCode());

        assertTrue(noiseYour.equals(new NoiseInYourSectorCard(null)) &&
                noiseYour.hashCode() == new NoiseInYourSectorCard(null).hashCode());

        assertTrue(noiseYourItem.equals(new NoiseInYourSectorItem(null)) &&
                noiseYourItem.hashCode() == new NoiseInYourSectorItem(null).hashCode());

        assertTrue(silence.equals(new SilenceCard(null)) &&
                silence.hashCode() == new SilenceCard(null).hashCode());

        assertFalse(noiseAny.equals(new SilenceCard(null)));

        assertFalse(noiseAnyItem.equals(new AdrenalineCard(null)));

        assertFalse(noiseYour.equals(new RedCard()));

        assertFalse(noiseYourItem.equals(new NoiseInYourSectorCard(null)));

        assertFalse(silence.equals(new BaseSectorDeck()));
    }

}
