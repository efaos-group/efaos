package it.polimi.server.model.player;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.server.model.Player;
import it.polimi.server.model.card.item.DefenseCard;
import it.polimi.server.model.exception.PlayerException;
import it.polimi.server.model.fake.FakeCharacter;
import it.polimi.server.model.fake.FakeDeckReceiver;
import it.polimi.server.model.fake.FakeGameClearer;
import it.polimi.server.model.fake.FakeItem;
import it.polimi.server.model.fake.FakePlayerListener;
import it.polimi.server.model.fake.FakeTableMap;

import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class GamerTest {

    private Player gamer1,gamer2;
    private FakeCharacter char1, char2;

    private FakeTableMap fakeMap;
    private FakePlayerListener listener;
    private FakeGameClearer gameClearer;

    @Before
    public void initTest() throws IOException {
        fakeMap = new FakeTableMap();
        listener = new FakePlayerListener();
        gameClearer = new FakeGameClearer();

        char1 = new FakeCharacter("character 1");
        char2 = new FakeCharacter("character 2");

        gamer1 = new Gamer("Alien1", char1, fakeMap);
        gamer1.setPlayerListeners(listener, gameClearer);
        gamer2 = new Gamer("Alien2", char2, fakeMap);
        gamer2.setPlayerListeners(listener, gameClearer);


    }

    @Test
    public void notifyDeathTest() {
        gamer1.notifyDeath();
        assertEquals(listener.caller.remove(), FakePlayerListener.CALL.EVENT);
    }

    @Test
    public void notifyAttackTest() {
        LinkedList<Player> list = new LinkedList<Player>();
        list.add(gamer1);
        list.add(gamer2);
        gamer1.notifyAttack(list);
        assertEquals(listener.caller.remove(), FakePlayerListener.CALL.EVENT);
    }

    @Test
    public void drewSectorCardTest() {
        gamer1.drawSectorCard(null);
        assertEquals(listener.caller.remove(), FakePlayerListener.CALL.SECTORCARD);
    }

    @Test
    public void drewItemCardTest() {
        gamer1.drawItemCard(new FakeItem(null));
        assertEquals(listener.caller.remove(), FakePlayerListener.CALL.ITEMCARD);

        for (int i=0; i<2; i++) {
            gamer1.drawItemCard(new FakeItem(null));
            listener.caller.remove();
        }

        gamer1.drawItemCard(new FakeItem(null));
        assertEquals(listener.caller.remove(), FakePlayerListener.CALL.ITEMCARD);
    }

    @Test
    public void discardTest() {
        FakeItem fakeItem = new FakeItem(new FakeDeckReceiver());
        gamer2.drawItemCard(fakeItem);
        listener.caller.remove();

        gamer2.discard(fakeItem);
        assertEquals(listener.caller.remove(), FakePlayerListener.CALL.DISCARDED);

        assertTrue("items' size should be 0", gamer2.getItemsSize() == 0);
    }

    @Test (expected = PlayerException.class)
    public void discardExceptionTest() {
        gamer2.discard(new FakeItem(null));
        assertEquals(listener.caller.remove(), FakePlayerListener.CALL.EXCEPTION);
    }

    @Test
    public void useDefenseTest() {
        gamer2.drawItemCard(new DefenseCard(new FakeDeckReceiver()));
        assertTrue("char2 should be able to defend", gamer2.getCharacter().canDefend());
        gamer2.usedDefense();
    }

    @Test
    public void hashCodeTest() {
        assertFalse("gamer1 and gamer2 shouldn't be equal", gamer1.equals(gamer2));
        Player gamer3 = new Gamer("Alien1", char1, fakeMap);
        assertTrue("gamer1 and gamer3 should be equal", gamer1.equals(gamer3));
        Set<Player> set = new HashSet<Player>();
        set.add(gamer1);
        assertTrue("set should contain gamer1", set.contains(gamer1));
        set.remove(gamer3);
        assertFalse("set shouldn't contain gamer1", set.contains(gamer1));
    }

    @Test
    public void useCardTest() {
        FakeItem fakeItem = new FakeItem(new FakeDeckReceiver());
        gamer2.drawItemCard(fakeItem);
        gamer2.useCard(fakeItem, null);
        assertTrue("gamer2 doesn't have the card anymore", gamer2.getItemsSize() == 0);
    }

    @Test (expected = PlayerException.class)
    public void useCardTestException() {
        gamer2.useCard( new FakeItem(null), null);
    }
}



