package it.polimi.server.model.notifier;

import static org.junit.Assert.assertEquals;
import it.polimi.server.model.BaseCharacter;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.SectorCard;
import it.polimi.server.model.card.sector.SilenceCard;
import it.polimi.server.model.event.card.item.ItemUsedEvent;
import it.polimi.server.model.event.card.sector.ChooseNoiseSectorEvent;
import it.polimi.server.model.event.card.sector.NoiseInSectorEvent;
import it.polimi.server.model.event.card.sector.NoiseInSectorItemEvent;
import it.polimi.server.model.event.card.sector.SilenceEvent;
import it.polimi.server.model.event.character.CharacterDeathEvent;
import it.polimi.server.model.event.game.AlienWinningEvent;
import it.polimi.server.model.event.game.HumanEscapedEvent;
import it.polimi.server.model.event.game.HumanWinningEvent;
import it.polimi.server.model.event.game.StartGameEvent;
import it.polimi.server.model.event.movement.DangerousMovementEvent;
import it.polimi.server.model.event.movement.IllegalMovementEvent;
import it.polimi.server.model.event.movement.NotEscapedEvent;
import it.polimi.server.model.event.movement.SecureMovementEvent;
import it.polimi.server.model.event.player.DiscardedItemCardEvent;
import it.polimi.server.model.event.player.DrawnItemCardEvent;
import it.polimi.server.model.event.player.DrawnSectorCardEvent;
import it.polimi.server.model.event.player.IllegalPlayerActionEvent;
import it.polimi.server.model.event.turn.EndTurnEvent;
import it.polimi.server.model.event.turn.StartTurnEvent;
import it.polimi.server.model.fake.FakeCharacter;
import it.polimi.server.model.fake.FakeItem;
import it.polimi.server.model.fake.FakeObserver;
import it.polimi.server.model.fake.FakePlayer;
import it.polimi.server.model.sector.walkable.DangerousSector;
import it.polimi.server.model.sector.walkable.EscapeSector;

import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

public class NotificationHandlerTest {
    private NotificationHandler notifier;
    private FakeObserver obs;
    private Player gamer;
    private BaseCharacter character;
    private Sector sector;
    private ItemCard item;
    private SectorCard card;

    @Before
    public void initTest() throws InterruptedException {
        notifier = new NotificationHandler();
        character = new FakeCharacter("char");
        gamer = new FakePlayer("player", character, null);
        obs = new FakeObserver();
        notifier.register(obs);
        sector = new DangerousSector('A',1,null);
        item = new FakeItem(null);
        card = new SilenceCard(null);
        notifier.notifyStartTurn(gamer);
        assertEquals(StartTurnEvent.class, obs.caller.remove());
    }

    @Test
    public void notifyDangerousMovementTest() throws InterruptedException {
        notifier.notifyDangerousMovement(sector);
        assertEquals(DangerousMovementEvent.class, obs.caller.remove());
    }

    @Test
    public void notifySecureMovementTest() throws InterruptedException {
        notifier.notifySecureMovement(sector);
        assertEquals(SecureMovementEvent.class, obs.caller.remove());
    }
    @Test
    public void notifyHumanNotEscapedTest() throws InterruptedException {
        notifier.notifyHumanNotEscaped(sector);
        assertEquals(NotEscapedEvent.class, obs.caller.remove());
    }
    @Test
    public void notifyHumanEscapedTest() throws InterruptedException {
        notifier.notifyHumanEscaped(new EscapeSector('C', 3, null));
        assertEquals(HumanEscapedEvent.class, obs.caller.remove());
    }
    @Test
    public void notifyCardUsageTest() throws InterruptedException {
        notifier.notifyCardUsed(new ItemUsedEvent(gamer, item));
        assertEquals(ItemUsedEvent.class, obs.caller.remove());
    }
    @Test
    public void notifyStartGameTest() throws InterruptedException {
        Collection <Player> pl = new LinkedList<Player>();
        notifier.notifyStartGame(pl);
        assertEquals(StartGameEvent.class, obs.caller.remove());
    }
    @Test
    public void notifyEndTurnTest() throws InterruptedException {
        notifier.notifyEndTurn();
        assertEquals(EndTurnEvent.class, obs.caller.remove());
    }
    @Test
    public void notifyDrawItemCardTest() throws InterruptedException {
        notifier.notifyDrawnItemCard(item);
        assertEquals(DrawnItemCardEvent.class, obs.caller.remove());
    }

    @Test
    public void notifyDiscardedCardEventTest() throws InterruptedException{
        notifier.notifyDiscardedCardEvent(item);
        assertEquals(DiscardedItemCardEvent.class, obs.caller.remove());
    }
    @Test
    public void notifyDrewSectorCardEventTest() throws InterruptedException {
        notifier.notifyDrawnSectorCard(card);
        assertEquals(DrawnSectorCardEvent.class, obs.caller.remove());
    }
    @Test
    public void notifyChooseSectorTest() throws InterruptedException {
        notifier.notifyChooseSector();
        assertEquals(ChooseNoiseSectorEvent.class, obs.caller.remove());
    }
    @Test
    public void notifyNoiseInSectorTest() throws InterruptedException {
        notifier.notifyNoiseInSector(sector);
        assertEquals(NoiseInSectorEvent.class, obs.caller.remove());
    }
    @Test
    public void notifyNoiseInSectorItemTest() throws InterruptedException {
        notifier.notifyNoiseInSectorItem(sector);
        assertEquals(NoiseInSectorItemEvent.class, obs.caller.remove());
    }
    @Test
    public void notifySilence() throws InterruptedException {
        notifier.notifySilence();
        assertEquals(SilenceEvent.class, obs.caller.remove());
    }
    @Test
    public void notifyCharacterEvent(){
        notifier.notifyCharacterEvent(new CharacterDeathEvent("", ""));
        assertEquals(CharacterDeathEvent.class, obs.caller.remove());
    }
    @Test
    public void notifyHumanWinTest(){
        notifier.notifyHumanWin();
        assertEquals(HumanWinningEvent.class, obs.caller.remove());
    }
    @Test
    public void notifyAlienWinTest() {
        notifier.notifyAlienWin();
        assertEquals(AlienWinningEvent.class, obs.caller.remove());
    }
    @Test
    public void notifyExceptionTest(){
        notifier.notifyException("");
        assertEquals(IllegalPlayerActionEvent.class, obs.caller.remove());
    }
    @Test
    public void notifyIllegalMovementEvent(){
        notifier.notifyIllegalMovementGamer();
        assertEquals(IllegalMovementEvent.class, obs.caller.remove());
    }
}
