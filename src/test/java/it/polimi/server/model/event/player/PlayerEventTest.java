package it.polimi.server.model.event.player;

import static org.junit.Assert.assertEquals;
import it.polimi.common.observer.ModelEvent;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.Player;
import it.polimi.server.model.SectorCard;
import it.polimi.server.model.card.sector.SilenceCard;
import it.polimi.server.model.fake.FakeCharacter;
import it.polimi.server.model.fake.FakeItem;
import it.polimi.server.model.fake.FakeView;
import it.polimi.server.model.fake.FakeView.EVT;
import it.polimi.server.model.player.Gamer;

import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

public class PlayerEventTest {

    private String playerID;
    private Player gamer;
    private SectorCard sectorCard;
    private Collection<Player> players;
    private ItemCard item;
    private FakeView view, view2;

    @Before
    public void initTest() {
        playerID = "player";
        gamer = new Gamer(playerID, new FakeCharacter("alien"), null);
        players = new LinkedList<Player>();
        sectorCard = new SilenceCard(null);
        players.add(gamer);
        item = new FakeItem(null);
        view = new FakeView(playerID);
        view2 = new FakeView("gamer2");
    }

    @Test
    public void discardedItemCardTest() {
        ModelEvent evt = new DiscardedItemCardEvent(playerID, item);
        assertEquals(playerID + " discarded a " + item.toString(), evt.getMessage());
        evt.reactToView(view);
        assertEquals(EVT.SHOW, view.caller.remove());
        assertEquals(EVT.REMOVED_CARD, view.caller.remove());
        assertEquals(EVT.DISABLE, view.caller.remove());
        assertEquals(EVT.ITEM_USAGE, view.caller.remove());
        assertEquals(EVT.END_TURN, view.caller.remove());
        assertEquals(EVT.COMMAND, view.caller.remove());

        evt.reactToView(view2);
        assertEquals(EVT.SHOW, view2.caller.remove());
        assertEquals(null, view2.caller.poll());
    }

    @Test
    public void drawItemCardEventTest() {
        ModelEvent evt = new DrawnItemCardEvent(playerID, item);
        assertEquals(playerID + " drew an item card", evt.getMessage());
        evt.reactToView(view);
        assertEquals(EVT.SHOW, view.caller.remove());
        assertEquals(EVT.ADD_CARD, view.caller.remove());
        assertEquals(EVT.DISABLE, view.caller.remove());
        assertEquals(EVT.ITEM_USAGE, view.caller.remove());
        assertEquals(EVT.END_TURN, view.caller.remove());
        assertEquals(EVT.COMMAND, view.caller.remove());

        evt.reactToView(view2);
        assertEquals(EVT.SHOW, view2.caller.remove());
    }

    @Test
    public void drawnSectorCardEvent() {
        ModelEvent evt = new DrawnSectorCardEvent(playerID, sectorCard);
        assertEquals(playerID +  " drew a sector card", evt.getMessage());
        evt.reactToView(view);
        assertEquals(EVT.SHOW, view.caller.remove());

        evt.reactToView(view2);
        assertEquals(EVT.SHOW, view2.caller.remove());
    }

    @Test
    public void illegalPlayerActionEventTest() {
        ModelEvent evt = new IllegalPlayerActionEvent(playerID, "something");
        assertEquals("You can't perform this action!" + "something", evt.getMessage());
        evt.reactToView(view);
        assertEquals(EVT.SHOW, view.caller.remove());

        evt.reactToView(view2);
        assertEquals(null, view2.caller.poll());
    }

}
