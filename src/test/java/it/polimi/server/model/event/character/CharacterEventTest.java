package it.polimi.server.model.event.character;

import static org.junit.Assert.assertEquals;
import it.polimi.common.observer.ModelEvent;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.fake.FakeCharacter;
import it.polimi.server.model.fake.FakeView;
import it.polimi.server.model.fake.FakeView.EVT;
import it.polimi.server.model.player.Gamer;
import it.polimi.server.model.sector.walkable.SecureSector;

import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

public class CharacterEventTest {

    private String playerID, enemy;
    private Player gamer;
    private Sector sector;
    private Collection<Player> players;
    private FakeView view;

    @Before
    public void initTest() {
        playerID = "player";
        enemy = "enemy";
        gamer = new Gamer(playerID, new FakeCharacter("alien"), null);
        sector = new SecureSector('A',1, null);
        players = new LinkedList<Player>();
        players.add(gamer);
        view = new FakeView(playerID);
    }

    @Test
    public void attackCardEventTest() {
        ModelEvent evt = new AttackEvent(playerID, sector, players);
        assertEquals(playerID + " attacked in " + sector.toString(), evt.getMessage());
        evt.reactToView(view);
        assertEquals(EVT.SHOW,view.caller.remove());
        assertEquals(EVT.DISABLE, view.caller.remove());
        assertEquals(EVT.END_TURN, view.caller.remove());
        assertEquals(EVT.COMMAND, view.caller.remove());
    }

    @Test
    public void characterDeathEventTest() {
        ModelEvent evt = new CharacterDeathEvent(playerID, enemy);
        assertEquals(enemy + " was attacked and died.", evt.getMessage());
        evt.reactToView(view);
        assertEquals(EVT.SHOW,view.caller.remove());
    }

}
