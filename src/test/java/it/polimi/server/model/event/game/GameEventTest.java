package it.polimi.server.model.event.game;

import static org.junit.Assert.assertEquals;
import it.polimi.common.observer.ModelEvent;
import it.polimi.server.model.Player;
import it.polimi.server.model.fake.FakeCharacter;
import it.polimi.server.model.fake.FakePlayer;
import it.polimi.server.model.fake.FakeView;
import it.polimi.server.model.fake.FakeView.EVT;
import it.polimi.server.model.sector.walkable.EscapeSector;

import java.util.Collection;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

public class GameEventTest {

    private String playerID;
    private FakeView view, view2;
    @Before
    public void initTest() {
        playerID = "player";
        view = new FakeView(playerID);
        view2 = new FakeView("gamer2");
    }

    @Test
    public void alienWinningEventTest() {
        ModelEvent evt = new AlienWinningEvent(playerID);
        assertEquals("Aliens won! " + playerID + " killed the last Human!", evt.getMessage());
        evt.reactToView(view);
        assertEquals(EVT.SHOW, view.caller.remove());
        assertEquals(EVT.DISABLE, view.caller.remove());
    }

    @Test
    public void humanEscapedEventTest() {
        ModelEvent evt = new HumanEscapedEvent(playerID, new EscapeSector('A', 2, null));
        assertEquals(playerID + " succesfully escaped!", evt.getMessage());
        evt.reactToView(view);
        assertEquals(EVT.SHOW, view.caller.remove());

        evt.reactToView(view2);
        assertEquals(EVT.SHOW, view2.caller.remove());
        assertEquals(null, view2.caller.poll());
    }

    @Test
    public void HumanWinningEvent() {
        ModelEvent evt = new HumanWinningEvent(playerID);
        assertEquals("Humans won! Finally" + playerID + " managed to escape!", evt.getMessage());
        evt.reactToView(view);
        assertEquals(EVT.SHOW, view.caller.remove());
    }

    @Test
    public void testStartGameEvent() {
        Collection<Player> players = new HashSet<Player>();
        players.add(new FakePlayer("gamer1", new FakeCharacter("human1"), null));
        players.add(new FakePlayer("gamer2", new FakeCharacter("alien1"), null));
        players.add(new FakePlayer("gamer3", new FakeCharacter("alien2"), null));

        ModelEvent evt = new StartGameEvent(players);
        assertEquals("The game has just started", evt.getMessage());
        evt.reactToView(view);
        assertEquals(EVT.SHOW, view.caller.remove());
        assertEquals(EVT.SET_POSITION, view.caller.remove());
        assertEquals(EVT.SET_BEHAVOIR, view.caller.remove());
    }
}
