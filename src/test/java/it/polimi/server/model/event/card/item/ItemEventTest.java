package it.polimi.server.model.event.card.item;

import static org.junit.Assert.assertEquals;
import it.polimi.common.observer.ModelEvent;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.fake.FakeCharacter;
import it.polimi.server.model.fake.FakeItem;
import it.polimi.server.model.fake.FakeView;
import it.polimi.server.model.fake.FakeView.EVT;
import it.polimi.server.model.player.Gamer;
import it.polimi.server.model.sector.walkable.SecureSector;

import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

public class ItemEventTest {

    private String playerID;
    private Player gamer;
    private Sector sector;
    private Collection<Player> players;
    private ItemCard item;
    private FakeView view, view2;

    @Before
    public void initTest() {
        playerID = "player";
        gamer = new Gamer(playerID, new FakeCharacter("alien"), null);
        sector = new SecureSector('A',1, null);
        players = new LinkedList<Player>();
        players.add(gamer);
        item = new FakeItem(null);
        view = new FakeView(playerID);
        view2 = new FakeView("gamer2");
    }

    @Test
    public void attackCardEventTest() {
        ModelEvent evt = new AttackCardEvent(gamer, players, sector, item);
        assertEquals(playerID + " used AttackCard in sector " + sector.toString(), evt.getMessage().split("\\n")[0]);
        evt.reactToView(view);
        assertEquals(EVT.SHOW, view.caller.remove());
        assertEquals(EVT.DISABLE, view.caller.remove());
        assertEquals(EVT.END_TURN, view.caller.remove());
        assertEquals(EVT.ITEM_USAGE, view.caller.remove());
        assertEquals(EVT.REMOVED_CARD, view.caller.remove());
        assertEquals(EVT.COMMAND, view.caller.remove());

    }

    @Test
    public void defenseCardEventTest() {
        ModelEvent evt = new DefenseCardEvent(gamer, item);
        assertEquals(playerID + " was attacked. He used a " + item.toString(), evt.getMessage());
        evt.reactToView(view);
        assertEquals(EVT.SHOW, view.caller.remove());
        assertEquals(EVT.REMOVED_CARD, view.caller.remove());

        evt.reactToView(view2);
        assertEquals(EVT.SHOW, view2.caller.remove());
        assertEquals(null, view2.caller.poll());
    }

    @Test
    public void spotlightCardEventTest() {
        Multimap<Sector, Player> spotted = ArrayListMultimap.create();
        spotted.put(sector, gamer);
        spotted.put(sector, gamer);
        ModelEvent evt = new SpotlightCardEvent(gamer, spotted , item);
        assertEquals(playerID + " used a Spotlight card", evt.getMessage().split("\\n")[0]);
        evt.reactToView(view);
        assertEquals(EVT.SHOW, view.caller.remove());
        assertEquals(EVT.REMOVED_CARD, view.caller.remove());
        assertEquals(EVT.COMMAND, view.caller.remove());
    }

    @Test
    public void itemUsedEventTest() {
        ModelEvent evt = new ItemUsedEvent(gamer, item);
        assertEquals(playerID + " used a " + item.toString(), evt.getMessage());
        evt.reactToView(view);
        assertEquals(EVT.SHOW, view.caller.remove());
        assertEquals(EVT.REMOVED_CARD, view.caller.remove());
        assertEquals(EVT.COMMAND, view.caller.remove());

        evt.reactToView(view2);
        assertEquals(EVT.SHOW, view2.caller.remove());
        assertEquals(null, view2.caller.poll());
    }
}
