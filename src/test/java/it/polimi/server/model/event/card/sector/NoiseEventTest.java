package it.polimi.server.model.event.card.sector;

import static org.junit.Assert.assertEquals;
import it.polimi.common.observer.ModelEvent;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.fake.FakeCharacter;
import it.polimi.server.model.fake.FakeView;
import it.polimi.server.model.fake.FakeView.EVT;
import it.polimi.server.model.player.Gamer;
import it.polimi.server.model.sector.walkable.SecureSector;

import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

public class NoiseEventTest {

    private String playerID;
    private Player gamer;
    private Sector sector;
    private Collection<Player> players;
    private FakeView view, view2;

    @Before
    public void initTest() {
        playerID = "player";
        gamer = new Gamer(playerID, new FakeCharacter("alien"), null);
        sector = new SecureSector('A',1, null);
        players = new LinkedList<Player>();
        players.add(gamer);
        view = new FakeView(playerID);
        view2 = new FakeView("other_gamer");
    }

    @Test
    public void chooseNoiseSectorEventTest() {
        ModelEvent evt = new ChooseNoiseSectorEvent(playerID);
        assertEquals(playerID + "", evt.getMessage());

        evt.reactToView(view);
        assertEquals(EVT.SHOW, view.caller.remove());
        assertEquals(EVT.DISABLE, view.caller.remove());
        assertEquals(EVT.SELECT_SECTOR, view.caller.remove());
        assertEquals(EVT.COMMAND, view.caller.remove());

        evt.reactToView(view2);
        assertEquals(null, view2.caller.poll());
    }

    @Test
    public void noiseInSectorEventTest() {
        ModelEvent evt = new NoiseInSectorEvent(playerID, sector);
        assertEquals("Noise in " + sector.toString(), evt.getMessage());
        evt.reactToView(view);
        assertEquals(EVT.SHOW, view.caller.remove());
        assertEquals(EVT.DISABLE, view.caller.remove());
        assertEquals(EVT.END_TURN, view.caller.remove());
        assertEquals(EVT.ITEM_USAGE, view.caller.remove());
        assertEquals(EVT.COMMAND, view.caller.remove());

        evt.reactToView(view2);
        assertEquals(EVT.SHOW, view2.caller.remove());
        assertEquals(null, view2.caller.poll());
    }

    @Test
    public void noiseInSectorItemEventTest() {
        ModelEvent evt = new NoiseInSectorItemEvent(playerID, sector);
        assertEquals("Noise in " + sector.toString(), evt.getMessage());
        evt.reactToView(view);
        assertEquals(EVT.SHOW, view.caller.remove());
        assertEquals(EVT.DISABLE, view.caller.remove());
        assertEquals(EVT.DRAW_ITEM, view.caller.remove());

        evt.reactToView(view2);
        assertEquals(EVT.SHOW, view2.caller.remove());
        assertEquals(null, view2.caller.poll());
    }

    @Test
    public void silenceEventTest() {
        ModelEvent evt = new SilenceEvent(playerID);
        assertEquals("Silence in all sector!", evt.getMessage());
        evt.reactToView(view);
        assertEquals(EVT.SHOW, view.caller.remove());
        assertEquals(EVT.DISABLE, view.caller.remove());
        assertEquals(EVT.END_TURN, view.caller.remove());
        assertEquals(EVT.ITEM_USAGE, view.caller.remove());
        assertEquals(EVT.COMMAND, view.caller.remove());

        evt.reactToView(view2);
        assertEquals(EVT.SHOW, view2.caller.remove());
        assertEquals(null, view2.caller.poll());
    }
}
