package it.polimi.server.model.event.turn;

import static org.junit.Assert.assertEquals;
import it.polimi.common.observer.ModelEvent;
import it.polimi.server.model.Player;
import it.polimi.server.model.fake.FakeCharacter;
import it.polimi.server.model.fake.FakeView;
import it.polimi.server.model.fake.FakeView.EVT;
import it.polimi.server.model.player.Gamer;

import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

public class TurnEventTest {

    private String playerID;
    private Player gamer;
    private Collection<Player> players;
    private FakeView view, view2;

    @Before
    public void initTest() {
        playerID = "player";
        gamer = new Gamer(playerID, new FakeCharacter("alien"), null);
        players = new LinkedList<Player>();
        players.add(gamer);
        view = new FakeView(playerID);
        view2 = new FakeView("gamer");
    }

    @Test
    public void startTurnEventTest() {
        ModelEvent evt = new StartTurnEvent(playerID);
        assertEquals("It's " + playerID + "'s turn.", evt.getMessage());
        evt.reactToView(view);
        assertEquals(EVT.SHOW, view.caller.remove());
        assertEquals(EVT.DISABLE, view.caller.remove());
        assertEquals(EVT.MOVEMENT, view.caller.remove());
        assertEquals(EVT.ITEM_USAGE, view.caller.remove());
        assertEquals(EVT.COMMAND, view.caller.remove());

        evt.reactToView(view2);
        assertEquals(EVT.SHOW, view2.caller.remove());
        assertEquals(null, view2.caller.poll());
    }

    @Test
    public void endTurnEventTest() {
        ModelEvent evt = new EndTurnEvent(playerID);
        assertEquals(playerID + "'s turn is over.", evt.getMessage());
        evt.reactToView(view);
        assertEquals(EVT.SHOW, view.caller.remove());

        evt.reactToView(view2);
        assertEquals(EVT.SHOW, view2.caller.remove());
        assertEquals(null, view2.caller.poll());
    }

}
