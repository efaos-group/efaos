package it.polimi.server.model.event.movement;

import static org.junit.Assert.assertEquals;
import it.polimi.common.observer.ModelEvent;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.fake.FakeCharacter;
import it.polimi.server.model.fake.FakeView;
import it.polimi.server.model.fake.FakeView.EVT;
import it.polimi.server.model.player.Gamer;
import it.polimi.server.model.sector.walkable.SecureSector;

import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

public class MovementEventTest {

    private String playerID;
    private Player gamer;
    private Sector sector;
    private Collection<Player> players;
    private FakeView view, view2;

    @Before
    public void initTest() {
        playerID = "player";
        gamer = new Gamer(playerID, new FakeCharacter("alien"), null);
        sector = new SecureSector('A',1, null);
        players = new LinkedList<Player>();
        players.add(gamer);
        view = new FakeView(playerID);
        view2 = new FakeView("gamer2");
    }

    @Test
    public void dangerousMovementEventTest() {
        ModelEvent evt = new DangerousMovementEvent(playerID, sector);
        assertEquals("You moved on a dangerous Sector:"+sector.toString(),evt.getMessage());
        evt.reactToView(view);
        assertEquals(EVT.SHOW, view.caller.remove());
        assertEquals(EVT.SET_POSITION, view.caller.remove());
        assertEquals(EVT.DISABLE, view.caller.remove());
        assertEquals(EVT.ATTACK, view.caller.remove());
        assertEquals(EVT.ATTACKCARD, view.caller.remove());
        assertEquals(EVT.DRAW_SECTOR_CARD, view.caller.remove());

        evt.reactToView(view2);
        assertEquals(null, view2.caller.poll());
    }

    @Test
    public void illegalMovementEventTest() {
        ModelEvent evt = new IllegalMovementEvent(playerID);
        assertEquals("You performed an illegal movement.\nTry a different Sector.",evt.getMessage());
        evt.reactToView(view);
        assertEquals(EVT.SHOW,view.caller.remove());
        assertEquals(EVT.DISABLE,view.caller.remove());
        assertEquals(EVT.MOVEMENT,view.caller.remove());
        assertEquals(EVT.COMMAND,view.caller.remove());

        evt.reactToView(view2);
        assertEquals(null, view2.caller.poll());
    }

    @Test
    public void notEscapedEventTest() {
        ModelEvent evt = new NotEscapedEvent(playerID, sector);
        assertEquals("You moved correctly to " + sector.toString() + " unfortunately, the Escape Hatch was broken!",evt.getMessage());
        evt.reactToView(view);
        assertEquals(EVT.SHOW,view.caller.remove());
        assertEquals(EVT.SET_POSITION,view.caller.remove());
        assertEquals(EVT.DISABLE,view.caller.remove());
        assertEquals(EVT.ITEM_USAGE,view.caller.remove());
        assertEquals(EVT.END_TURN,view.caller.remove());
        assertEquals(EVT.COMMAND,view.caller.remove());

        evt.reactToView(view2);
        assertEquals(EVT.SHOW, view2.caller.remove());
        assertEquals(null, view2.caller.poll());
    }

    @Test
    public void secureMovementEvent() {
        ModelEvent evt = new SecureMovementEvent(playerID, sector);
        assertEquals("You moved correctly to " + sector.toString(),evt.getMessage());
        evt.reactToView(view);
        assertEquals(EVT.SHOW,view.caller.remove());
        assertEquals(EVT.SET_POSITION,view.caller.remove());
        assertEquals(EVT.DISABLE,view.caller.remove());
        assertEquals(EVT.ATTACK,view.caller.remove());
        assertEquals(EVT.ATTACKCARD,view.caller.remove());
        assertEquals(EVT.ITEM_USAGE,view.caller.remove());
        assertEquals(EVT.END_TURN,view.caller.remove());

        evt.reactToView(view2);
        assertEquals(null, view2.caller.poll());
    }
}
