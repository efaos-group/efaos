/**
 *
 */
package it.polimi.server.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.server.model.Sector;
import it.polimi.server.model.exception.BadSectorException;
import it.polimi.server.model.sector.unwalkable.AlienSector;
import it.polimi.server.model.sector.unwalkable.HumanSector;
import it.polimi.server.model.sector.unwalkable.InvisibleSector;
import it.polimi.server.model.sector.walkable.DangerousSector;
import it.polimi.server.model.sector.walkable.EscapeSector;
import it.polimi.server.model.sector.walkable.SecureSector;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 *
 */
public class SectorTest {

    private Sector l5, l7, l8;
    private Sector k5, k6, i8;

    @Before
    public void initSectorTest() {
        l5 = new DangerousSector('L', 5, null);
        k6 = new SecureSector('K', 6, null);
        k5 = new EscapeSector('K', 5, null);
        l7 = new InvisibleSector('L', 7);
        l8 = new AlienSector('L', 8);
        i8 = new SecureSector('I', 8, null);
    }

    @Test(expected = BadSectorException.class)
    public void testUpperRowBounds() {
        new DangerousSector('D', 0, null);
    }

    @Test(expected = BadSectorException.class)
    public void testLowerRowBounds() {
        new DangerousSector('E', 15, null);
    }

    @Test(expected = BadSectorException.class)
    public void testUpperColBounds() {
        new DangerousSector('a', 2, null);
    }

    @Test(expected = BadSectorException.class)
    public void testLowerColBounds() {
        new DangerousSector('Y', 7, null);
    }

    @Test
    public void testAdjacent() {
        assertTrue("L5 and K6 should be ajacent", l5.areAdjacents(k6));
        assertTrue("K5 and L5 should be ajacent", k5.areAdjacents(l5));
        assertTrue("L7 and L8 should be ajacent", l7.areAdjacents(l8));
        assertTrue("K5 and L6 should be ajacent", k6.areAdjacents(l5));

        assertFalse("K5 shouldn't be ajacent of itself", k5.areAdjacents(k5));
        assertFalse("L7 shouldn't be ajacent of I8", l7.areAdjacents(i8));
        assertFalse("L7 and L5 shouldn't be ajacent", l7.areAdjacents(l5));
    }

    @Test
    public void testConstructor(){

        HumanSector l6 = new HumanSector('L', 6);
        assertTrue("Column should be k", l6.getCol() == 'L');
        assertTrue("Row should be 6", l6.getRow() == 6);
    }

}
