/**
 *
 */
package it.polimi.server.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import it.polimi.server.model.card.DeckCardFactory;
import it.polimi.server.model.card.item.DefenseCard;
import it.polimi.server.model.fake.FakeCharacter;
import it.polimi.server.model.fake.FakeDeckReceiver;
import it.polimi.server.model.fake.FakeNotifier;
import it.polimi.server.model.fake.FakePlayer;
import it.polimi.server.model.fake.FakeSectorListener;
import it.polimi.server.model.fake.FakeTableMap;
import it.polimi.server.model.game.Logic;
import it.polimi.server.model.sector.walkable.SecureSector;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class GameLogicTest {

    private GameLogic game;
    private FakeTableMap fakeMap;
    private FakeNotifier fakeNotifier;

    private FakePlayer player1, player2, player3, player4;

    @Before
    public void setUp() throws Exception {
        fakeMap = new FakeTableMap();
        fakeNotifier = new FakeNotifier();

        player1 = new FakePlayer("player1", new FakeCharacter("alien1"), fakeMap);
        player2 = new FakePlayer("player2", new FakeCharacter("alien2"), fakeMap);
        player3 = new FakePlayer("player3", new FakeCharacter("human1"), fakeMap);
        player4 = new FakePlayer("player4", new FakeCharacter("human2"), fakeMap);
        Map<String, Player> fakePlayers = new HashMap<String, Player>();
        fakePlayers.put("player1", player1);
        fakePlayers.put("player2", player2);
        fakePlayers.put("player3", player3);
        fakePlayers.put("player4", player4);

        game = new Logic(fakeMap, fakePlayers, fakeNotifier, new DeckCardFactory());
    }

    @Test
    public void testSequence() {

        assertEquals(FakeNotifier.EVENT.STARTGAME, fakeNotifier.bufferCaller.poll());

        assertEquals(FakeNotifier.EVENT.STARTTURN, fakeNotifier.bufferCaller.poll());

        FakeSectorListener fakeSectorListener = new FakeSectorListener();
        String playerID = game.getGamerIdentifier();

        assertEquals(player1.getUsername(), playerID);

        game.move(new SecureSector('A', 2, fakeSectorListener));

        assertEquals(FakeTableMap.EVENT.MOVE, fakeMap.buffer.poll());

        game.movedIntoSecureSector(new SecureSector('A', 2, fakeSectorListener));
        assertEquals(FakeNotifier.EVENT.SECURE, fakeNotifier.bufferCaller.poll());

        ItemCard card = new DefenseCard(new FakeDeckReceiver());
        card.addCardListener(fakeNotifier);
        game.useCard(card, null);
        assertEquals(FakePlayer.EVENT.CARD, player1.calling.poll());


        card = new DefenseCard(new FakeDeckReceiver());
        card.addCardListener(fakeNotifier);
        game.useCard(card, null);
        assertEquals(FakePlayer.EVENT.CARD, player1.calling.poll());

        game.passTurn();
        assertEquals(FakeNotifier.EVENT.ENDTURN, fakeNotifier.bufferCaller.poll());
        assertEquals(FakeNotifier.EVENT.STARTTURN, fakeNotifier.bufferCaller.poll());

        playerID = game.getGamerIdentifier();

        assertFalse("The gamer should has changed", player1.getUsername().equals(playerID));
    }

}
