package it.polimi.server.model.fake;

import it.polimi.server.model.BaseCharacter;
import it.polimi.server.model.Sector;
import it.polimi.server.model.sector.walkable.SecureSector;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class FakeCharacter extends BaseCharacter {

    public static enum CALL {SEDATE, RESET}
    public Queue<CALL> caller;
    private boolean defense;
    private boolean permission = true;

    public FakeCharacter(String name) {
        super(name);
        this.position = new SecureSector('A', 1, null);
        caller = new LinkedList<CALL>();
    }

    @Override
    public void die(){
        alive = false;
    }

    public void allowUseCard(boolean permission) {
        this.permission = permission;
    }

    @Override
    public boolean canUseCard() {
        return permission;
    }

    @Override
    public boolean canDefend() { return defense; }

    @Override
    public void setDefense(boolean defense) {
        this.defense = defense;
    }

    @Override
    public void teleportTo(Sector to) {}

    @Override
    public void attack() { }

    @Override
    public void boostMovement() {}

    @Override
    public void sedate() {
        caller.add(CALL.SEDATE);
    }

    @Override
    public void resetStatus() {
        caller.add(CALL.RESET);
    }

    @Override
    public boolean isSedated() {
        return caller.contains(CALL.SEDATE);
    }

}
