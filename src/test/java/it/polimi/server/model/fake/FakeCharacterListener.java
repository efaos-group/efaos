/**
 *
 */
package it.polimi.server.model.fake;

import it.polimi.server.model.CharacterListener;
import it.polimi.server.model.Player;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 *
 */
public class FakeCharacterListener implements CharacterListener {

    public static enum TYPE {DEAD, DEFENSE, NONE}
    private Queue<TYPE> event;

    public FakeCharacterListener(){
        event = new LinkedList<TYPE>();
    }

    @Override
    public void notifyDeath() {
        event.add(TYPE.DEAD);
    }

    @Override
    public void usedDefense() {
        event.add(TYPE.DEFENSE);
    }

    public TYPE popEvent(){
        if(event.isEmpty()){
            return TYPE.NONE;
        } else {
            return event.remove();
        }
    }

    @Override
    public void notifyAttack(Collection<Player> characterAttacked) {
    }

}
