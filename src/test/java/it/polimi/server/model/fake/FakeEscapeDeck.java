/**
 *
 */
package it.polimi.server.model.fake;

import it.polimi.server.model.EscapeCard;
import it.polimi.server.model.EscapeDeck;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class FakeEscapeDeck implements EscapeDeck {

    private Queue<EscapeCard> bufferCard;

    public FakeEscapeDeck() {
        bufferCard = new LinkedList<EscapeCard>();
    }

    public void setCardToDraw(EscapeCard card) {
        bufferCard.add(card);
    }

    @Override
    public EscapeCard draw() {
        return bufferCard.poll();
    }

    @Override
    public EscapeCard generateRedCard() {
        return null;
    }

}
