/**
 *
 */
package it.polimi.server.model.fake;

import it.polimi.server.model.DeckFactory;
import it.polimi.server.model.EscapeDeck;
import it.polimi.server.model.ItemDeck;
import it.polimi.server.model.SectorDeck;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class FakeDeckFactory implements DeckFactory {

    private EscapeDeck escapeDeck = new FakeEscapeDeck();
    private ItemDeck itemDeck = new FakeItemDeck();
    private SectorDeck sectorDeck = new FakeSectorDeck();

    @Override
    public EscapeDeck getEscapeDeckInstance() {
        return escapeDeck;
    }

    @Override
    public ItemDeck getItemDeckInstance() {
        return itemDeck;
    }

    @Override
    public SectorDeck getSectorDeckInstance() {
        return sectorDeck;
    }

}
