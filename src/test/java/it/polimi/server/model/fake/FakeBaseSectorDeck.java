package it.polimi.server.model.fake;

import it.polimi.server.model.SectorCard;
import it.polimi.server.model.card.sector.BaseSectorDeck;

import java.util.LinkedList;
import java.util.List;

public class FakeBaseSectorDeck extends BaseSectorDeck {

    private enum CALL{DISCARDED}
    public final List<CALL> caller;
    public FakeBaseSectorDeck() {
        caller = new LinkedList<CALL>();
    }
    
    @Override
    public void discarded(SectorCard card){
        caller.add(CALL.DISCARDED);
    }

}
