/**
 *
 */
package it.polimi.server.model.fake;

import it.polimi.common.map.MapType;
import it.polimi.server.model.BaseCharacter;
import it.polimi.server.model.MapTable;
import it.polimi.server.model.MapTableListener;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.sector.Mapper;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 *
 */
public class FakeTableMap extends MapTable {

    public static enum EVENT {MOVE}
    public final Queue<EVENT> buffer;

    private Integer distance;
    private Collection<Player> collection;

    public FakeTableMap() throws IOException {
        super(new Mapper(MapType.GALILEI));
        distance = 0;
        collection = new LinkedList<Player>();
        buffer = new LinkedList<EVENT>();
    }

    public void setDistance(Integer distance){
        this.distance = distance;
    }

    @Override
    public int getDirectDistance(Sector from, Sector to) {
        return distance;
    }

    public void setCharacterCollection(Player charac){
        collection.add(charac);
    }

    @Override
    public Collection<Player> getCharacters(Sector sec) {
        return collection;
    }

    @Override
    public void move(BaseCharacter character, Sector to) {
        buffer.add(EVENT.MOVE);
    }

    @Override
    public void removeDeadPlayers() { }

    @Override
    public void teleport(Player character) {
        collection.clear();
        collection.add(character);
    }

    @Override
    public void setMapTableListener(MapTableListener receiver) { }

    @Override
    public Sector getSector(Character col, Integer row) {
        return null;
    }

    @Override
    public void blockSector(Sector sector) {
    }

    @Override
    public Multimap<Sector, Player> spotlight(Sector sector) {
        Multimap<Sector, Player> map = ArrayListMultimap.create();
        return map;
    }

    @Override
    public void removeEscapedCharacter(BaseCharacter character) { }

    @Override
    public boolean isBlocked(Sector sector) {
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof FakeTableMap;
    }
}
