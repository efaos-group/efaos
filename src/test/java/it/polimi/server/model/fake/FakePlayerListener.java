package it.polimi.server.model.fake;

import it.polimi.common.observer.ModelEvent;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.Player;
import it.polimi.server.model.PlayerListener;
import it.polimi.server.model.Sector;
import it.polimi.server.model.SectorCard;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class FakePlayerListener implements PlayerListener {

    public static enum CALL {NOT_ESCAPED, ESCAPED, EVENT, SECTORCARD, ITEMCARD, TOO_MANY, DISCARDED, EXCEPTION}
    public Queue<CALL> caller;

    public FakePlayerListener() {
        caller = new LinkedList<CALL>();
    }

    @Override
    public void notifyHumanNotEscaped(Sector sector) {
        caller.add(CALL.NOT_ESCAPED);
    }


    @Override
    public void notifyHumanEscaped(Sector sector) {
        caller.add(CALL.ESCAPED);
    }

    @Override
    public void notifyCharacterEvent(ModelEvent event) {
        caller.add(CALL.EVENT);
    }

    @Override
    public void notifyException(String message) {
        caller.add(CALL.EXCEPTION);
    }

    @Override
    public void notifyDrawnSectorCard(SectorCard card) {
        caller.add(CALL.SECTORCARD);
    }

    @Override
    public void notifyDrawnItemCard(ItemCard card) {
        caller.add(CALL.ITEMCARD);
    }

    @Override
    public void notifyDiscardedCardEvent(ItemCard card) {
        caller.add(CALL.DISCARDED);
    }

    @Override
    public void notifyStartTurn(Player player) { }

    @Override
    public void notifyEndTurn() { }

    @Override
    public void notifyItemCardUsedAfterMove(ModelEvent event) {
        // TODO Auto-generated method stub
    }

    @Override
    public void notifyDrawnTooManyItemCard(ItemCard card) {
        caller.add(CALL.ITEMCARD);
    }

}