package it.polimi.server.model.fake;

import it.polimi.server.model.MapTableListener;

public class FakeMapTableReceiver implements MapTableListener {

    public FakeMapTableReceiver() {
    }

    @Override
    public void notifyIllegalMovementGamer() {  }

}
