package it.polimi.server.model.fake;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.ItemDeck;

import java.util.LinkedList;
import java.util.Queue;

/**
 *
 */

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class FakeDeckReceiver implements ItemDeck {

    private Queue<ItemCard> event;

    public FakeDeckReceiver(){
        event = new LinkedList<ItemCard>();
    }

    @Override
    public ItemCard draw() {
        return null;
    }

    @Override
    public void discarded(ItemCard card) {
        event.add(card);
    }

    public ItemCard popEvent(){
        if(event.isEmpty()){
            return null;
        } else {
            return event.remove();
        }
    }

    @Override
    public ItemCard generateCardFromString(String cardType) {
        return null;
    }

}
