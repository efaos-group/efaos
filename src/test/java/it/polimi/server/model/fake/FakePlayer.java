/**
 *
 */
package it.polimi.server.model.fake;

import it.polimi.server.model.BaseCharacter;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.MapTable;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.SectorCard;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class FakePlayer extends Player {

    public boolean sedated = false;
    public final List<ItemCard> items;

    public static enum EVENT {CARD, SECTORCARD, DISCARD}
    public Queue<EVENT> calling;

    public FakePlayer(String username, BaseCharacter character, MapTable map) {
        super(username, character, map);
        items = new LinkedList<ItemCard>();
        calling = new LinkedList<EVENT>();
    }

    @Override
    public void usedDefense() { }

    @Override
    public void drawItemCard(ItemCard card) {
        items.add(card);
    }

    @Override
    public void discard(ItemCard card) {
        calling.add(EVENT.DISCARD);
    }

    @Override
    public void useCard(ItemCard card, Sector sector) {
        calling.add(EVENT.CARD);
    }

    @Override
    public int getItemsSize() {
        return items.size();
    }

    @Override
    public boolean isSedated() {
        return sedated;
    }

    @Override
    public void sedate() {
        this.sedated = true;
    }

    @Override
    public void attack() { }

    @Override
    public void notifyAttack(Collection<Player> characterAttacked) { }

    @Override
    public void notifyDeath() { }

    @Override
    public void drawSectorCard(SectorCard card) {
        calling.add(EVENT.SECTORCARD);
    }

    @Override
    public void fireIllegalActionEvent() { }

    @Override
    public void randomDiscard() {

    }

}
