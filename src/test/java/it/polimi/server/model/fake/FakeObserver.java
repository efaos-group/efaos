package it.polimi.server.model.fake;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.Observable;
import it.polimi.common.observer.Observer;

import java.util.LinkedList;
import java.util.Queue;

public class FakeObserver implements Observer {
    public Queue <Class> caller;
    public FakeObserver() {
        caller = new LinkedList<Class>();
    }

    @Override
    public void notify(Observable source, ModelEvent gameEvent) {
        caller.add(gameEvent.getClass());
    }

}
