/**
 *
 */
package it.polimi.server.model.fake;

import it.polimi.server.model.ItemCard;
import it.polimi.server.model.ItemDeck;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class FakeItemDeck implements ItemDeck {

    private Queue<ItemCard> buffer = new LinkedList<ItemCard>();

    @Override
    public ItemCard draw() {
        return buffer.remove();
    }

    @Override
    public void discarded(ItemCard card) {
        // TODO Auto-generated method stub

    }

    @Override
    public ItemCard generateCardFromString(String cardType) {
        return buffer.poll();
    }

    public void setCardToDraw(ItemCard card) {
        buffer.add(card);
    }


}
