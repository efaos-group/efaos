package it.polimi.server.model.fake;

import it.polimi.server.model.BaseCharacter;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.ItemDeck;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 *
 */
public class FakeItem extends ItemCard {

    private Queue<BaseCharacter> caller;
    public FakeItem(ItemDeck itemDeck) {
        super(itemDeck);

        caller = new LinkedList<BaseCharacter>();
    }

    @Override
    public void use(Player player, Sector sector) {
        caller.add(player.getCharacter());
    }

    public BaseCharacter popEvent(){
        if(caller.isEmpty()) {
            return null;
        }
        return caller.remove();
    }

    @Override
    public void discard(String playerID) { }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = prime * super.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        return obj == this && obj instanceof FakeItem;
    }
}