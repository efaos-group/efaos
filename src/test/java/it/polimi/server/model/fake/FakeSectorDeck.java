/**
 *
 */
package it.polimi.server.model.fake;

import it.polimi.server.model.SectorCard;
import it.polimi.server.model.SectorDeck;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class FakeSectorDeck implements SectorDeck {

    private Queue<SectorCard> buffer = new LinkedList<SectorCard>();

    @Override
    public SectorCard draw() {
        return buffer.poll();
    }

    @Override
    public void discarded(SectorCard card) {
    }

    public void setCardToDraw(SectorCard card) {
        buffer.add(card);
    }

}
