package it.polimi.server.model.fake;

import it.polimi.network.Communicator;
import it.polimi.network.CommunicatorListener;

import java.util.LinkedList;
import java.util.Queue;

public class FakeCommunicatorListener implements CommunicatorListener {

    public Queue <String> messages;
    public Communicator source;
    public FakeCommunicatorListener() {
        messages = new LinkedList<String>();
    }

    @Override
    public void notifyMessageReceived(Communicator socketSource, String msg) {
        source = socketSource;
        messages.add(msg);
    }

    @Override
    public void notifyConnectionClosed(Communicator socketSource) {
        // TODO Auto-generated method stub

    }

}
