/**
 *
 */
package it.polimi.server.model.fake;

import it.polimi.common.observer.ModelEvent;
import it.polimi.server.model.CardListener;
import it.polimi.server.model.Sector;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class FakeCardListener implements CardListener {

    private Queue<ModelEvent> event;

    public FakeCardListener() {
        event = new LinkedList<ModelEvent>();
    }

    @Override
    public void notifyCardUsed(ModelEvent event) {
        this.event.add(event);
    }

    public ModelEvent popEvent(){
        return event.remove();
    }

    @Override
    public void notifyNoiseInSector(Sector sector) {

    }

    @Override
    public void notifySilence() {

    }

    @Override
    public void notifyChooseSector() {
    }

    @Override
    public void notifyNoiseInSectorItem(Sector sector) {
    }


}
