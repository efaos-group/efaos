package it.polimi.server.model.fake;

import it.polimi.server.model.MapLoader;
import it.polimi.server.model.Sector;
import it.polimi.server.model.SectorListener;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 *
 */
public class FakeSectorGiver implements MapLoader {

    @Override
    public Sector getSector(Character col, Integer row) {
        return null;
    }

    @Override
    public Sector getHumanSector() {
        return null;
    }

    @Override
    public Sector getAlienSector() {
        return null;
    }

    @Override
    public void setSectorListener(SectorListener listener) { }

    @Override
    public void blockSector(Sector sector) {

    }

    @Override
    public boolean isBlocked(Sector sector) {
        return false;
    }

}
