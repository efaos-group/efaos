/**
 *
 */
package it.polimi.server.model.fake;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.Observable;
import it.polimi.common.observer.View;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class FakeView implements View {
    public enum EVT{COMMAND, REMOVED_CARD, DISABLE,SELECT_SECTOR, SHOW,
                        ITEM_USAGE, END_TURN, DRAW_ITEM, ADD_CARD, DISCARD,MOVEMENT,
                        SET_POSITION, ATTACK, ATTACKCARD, DRAW_SECTOR_CARD, SET_BEHAVOIR}
    private String player;

    public Queue<EVT> caller;
    public FakeView(String username) {
        caller = new LinkedList<EVT>();
        this.player = username;
    }

    @Override
    public void showCardDialog(String msg) {
        caller.add(EVT.SHOW);
    }

    @Override
    public void showEventDialog(String msg) {
        caller.add(EVT.SHOW);
    }

    @Override
    public void showErrorDialog(String msg) {
        caller.add(EVT.SHOW);
    }

    @Override
    public void showNoiseInSectorDialog(String msg) {
        caller.add(EVT.SHOW);
    }

    @Override
    public void showStartTurnDialog(String msg, String playerID) {
        caller.add(EVT.SHOW);
    }

    @Override
    public void showEndGameDialog(String msg) {
        caller.add(EVT.SHOW);
    }

    @Override
    public void enableMovement() {
        caller.add(EVT.MOVEMENT);
    }

    @Override
    public void enableItemUsage() {
        caller.add(EVT.ITEM_USAGE);
    }

    @Override
    public void enableAttack() {
        caller.add(EVT.ATTACK);
    }

    @Override
    public void enableDrawSectorCard() {
        caller.add(EVT.DRAW_SECTOR_CARD);
    }

    @Override
    public void enableDiscard() {
        caller.add(EVT.DISCARD);
    }

    @Override
    public void enableEndTurn() {
        caller.add(EVT.END_TURN);
    }

    @Override
    public void enableAttackCard() {
        caller.add(EVT.ATTACKCARD);
    }

    @Override
    public void enableSelectSector() {
        caller.add(EVT.SELECT_SECTOR);
    }

    @Override
    public void enableDrawItemCard() {
        caller.add(EVT.DRAW_ITEM);
    }

    @Override
    public void askForCommand() {
        caller.add(EVT.COMMAND);
    }

    @Override
    public void setCharacterBehaviour(String character) {
        caller.add(EVT.SET_BEHAVOIR);

    }

    @Override
    public void setPosition(String position) {
        caller.add(EVT.SET_POSITION);
    }

    @Override
    public void addNewItemCard(String card) {
        caller.add(EVT.ADD_CARD);
    }

    @Override
    public void removeUsedItemCard(String card) {
        caller.add(EVT.REMOVED_CARD);
    }

    @Override
    public void disableAll() {
        caller.add(EVT.DISABLE);
    }

    @Override
    public String getPlayerID() {
        return player;
    }

    @Override
    public void askForPlayerID() {
        // TODO Auto-generated method stub

    }

    @Override
    public void notify(Observable source, ModelEvent gameEvent) {
        // TODO Auto-generated method stub

    }

    @Override
    public void askForMap() {
        // TODO Auto-generated method stub

    }

    @Override
    public void sendPlayerChoice() {
        // TODO Auto-generated method stub

    }

    @Override
    public void choiceAccepted(String msg) {
        // TODO Auto-generated method stub
    }

    @Override
    public void blockEscapeSector(String msg) {
        // TODO Auto-generated method stub

    }


    @Override
    public void showPictureDialog(String msg, String card) {
        caller.add(EVT.SHOW);
        
    }

    @Override
    public void showStartGameEvent(String msg, String character, Collection<String> players) {
        caller.add(EVT.SHOW);
    }

}
