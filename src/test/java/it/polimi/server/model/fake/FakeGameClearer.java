/**
 *
 */
package it.polimi.server.model.fake;

import it.polimi.server.model.GameClearer;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class FakeGameClearer implements GameClearer {

    /**
     *
     */
    public FakeGameClearer() { }

    @Override
    public void deleteDeadPlayer(String playerID) { }

    @Override
    public void deleteEscapedPlayer(String playerID) { }

}
