/**
 *
 */
package it.polimi.server.model.fake;

import it.polimi.common.observer.ModelEvent;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.Notifier;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.SectorCard;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class FakeNotifier extends Notifier {

    public static enum EVENT {CARD, NOISE, NOISEITEM, SILENCE, CHOOSESECTOR, ILLEGALMOVEMENT,
        NOTESCAPED, ESCAPED, CHARACTER, EXCEPTION, DRAWITEM, DRAWSECTOR, DISCARDED,
        STARTTURN, ENDTURN, DANGEROUS, SECURE, STARTGAME, HWIN, AWIN, OVER, DISCONNECT}
    public Queue<EVENT> bufferCaller;

    public FakeNotifier() {
        bufferCaller = new LinkedList<EVENT>();
    }

    @Override
    public void notifyCardUsed(ModelEvent event) {
        bufferCaller.add(EVENT.CARD);
    }

    @Override
    public void notifyNoiseInSector(Sector sector) {
        bufferCaller.add(EVENT.NOISE);
    }

    @Override
    public void notifyNoiseInSectorItem(Sector sector) {
        bufferCaller.add(EVENT.NOISEITEM);
    }

    @Override
    public void notifySilence() {
        bufferCaller.add(EVENT.SILENCE);
    }

    @Override
    public void notifyChooseSector() {
        bufferCaller.add(EVENT.CHOOSESECTOR);
    }

    @Override
    public void notifyIllegalMovementGamer() {
        bufferCaller.add(EVENT.ILLEGALMOVEMENT);
    }

    @Override
    public void notifyHumanNotEscaped(Sector sector) {
        bufferCaller.add(EVENT.NOTESCAPED);
    }

    @Override
    public void notifyHumanEscaped(Sector sector) {
        bufferCaller.add(EVENT.ESCAPED);
    }

    @Override
    public void notifyCharacterEvent(ModelEvent event) {
        bufferCaller.add(EVENT.CHARACTER);
    }

    @Override
    public void notifyException(String message) {
        bufferCaller.add(EVENT.EXCEPTION);
    }

    @Override
    public void notifyDrawnSectorCard(SectorCard card) {
        bufferCaller.add(EVENT.DRAWSECTOR);
    }

    @Override
    public void notifyDrawnItemCard(ItemCard card) {
        bufferCaller.add(EVENT.DRAWITEM);
    }

    @Override
    public void notifyDiscardedCardEvent(ItemCard card) {
        bufferCaller.add(EVENT.DISCARDED);
    }

    @Override
    public void notifyStartTurn(Player player) {
        bufferCaller.add(EVENT.STARTTURN);
    }

    @Override
    public void notifyEndTurn() {
        bufferCaller.add(EVENT.ENDTURN);
    }

    @Override
    public void notifyDangerousMovement(Sector sector) {
        bufferCaller.add(EVENT.DANGEROUS);
    }

    @Override
    public void notifySecureMovement(Sector sector) {
        bufferCaller.add(EVENT.SECURE);
    }

    @Override
    public void notifyStartGame(Collection<Player> players) {
        bufferCaller.add(EVENT.STARTGAME);
    }

    @Override
    public void notifyHumanWin() {
        bufferCaller.add(EVENT.HWIN);
    }

    @Override
    public void notifyAlienWin() {
        bufferCaller.add(EVENT.AWIN);
    }

    @Override
    public void notifyGameOver() {
        bufferCaller.add(EVENT.OVER);
    }

    @Override
    public void notifyDisconnection(String playerID) {
        bufferCaller.add(EVENT.DISCONNECT);
    }

    @Override
    public void notifyItemCardUsedAfterMove(ModelEvent event) {
        // TODO Auto-generated method stub
    }

    @Override
    public void notifyDrawnTooManyItemCard(ItemCard card) {
        // TODO Auto-generated method stub

    }

}
