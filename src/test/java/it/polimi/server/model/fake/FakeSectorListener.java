/**
 *
 */
package it.polimi.server.model.fake;

import it.polimi.server.model.Sector;
import it.polimi.server.model.SectorListener;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 *
 */
public class FakeSectorListener implements SectorListener {

    public static enum TYPE {ESCAPE, DANGEROUS, SECURE,  NONE}
    private Queue<TYPE> event;
    public FakeSectorListener(){
        event = new LinkedList<TYPE>();
    }

    public TYPE popEvent(){
        if(event.isEmpty()){
            return TYPE.NONE;
        } else {
            return event.remove();
        }
    }

    @Override
    public void movedIntoEscapeSector(Sector sector) {
        event.add(TYPE.ESCAPE);
    }

    @Override
    public void movedIntoDangerousSector(Sector sector) {
        event.add(TYPE.DANGEROUS);
    }

    @Override
    public void movedIntoSecureSector(Sector sector) {
        event.add(TYPE.SECURE);
    }

}
