/**
 *
 */
package it.polimi.server.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.card.item.TeleportCard;
import it.polimi.server.model.exception.PlayerException;
import it.polimi.server.model.fake.FakeCharacter;
import it.polimi.server.model.fake.FakeGameClearer;
import it.polimi.server.model.fake.FakePlayerListener;
import it.polimi.server.model.fake.FakeTableMap;
import it.polimi.server.model.player.Gamer;
import it.polimi.server.model.sector.walkable.SecureSector;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class PlayerTest {

    private Player player1,player2;
    private FakeCharacter char1, char2;

    private FakeTableMap fakeMap;
    private FakePlayerListener listener;
    private FakeGameClearer gameClearer;

    @Before
    public void initializeTest() throws IOException {
        fakeMap = new FakeTableMap();
        listener = new FakePlayerListener();
        gameClearer = new FakeGameClearer();

        char1 = new FakeCharacter("character 1");
        char2 = new FakeCharacter("character 2");

        player1 = new Gamer("Alien1", char1, fakeMap);
        player1.setPlayerListeners(listener, gameClearer);
        player2 = new Gamer("Alien2", char2, fakeMap);
        player2.setPlayerListeners(listener, gameClearer);
    }

    @Test (expected = PlayerException.class)
    public void discardExceptionTest() {
        ItemCard card = new TeleportCard(null);
        player2.discard(card);
    }

    @Test
    public void getterTest() {
        assertTrue("Username doesn't match", player1.getUsername().equalsIgnoreCase("Alien1"));
        assertTrue("Character doesn't match", player2.getCharacter().equals(char2));
    }

    @Test
    public void testCharacterPosition() {
        Sector defaultSector = new SecureSector('A', 1, null);
        assertEquals(player1.getCharacterPosition(), defaultSector);
    }

    @Test
    public void testGetMap() {
        assertEquals(player1.getMap(), fakeMap);
    }

    @Test
    public void testSedate() {
        player2.sedate();
        assertTrue("Player should be sedated", player2.isSedated());
        assertEquals(char2.caller.remove(), FakeCharacter.CALL.SEDATE);
    }

    @Test
    public void testResetStatus() {
        player1.resetCharacterStatus();
        assertEquals(char1.caller.remove(), FakeCharacter.CALL.RESET);
    }

    @Test
    public void testNotification() {
        player1.notEscape();
        assertEquals(listener.caller.remove(), FakePlayerListener.CALL.NOT_ESCAPED);

        player1.escape();
        assertEquals(listener.caller.remove(), FakePlayerListener.CALL.ESCAPED);
    }
}
