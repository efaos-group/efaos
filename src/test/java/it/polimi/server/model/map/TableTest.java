package it.polimi.server.model.map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.common.map.MapType;
import it.polimi.server.model.BaseCharacter;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.character.AlienCharacter;
import it.polimi.server.model.character.HumanCharacter;
import it.polimi.server.model.fake.FakeGameClearer;
import it.polimi.server.model.fake.FakeMapTableReceiver;
import it.polimi.server.model.fake.FakePlayerListener;
import it.polimi.server.model.fake.FakeSectorListener;
import it.polimi.server.model.player.Gamer;
import it.polimi.server.model.sector.Mapper;
import it.polimi.server.model.sector.unwalkable.AlienSector;
import it.polimi.server.model.sector.unwalkable.HumanSector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Multimap;

public class TableTest {

    private Table map;
    private Sector l2,l3,n6,p5,m6,i7,l6,l8,k6,l4,m9,j8,j11;
    private BaseCharacter alien1,human1,alien2,human2;
    private Player p1,p2,p3,p4;
    private Mapper galilei;

    @Before
    public void InitTest() throws IOException {
        galilei = new Mapper(MapType.GALILEI);
        map = new Table(galilei);
        map.setSectorListener(new FakeSectorListener());
        map.setMapTableListener(new FakeMapTableReceiver());


        l2 = map.getSector('L', 2);
        l3 = map.getSector('L', 3);
        l4 = map.getSector('L',4);
        l6 = map.getSector('L',6);
        l8 = map.getSector('L',8);
        k6 = map.getSector('K', 6);
        n6 = map.getSector('N', 6);
        p5 = map.getSector('P', 5);
        m6 = map.getSector('M', 6);
        i7 = map.getSector('I', 7);
        m9 = map.getSector('M', 9);
        j8 = map.getSector('J', 8);
        j11 = map.getSector('J', 11);

        alien1 = new AlienCharacter("alien1",(AlienSector)l6, map);
        alien2 = new AlienCharacter("alien2",(AlienSector)l6, map);
        human1 = new HumanCharacter("human1",(HumanSector)l8,map);
        human2 = new HumanCharacter("human2",(HumanSector)l8,map);

        p1 = new Gamer("p1",alien1, map);
        p2 = new Gamer("p2",alien2, map);
        p3 = new Gamer("p3",human1, map);
        p4 = new Gamer("p4",human2, map);

        p1.setPlayerListeners( new FakePlayerListener(), new FakeGameClearer());

        alien1.setParent(p1);
        alien2.setParent(p2);
        human1.setParent(p3);
        human2.setParent(p4);

        Collection<Player> list = new ArrayList<Player>();
        list.add(p1);
        list.add(p2);
        list.add(p3);
        list.add(p4);

        map.setCharacters(list);
    }

    @Test
    public void getDirectDistanceTest() {
       assertTrue("distance from l2 to l3 should be 1",map.getDirectDistance(l2, l3)==1);
       assertTrue("distance from n6 to p5 should be 2",map.getDirectDistance(n6, p5)==2);
       assertTrue("distance from m6 to i7 should be 4",map.getDirectDistance(m6, i7)==4);
       assertTrue("distance from m6 to j8 should be 6",map.getDirectDistance(m6, j8)==6);
       assertTrue("distance from m6 to j11 should be 9",map.getDirectDistance(m6, j11)==9);
    }


    @Test
    public void moveTest() {
        map.move(alien1, k6);
        assertTrue("alien1 should be in Dangerous Sector K6",alien1.getPosition().equals(k6));
        assertTrue("map should contain alien1 in k6",map.getCharacters(k6).contains(p1));
        assertFalse("map shouldn't contain alien1 in l6", map.getCharacters(l6).contains(p1));
        map.move(alien1, l4);
        assertTrue("alien1 should be in l4", alien1.getPosition().equals(l4));
        assertTrue("map should contain alien1 in l4", map.getCharacters(l4).contains(p1));
        assertFalse("map shouldn't contain alien1 in k6", map.getCharacters(k6).contains(p1));
        map.move(human1, m9);
        assertTrue("human1 should be in m9", human1.getPosition().equals(m9));
        assertTrue("map should contain human1 in m9", map.getCharacters(m9).contains(p3));
        assertFalse("map shouldn't contain human1 in l8", map.getCharacters(l8).contains(p3));
    }

    @Test
    public void illegalMovementTest() {
        map.move(human2, i7);
        assertTrue("human2 is still in l8",human2.getPosition().equals(l8));
        assertFalse("map shouldn't contain human2 in i7",map.getCharacters(i7).contains(p4));
    }

    @Test
    public void teleportTest() {
        map.move(human2,m9);
        map.teleport(p4);
        assertTrue("human2 should be in Human Sector", human2.getPosition().equals(l8));
        assertTrue("map should contain human2 in l8", map.getCharacters(l8).contains(p4));
        assertFalse("map shouldn't contain human2 in m9",map.getCharacters(m9).contains(p4));
    }

    @Test
    public void blockSectorTest() {
        map.blockSector(map.getSector('B', 2));
        assertTrue("b2 should be blocked",map.isBlocked(map.getSector('B', 2)));
    }

    @Test
    public void removeEscapedCharacterTest() {
        map.removeEscapedCharacter(human1);
        Sector sector = human1.getPosition();
        assertFalse("human1 shouldn't be in the map", map.getCharacters(sector).contains(human1));
    }

    @Test
    public void spotlightTest() {
        Multimap <Sector,Player> result = map.spotlight(map.getSector('L', 7));
        assertTrue("result should contain p1", result.containsValue(p1));
        assertTrue("result should contain p2", result.containsValue(p2));
        assertTrue("result should contain p3", result.containsValue(p3));
        assertTrue("result should contain p4", result.containsValue(p4));
    }
    
    @Test
    public void removeDeadPlayerTest(){
        alien1.die();
        map.removeDeadPlayers();
        Collection<Player> list = map.getCharacters(alien1.getPosition());
        assertFalse("list shouldn't contain alien 1",list.contains(alien1));
    }
}

