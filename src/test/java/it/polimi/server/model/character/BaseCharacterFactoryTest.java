/**
 *
 */
package it.polimi.server.model.character;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import it.polimi.server.model.CharacterFactory.CharacterType;
import it.polimi.server.model.character.AlienCharacter;
import it.polimi.server.model.character.BaseCharacterFactory;
import it.polimi.server.model.character.HumanCharacter;
import it.polimi.server.model.exception.PlayerException;
import it.polimi.server.model.fake.FakeTableMap;

import java.io.IOException;

import org.junit.Test;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 *
 */
public class BaseCharacterFactoryTest {

    @Test
    public void testFactory() throws IOException {
        BaseCharacterFactory baseCharacterFactory = new BaseCharacterFactory();

        assertTrue("The character created should be human", baseCharacterFactory.factory(CharacterType.HUMAN, new FakeTableMap())
                                                                instanceof HumanCharacter);
        assertTrue("The character created should be alien", baseCharacterFactory.factory(CharacterType.ALIEN, new FakeTableMap())
                                                                instanceof AlienCharacter);
    }

    @Test
    public void testGetterName(){
        BaseCharacterFactory baseCharacterFactory = new BaseCharacterFactory();
        assertEquals(baseCharacterFactory.getNewHumanName(), "The Captain Ennio Maria Dominoni");

        baseCharacterFactory.getNewHumanName();
        baseCharacterFactory.getNewHumanName();

        assertEquals(baseCharacterFactory.getNewHumanName(), "The Soldier Tuccio Brendon a.k.a. “Piri”");


        assertEquals(baseCharacterFactory.getNewAlienName(), "Piero Ceccarella");

        baseCharacterFactory.getNewAlienName();

        assertEquals(baseCharacterFactory.getNewAlienName(), "Maria Galbani");

    }

    @Test(expected=PlayerException.class)
    public void testHumanThrowPlayerException() {
        BaseCharacterFactory baseCharacterFactory = new BaseCharacterFactory();

        baseCharacterFactory.getNewHumanName();
        baseCharacterFactory.getNewHumanName();
        baseCharacterFactory.getNewHumanName();
        baseCharacterFactory.getNewHumanName();
        baseCharacterFactory.getNewHumanName();
    }

    @Test(expected=PlayerException.class)
    public void testAlienThrowPlayerException() {
        BaseCharacterFactory baseCharacterFactory = new BaseCharacterFactory();

        baseCharacterFactory.getNewAlienName();
        baseCharacterFactory.getNewAlienName();
        baseCharacterFactory.getNewAlienName();
        baseCharacterFactory.getNewAlienName();
        baseCharacterFactory.getNewAlienName();
    }

}
