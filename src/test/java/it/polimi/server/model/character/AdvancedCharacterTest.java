/**
 *
 */
package it.polimi.server.model.character;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import it.polimi.server.model.exception.BadCharacterException;
import it.polimi.server.model.fake.FakeCharacterListener;
import it.polimi.server.model.fake.FakeCharacterListener.TYPE;
import it.polimi.server.model.fake.FakeItem;
import it.polimi.server.model.fake.FakePlayer;
import it.polimi.server.model.fake.FakeSectorListener;
import it.polimi.server.model.fake.FakeTableMap;
import it.polimi.server.model.sector.unwalkable.AlienSector;
import it.polimi.server.model.sector.unwalkable.HumanSector;
import it.polimi.server.model.sector.unwalkable.InvisibleSector;
import it.polimi.server.model.sector.walkable.DangerousSector;
import it.polimi.server.model.sector.walkable.SecureSector;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 *
 */
public class AdvancedCharacterTest {

    private HumanCharacter human;
    private AlienCharacter alien;
    private FakeTableMap map;
    private FakeSectorListener listener;
    private FakeCharacterListener charListener;

    @Before
    public void setUp() throws IOException{
        map = new FakeTableMap();
        listener = new FakeSectorListener();
        charListener = new FakeCharacterListener();

        human = new HumanCharacter("human", new HumanSector('A', 1), map);
        human.setParent(charListener);
        alien = new AlienCharacter("alien", new AlienSector('B', 1), map);
        alien.setParent(charListener);
    }


    @Test
    public void testCanMoveTo() {
        SecureSector sec = new SecureSector('A', 2, null);
        InvisibleSector inv = new InvisibleSector('B', 2);
        DangerousSector dan = new DangerousSector('B', 3, listener);
        DangerousSector dan2 = new DangerousSector('C', 3, listener);

        assertTrue("Human should can move on an adjacent sector", human.canMoveTo(sec));
        assertFalse("Human shouldn't move on a non-walkable sector", human.canMoveTo(inv));
        assertFalse("Human shouldn't move on a non-adjacent sector", human.canMoveTo(dan));

        map.setDistance(2);
        human.boostMovement();
        assertTrue("Human boosted should can move on a two-distance sector", human.canMoveTo(dan));

        map.setDistance(0);
        assertFalse("Human boosted should can move on a two-distance sector", human.canMoveTo(dan));

        assertTrue("Alien can move on an adjacent Secure sector", alien.canMoveTo(sec));

        map.setDistance(3);
        alien.boostMovement();
        assertTrue("Boosted Alien could move on a three-distance sector", alien.canMoveTo(dan2));

        alien.resetStatus();
        human.resetStatus();

        map.setDistance(2);
        assertTrue("Alien could move on a two-distance sector", alien.canMoveTo(dan2));

        map.setDistance(0);
        assertFalse("Alien couldn't move on a zero-distance sector", alien.canMoveTo(dan2));
        assertFalse("Human couldn't move on a zero-distance sector", human.canMoveTo(dan2));
    }

    @Test
    public void testCatchingException(){
        DangerousSector dan = new DangerousSector('B', 3, listener);
        map.setDistance(-1);
        assertFalse("Human should catch an exception and return false", human.canMoveTo(dan));
        assertFalse("Alien should catch an exception and return false", alien.canMoveTo(dan));
    }

    @Test
    public void testDefense(){
        assertFalse("Human shouldn't can defend without a card", human.canDefend());
        human.setDefense(true);
        assertTrue("Human should can defend without a card", human.canDefend());
        human.die();
        assertFalse("Human shouldn't can defend after using the card", human.canDefend());
        assertTrue("Human should send a defense event to its parent", charListener.popEvent() == TYPE.DEFENSE);

        assertFalse("Alien shouldn't can defend", alien.canDefend());
    }

    @Test
    public void testDie(){
        human.setDefense(false);
        human.die();
        assertTrue("Human should send a death event to its parent", charListener.popEvent() == TYPE.DEAD);

        alien.die();
        assertTrue("Alien should send a death event to its parent", charListener.popEvent() == TYPE.DEAD);
    }

    @Test
    public void testTeleportHuman(){
        HumanSector to = new HumanSector('E', 3);
        assertTrue("Human should be in a different position to get a right test", !human.getPosition().equals(to));
        human.teleportTo(to);
        assertTrue("Humna shuold be in a human sector", human.getPosition().equals(to));
    }

    @Test(expected=BadCharacterException.class)
    public void testTeleportAlien(){
        alien.teleportTo(null);
    }

    @Test
    public void testAttack(){
        FakeItem item = new FakeItem(null);

        human = new HumanCharacter("human", new HumanSector('A', 1), map);
        human.setParent(charListener);

        HumanCharacter _human = new HumanCharacter("human", new HumanSector('A', 1), map);
        _human.setParent(charListener);
        FakePlayer player2 = new FakePlayer("fake2", human, map);
        FakePlayer player = new FakePlayer("fake1", _human, map);

        map.setCharacterCollection(player);
        _human.setDefense(true);
        map.setCharacterCollection(player2);
        alien.attack();
        assertFalse("Human should be dead", human.isAlive());
        assertTrue("Human shouldn't be dead", _human.isAlive());
        assertNull("Alien shouldn't call the item", item.popEvent());

    }

    @Test(expected=BadCharacterException.class)
    public void testHumanAttackThrowingException() {
        HumanCharacter human = new HumanCharacter("human", new HumanSector('A', 1), map);
        human.attack();
    }

    @Test
    public void testEquals(){
        HumanCharacter human = new HumanCharacter("no-one", new HumanSector('A', 1), map);
        human.setParent(charListener);
        AlienCharacter alien = new AlienCharacter("no-one", new AlienSector('A', 1), map);
        alien.setParent(charListener);

        assertFalse("Human and Alien shouldn't be equals", human.equals(alien));
    }

    @Test(expected=BadCharacterException.class)
    public void testSedateAlienException() {
        assertFalse("Alien shouldn't be sedated", alien.isSedated());
        alien.sedate();
    }

    @Test
    public void testSedateHuman() {
        assertFalse("Human shouldn't be sedated", human.isSedated());
        human.sedate();
        assertTrue("Human should be sedated", human.isSedated());
    }
}
