/**
 *
 */
package it.polimi.server.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.server.model.character.AlienCharacter;
import it.polimi.server.model.character.HumanBoostedState;
import it.polimi.server.model.character.HumanCharacter;
import it.polimi.server.model.character.HumanNotBoostedState;
import it.polimi.server.model.character.NotSedatedState;
import it.polimi.server.model.character.SedatedState;
import it.polimi.server.model.character.SedativeState;
import it.polimi.server.model.exception.IllegalMovementException;
import it.polimi.server.model.fake.FakeCharacter;
import it.polimi.server.model.fake.FakeSectorListener;
import it.polimi.server.model.fake.FakeTableMap;
import it.polimi.server.model.sector.unwalkable.AlienSector;
import it.polimi.server.model.sector.unwalkable.HumanSector;
import it.polimi.server.model.sector.unwalkable.InvisibleSector;
import it.polimi.server.model.sector.walkable.EscapeSector;
import it.polimi.server.model.sector.walkable.SecureSector;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 *
 */
public class BaseCharacterTest {

    private BaseCharacter human, alien;
    private FakeTableMap fakeMap;

    @Before
    public void setUp() throws Exception {
        fakeMap = new FakeTableMap();
        human = new HumanCharacter("1", new HumanSector('D', 3), fakeMap);
        alien = new AlienCharacter("2", new AlienSector('E', 4), fakeMap);
    }

    @Test
    public void testConstructor() {
        HumanSector position = new HumanSector('C', 7);
        BaseCharacter t = new HumanCharacter("test", position, null);
        assertTrue("The position of start should be equals!", position.equals(t.getPosition()));
        assertTrue("BaseCharacter should be alive at the beginning", t.isAlive());
    }

    @Test
    public void testCanMoveTo() {
        Sector inv = new InvisibleSector('D', 4);
        Sector sec = new SecureSector('D', 2, null);

        assertFalse("BaseCharacter can's move to an invisible sector", human.canMoveTo(inv));
        assertTrue("BaseCharacter can move to adjacent sector", human.canMoveTo(sec));
    }

    @Test
    public void testMoveTo(){
        FakeSectorListener listener = new FakeSectorListener();
        Sector sec = new SecureSector('D', 2, listener);
        human.moveTo(sec);
        assertTrue("BaseCharacter should be in a different sector", sec.equals(human.getPosition()));
    }

    @Test(expected=IllegalMovementException.class)
    public void testHumanMoveToIllegalSector() {
        human.moveTo(new InvisibleSector('G', 5));
        }

   @Test(expected=IllegalMovementException.class)
    public void testAlienMoveToIllegalSector() {
            alien.moveTo(new EscapeSector('A', 1, null));
    }

   @Test
   public void testHashCode(){
       BaseCharacter _human = new HumanCharacter("1", new HumanSector('D', 3), fakeMap);
       BaseCharacter _alien = new AlienCharacter("2", new AlienSector('E', 4), fakeMap);

       assertTrue("Hash code of equals object should be the sames", human.hashCode() == _human.hashCode() && human.equals(_human));

       assertTrue("Hash code of equals object should be the sames", alien.hashCode() == _alien.hashCode() && alien.equals(_alien));
   }

   @Test
   public void testEquals(){
       BaseCharacter test = new HumanCharacter("2", new HumanSector('E', 4), null);
       FakeCharacter fakeChar = new FakeCharacter("fake");

       assertTrue("BaseCharacter with the same attribute should be the same", human.equals(human));
       assertFalse("BaseCharacter with different position shouldn't be the same", alien.equals(test));
       assertFalse("BaseCharacter with different name shouldn't be the same", alien.equals(human));
       assertFalse("Two different type of object shouldn't be equals", alien.equals(fakeChar));
   }

   @Test
   public void testMovementState() {
       CharacterMovementState state1 = new HumanNotBoostedState(fakeMap);
       CharacterMovementState state2 = new HumanNotBoostedState(fakeMap);
       assertTrue("state1 and state2 should be equal", state1.equals(state2));
       assertTrue("state1 and state2' hashCode should be equal", state1.hashCode() == state2.hashCode());

       CharacterMovementState state3 = new HumanBoostedState(fakeMap);
       CharacterMovementState state4 = new HumanBoostedState(fakeMap);
       assertTrue("state3 and state4 should be equal", state3.equals(state4));
       assertTrue("state3 and state4' hashCode should be equal", state3.hashCode() == state4.hashCode());
   }

   @Test
   public void testSedateState() {
       SedativeState state1 = new SedatedState();
       SedativeState state2 = new NotSedatedState();
       assertFalse("state1 and state2 shouldn't be equal", state1.equals(state2));
       assertFalse("state1 and state2' hashCode shouldn't be equal", state1.hashCode() == state2.hashCode());
   }
}
