package it.polimi.server.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.server.model.EscapeCard;
import it.polimi.server.model.card.escape.GreenCard;
import it.polimi.server.model.card.escape.RedCard;
import it.polimi.server.model.card.item.AdrenalineCard;
import it.polimi.server.model.card.item.DefenseCard;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class EscapeCardTest {
    
    private EscapeCard red, red1, green, green1;
    
    @Before
    public void initEscapeCardTest() {
        red = new RedCard();
        green = new GreenCard();
        red1 = new RedCard();
        green1 = new GreenCard();
    }
    
    @Test
    public void canEscapeTest() {
        assertFalse("Red card canEscape should be false",red.canEscape());
        assertTrue("Green card canEscape should be true",green.canEscape());
    }
    
    @Test
    public void hashTest() {
        Map <EscapeCard, Integer> map = new HashMap<EscapeCard, Integer>();
        map.put(green, 1);
        assertTrue("result should be 1",map.get(green1) == 1);
        assertTrue("green and green1 hash should be equal", green.hashCode() == green1.hashCode());
        assertTrue("red and red1 hash should be equal", red.hashCode() == red1.hashCode());
    }
    
    @Test
    public void equalsTest() {
        assertTrue("green and green1 should be equal", green.equals(green));
        assertTrue("red and red1 should be equal", red.equals(red1));
        assertFalse("red shouldn't be equal to other class", red.equals(new AdrenalineCard(null)));
        assertFalse("green shouldn't be equal to other class", green.equals(new DefenseCard(null)));
    }

}
