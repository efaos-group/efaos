/**
 *
 */
package it.polimi.server.model.game;

import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import it.polimi.server.model.GameLogic;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.Player;
import it.polimi.server.model.card.escape.GreenCard;
import it.polimi.server.model.card.item.AdrenalineCard;
import it.polimi.server.model.card.item.AttackCard;
import it.polimi.server.model.card.item.DefenseCard;
import it.polimi.server.model.card.item.SedativesCard;
import it.polimi.server.model.card.item.TeleportCard;
import it.polimi.server.model.card.sector.NoiseInYourSectorItem;
import it.polimi.server.model.card.sector.SilenceCard;
import it.polimi.server.model.fake.FakeCharacter;
import it.polimi.server.model.fake.FakeDeckFactory;
import it.polimi.server.model.fake.FakeDeckReceiver;
import it.polimi.server.model.fake.FakeEscapeDeck;
import it.polimi.server.model.fake.FakeItemDeck;
import it.polimi.server.model.fake.FakeNotifier;
import it.polimi.server.model.fake.FakePlayer;
import it.polimi.server.model.fake.FakeSectorDeck;
import it.polimi.server.model.fake.FakeSectorListener;
import it.polimi.server.model.fake.FakeTableMap;
import it.polimi.server.model.sector.walkable.DangerousSector;
import it.polimi.server.model.sector.walkable.EscapeSector;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class LogicTest {

    private GameLogic game;
    private FakeTableMap fakeMap;
    private FakeNotifier fakeNotifier;
    private FakeDeckFactory deckFactory;

    private FakePlayer player1, player2, player3;

    @Before
    public void setUp() throws IOException {

        fakeMap = new FakeTableMap();
        fakeNotifier = new FakeNotifier();

        player1 = new FakePlayer("player1", new FakeCharacter("alien1"), fakeMap);
        player1.setPlayerListeners(fakeNotifier, game);
        player2 = new FakePlayer("player2", new FakeCharacter("alien2"), fakeMap);
        player3 = new FakePlayer("player3", new FakeCharacter("human1"), fakeMap);
        Map<String, Player> fakePlayers = new HashMap<String, Player>();
        fakePlayers.put("player3", player3);
        fakePlayers.put("player1", player1);
        fakePlayers.put("player2", player2);
        deckFactory = new FakeDeckFactory();
        game = new Logic(fakeMap, fakePlayers, fakeNotifier, deckFactory);

        ((FakeCharacter)player1.getCharacter()).allowUseCard(false);
        ((FakeCharacter)player2.getCharacter()).allowUseCard(false);
    }

    @Test
    public void testCompleteSequence() throws IOException {
        setUp();

        assertEquals(FakeNotifier.EVENT.STARTGAME, fakeNotifier.bufferCaller.poll());

        assertEquals(FakeNotifier.EVENT.STARTTURN, fakeNotifier.bufferCaller.poll());

        FakeSectorListener fakeSectorListener = new FakeSectorListener();
        String playerID = game.getGamerIdentifier();

        assertEquals(player1.getUsername(), playerID);

        game.move(new DangerousSector('A', 2, fakeSectorListener));

        assertEquals(FakeTableMap.EVENT.MOVE, fakeMap.buffer.poll());

        game.movedIntoDangerousSector(new DangerousSector('A', 2, fakeSectorListener));
        assertEquals(FakeNotifier.EVENT.DANGEROUS, fakeNotifier.bufferCaller.poll());

        ((FakeSectorDeck) deckFactory.getSectorDeckInstance()).setCardToDraw(new SilenceCard(deckFactory.getSectorDeckInstance()));
        game.drawSector();
        assertEquals(FakePlayer.EVENT.SECTORCARD, player1.calling.poll());
        assertTrue("", fakeNotifier.bufferCaller.poll() == FakeNotifier.EVENT.SILENCE);

        ItemCard card = new DefenseCard(new FakeDeckReceiver());
        player1.drawItemCard(card);
        game.useCard(card, null);
        assertEquals(FakePlayer.EVENT.CARD, player1.calling.poll());

        game.passTurn();
        assertEquals(FakeNotifier.EVENT.ENDTURN, fakeNotifier.bufferCaller.poll());
        assertEquals(FakeNotifier.EVENT.STARTTURN, fakeNotifier.bufferCaller.poll());
    }

    @Test
    public void testDeleteDeadPlayer() throws IOException {
        setUp();

        fakeNotifier.bufferCaller.poll(); //STARTGAME
        fakeNotifier.bufferCaller.poll(); //STARTTURN

        game.deleteDeadPlayer("player3");
        assertEquals(FakeNotifier.EVENT.AWIN, fakeNotifier.bufferCaller.poll());

    }

    @Test
    public void testDeleteEscapedPlayer() throws IOException {
        setUp();
        ((FakeCharacter)player1.getCharacter()).allowUseCard(false);
        ((FakeCharacter)player2.getCharacter()).allowUseCard(false);

        fakeNotifier.bufferCaller.poll(); //STARTGAME
        fakeNotifier.bufferCaller.poll(); //STARTTURN
        fakeMap.removeEscapedCharacter(player2.getCharacter());
        game.deleteEscapedPlayer("player3");
        assertEquals(FakeNotifier.EVENT.HWIN, fakeNotifier.bufferCaller.poll());

    }

    @Test
    public void testGetItemCard() {
        ((FakeItemDeck) deckFactory.getItemDeckInstance()).setCardToDraw(new AdrenalineCard(null));
        assertTrue("The card should be an adrenaline card", game.getItemCard("ADRENALINE") instanceof AdrenalineCard);
    }

    @Test
    public void testDrawItemCard() throws IOException {
        setUp();

        assertEquals(FakeNotifier.EVENT.STARTGAME, fakeNotifier.bufferCaller.poll());

        assertEquals(FakeNotifier.EVENT.STARTTURN, fakeNotifier.bufferCaller.poll());

        FakeSectorListener fakeSectorListener = new FakeSectorListener();
        String playerID = game.getGamerIdentifier();

        assertEquals(player1.getUsername(), playerID);

        game.move(new DangerousSector('A', 2, fakeSectorListener));

        assertEquals(FakeTableMap.EVENT.MOVE, fakeMap.buffer.poll());

        game.movedIntoDangerousSector(new DangerousSector('A', 2, fakeSectorListener));
        assertEquals(FakeNotifier.EVENT.DANGEROUS, fakeNotifier.bufferCaller.poll());

        player1.drawItemCard(new TeleportCard(null));
        player1.drawItemCard(new AttackCard(null));
        player1.drawItemCard(new SedativesCard(null));

        ((FakeSectorDeck) deckFactory.getSectorDeckInstance()).setCardToDraw(new NoiseInYourSectorItem(deckFactory.getSectorDeckInstance()));
        game.drawSector();
        assertEquals(FakePlayer.EVENT.SECTORCARD, player1.calling.poll());

        ItemCard card = new DefenseCard(new FakeDeckReceiver());
        ((FakeItemDeck) deckFactory.getItemDeckInstance()).setCardToDraw(card);

        assertEquals(FakeNotifier.EVENT.NOISEITEM, fakeNotifier.bufferCaller.poll());
        game.drawItemCard();
        assertTrue("Player should have drawn the setted card", player1.items.contains(card));
        assertTrue("The player should have drawn the card setted", player1.items.contains(card));

        game.discardCard(card);
        assertEquals(FakePlayer.EVENT.DISCARD, player1.calling.poll());
    }

    @Test
    public void testEndTimer() throws IOException {
        setUp();

        assertEquals(player1.getUsername(), game.getGamerIdentifier());

        ((Logic)game).endGameTimer();
        assertThat(player1.getUsername(), not(game.getGamerIdentifier()));
    }

    @Test
    public void testDisconnectPlayer() throws IOException {
        setUp();

        assertEquals(player1.getUsername(), game.getGamerIdentifier());

        game.removeDisconnectedPlayerFromGame(player1.getUsername());
        assertThat(player1.getUsername(), not(game.getGamerIdentifier()));

        game.removeDisconnectedPlayerFromGame(player2.getUsername());
        assertThat(player2.getUsername(), not(game.getGamerIdentifier()));

        game.removeDisconnectedPlayerFromGame(player3.getUsername());
    }

    @Test
    public void testEscapeMovement() throws IOException {
        setUp();
        ((FakeCharacter)player2.getCharacter()).allowUseCard(false);
        ((FakeCharacter)player3.getCharacter()).allowUseCard(false);

        assertEquals(FakeNotifier.EVENT.STARTGAME, fakeNotifier.bufferCaller.poll());

        assertEquals(FakeNotifier.EVENT.STARTTURN, fakeNotifier.bufferCaller.poll());

        FakeSectorListener fakeSectorListener = new FakeSectorListener();
        String playerID = game.getGamerIdentifier();

        assertEquals(player1.getUsername(), playerID);

        game.move(new EscapeSector('A', 2, fakeSectorListener));

        assertEquals(FakeTableMap.EVENT.MOVE, fakeMap.buffer.poll());

        ((FakeEscapeDeck) deckFactory.getEscapeDeckInstance()).setCardToDraw(new GreenCard());

        ((FakeCharacter)player1.getCharacter()).allowUseCard(false);
        game.movedIntoEscapeSector(new EscapeSector('A', 2, fakeSectorListener));
        assertEquals(FakeNotifier.EVENT.ESCAPED, fakeNotifier.bufferCaller.poll());
    }

}
