/**
 *
 */
package it.polimi.server.model.sector;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.server.model.exception.IllegalMovementException;
import it.polimi.server.model.fake.FakeSectorListener;
import it.polimi.server.model.sector.unwalkable.AlienSector;
import it.polimi.server.model.sector.unwalkable.HumanSector;
import it.polimi.server.model.sector.unwalkable.InvisibleSector;
import it.polimi.server.model.sector.walkable.DangerousSector;
import it.polimi.server.model.sector.walkable.EscapeSector;
import it.polimi.server.model.sector.walkable.SecureSector;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 *
 */
public class AdvancedSectorTest {

    private FakeSectorListener receiver;

    private HumanSector A1;
    private DangerousSector C2;
    private AlienSector L8;
    private InvisibleSector J6;
    private SecureSector M10;
    private EscapeSector P13;

    @Before
    public void setUp() {

        receiver = new FakeSectorListener();

        A1 = new HumanSector('A', 1);
        C2 = new DangerousSector('C', 2, receiver);
        L8 = new AlienSector('L', 8);
        J6 = new InvisibleSector('J', 6);
        M10 = new SecureSector('M', 10, receiver);
        P13 = new EscapeSector('P', 13, receiver);

    }

    @Test
    public void testHashCode(){
        HumanSector a1 = new HumanSector('A', 1);
        assertTrue("Equals Sector should have same hash code", a1.equals(A1) && A1.hashCode() == a1.hashCode());

        DangerousSector c2 = new DangerousSector('C', 2, receiver);
        assertTrue("Equals Sector should have same hash code", c2.equals(C2) && c2.hashCode() == C2.hashCode());

        AlienSector l8 = new AlienSector('L', 8);
        assertTrue("Equals Sector should have same hash code", l8.equals(L8) && l8.hashCode() == L8.hashCode());

        InvisibleSector j6 = new InvisibleSector('J', 6);
        assertTrue("Equals Sector should have same hash code", j6.equals(J6) && J6.hashCode() == j6.hashCode());

        SecureSector m10 = new SecureSector('M', 10, receiver);
        assertTrue("Equals Sector should have same hash code", m10.equals(M10) && m10.hashCode() == M10.hashCode());

        EscapeSector p13 = new EscapeSector('P', 13, receiver);
        assertTrue("Equals Sector should have same hash code", p13.equals(P13) && p13.hashCode() == P13.hashCode());
    }

    @Test
    public void testWalkable() {

        assertFalse("Human sector should not be walkable", A1.isWalkable());
        assertTrue("Dangerous sector should be walkable", C2.isWalkable());
        assertFalse("Alien sector should not be walkable", L8.isWalkable());

        assertFalse("Invisible sector should not be walkable", J6.isWalkable());
        assertTrue("Escape sector should be walkable", P13.isWalkable());
        assertTrue("Secure sector should be walkable", M10.isWalkable());
    }

    @Test
    public void testEquals() {
        InvisibleSector a1I = new InvisibleSector('A', 1);
        HumanSector c2H = new HumanSector('C', 2);
        InvisibleSector l8I = new InvisibleSector('L', 8);
        HumanSector j6H = new HumanSector('J', 6);
        SecureSector p13S = new SecureSector('P', 13, receiver);
        AlienSector p13A = new AlienSector('P', 13);

        assertTrue("HumanSector is equals to itself", A1.equals(A1));
        assertFalse("Human and Invisible Sector are different", A1.equals(a1I));

        DangerousSector c3 = new DangerousSector('C', 3, receiver);
        assertFalse("DangerousSector isn't equals to a different position", C2.equals(c3));
        assertFalse("Human and Dangerous are different sector", C2.equals(c2H));

        InvisibleSector k9 = new InvisibleSector('K', 9);
        assertFalse("L8 Alien and K9 Alien are different", L8.equals(k9));
        assertFalse("Invisible and Alien Sector are different", L8.equals(l8I));

        assertTrue("InvisibleSector must be equals to itself", J6.equals(J6));
        assertFalse("Invisible and Human Sector are different", J6.equals(j6H));

        assertFalse("Escape and Secure Sector are different", p13S.equals(P13));
        assertFalse("Escape and Secure Sector are different on different position", M10.equals(P13));

        EscapeSector r14 = new EscapeSector('R', 14, receiver);
        assertFalse("P13 Escape and Secure Sector are different", P13.equals(p13A));
        assertFalse("P13 and R14 are different sector", P13.equals(r14));
    }

    @Test(expected = IllegalMovementException.class)
    public void testHumanMovedOn() {
        A1.movedInto();
    }

    @Test
    public void testMovedOn(){

        P13.movedInto();
        assertTrue("Should be moved over an escape sector", receiver.popEvent() == FakeSectorListener.TYPE.ESCAPE);

        C2.movedInto();
        assertTrue("Should be moved over a dangerous sector", receiver.popEvent() == FakeSectorListener.TYPE.DANGEROUS);

        M10.movedInto();
        assertTrue("Should be moved over a secure sector", receiver.popEvent() == FakeSectorListener.TYPE.SECURE);
    }

}
