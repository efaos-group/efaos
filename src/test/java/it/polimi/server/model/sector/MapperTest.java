/**
 *
 */
package it.polimi.server.model.sector;

import static org.junit.Assert.assertTrue;
import it.polimi.common.map.MapType;
import it.polimi.server.model.MapLoader;
import it.polimi.server.model.Sector;
import it.polimi.server.model.fake.FakeSectorListener;
import it.polimi.server.model.sector.unwalkable.AlienSector;
import it.polimi.server.model.sector.unwalkable.HumanSector;
import it.polimi.server.model.sector.unwalkable.InvisibleSector;
import it.polimi.server.model.sector.walkable.EscapeSector;
import it.polimi.server.model.sector.walkable.SecureSector;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class MapperTest {
    private MapLoader map;
    private Sector h,_h,a,_a;
    private Sector b2,_b2;
    private Sector a13, _a13, b7, _b7;
    private FakeSectorListener listener;

    @Before
    public void initializeTest() throws IOException {
        map = new Mapper(MapType.GALILEI);
        FakeSectorListener fakeListener = new FakeSectorListener();
        map.setSectorListener(fakeListener);

        h = map.getHumanSector();
        _h = new HumanSector('L', 8);
        a = map.getAlienSector();
        _a = new AlienSector('L', 6);
        b2 = map.getSector('B',2);
        _b2 = new EscapeSector('B', 2, fakeListener);
        a13 = map.getSector('A', 13);
        _a13 = new SecureSector('A', 13, fakeListener);
        b7 = map.getSector('B', 7);
        _b7 = new InvisibleSector('B',7);

    }

    @Test
    public void specialSectorTest() {
        assertTrue("h should be equal to _h",h.equals(_h));
        assertTrue("a should be equal to _a",a.equals(_a));
        assertTrue("b2 should be equal to _b2", b2.equals(_b2));
        assertTrue("a13 should be equal to _a13", a13.equals(_a13));
        assertTrue("b7 should be equal to _b7", b7.equals(_b7));
    }

    @Test
    public void setSectorListenerTest() throws IOException {
        map.setSectorListener(listener);
    }

}
