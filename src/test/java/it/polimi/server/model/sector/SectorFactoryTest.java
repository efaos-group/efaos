/**
 *
 */
package it.polimi.server.model.sector;

import static org.junit.Assert.assertTrue;
import it.polimi.server.model.Sector;
import it.polimi.server.model.SectorType;
import it.polimi.server.model.sector.Coordinate;
import it.polimi.server.model.sector.SectorFactory;
import it.polimi.server.model.sector.unwalkable.AlienSector;
import it.polimi.server.model.sector.unwalkable.HumanSector;
import it.polimi.server.model.sector.unwalkable.InvisibleSector;
import it.polimi.server.model.sector.walkable.DangerousSector;
import it.polimi.server.model.sector.walkable.SecureSector;

import org.junit.Test;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 *
 */
public class SectorFactoryTest {

    @Test
    public void testFactory() {
        Coordinate location = new Coordinate('A', 3);

        Sector aln = SectorFactory.factory(SectorType.ALIEN, location, null);
        assertTrue("SectorFactory should have created an AlienSector", aln instanceof AlienSector);

        Sector hum = SectorFactory.factory(SectorType.HUMAN, location, null);
        assertTrue("SectorFactory should have created an HumanSector", hum instanceof HumanSector);

        Sector sec = SectorFactory.factory(SectorType.SECURE, location, null);
        assertTrue("SectorFactory should have created an SecureSector", sec instanceof SecureSector);

        Sector inv = SectorFactory.factory(SectorType.INVISIBLE, location, null);
        assertTrue("SectorFactory should have created an InvisibleSector", inv instanceof InvisibleSector);

        Sector dan = SectorFactory.factory(SectorType.DANGEROUS, location, null);
        assertTrue("SectorFactory should have created an DangerousSector", dan instanceof DangerousSector);
    }

}
