package it.polimi.server.model;

import static org.junit.Assert.assertTrue;
import it.polimi.server.model.card.item.AttackCard;
import it.polimi.server.model.card.sector.NoiseInAnySectorCard;
import it.polimi.server.model.card.sector.NoiseInAnySectorItem;
import it.polimi.server.model.card.sector.NoiseInYourSectorCard;
import it.polimi.server.model.card.sector.NoiseInYourSectorItem;
import it.polimi.server.model.card.sector.SilenceCard;
import it.polimi.server.model.exception.BadGameTransactionException;
import it.polimi.server.model.fake.FakeBaseSectorDeck;
import it.polimi.server.model.fake.FakeCardListener;
import it.polimi.server.model.fake.FakeCharacter;
import it.polimi.server.model.fake.FakeDeckReceiver;
import it.polimi.server.model.fake.FakeGameClearer;
import it.polimi.server.model.fake.FakeItem;
import it.polimi.server.model.fake.FakePlayer;
import it.polimi.server.model.fake.FakePlayerListener;
import it.polimi.server.model.fake.FakeTableMap;
import it.polimi.server.model.game.state.AfterMovementState;
import it.polimi.server.model.game.state.DrawItemState;
import it.polimi.server.model.game.state.EndGameState;
import it.polimi.server.model.game.state.EndTurnState;
import it.polimi.server.model.game.state.HumanEscapedState;
import it.polimi.server.model.game.state.InitialState;
import it.polimi.server.model.game.state.MovedIntoDangerousSectorState;
import it.polimi.server.model.game.state.MovedIntoEscapeSectorState;
import it.polimi.server.model.game.state.MovedIntoSecureSectorState;
import it.polimi.server.model.game.state.NoiseInSectorItemState;
import it.polimi.server.model.game.state.NoiseInSectorState;
import it.polimi.server.model.game.state.TooManyCardState;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class GameStateTest {

    GameState initial, current;
    FakeTableMap map;
    FakePlayer playerSedated, player, player4Cards;
    FakePlayerListener listener;
    FakeGameClearer clearer;
    FakeCardListener cardListener;
    FakeBaseSectorDeck sectorDeck;
    FakeDeckReceiver itemDeck;
    FakeItem item;

    @Before
    public void initTest() throws IOException {
        map = new FakeTableMap();

        playerSedated = new FakePlayer("player1", new FakeCharacter("Sedated"), map);
        playerSedated.sedate();

        player = new FakePlayer("player2", new FakeCharacter("notsedated"), map);
        listener = new FakePlayerListener();
        clearer = new FakeGameClearer();

        player4Cards = new FakePlayer("player3", new FakeCharacter("4cards"), map);
        for(int i=0; i< 4; i++) {
            player4Cards.drawItemCard(item);
        }

        cardListener = new FakeCardListener();
        sectorDeck = new FakeBaseSectorDeck();
        itemDeck = new FakeDeckReceiver();
        item = new FakeItem(itemDeck);

        player.setPlayerListeners(listener, clearer);
        initial = new InitialState(player);
    }

    @Test
    public void initialStateTest(){
        initial.move(null, map);

        GameState endGame = initial.endGame();
        assertTrue("endGame should be EndGameState", endGame.getClass() == EndGameState.class);

        GameState usedItem = initial.useCard(item, null);
        assertTrue("usedItem should be InitialState", usedItem.getClass() == InitialState.class);
    }

    @Test (expected = BadGameTransactionException.class)
    public void initialStateExceptionTest() {
        initial.useCard(new AttackCard(itemDeck), null);
    }

    @Test
    public void humanEscapedStateTest() {
        GameState humanEscaped = new HumanEscapedState(player, map);
        GameState endTurn = humanEscaped.endTurn();
        assertTrue("endTurn should be EndTurnState", endTurn.getClass() == EndTurnState.class);

        GameState endGame = humanEscaped.endGame();
        assertTrue("endGame should be EndGameState", endGame.getClass() == EndGameState.class);
    }

    @Test
    public void movedOnEscapeStateTest() {
        GameState escape = new MovedIntoEscapeSectorState(player);
        GameState endTurn = escape.endTurn();
        assertTrue("endTurn should be EndTurnState", endTurn.getClass() == EndTurnState.class);

        GameState usedItem = escape.useCard(item, null);
        assertTrue("usedItem should be AfterMovementState", usedItem.getClass() == AfterMovementState.class);
    }

    @Test
    public void endTurnStateTest() {
        GameState end = new EndTurnState(player);
        GameState initial = end.startTurn(player);
        assertTrue("initial should be InitialState", initial.getClass() == InitialState.class);
    }

    @Test
    public void movedOnSecureSectorStateTest() {
        GameState secure = new MovedIntoSecureSectorState(player);
        GameState after = secure.attack();
        assertTrue("after should be AfterMovementState", after.getClass() == AfterMovementState.class);

        after = secure.useCard(item, null);
        assertTrue("after should be AfterMovementState", after.getClass() == AfterMovementState.class);

        GameState endTurn = secure.endTurn();
        assertTrue("endTurn should be EndTurnState", endTurn.getClass() == EndTurnState.class);
    }

    @Test
    public void AfterMovementTest() {
        GameState after = new AfterMovementState(player);
        GameState usedItem = after.useCard(item, null);
        assertTrue("usedItem should be AfterMovementState", usedItem.getClass() == AfterMovementState.class);

        GameState endTurn = after.endTurn();
        assertTrue("endTurn should be EndTurnState", endTurn.getClass() == EndTurnState.class);

        GameState endGame = after.endGame();
        assertTrue("endGame should be EndGameState", endGame.getClass() == EndGameState.class);
    }

    @Test
    public void MovedOnDangerousSectorStateTest() {
        GameState dangerous = new MovedIntoDangerousSectorState(player);
        GameState after = dangerous.attack();
        assertTrue("after should be AfterMovementState", after.getClass() == AfterMovementState.class);

        SectorCard card = new NoiseInYourSectorCard(sectorDeck);
        card.setCardListener(cardListener);

        GameState noiseYour = dangerous.drawSectorCard(card);
        assertTrue("noiseYour should be AfterMovement", noiseYour.getClass() == AfterMovementState.class);

        card = new NoiseInAnySectorCard(sectorDeck);
        card.setCardListener(cardListener);
        GameState noiseAny = dangerous.drawSectorCard(card);
        assertTrue("noiseAny should be NoiseInSectorState", noiseAny.getClass() == NoiseInSectorState.class);

        card = new NoiseInYourSectorItem(sectorDeck);
        card.setCardListener(cardListener);
        GameState noiseYourItem = dangerous.drawSectorCard(card);
        assertTrue("noiseYourItem should be DrawItemState", noiseYourItem.getClass() == DrawItemState.class);

        card = new NoiseInAnySectorItem(sectorDeck);
        card.setCardListener(cardListener);
        GameState noiseInAnyItem = dangerous.drawSectorCard(card);
        assertTrue("noiseInAnyItem should be NoiseInSectorItem", noiseInAnyItem.getClass() == NoiseInSectorItemState.class);

        card = new SilenceCard(sectorDeck);
        card.setCardListener(cardListener);
        GameState silence = dangerous.drawSectorCard(card);
        assertTrue ("silence should be AfterMovementState", silence.getClass() == AfterMovementState.class);
    }

    @Test
    public void drawItemStateTest() {
        GameState draw = new DrawItemState(player);
        GameState after = draw.drawItemCard(item);
        assertTrue("after should be AfterMovementState", after.getClass() == AfterMovementState.class);

        GameState draw4 = new DrawItemState(player4Cards);
        GameState tooMany = draw4.drawItemCard(item);
        assertTrue("tooMany should be TooManyCardsState", tooMany.getClass() == TooManyCardState.class);
    }

    @Test
    public void tooManyCardsStateTest() {
        GameState tooMany = new TooManyCardState(player);
        GameState after = tooMany.discardCard(item);
        assertTrue("after should be AfterMovementState", after.getClass() == AfterMovementState.class);

        after = tooMany.useCard(item, null);
        assertTrue("after should be AfterMovementState", after.getClass() == AfterMovementState.class);
    }

    @Test (expected = BadGameTransactionException.class)
    public void startTurnExceptionTest() {
        initial.startTurn(player);
    }

    @Test (expected = BadGameTransactionException.class)
    public void endTurnExceptionTest() {
        initial.endTurn();
    }

    @Test (expected = BadGameTransactionException.class)
    public void discardCardExceptionTest() {
        initial.discardCard(item);
    }

    @Test (expected = BadGameTransactionException.class)
    public void attackExceptionTest() {
        initial.attack();
    }

    @Test (expected = BadGameTransactionException.class)
    public void drawItemCardExceptionTest() {
        initial.drawItemCard(item);
    }

    @Test (expected = BadGameTransactionException.class)
    public void drawSectorExceptionTest() {
        initial.drawSectorCard(null);
    }

    @Test (expected = BadGameTransactionException.class)
    public void moveExceptionTest() {
        new AfterMovementState(player).move(null, map);
    }

    @Test (expected = BadGameTransactionException.class)
    public void useExceptionTest() {
        new EndTurnState(player).useCard(null, null);
    }

    @Test (expected = BadGameTransactionException.class)
    public void dangerousStateExceptionTest() {
        new EndTurnState(player).setDangerousSectorState();
    }

    @Test (expected = BadGameTransactionException.class)
    public void secureStateExceptionTest() {
        new EndTurnState(player).setSecureSectorState();
    }

    @Test (expected = BadGameTransactionException.class)
    public void escapeStateExceptionTest() {
        new AfterMovementState(player).setEscapeSectorState(null, null);
    }

    @Test (expected = BadGameTransactionException.class)
    public void noiseStateExceptionTest() {
        new AfterMovementState(player).notifyNoise(null);
    }

    @Test (expected = BadGameTransactionException.class)
    public void endStateExceptionTest() {
        new TooManyCardState(player).endGame();
    }
}
