package it.polimi.server.model.card.escape;

import static org.junit.Assert.assertTrue;
import it.polimi.server.model.EscapeDeck.EscapeType;
import it.polimi.server.model.card.escape.EscapeFactory;
import it.polimi.server.model.card.escape.GreenCard;
import it.polimi.server.model.card.escape.RedCard;

import org.junit.Test;

public class EscapeFactoryTest {

    @Test
    public void testEscapeCardFactory() {

        assertTrue("CardFactory should generate an RedCard", EscapeFactory.factory(EscapeType.RED) instanceof RedCard);
        assertTrue("CardFactory should generate an GreenCard", EscapeFactory.factory(EscapeType.GREEN) instanceof GreenCard);
    }

}
