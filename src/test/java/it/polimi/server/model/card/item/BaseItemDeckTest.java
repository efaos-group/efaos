/**
 *
 */
package it.polimi.server.model.card.item;

import static org.junit.Assert.*;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.ItemDeck.ItemType;
import it.polimi.server.model.exception.BadCardException;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class BaseItemDeckTest {

    @Test
    public void testGenerationCard() {
        Map<ItemCard, Integer> numberCard = new HashMap<ItemCard, Integer>();
        BaseItemDeck deck = new BaseItemDeck();

        for(int i=0; i<12; i++) {
            ItemCard card = deck.draw();
            int t = 1;
            if(numberCard.containsKey(card)) {
                t += numberCard.get(card);

            }
            numberCard.put(card, t);
        }

        for(ItemCard key: numberCard.keySet()) {
            assertTrue("The number for each card should be 2", numberCard.get(key) == 2);
        }

    }

    @Test
    public void testDiscarded(){
        BaseItemDeck deck = new BaseItemDeck();
        ItemCard card = deck.draw();

        deck.discarded(card);
    }

    @Test(expected=BadCardException.class)
    public void testThrowingBadCardException() {
        BaseItemDeck deck = new BaseItemDeck();
        deck.discarded(ItemFactory.factory(ItemType.ATTACK, deck));
    }

    @Test(expected=BadCardException.class)
    public void testThrowingExceptionEmptyDeck(){
        BaseItemDeck deck = new BaseItemDeck();

        for(int i=0; i<13; i++) {
            deck.draw();
        }
    }

    @Test
    public void testExchangeDeck() {
        BaseItemDeck deck = new BaseItemDeck();

        for(int i=0; i<12; i++) {
            deck.draw();
        }

        deck.discarded(ItemFactory.factory(ItemType.SEDATIVES, deck));
        assertEquals(SedativesCard.class, deck.draw().getClass());
    }

}
