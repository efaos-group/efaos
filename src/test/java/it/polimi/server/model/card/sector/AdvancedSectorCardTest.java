/**
 *
 */
package it.polimi.server.model.card.sector;

import static org.junit.Assert.assertTrue;
import it.polimi.server.model.character.HumanCharacter;
import it.polimi.server.model.fake.FakeCardListener;
import it.polimi.server.model.fake.FakePlayer;
import it.polimi.server.model.fake.FakeSectorDeck;
import it.polimi.server.model.game.state.AfterMovementState;
import it.polimi.server.model.game.state.DrawItemState;
import it.polimi.server.model.game.state.NoiseInSectorItemState;
import it.polimi.server.model.game.state.NoiseInSectorState;
import it.polimi.server.model.sector.unwalkable.HumanSector;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class AdvancedSectorCardTest {

    private NoiseInYourSectorCard yNoise;
    private NoiseInYourSectorItem yItemNoise;
    private NoiseInAnySectorCard aNoise;
    private NoiseInAnySectorItem aItemNoise;
    private SilenceCard silence;
    private FakeSectorDeck fakeDeck;

    @Before
    public void setUp() {
        fakeDeck = new FakeSectorDeck();
        FakeCardListener cardListener = new FakeCardListener();

        yNoise = new NoiseInYourSectorCard(fakeDeck);
        yNoise.setCardListener(cardListener);
        yItemNoise = new NoiseInYourSectorItem(fakeDeck);
        yItemNoise.setCardListener(cardListener);
        aNoise = new NoiseInAnySectorCard(fakeDeck);
        aNoise.setCardListener(cardListener);
        aItemNoise = new NoiseInAnySectorItem(fakeDeck);
        aItemNoise.setCardListener(cardListener);
        silence = new SilenceCard(fakeDeck);
        silence.setCardListener(cardListener);

    }

    @Test
    public void testUsage() {
        FakePlayer gamer = new FakePlayer("", new HumanCharacter("human", new HumanSector('A', 1), null), null);
        assertTrue("", yNoise.use(gamer) instanceof AfterMovementState);
        assertTrue("", yItemNoise.use(gamer) instanceof DrawItemState);
        assertTrue("", aNoise.use(gamer) instanceof NoiseInSectorState);
        assertTrue("", aItemNoise.use(gamer) instanceof NoiseInSectorItemState);
        assertTrue("", silence.use(gamer) instanceof AfterMovementState);
    }

}
