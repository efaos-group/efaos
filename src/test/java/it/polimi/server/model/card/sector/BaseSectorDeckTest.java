/**
 *
 */
package it.polimi.server.model.card.sector;

import static org.junit.Assert.assertTrue;
import it.polimi.server.model.SectorCard;
import it.polimi.server.model.card.sector.BaseSectorDeck;
import it.polimi.server.model.card.sector.NoiseInAnySectorCard;
import it.polimi.server.model.card.sector.SilenceCard;
import it.polimi.server.model.exception.BadCardException;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class BaseSectorDeckTest {


    @Test
    public void testGenerationCard() {
        Map<SectorCard, Integer> numberCard = new HashMap<SectorCard, Integer>();
        BaseSectorDeck deck = new BaseSectorDeck();

        for(int i=0; i<21; i++) {
            SectorCard card = deck.draw();
            int t = 1;
            if(numberCard.containsKey(card)) {
                t += numberCard.get(card);

            }
            numberCard.put(card, t);
        }

        for(SectorCard key: numberCard.keySet()) {
            assertTrue("The number for each card should be 8", numberCard.get(key) >= 4);
        }
    }

    @Test(expected=BadCardException.class)
    public void testThrowingExceptionDiscarded() {
        BaseSectorDeck deck = new BaseSectorDeck();
        deck.discarded(new NoiseInAnySectorCard(deck));
    }

    @Test
    public void testDrawEmptyDeck() {
        BaseSectorDeck deck = new BaseSectorDeck();

        for(int i=0; i<21; i++) {
            deck.draw();
        }
        SilenceCard silence = new SilenceCard(deck);
        deck.discarded(silence);
        assertTrue("I should draw the card previously discarded", deck.draw().equals(silence));
    }

}
