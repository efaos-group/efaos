/**
 *
 */
package it.polimi.server.model.card;

import static org.junit.Assert.assertTrue;
import it.polimi.server.model.EscapeDeck;
import it.polimi.server.model.ItemDeck;
import it.polimi.server.model.SectorDeck;
import it.polimi.server.model.card.DeckCardFactory;

import org.junit.Test;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class DeckCardFactoryTest {

    @Test
    public void testFactory() {
        DeckCardFactory factory = new DeckCardFactory();

        assertTrue("Should be generated an EscapeDeck", factory.getEscapeDeckInstance() instanceof EscapeDeck);

        assertTrue("Should be generated an ItemDeck", factory.getItemDeckInstance() instanceof ItemDeck);

        assertTrue("Should be generated an ItemDeck", factory.getSectorDeckInstance() instanceof SectorDeck);
    }

}
