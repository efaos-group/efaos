/**
 *
 */
package it.polimi.server.model.card.item;

import static org.junit.Assert.assertTrue;
import it.polimi.server.model.ItemDeck.ItemType;
import it.polimi.server.model.card.item.AdrenalineCard;
import it.polimi.server.model.card.item.AttackCard;
import it.polimi.server.model.card.item.DefenseCard;
import it.polimi.server.model.card.item.ItemFactory;
import it.polimi.server.model.card.item.SedativesCard;
import it.polimi.server.model.card.item.SpotlightCard;
import it.polimi.server.model.card.item.TeleportCard;

import org.junit.Test;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class ItemFactoryTest {

    @Test
    public void testItemCardFactory() {

        assertTrue("CardFactory should generate an AdrenalineCard", ItemFactory.factory(ItemType.ADRENALINE, null) instanceof AdrenalineCard);
        assertTrue("CardFactory should generate an AdrenalineCard", ItemFactory.factory(ItemType.ATTACK, null) instanceof AttackCard);
        assertTrue("CardFactory should generate an AdrenalineCard", ItemFactory.factory(ItemType.DEFENSE, null) instanceof DefenseCard);
        assertTrue("CardFactory should generate an AdrenalineCard", ItemFactory.factory(ItemType.SEDATIVES, null) instanceof SedativesCard);
        assertTrue("CardFactory should generate an AdrenalineCard", ItemFactory.factory(ItemType.SPOTLIGHT, null) instanceof SpotlightCard);
        assertTrue("CardFactory should generate an AdrenalineCard", ItemFactory.factory(ItemType.TELEPORT, null) instanceof TeleportCard);
    }

}
