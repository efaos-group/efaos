package it.polimi.server.model.card.sector;

import static org.junit.Assert.assertTrue;
import it.polimi.server.model.SectorDeck.SectorCardType;
import it.polimi.server.model.card.sector.NoiseInAnySectorCard;
import it.polimi.server.model.card.sector.NoiseInAnySectorItem;
import it.polimi.server.model.card.sector.NoiseInYourSectorCard;
import it.polimi.server.model.card.sector.NoiseInYourSectorItem;
import it.polimi.server.model.card.sector.SectorCardFactory;
import it.polimi.server.model.card.sector.SilenceCard;

import org.junit.Test;

public class SectorCardFactoryTest {
    
    @Test
    public void testSectorCardFactory() {
        assertTrue("CardFactory should generate an NoiseInAnySectorCard", SectorCardFactory.factory(SectorCardType.NOISE_IN_ANY_SECTOR, null) instanceof NoiseInAnySectorCard);
        assertTrue("CardFactory should generate an NoiseInYourSectorCard", SectorCardFactory.factory(SectorCardType.NOISE_IN_YOUR_SECTOR, null) instanceof NoiseInYourSectorCard);
        assertTrue("CardFactory should generate an SilenceCard", SectorCardFactory.factory(SectorCardType.SILENCE, null) instanceof SilenceCard);
        assertTrue("CardFactory should generate an NoiseInAnySectorItem", SectorCardFactory.factory(SectorCardType.NOISE_IN_ANY_SECTOR_ITEM, null) instanceof NoiseInAnySectorItem);
        assertTrue("CardFactory should generate an NoiseInYourSectorItem",SectorCardFactory.factory(SectorCardType.NOISE_IN_YOUR_SECTOR_ITEM, null) instanceof NoiseInYourSectorItem);
    }
    
}
