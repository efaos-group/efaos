/**
 *
 */
package it.polimi.server.model.card.item;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.server.model.character.AlienCharacter;
import it.polimi.server.model.fake.FakeCharacter;
import it.polimi.server.model.fake.FakeCharacterListener;
import it.polimi.server.model.fake.FakeDeckReceiver;
import it.polimi.server.model.fake.FakeItem;
import it.polimi.server.model.fake.FakePlayer;
import it.polimi.server.model.fake.FakeTableMap;
import it.polimi.server.model.sector.unwalkable.AlienSector;
import it.polimi.server.model.sector.walkable.SecureSector;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;


/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class AdvancedItemCardTest {

    private FakeDeckReceiver fakeDeck;
    private AdrenalineCard adrenaline;
    private AttackCard attack;
    private DefenseCard defense;
    private SedativesCard sedative;
    private SpotlightCard spotlight;
    private TeleportCard teleport;


    @Before
    public void setUp() throws Exception {

        fakeDeck = new FakeDeckReceiver();

        adrenaline = new AdrenalineCard(fakeDeck);
        attack = new AttackCard(fakeDeck);
        defense = new DefenseCard(fakeDeck);
        sedative = new SedativesCard(fakeDeck);
        spotlight = new SpotlightCard(fakeDeck);
        teleport = new TeleportCard(fakeDeck);
    }

    @Test
    public void testHashCode() {

        AdrenalineCard _ad = new AdrenalineCard(fakeDeck);
        AttackCard _atk = new AttackCard(fakeDeck);
        DefenseCard _def = new DefenseCard(fakeDeck);
        SedativesCard _sed = new SedativesCard(fakeDeck);
        SpotlightCard _spt = new SpotlightCard(fakeDeck);
        TeleportCard _tlp = new TeleportCard(fakeDeck);

        assertTrue("The hash code of two equals card should be the same", adrenaline.hashCode() == _ad.hashCode() && _ad.equals(adrenaline));
        assertTrue("The hash code of two equals card should be the same", attack.hashCode() == _atk.hashCode() && _atk.equals(attack));
        assertTrue("The hash code of two equals card should be the same", defense.hashCode() == _def.hashCode() && _def.equals(defense));
        assertTrue("The hash code of two equals card should be the same", sedative.hashCode() == _sed.hashCode() && _sed.equals(sedative));
        assertTrue("The hash code of two equals card should be the same", spotlight.hashCode() == _spt.hashCode() && _spt.equals(spotlight));
        assertTrue("The hash code of two equals card should be the same", teleport.hashCode() == _tlp.hashCode() && _tlp.equals(teleport));
    }

    @Test
    public void testEquals() {
        FakeItem fakeCard = new FakeItem(null);
        assertFalse("AdrenalineCard should be different than an AttackCard", adrenaline.equals(attack));
        assertFalse("AdrenalineCard should be different than an AttackCard", attack.equals(defense));

        assertFalse("AdrenalineCard should be different than an AttackCard", defense.equals(sedative));
        assertFalse("AdrenalineCard should be different than an AttackCard", sedative.equals(spotlight));

        assertFalse("AdrenalineCard should be different than an AttackCard", spotlight.equals(teleport));
        assertFalse("AdrenalineCard should be different than an AttackCard", teleport.equals(adrenaline));

        assertFalse("AdrenalineCard should be different than an AttackCard", adrenaline.equals(fakeCard));
        assertFalse("AdrenalineCard should be different than an AttackCard", attack.equals(fakeCard));

        assertFalse("AdrenalineCard should be different than an AttackCard", defense.equals(fakeCard));
        assertFalse("AdrenalineCard should be different than an AttackCard", sedative.equals(fakeCard));

        assertFalse("AdrenalineCard should be different than an AttackCard", spotlight.equals(fakeCard));
        assertFalse("AdrenalineCard should be different than an AttackCard", teleport.equals(fakeCard));
    }

    @Test
    public void testUse() throws IOException{
        FakeTableMap fakeMap = new FakeTableMap();

        AlienCharacter owner = new AlienCharacter("own", new AlienSector('A', 1), fakeMap);
        FakeCharacter prey = new FakeCharacter("prey");

        FakeCharacterListener charListener = new FakeCharacterListener();
        owner.setParent(charListener);

        FakePlayer player = new FakePlayer("fakePlayer", owner, fakeMap);
        FakePlayer player2 = new FakePlayer("fakePlayer2", prey, fakeMap);

        fakeMap.setDistance(3);
        assertFalse("Alien character can't move so far if is not sedated", owner.canMoveTo(new SecureSector('C', 2, null)));
        adrenaline.use(player, null);
        assertTrue("The character should be boosted", owner.canMoveTo(new SecureSector('C', 2, null)));
        fakeDeck.popEvent();

        fakeMap.setCharacterCollection(player);
        fakeMap.setCharacterCollection(player2);
        attack.use(player, null);
        assertFalse("The character should be dead", prey.isAlive());
        fakeDeck.popEvent();

        teleport.use(player, null);
        assertTrue("The character teleported should be the same as the one passed to the card",
                                fakeMap.getCharacters(null).contains(player));
        fakeDeck.popEvent();

        defense.use(player, null);
        assertTrue("", fakeDeck.popEvent().isDefenseCard());

        sedative.use(player, null);
        assertEquals(SedativesCard.class, fakeDeck.popEvent().getClass());

        spotlight.use(player, new AlienSector('A', 1));
        assertTrue("", fakeDeck.popEvent().isSpotlightCard());

    }

    @Test
    public void testDiscard(){
        adrenaline.discard("");
        assertEquals(AdrenalineCard.class, fakeDeck.popEvent().getClass());

        attack.discard("");
        assertTrue("The card discarded should be an AttackCard", fakeDeck.popEvent().isAttackCard());

        defense.discard("");
        assertTrue("The card discarded should be an AttackCard", fakeDeck.popEvent().isDefenseCard());

        sedative.discard("");
        assertEquals(SedativesCard.class, fakeDeck.popEvent().getClass());

        spotlight.discard("");
        assertTrue("The card discarded should be an AttackCard", fakeDeck.popEvent().isSpotlightCard());

        teleport.discard("");
        assertEquals(TeleportCard.class, fakeDeck.popEvent().getClass());
    }

    @Test
    public void testIsCard() {
        assertTrue("", attack.isAttackCard());
        assertTrue("", spotlight.isSpotlightCard());
        assertTrue("", defense.isDefenseCard());
    }

}
