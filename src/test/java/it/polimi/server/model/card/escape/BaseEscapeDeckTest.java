/**
 *
 */
package it.polimi.server.model.card.escape;

import static org.junit.Assert.assertTrue;
import it.polimi.server.model.EscapeCard;
import it.polimi.server.model.exception.BadCardException;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class BaseEscapeDeckTest {

    @Test
    public void testGenerationCard() {
        Map<EscapeCard, Integer> numberCard = new HashMap<EscapeCard, Integer>();
        BaseEscapeDeck deck = new BaseEscapeDeck();

        for(int i=0; i<6; i++) {
            EscapeCard type = deck.draw();
            int t = 1;
            if(numberCard.containsKey(type)) {
                t += numberCard.get(type);

            }
            numberCard.put(type, t);
        }

        for(EscapeCard key: numberCard.keySet()) {
            assertTrue("The number for each card should be 3", numberCard.get(key) == 3);
        }

    }

    @Test(expected=BadCardException.class)
    public void testThrowingExceptionEmptyDeck(){
        BaseEscapeDeck deck = new BaseEscapeDeck();

        for(int i=0; i<8; i++) {
            deck.draw();
        }
    }

}
