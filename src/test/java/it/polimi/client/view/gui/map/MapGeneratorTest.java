/**
 *
 */
package it.polimi.client.view.gui.map;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import it.polimi.client.view.gui.map.HexagonShape;
import it.polimi.client.view.gui.map.MapGenerator;
import it.polimi.common.map.MapType;

import org.junit.Test;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class MapGeneratorTest {

    @Test
    public void test() {
        MapGenerator generator = new MapGenerator(MapType.FERMI);

        assertThat("The given map should be a matrix of HexagonShape", generator.getHexagonMap(), instanceOf(HexagonShape[][].class));

        assertTrue("The number of rows should be 14", generator.getHexagonMap().length == 14);

        assertTrue("The number of columns should be 23", generator.getHexagonMap()[0].length == 23);
    }

}
