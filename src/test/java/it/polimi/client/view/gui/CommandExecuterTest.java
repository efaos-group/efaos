package it.polimi.client.view.gui;

import static org.junit.Assert.assertEquals;
import it.polimi.client.view.fake.FakeController;
import it.polimi.client.view.gui.item.CardComponentFactory;
import it.polimi.server.model.fake.FakeView;
import it.polimi.server.model.sector.Coordinate;

import org.junit.Before;
import org.junit.Test;

public class CommandExecuterTest {

    private CommandExecuter executor;
    private FakeView view;
    private FakeController controller;

    @Before
    public void setUp() {
        view = new FakeView("player1");
        controller = new FakeController();

        executor = new CommandExecuter(view, controller);
    }

    @Test
    public void testMouseClicked() {
        executor.mouseClicked(new Coordinate('G', 8));
        assertEquals(FakeController.COMMANDS.MOVE, controller.commands.poll());
    }

    @Test
    public void testMouseClickedForNoise() {
        executor.mouseClickedForNoise(new Coordinate('R', 10));
        assertEquals(FakeController.COMMANDS.SELECT_SECTOR, controller.commands.poll());
    }

    @Test
    public void testPassTurn() {
        executor.passTurn();
        assertEquals(FakeController.COMMANDS.END, controller.commands.poll());
    }

    @Test
    public void testDiscardCard() {
        executor.discardCard("attack");
        assertEquals(FakeController.COMMANDS.DISCARD, controller.commands.poll());
    }

    @Test
    public void testDrawSectorCard() {
        executor.drawSector();
        assertEquals(FakeController.COMMANDS.DRAW_SECTOR, controller.commands.poll());
    }

    @Test
    public void testAttack() {
        executor.attack();
        assertEquals(FakeController.COMMANDS.ATTACK, controller.commands.poll());
    }

    @Test
    public void testUseCard() {
        executor.useCard(CardComponentFactory.factory("Defense"));
        assertEquals(FakeController.COMMANDS.USE_CARD, controller.commands.poll());

        executor.useCard(CardComponentFactory.factory("Spotlight"));
        assertEquals(null, controller.commands.poll());
        executor.mouseClicked(new Coordinate('X', 1));
        assertEquals(FakeController.COMMANDS.USE_CARD, controller.commands.poll());
    }
}
