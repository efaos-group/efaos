/**
 *
 */
package it.polimi.client.view.gui.item;

import static org.junit.Assert.assertEquals;
import it.polimi.server.model.exception.BadCardException;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class ItemsPanelTest {

    private ItemsPanel pane;

    @Before
    public void setUp() {
        pane = new ItemsPanel();
    }

    @Test
    public void testAddCard() {

        ItemsPanel pane = new ItemsPanel();

        assertEquals(0, pane.countItems());
        assertEquals(3, pane.countComponents());

        pane.addNewItemCard("attack");
        assertEquals(1, pane.countItems());
        assertEquals(3, pane.countComponents());

        pane.addNewItemCard("defense");
        assertEquals(2, pane.countItems());
        assertEquals(3, pane.countComponents());

        pane.addNewItemCard("sedatives");
        assertEquals(3, pane.countItems());
        assertEquals(3, pane.countComponents());

        pane.addNewItemCard("attack");
        assertEquals(4, pane.countItems());
        assertEquals(4, pane.countComponents());
    }


    @Test
    public void testRemoveCard() {

        pane.addNewItemCard("attack");
        pane.addNewItemCard("attack");
        pane.addNewItemCard("sedatives");

        pane.removeUsedItemCard("attack");
        assertEquals(2, pane.countItems());
        assertEquals(3, pane.countComponents());

        pane.addNewItemCard("defense");
        pane.addNewItemCard("teleport");

        pane.removeUsedItemCard("defense");
        assertEquals(3, pane.countItems());
        assertEquals(3, pane.countComponents());
    }

    @Test(expected=BadCardException.class)
    public void testRemoveException() {

        pane.addNewItemCard("attack");
        pane.addNewItemCard("sedatives");

        pane.removeUsedItemCard("teleport");
    }

    @Test
    public void testSelected() {
        assertEquals(null, pane.getSelectedCard());
    }

    @Test
    public void testEnabling() {
        pane.enableAttackCard();

        pane.enableAttackCard();

        pane.enableDiscardCard();

        pane.enableItemUsage();

        pane.disableAttackCard();

        pane.disableItemUsage();
    }

}
