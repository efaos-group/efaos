/**
 *
 */
package it.polimi.client.view.gui.item;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import it.polimi.client.view.exception.ResourceNotFoundException;

import org.junit.Test;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class CardComponentFactoryTest {

    @Test
    public void testFactory() {

        assertThat(CardComponentFactory.factory("default"), instanceOf(DefaultComponent.class));

        assertThat(CardComponentFactory.factory("ATTACK"), instanceOf(AttackComponent.class));

        assertThat(CardComponentFactory.factory("ADRENALINE"), instanceOf(CardComponent.class));

        assertThat(CardComponentFactory.factory("DEFENSE"), instanceOf(DefenseComponent.class));

        assertThat(CardComponentFactory.factory("SEDATIVES"), instanceOf(CardComponent.class));

        assertThat(CardComponentFactory.factory("SPOTLIGHT"), instanceOf(SpotlightComponent.class));

        assertThat(CardComponentFactory.factory("TELEPORT"), instanceOf(CardComponent.class));
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testException() {
        CardComponentFactory.factory("error");
    }

}
