/**
 *
 */
package it.polimi.client.view.cli;

import static org.junit.Assert.assertEquals;
import it.polimi.client.view.cli.questioner.Console;
import it.polimi.client.view.cli.questioner.Questioner;
import it.polimi.client.view.fake.FakeConsole;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class QuestionerTest {

    private FakeConsole console;
    private Questioner questioner;


    @Before
    public void setUp() throws Exception {
        console = new FakeConsole();

        questioner = new Questioner(console);
    }

    @Test
    public void testAskPlayerID() {

        console.inBuffer.add("@invalid");
        console.inBuffer.add("valid");
        String answer = questioner.askForPlayerID();

        assertEquals(Console.USERNAME_QUESTION, console.outBuffer.poll());
        assertEquals(Console.USERNAME_ERROR, console.outBuffer.poll());
        assertEquals(null, console.outBuffer.poll());

        assertEquals("valid", answer);
    }

    @Test
    public void testAskMap() {

        console.inBuffer.add("error_map");
        console.inBuffer.add("fermi");
        String answer = questioner.askForMap();

        assertEquals(Console.MAP_QUESTION, console.outBuffer.poll());
        assertEquals(Console.MAP_ERROR, console.outBuffer.poll());
        assertEquals(null, console.outBuffer.poll());

        assertEquals("fermi", answer);
    }

    @Test
    public void testAskForWhichSector() {

        console.inBuffer.add("select 3 A");
        console.inBuffer.add("select A 3");
        String answer = questioner.askForWhichSector();

        assertEquals(Console.SECTOR_QUESTION, console.outBuffer.poll());
        assertEquals(Console.SECTOR_CHOICE_ERROR, console.outBuffer.poll());
        assertEquals(null, console.outBuffer.poll());

        assertEquals("select A 3", answer);
    }

    @Test
    public void testAskForItemToDiscard() {

        console.inBuffer.add("discard none");
        console.inBuffer.add("attack card");

        List<String> cardList = new LinkedList<String>();
        cardList.add("defense card");
        cardList.add("attack card");
        cardList.add("sedatives card");

        String answer = questioner.askForWhichItemToDiscard(cardList);

        assertEquals(Console.DISCARD_QUESTION, console.outBuffer.poll());
        assertEquals(Console.DISCARD_ERROR, console.outBuffer.poll());
        assertEquals(Console.DISCARD_QUESTION, console.outBuffer.poll());
        assertEquals(null, console.outBuffer.poll());

        assertEquals("attack card", answer);
    }

    @Test
    public void testAskForItemToUse() {
        console.inBuffer.add("sedatives");

        List<String> cardList = new LinkedList<String>();
        cardList.add("defense");
        cardList.add("attack");
        cardList.add("sedatives");

        String answer = questioner.askForWhichItemToUse(cardList);

        assertEquals(Console.ITEM_USAGE_QUESTION, console.outBuffer.poll());
        assertEquals(null, console.outBuffer.poll());

        assertEquals("sedatives", answer);
    }

}
