package it.polimi.client.view.cli.behaviour;

import static org.junit.Assert.*;
import it.polimi.client.view.fake.FakeController;
import it.polimi.client.view.fake.FakeController.COMMANDS;
import it.polimi.client.view.fake.FakeQuestioner.QUESTIONS;
import it.polimi.client.view.fake.FakeQuestioner;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class CommandExecuterTest {

    private CommandExecuter executer;
    private FakeController controller;
    private List<String> items;
    private FakeQuestioner questioner;
    @Before
    public void initTest(){
        questioner = new FakeQuestioner(null);
        controller = new FakeController();
        items = new LinkedList<String>();
        executer = new CommandExecuter("player", questioner, items, controller);
    }
    
    @Test
    public void executeTest(){
        executer.execute(CommandMatcher.MOVE_COMMAND, "move L 4");
        assertEquals(COMMANDS.MOVE, controller.commands.poll());
        
        executer.execute(CommandMatcher.ATTACK, "attack");
        assertEquals(COMMANDS.ATTACK, controller.commands.poll());
        
        items.add("Attack Card");
        executer.execute(CommandMatcher.ATTACK_CARD, "use attack card");
        assertEquals(COMMANDS.USE_CARD, controller.commands.poll());
        
        items.add("Spotlight Card");
        executer.execute(CommandMatcher.DISCARD, "discard");
        assertEquals(COMMANDS.DISCARD, controller.commands.poll());
        assertEquals(QUESTIONS.WRITE, questioner.questions.poll());
        assertEquals(QUESTIONS.WRITE, questioner.questions.poll());
        assertEquals(QUESTIONS.ITEM_DISCARD, questioner.questions.poll());
        
        items.add("Teleport Card");
        executer.execute(CommandMatcher.ITEM_COMMAND, "item");
        assertEquals(COMMANDS.USE_CARD, controller.commands.poll());
        assertEquals(QUESTIONS.WRITE, questioner.questions.poll());
        assertEquals(QUESTIONS.WRITE, questioner.questions.poll());
        assertEquals(QUESTIONS.ITEM_USE, questioner.questions.poll());
        
        executer.execute(CommandMatcher.DRAW_ITEM, "draw item card");
        assertEquals(COMMANDS.DRAW_ITEM, controller.commands.poll());
        
        executer.execute(CommandMatcher.DRAW_SECTOR, "draw sector card");
        assertEquals(COMMANDS.DRAW_SECTOR, controller.commands.poll());
        
        executer.execute(CommandMatcher.PASS_TURN, "end");
        assertEquals(COMMANDS.END, controller.commands.poll());
        
        executer.execute(CommandMatcher.SELECT_SECTOR, "select A 3");
        assertEquals(COMMANDS.SELECT_SECTOR, controller.commands.poll());
        }
}
