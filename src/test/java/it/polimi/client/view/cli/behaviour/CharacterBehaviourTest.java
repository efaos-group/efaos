/**
 * 
 */
package it.polimi.client.view.cli.behaviour;

import static org.junit.Assert.assertEquals;
import it.polimi.client.view.exception.CommandNotFoundException;
import it.polimi.client.view.fake.FakeCommandExecuter;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class CharacterBehaviourTest {
    
    private CharacterBehaviour human,alien;
    private FakeCommandExecuter executer;
    
    @Before
    public void initTest(){
        executer = new FakeCommandExecuter(null, null, null, null);
        human = BehaviourFactory.factory("HUMAN", executer);
        alien = BehaviourFactory.factory("ALIEN", executer);
    }

    @Test
    public void enableMovementTest(){
        String move = "move A 2";
        human.enableMovement();
        human.executeCommand(move);
        assertEquals(CommandMatcher.MOVE_COMMAND,executer.commands.poll());
        assertEquals(move, executer.answers.poll());
    }
    
    @Test (expected = CommandNotFoundException.class)
    public void disableMovementTest(){
        alien.enableMovement();
        String move = "move D 2";
        alien.disableMovement();
        alien.executeCommand(move);
    }
    
    @Test
    public void humanEnableItemUsageTest(){
        String item = "item";
        human.enableItemUsage();
        human.executeCommand(item);
        assertEquals(CommandMatcher.ITEM_COMMAND,executer.commands.poll());
        assertEquals(item, executer.answers.poll());
    }
    
    @Test (expected = CommandNotFoundException.class)
    public void alienEnableItemUsageTest(){
        String item = "item";
        alien.enableItemUsage();
        alien.executeCommand(item);
    }
    
    @Test (expected = CommandNotFoundException.class)
    public void disableItemUsageTest(){
        String item = "item";
        human.enableItemUsage();
        human.disableItemUsage();
        human.executeCommand(item);
    }
    
    @Test (expected = CommandNotFoundException.class)
    public void humanEnableAttackTest(){
        String attack = "attack";
        human.enableAttack();;
        human.executeCommand(attack);
    }
    
    @Test
    public void alienEnableAttackTest(){
        String attack = "attack";
        alien.enableAttack();
        alien.executeCommand(attack);
        assertEquals(CommandMatcher.ATTACK,executer.commands.poll());
        assertEquals(attack, executer.answers.poll());
    }
    
    @Test (expected = CommandNotFoundException.class)
    public void alienDisableAttackTest(){
        String attack = "attack";
        alien.enableAttack();
        alien.disableAttack();
        alien.executeCommand(attack);
    }
    
    @Test
    public void enableEndTurnTest(){
        String end = "end";
        alien.enableEndTurn();
        alien.executeCommand(end);
        assertEquals(CommandMatcher.PASS_TURN,executer.commands.poll());
        assertEquals(end, executer.answers.poll());
    }
    
    @Test (expected = CommandNotFoundException.class)
    public void disableEndTurnTest(){
        String end = "end";
        human.enableEndTurn();
        human.disableEndTurn();
        human.executeCommand(end);
    }
    
    @Test
    public void enableDrawSectorCard(){
        String draw = "draw sector card";
        alien.enableDrawSectorCard();
        alien.executeCommand(draw);
        assertEquals(CommandMatcher.DRAW_SECTOR,executer.commands.poll());
        assertEquals(draw, executer.answers.poll());
    }
    
    @Test (expected = CommandNotFoundException.class)
    public void disableDrawSectorCard(){
        String draw = "draw sector card";
        human.enableDrawSectorCard();
        human.disableDrawSectorCard();
        human.executeCommand(draw);
    }
    
    @Test
    public void enableDiscardTest(){
        String discard = "discard";
        alien.enableDiscard();
        alien.executeCommand(discard);
        assertEquals(CommandMatcher.DISCARD,executer.commands.poll());
        assertEquals(discard, executer.answers.poll());
    }
    
    @Test (expected = CommandNotFoundException.class)
    public void disableDiscardTest(){
        String discard = "discard";
        human.enableDiscard();
        human.disableDiscard();
        human.executeCommand(discard);
    }
    
    @Test
    public void humanEnableAttackCardTest(){
        String attack = "use attack card";
        human.enableAttackCard();
        human.executeCommand(attack);
        assertEquals(CommandMatcher.ATTACK_CARD,executer.commands.poll());
        assertEquals(attack, executer.answers.poll());
    }
    
    @Test (expected = CommandNotFoundException.class)
    public void alienDisableAttackCardTest(){
        String attack = "use attack card";
        alien.enableAttackCard();
        alien.executeCommand(attack);
    }
    
    @Test (expected = CommandNotFoundException.class)
    public void disableAttackCardTest(){
        String attack = "use attack card";
        human.enableAttackCard();
        human.disableAttackCard();
        human.executeCommand(attack);
    }
    
    @Test
    public void enableSelectSectorTest(){
        String sector = "select A 2";
        human.enableSelectSector();
        human.executeCommand(sector);
        assertEquals(CommandMatcher.SELECT_SECTOR,executer.commands.poll());
        assertEquals(sector, executer.answers.poll());
    }
    
    @Test (expected = CommandNotFoundException.class)
    public void disableSelectSectorTest(){
        String sector = "select A 3";
        alien.enableSelectSector();
        alien.disableSelectSector();
        alien.executeCommand(sector);
    }
    
    @Test
    public void enableDrawItemCard(){
        String draw = "draw item card";
        human.enableDrawItemCard();
        human.executeCommand(draw);
        assertEquals(CommandMatcher.DRAW_ITEM,executer.commands.poll());
        assertEquals(draw, executer.answers.poll());
    }
    
    @Test (expected = CommandNotFoundException.class)
    public void disableDrawItemCard(){
        String draw = "draw item card";
        alien.enableDrawItemCard();
        alien.disableDrawItemCard();
        alien.executeCommand(draw);
    }
    
    @Test
    public void getAvailableActionsTest(){
        alien.enableAttack();
        String actions = alien.getAvailableActions();
        assertEquals(actions,"\n" + CommandMatcher.ATTACK_DESCRIPTION);
    }
}
