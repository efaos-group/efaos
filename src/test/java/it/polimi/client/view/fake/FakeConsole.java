/**
 *
 */
package it.polimi.client.view.fake;

import it.polimi.client.view.cli.questioner.Console;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class FakeConsole extends Console {

    public Queue<String> outBuffer;
    public Queue<String> inBuffer;

    public FakeConsole() {
        outBuffer = new LinkedList<String>();
        inBuffer = new LinkedList<String>();
    }

    @Override
    public void write(String msg){
        outBuffer.add(msg);
    }

    @Override
    public String read(){
        return inBuffer.poll();
    }

    @Override
    public void close(){
    }

}
