package it.polimi.client.view.fake;

import it.polimi.client.view.cli.questioner.Console;
import it.polimi.client.view.cli.questioner.Questioner;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class FakeQuestioner extends Questioner {

    public static enum QUESTIONS{
        PLAYER,MAP,SECTOR,ITEM_DISCARD,ITEM_USE,WRITE,READ;
    }
    
    public Queue<QUESTIONS> questions;
    
    public FakeQuestioner(Console console) {
        super(console);
        questions = new LinkedList<QUESTIONS>();
    }

    @Override
    public String askForPlayerID() {
        questions.add(QUESTIONS.PLAYER);
        return "";
    }

    @Override
    public String askForMap() {
        questions.add(QUESTIONS.MAP);
        return "";
    }

    @Override
    public String askForWhichSector() {
        questions.add(QUESTIONS.SECTOR);
        return "";
    }

    @Override
    public String askForWhichItemToDiscard(List<String> items) {
        questions.add(QUESTIONS.ITEM_DISCARD);
        return "";
    }

    @Override
    public String askForWhichItemToUse(List<String> items) {
        questions.add(QUESTIONS.ITEM_USE);
        return "";
    }

    @Override
    public void write(String message) {
        questions.add(QUESTIONS.WRITE);
    }

    @Override
    public String read() {
        questions.add(QUESTIONS.READ);
        return "";
    }

    
}
