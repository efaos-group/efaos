package it.polimi.client.view.fake;

import it.polimi.client.remotecontroller.RemoteControllerInterface;
import it.polimi.common.observer.View;

import java.util.LinkedList;
import java.util.Queue;

public class FakeController implements RemoteControllerInterface {
    public static enum COMMANDS{
        END,MOVE,USE_CARD,DRAW_ITEM,DRAW_SECTOR,DISCARD,ATTACK,SELECT_SECTOR;
    }

    public Queue<COMMANDS> commands;

    public FakeController(){
        commands = new LinkedList<COMMANDS>();
    }
    @Override
    public void passTurn(String playerID) {
        commands.add(COMMANDS.END);
    }

    @Override
    public void move(String playerID, String col, String rowString) {
        commands.add(COMMANDS.MOVE);
    }

    @Override
    public void useCard(String playerID, String cardType, String col,
            String rowString) {
        commands.add(COMMANDS.USE_CARD);
    }

    @Override
    public void drawItemCard(String playerID) {
        commands.add(COMMANDS.DRAW_ITEM);
    }

    @Override
    public void discardCard(String playerID, String cardType) {
        commands.add(COMMANDS.DISCARD);
    }

    @Override
    public void drawSectorCard(String playerID) {
        commands.add(COMMANDS.DRAW_SECTOR);
    }

    @Override
    public void attack(String playerID) {
        commands.add(COMMANDS.ATTACK) ;
    }

    @Override
    public void selectSectorForNoise(String playerID, String col,
            String rowString) {
        commands.add(COMMANDS.SELECT_SECTOR);
    }

    @Override
    public void removeDisconnectedPlayerFromGame(String playerID) {
        // TODO Auto-generated method stub

    }

    @Override
    public void sendPlayerChoice(String playerID, String map) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setView(View view) {
        // TODO Auto-generated method stub

    }


}
