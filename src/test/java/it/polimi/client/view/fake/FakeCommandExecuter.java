package it.polimi.client.view.fake;

import it.polimi.client.view.cli.behaviour.CommandExecuter;
import it.polimi.client.view.cli.questioner.Questioner;
import it.polimi.server.controller.Controller;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class FakeCommandExecuter extends CommandExecuter {

    public final Queue <String> commands;
    public final Queue <String> answers;
    
    public FakeCommandExecuter(String playerID, Questioner questioner,
            List<String> items, Controller controller) {
        super(playerID, questioner, items, controller);
        commands = new LinkedList<String>();
        answers = new LinkedList<String>();
    }

    @Override
    public void execute(String command, String answer) {
        commands.add(command);
        answers.add(answer);
    }

}
