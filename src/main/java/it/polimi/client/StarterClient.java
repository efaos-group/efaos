/**
 *
 */
package it.polimi.client;

import it.polimi.client.view.gui.dialog.picture.Picture;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class StarterClient extends JFrame {

    private static final long serialVersionUID = -2898921674902027411L;
    private static final String TITLE = "General Settings";
    private static final String PATH = "/image/starterlogo.jpg";
    private static final Dimension NORMAL_SIZE = new Dimension(250,290);

    private final StarterDialog dialog;
    private final Picture logo;

    public StarterClient() {
        super.setAlwaysOnTop(true);
        super.setTitle(TITLE);
        super.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        dialog = new StarterDialog(this);
        logo = new Picture(PATH, NORMAL_SIZE);

        setLayout(new BorderLayout());
        add(logo,BorderLayout.NORTH);
        add(dialog,BorderLayout.SOUTH);
        setResizable(false);
        setVisible(true);
        pack();
    }

}
