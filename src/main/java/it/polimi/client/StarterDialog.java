/**
 *
 */
package it.polimi.client;

import it.polimi.client.remotecontroller.RemoteController;
import it.polimi.client.remotecontroller.RemoteControllerInterface;
import it.polimi.client.view.cli.MainCLI;
import it.polimi.client.view.gui.MainGUI;
import it.polimi.common.observer.View;
import it.polimi.network.rmi.factory.RMIFactory;
import it.polimi.network.socket.factory.SocketFactory;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class StarterDialog extends JPanel implements ActionListener {
    private static final long serialVersionUID = 4100046325366987315L;

    private static final String VIEW_MESSGE = "Graphic:";
    private static final String COMUNICATION_MESSAGE = "Connection:";
    private static final String BUTTON_LABEL = "Play!";

    private final JPanel comunicationPane;
    private final JComboBox<String> comunicationComboBox;
    private final JFrame parent;

    private final JPanel viewPane;
    private final JComboBox<String> viewComboBox;

    private final JPanel buttonPane;

    public StarterDialog(JFrame parent) {
        this.parent = parent;

        viewPane = new JPanel();
        viewComboBox = new JComboBox<String>();
        initViewPane();


        comunicationPane = new JPanel();
        comunicationComboBox = new JComboBox<String>();
        initComunicationPane();


        buttonPane = new JPanel();
        initButtonPane();
        setLayout(new BorderLayout());
        add(viewPane, BorderLayout.CENTER);
        add(comunicationPane, BorderLayout.NORTH);
        add(buttonPane, BorderLayout.SOUTH);
    }

    private void initViewPane() {
        JLabel viewMessage = new JLabel(VIEW_MESSGE);
        initViewChoose();

        viewPane.add(viewMessage);
        viewPane.add(viewComboBox);
    }

    private void initComunicationPane() {
        JLabel comunicationMessage = new JLabel(COMUNICATION_MESSAGE);
        initCominicationChoose();

        comunicationPane.add(comunicationMessage);
        comunicationPane.add(comunicationComboBox);
    }

    private void initButtonPane() {
        JButton confirmButton = new JButton(BUTTON_LABEL);
        confirmButton.addActionListener(this);
        buttonPane.add(confirmButton);
    }

    private void initViewChoose() {
        viewComboBox.addItem("GUI");
        viewComboBox.addItem("CLI");

        viewComboBox.setSelectedIndex(0);
    }

    private void initCominicationChoose() {
        comunicationComboBox.addItem("Socket");
        comunicationComboBox.addItem("RMI");

        comunicationComboBox.setSelectedIndex(0);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        parent.dispose();

        String view = (String) viewComboBox.getSelectedItem();

        String comunication = (String) comunicationComboBox.getSelectedItem();

        factory(view, comunication);
    }

    private void factory(String choiceView, String choiceComunication) {
        RemoteControllerInterface connection = null;
        View view = null;

        if("rmi".equalsIgnoreCase(choiceComunication)) {
            connection = new RemoteController(new RMIFactory());
        } else if("socket".equalsIgnoreCase(choiceComunication)) {
            connection = new RemoteController(new SocketFactory());
        }

        if("cli".equalsIgnoreCase(choiceView)) {
            view = new MainCLI(connection);
        } else if("gui".equalsIgnoreCase(choiceView)) {
            view = new MainGUI(connection);
        }

        connection.setView(view);
    }

}
