package it.polimi.client;

import java.awt.Dimension;
import java.awt.Toolkit;


/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class Main {

    private Main() {
    }

    public static void main(String[] args) {
        StarterClient starter = new StarterClient();

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        starter.setLocation(dim.width/2-starter.getSize().width/2, dim.height/2-starter.getSize().height/2);
        starter.pack();
    }

}
