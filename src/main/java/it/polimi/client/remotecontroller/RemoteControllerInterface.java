/**
 * 
 */
package it.polimi.client.remotecontroller;

import it.polimi.common.observer.View;
import it.polimi.server.controller.Controller;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public interface RemoteControllerInterface extends Controller {
    
    public void sendPlayerChoice(String playerID, String map);
    public void setView(View view);
}
