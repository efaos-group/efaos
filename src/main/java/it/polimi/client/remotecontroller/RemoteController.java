/**
 *
 */
package it.polimi.client.remotecontroller;

import it.polimi.common.observer.View;
import it.polimi.network.Communicator;
import it.polimi.network.CommunicatorListener;
import it.polimi.network.NetworkFactory;
import it.polimi.network.ProtocolParser;
import it.polimi.network.Subscriber;


/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class RemoteController implements CommunicatorListener, RemoteControllerInterface {

    private View view;
    private final Communicator communicator;
    private final ProtocolParser parser;
    private final NetworkFactory netFactory;
    private String topic;

    public RemoteController(NetworkFactory netFactory) {
        this.netFactory = netFactory;
        this.communicator = netFactory.getNewDirectConnection();
        this.communicator.setListener(this);
        parser = new ProtocolParser();
    }

    @Override
    public void sendPlayerChoice(String playerID, String map) {
        parser.setPlayerID(playerID);
        parser.setCommands(map);

        send();
    }

    @Override
    public void passTurn(String playerID) {
        parser.setPlayerID(playerID);
        parser.setTopic(topic);
        parser.setCommands(ProtocolParser.PASS_TURN);

        send();
    }

    @Override
    public void move(String playerID, String col, String rowString) {
        parser.setPlayerID(playerID);
        parser.setTopic(topic);
        parser.setCommands(ProtocolParser.MOVE);

        String []arguments = new String[]{col, rowString};
        parser.setMessageArguments(arguments);

        send();
    }

    @Override
    public void useCard(String playerID, String cardType, String col, String rowString) {
        parser.setPlayerID(playerID);
        parser.setTopic(topic);
        parser.setCommands(ProtocolParser.USE_CARD);

        String []arguments = new String[]{cardType, col, rowString};
        parser.setMessageArguments(arguments);

        send();
    }

    @Override
    public void drawItemCard(String playerID) {
        parser.setPlayerID(playerID);
        parser.setTopic(topic);
        parser.setCommands(ProtocolParser.DRAW_ITEM_CARD);

        send();
    }

    @Override
    public void discardCard(String playerID, String cardType) {
        parser.setPlayerID(playerID);
        parser.setTopic(topic);
        parser.setCommands(ProtocolParser.DISCARD_CARD);

        String []arguments = new String[]{cardType};
        parser.setMessageArguments(arguments);

        send();
    }

    @Override
    public void drawSectorCard(String playerID) {
        parser.setPlayerID(playerID);
        parser.setTopic(topic);
        parser.setCommands(ProtocolParser.DRAW_SECTOR_CARD);

        send();
    }

    @Override
    public void attack(String playerID) {
        parser.setPlayerID(playerID);
        parser.setTopic(topic);
        parser.setCommands(ProtocolParser.ATTACK);

        send();
    }

    @Override
    public void selectSectorForNoise(String playerID, String col, String rowString) {
        parser.setPlayerID(playerID);
        parser.setTopic(topic);
        parser.setCommands(ProtocolParser.SELECT_SECTOR);

        String []arguments = new String[]{col, rowString};
        parser.setMessageArguments(arguments);

        send();
    }

    @Override
    public void setView(View view) {
        this.view = view;
    }

    @Override
    public void notifyMessageReceived(Communicator socketSource, String msg) {
        if(isEmptyMessage(msg)) {
            return;
        }

        parser.parseMessage(msg);
        String answer = parser.getCommands()[0];

        if (answer.equalsIgnoreCase(ProtocolParser.ACK)) {
            view.choiceAccepted("Request accepted, the game will begin soon...");
            setTopicFromMessage();
            createSubscriber();

        } else if (answer.equalsIgnoreCase(ProtocolParser.REJECTED)) {
            view.showErrorDialog("Your username was already taken");
            view.askForPlayerID();
            view.sendPlayerChoice();
        }
    }

    private void send() {
        String message = parser.getParsedMessage();
        communicator.send(message);
    }

    private boolean isEmptyMessage(String msg) {
        return msg.length() < ProtocolParser.EMPTY_MESSAGE;
    }

    private void setTopicFromMessage() {
        topic = parser.getCommands()[2];
    }

    private void createSubscriber() {
        Subscriber subscriber = netFactory.getNewSubscriber(topic);
        subscriber.register(view);
    }

    @Override
    public void removeDisconnectedPlayerFromGame(String playerID) {
        // Do nothing because this method is used only on server-side
    }

    @Override
    public void notifyConnectionClosed(Communicator socketSource) {
        // Do nothing because this method is used only on server-side

    }

}
