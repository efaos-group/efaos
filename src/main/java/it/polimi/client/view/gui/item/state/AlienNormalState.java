/**
 *
 */
package it.polimi.client.view.gui.item.state;

import it.polimi.client.view.gui.listener.GameActionListener;

import javax.swing.JButton;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class AlienNormalState extends AlienState {
    private static final long serialVersionUID = -662566383306164484L;

    public AlienNormalState(GameActionListener listener, JButton attack, JButton use, JButton discard, JButton end, JButton draw) {
        super(listener, attack, use, discard, end, draw);

        attack.setEnabled(false);
        use.setEnabled(false);
        discard.setEnabled(false);
        end.setEnabled(false);
        draw.setEnabled(false);
    }

    @Override
    public ItemState disableAll() {
        return this;
    }

}
