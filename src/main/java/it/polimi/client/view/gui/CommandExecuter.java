/**
 *
 */
package it.polimi.client.view.gui;

import it.polimi.client.remotecontroller.RemoteControllerInterface;
import it.polimi.client.view.gui.item.CardComponent;
import it.polimi.client.view.gui.listener.GameActionListener;
import it.polimi.client.view.gui.listener.MouseClickListener;
import it.polimi.common.observer.View;
import it.polimi.server.model.sector.Coordinate;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class CommandExecuter implements MouseClickListener, GameActionListener{
    private static final long serialVersionUID = -9176123005235630071L;


    private final transient RemoteControllerInterface controller;
    private final transient View view;
    private final transient Queue<String> cardBuffer;

    public CommandExecuter(View view, RemoteControllerInterface controller) {
        this.view = view;
        this.controller = controller;
        cardBuffer = new LinkedList<String>();
    }

    @Override
    public void mouseClicked(Coordinate position) {
        String card = cardBuffer.poll();
        if(card == null) {
            controller.move(view.getPlayerID(), position.getCol()+"", position.getRow()+"");
        } else {
            controller.useCard(view.getPlayerID(), card, position.getCol()+"", position.getRow()+"");
        }
    }

    @Override
    public void mouseClickedForNoise(Coordinate position) {
        controller.selectSectorForNoise(view.getPlayerID(), position.getCol()+"", position.getRow()+"");
    }

    @Override
    public void passTurn() {
        controller.passTurn(view.getPlayerID());
    }

    @Override
    public void attack() {
        controller.attack(view.getPlayerID());
    }

    @Override
    public void useCard(CardComponent card) {
        if(card.needSectorChoice()) {
            cardBuffer.add(card.toString());
            view.enableMovement();
        } else {
            controller.useCard(view.getPlayerID(), card.toString(), " ", " ");
        }
    }

    @Override
    public void discardCard(String card) {
        controller.discardCard(view.getPlayerID(),card);
    }

    @Override
    public void drawSector() {
        controller.drawSectorCard(view.getPlayerID());
    }

}
