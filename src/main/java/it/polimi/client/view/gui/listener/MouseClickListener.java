/**
 *
 */
package it.polimi.client.view.gui.listener;

import it.polimi.server.model.sector.Coordinate;

import java.io.Serializable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public interface MouseClickListener extends Serializable {

    public void mouseClicked(Coordinate position);

    public void mouseClickedForNoise(Coordinate position);

}
