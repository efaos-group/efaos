/**
 *
 */
package it.polimi.client.view.gui.dialog;

import it.polimi.client.view.gui.dialog.picture.EventImageType;
import it.polimi.client.view.gui.dialog.picture.Picture;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.beans.EventHandler;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class PictureDialog extends JDialog {

    private static final long serialVersionUID = -9121204721623351616L;
    private static final Dimension NORMAL_SIZE = new Dimension(200,306);

    public PictureDialog(String message, String card) {
        super.setTitle(card.replaceAll("_", " "));
        super.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        super.setBackground(Color.LIGHT_GRAY);

        JComponent pic = new Picture(EventImageType.parseInput(card),NORMAL_SIZE);
        JPanel picPanel = new JPanel();
        picPanel.add(pic);
        picPanel.setBackground(Color.LIGHT_GRAY);

        JPanel labelContainer = new JPanel();
        setLabelConfig(message, labelContainer);


        JPanel buttonContainer = new JPanel();
        setButtonConfig(buttonContainer);

        add(picPanel,BorderLayout.NORTH);
        add(labelContainer,BorderLayout.CENTER);
        add(buttonContainer,BorderLayout.SOUTH);

        createWindow();
    }

    private void setButtonConfig(JPanel buttonContainer) {
        JButton button = new JButton("OK");
        button.addActionListener(EventHandler.create(ActionListener.class, this, "dispose"));
        buttonContainer.add(button);
        buttonContainer.setBackground(Color.LIGHT_GRAY);
    }

    private void createWindow() {
        super.pack();
        super.setLocationRelativeTo(null);
        super.setResizable(false);
        super.setAlwaysOnTop(true);
        super.setVisible(true);
    }

    private void setLabelConfig(String message, JPanel labelContainer) {

        String formattedMessage = "<html>"  + message.replaceAll("\\n", "<br/>") + "</html>";
        formattedMessage = formattedMessage.replaceAll("_", " ");
        formattedMessage = formattedMessage.replaceAll("\\t", "&nbsp;&nbsp;&nbsp;&nbsp;");

        JLabel msg = new JLabel(formattedMessage);
        labelContainer.add(msg);
        labelContainer.setBackground(Color.LIGHT_GRAY);
    }

}
