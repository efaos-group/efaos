/**
 *
 */
package it.polimi.client.view.gui.map;

import it.polimi.client.view.gui.listener.MouseClickListener;
import it.polimi.client.view.gui.map.state.ClickState;
import it.polimi.client.view.gui.map.state.ClickableState;
import it.polimi.common.map.MapType;
import it.polimi.server.model.sector.Coordinate;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class MapPane extends JPanel implements MouseClickListener, ActionListener {
    private static final long serialVersionUID = 8508592441986959686L;

    private static final Integer HEIGHT = 772;
    private static final Integer WIDTH = 1050;

    private Integer lastRow;
    private Integer lastCol;

    private Timer timerNoise;

    private final HexagonShape[][] map;

    private ClickState state;

    public MapPane(MapType map, MouseClickListener listener) {
        super();

        lastRow = 0;
        lastCol = 0;

        MapGenerator mapGenerator = new MapGenerator(map);
        this.map = mapGenerator.getHexagonMap();

        super.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        setHexagonAsListeners();

        this.state = new ClickableState(listener);
    }

    private void setHexagonAsListeners() {
        for(int i = 0; i < map.length; i++) {
            for(int j = 0; j < map[0].length; j++) {
                super.addMouseListener(map[i][j]);
                map[i][j].setClickListener(this);
            }
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        for(int i = 0; i < map.length; i++) {
            for(int j = 0; j < map[0].length; j++) {
                map[i][j].restorePointsFromBackup();
                map[i][j].horizontalShift(getHorizontalShift(j));
                map[i][j].verticalShift(getVerticalShift(j,i));
                map[i][j].draw(g2);
            }
        }
    }

    private Double getHorizontalShift(int j) {
        int radius = MapGenerator.RADIUS;

        if(j%2==0) {
            return (double) (radius*(j/2) + radius*j + radius/2);
        } else {
            return (double) ( radius*(j/2) + (radius) +radius*j);
        }
    }

    private Double getVerticalShift(int j, int i) {
        int radius = MapGenerator.RADIUS;

        if(j%2==0) {
            return (double) ((int) ((radius+1) * Math.sqrt(3))*i);
        } else {
            return (double) ((int) ((radius+1) * Math.sqrt(3))/2 +  (int) ((radius+1) * Math.sqrt(3)) * i);
        }
    }

    public void disableClick() {
        state = state.disableClick();
    }

    public void enableClick() {
        state = state.enableClick();
    }

    public void enableNoise() {
        state = state.enableNoise();
    }

    @Override
    public void mouseClicked(Coordinate position) {
        state.notifyClick(position);
    }

    @Override
    public void mouseClickedForNoise(Coordinate position) {
        state.notifyClick(position);
    }

    public void makeNoise(String msg) {

        if(msg.contains("Silence")){
            return;
        }

        final int startDelay = 20;
        final int pause = 2000;

        int row = extractRow(msg);
        int col = extractCol(msg);

        map[row][col].makeNoise();

        timerNoise = new Timer(startDelay, map[row][col]);
        timerNoise.addActionListener(this);
        timerNoise.setInitialDelay(pause);

        timerNoise.start();

        update();
    }

    public void blockSector(String position) {
        int row = extractRow(position);
        int col = extractCol(position);

        map[row][col].block();
    }

    public void setPosition(String position) {

        map[lastRow][lastCol].movedAway();

        lastRow = extractRow(position);
        lastCol = extractCol(position);

        map[lastRow][lastCol].movedOver();

        update();
    }

    private Integer extractRow(String position) {
        final String split = ",";
        final String end = "]";

        String row = position.split(split)[1];
        row = row.substring(0, row.indexOf(end));
        return Integer.parseInt(row) - 1;
    }

    private Integer extractCol(String position) {
        final String split = ",";
        final String start = "[";

        String col = position.split(split)[0];
        col = col.substring(col.indexOf(start)+1, col.length());
        return col.charAt(0) - 'A';
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        update();
        timerNoise.stop();
    }

    public void colorChanged(Color newColor) {
        setBackground(newColor);
        update();
    }

    protected void update() {
        invalidate();
        validate();
        repaint();
    }

}
