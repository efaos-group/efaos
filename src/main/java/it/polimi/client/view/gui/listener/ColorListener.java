/**
 *
 */
package it.polimi.client.view.gui.listener;

import java.awt.Color;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public interface ColorListener {

    public void colorChanged(Color newColor);

}
