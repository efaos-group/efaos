/**
 *
 */
package it.polimi.client.view.gui.dialog.map;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class MapPanel extends JPanel {
    private static final long serialVersionUID = 4051706972702556990L;

    private final CardLayout layout = new CardLayout();
    private JPanel screen;

    public MapPanel() {
        setLayout(new BorderLayout());
        initMapComponent();
        initButtons();
        add(screen,BorderLayout.CENTER);
    }

    private void initMapComponent(){
        screen = new JPanel(layout);
        for (MapImageType type : MapImageType.values()){
            JPanel mapContainer = new JPanel(new BorderLayout());


            JLabel title = new JLabel(type.toString());
            MapComponent component = new MapComponent(type);

            JPanel titlePanel = new JPanel();
            titlePanel.add(title);

            mapContainer.add(titlePanel, BorderLayout.NORTH);
            mapContainer.add(component, BorderLayout.CENTER);

            screen.add(mapContainer);
        }
    }

    private void initButtons(){
        JButton next = new JButton(">");
        next.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                layout.next(screen);
            }
        });

        JButton previous = new JButton("<");
        previous.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                layout.previous(screen);
            }
        });

        JPanel buttonContainer = new JPanel(new FlowLayout());
        buttonContainer.add(previous);
        buttonContainer.add(next);
        add(buttonContainer, BorderLayout.SOUTH);
    }

}
