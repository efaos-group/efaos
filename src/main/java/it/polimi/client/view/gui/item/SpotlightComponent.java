/**
 * 
 */
package it.polimi.client.view.gui.item;


/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class SpotlightComponent extends CardComponent {

    private static final long serialVersionUID = -5982476669902158142L;

    public SpotlightComponent(String name, String path) {
        super(name, path);
    }
    
    @Override
    public boolean needSectorChoice(){
        return true;
    }
    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof SpotlightComponent)) {
            return false;
        }
        if (obj == this) {
            return true;
        }

        SpotlightComponent c = (SpotlightComponent) obj;
        return c.getPath().equals(super.getPath()) && c.toString().equals(super.toString());
    }

    @Override
    public int hashCode() {
        return super.getPath().hashCode() + super.toString().hashCode();
    }

}
