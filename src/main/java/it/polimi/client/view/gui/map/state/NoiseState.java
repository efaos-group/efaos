/**
 *
 */
package it.polimi.client.view.gui.map.state;

import it.polimi.client.view.gui.listener.MouseClickListener;
import it.polimi.server.model.sector.Coordinate;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class NoiseState extends ClickState {
    private static final long serialVersionUID = 7445286878235058277L;

    public NoiseState(MouseClickListener listener) {
        super(listener);
    }

    @Override
    public void notifyClick(Coordinate clickPosition) {
        listener.mouseClickedForNoise(clickPosition);
    }

}
