/**
 *
 */
package it.polimi.client.view.gui.item.state;

import it.polimi.client.view.gui.listener.GameActionListener;
import it.polimi.server.model.exception.BadCharacterException;

import javax.swing.JButton;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class ItemStateFactory {

    private ItemStateFactory() {
    }

    public static ItemState factory(String behavior, GameActionListener listener, JButton attack, JButton use,
                                                                                    JButton discard, JButton end, JButton draw) {

        if("human".equalsIgnoreCase(behavior)) {
            return new HumanNormalState(listener, attack, use, discard, end, draw);
        } else if("alien".equalsIgnoreCase(behavior)) {
            return new AlienNormalState(listener, attack, use, discard, end, draw);
        }

        throw new BadCharacterException("Error in creating a Character Behaviour for the view");
    }

}
