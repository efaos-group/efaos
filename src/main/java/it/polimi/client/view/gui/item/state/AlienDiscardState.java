/**
 *
 */
package it.polimi.client.view.gui.item.state;

import it.polimi.client.view.gui.listener.GameActionListener;

import javax.swing.JButton;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class AlienDiscardState extends AlienState {
    private static final long serialVersionUID = 1430485633906571795L;

    public AlienDiscardState(GameActionListener listener, JButton attack, JButton use, JButton discard, JButton end, JButton draw) {
        super(listener, attack, use, discard, end, draw);

        discard.setEnabled(true);
        use.setEnabled(false);
    }

    @Override
    public ItemState enableDiscardCard() {
        return this;
    }

}
