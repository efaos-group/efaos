/**
 *
 */
package it.polimi.client.view.gui.map;

import it.polimi.server.model.sector.Coordinate;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class DangerousHexagon extends HexagonShape {
    private static final long serialVersionUID = 4206886965899588381L;

    private static final Color DANGEROUS_COLOR = new Color(20, 20, 20, 100);

    public DangerousHexagon(int radius, Coordinate position) {
        super(radius, position);
        color = DANGEROUS_COLOR;
    }

    public DangerousHexagon(double x, double y, int radius, Coordinate position) {
        super(x, y, radius, position);
        color = DANGEROUS_COLOR;
    }

    public DangerousHexagon(java.awt.geom.Point2D.Double start, int radius, Coordinate position) {
        super(start, radius, position);
        color = DANGEROUS_COLOR;
    }

    @Override
    public void draw(Graphics2D g) {
        super.draw(g);

        Symbol symbol = new SectorLabel(radius, points[0], position);
        symbol.draw(g);
    }

    @Override
    public void movedAway() {
        if(color == Color.GREEN) {
            color = DANGEROUS_COLOR;
        } else {
            color = beforeNoise;
        }
    }
}
