/**
 *
 */
package it.polimi.client.view.gui.map;

import it.polimi.server.model.sector.Coordinate;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class SecureHexagon extends HexagonShape {
    private static final long serialVersionUID = 529115106304412867L;

    private static final Color SECURE_COLOR = new Color(50, 50, 50, 50);

    public SecureHexagon(int radius, Coordinate position) {
        super(radius, position);
        color = SECURE_COLOR;
    }

    public SecureHexagon(double x, double y, int radius, Coordinate position) {
        super(x, y, radius, position);
        color = SECURE_COLOR;
    }

    public SecureHexagon(java.awt.geom.Point2D.Double start, int radius, Coordinate position) {
        super(start, radius, position);
        color = SECURE_COLOR;
    }

    @Override
    public void draw(Graphics2D g) {
        super.draw(g);

        SectorLabel symbol = new SectorLabel(radius, points[0], position);
        symbol.draw(g);
    }

    @Override
    public void movedAway() {
        if(color == Color.GREEN) {
            color = SECURE_COLOR;
        } else {
            color = beforeNoise;
        }
    }

}
