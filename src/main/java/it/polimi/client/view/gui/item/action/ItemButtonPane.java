/**
 *
 */
package it.polimi.client.view.gui.item.action;

import it.polimi.client.view.gui.custom.PlainButton;
import it.polimi.client.view.gui.item.state.ItemState;
import it.polimi.client.view.gui.item.state.ItemStateFactory;
import it.polimi.client.view.gui.listener.GameActionListener;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class ItemButtonPane extends JPanel implements ActionListener {
    private static final long serialVersionUID = 7659380898159317523L;

    private static final Dimension NORMAL_SIZE = new Dimension(450,190);
    private ItemState state;

    private final PlainButton drawSectorButton;
    private final PlainButton attackButton;
    private final PlainButton useCardButton;
    private final PlainButton discardCardButton;
    private final PlainButton endTurnButton;

    public ItemButtonPane() {
        super();
        super.setLayout(new BorderLayout());

        drawSectorButton = new PlainButton("Draw Sector card");
        attackButton = new PlainButton("Attack");
        useCardButton = new PlainButton("Use card");
        discardCardButton = new PlainButton("Discard card");
        endTurnButton = new PlainButton("End your Turn");

        drawSectorButton.addActionListener(this);
        attackButton.addActionListener(this);
        useCardButton.addActionListener(this);
        discardCardButton.addActionListener(this);
        endTurnButton.addActionListener(this);

        JPanel northPane = new JPanel();
        northPane.add(drawSectorButton, BorderLayout.CENTER);
        northPane.add(attackButton, BorderLayout.WEST);

        JPanel southPane = new JPanel();
        southPane.add(useCardButton, BorderLayout.NORTH);
        southPane.add(discardCardButton, BorderLayout.CENTER);
        southPane.add(endTurnButton, BorderLayout.SOUTH);

        add(northPane, BorderLayout.NORTH);
        add(southPane, BorderLayout.CENTER);


        drawSectorButton.setEnabled(false);
        attackButton.setEnabled(false);
        useCardButton.setEnabled(false);
        discardCardButton.setEnabled(false);
        endTurnButton.setEnabled(false);

        state = ItemStateFactory.factory("alien", null, attackButton, useCardButton, discardCardButton, endTurnButton, drawSectorButton);
        setPreferredSize(NORMAL_SIZE);
        setTitledBorder();
    }

    private void setTitledBorder() {
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        TitledBorder title = BorderFactory.createTitledBorder(border, "Commands");
        title.setTitleJustification(TitledBorder.CENTER);
        setBorder(title);
    }

    public void setBehavoir(String behavior, GameActionListener listener) {
        state = ItemStateFactory.factory(behavior, listener, attackButton, useCardButton, discardCardButton, endTurnButton, drawSectorButton);
    }

    public void disableAll() {
        state = state.disableAll();
    }

    public void enableDrawSector() {
        state = state.enableDrawSector();
    }

    public void enableAttack() {
        state = state.enableAttack();
    }

    public void enableUseCard() {
        state = state.enableUseCard();
    }

    public void enableDiscardCard() {
        state = state.enableDiscardCard();
    }

    public void enableEndTurn() {
        state = state.enableEndTurn();
    }

    public void enableDrawSectorCard() {
        state = state.enableDrawSector();
    }

    @Override
    public void actionPerformed(ActionEvent event) {

        if(event.getSource() == attackButton) {
            state.attack();
        } else if(event.getSource() == useCardButton) {
            state.useCard();
        } else if(event.getSource() == discardCardButton) {
            state.discardCard();
        } else if(event.getSource() == endTurnButton) {
            state.endTurn();
        } else if(event.getSource() == drawSectorButton) {
            state.drawSector();
        }
    }
}
