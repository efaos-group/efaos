/**
 *
 */
package it.polimi.client.view.gui.map;

import it.polimi.server.model.sector.Coordinate;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class HumanHexagon extends HexagonShape {
    private static final long serialVersionUID = 2631368844224074515L;

    public HumanHexagon(int radius, Coordinate position) {
        super(radius, position);
        color = Color.BLACK;
    }

    public HumanHexagon(double x, double y, int radius, Coordinate position) {
        super(x, y, radius, position);
        color = Color.BLACK;
    }

    public HumanHexagon(java.awt.geom.Point2D.Double start, int radius, Coordinate position) {
        super(start, radius, position);
        color = Color.BLACK;
    }

    @Override
    public void draw(Graphics2D g) {
        super.draw(g);

        HumanSymbol symbol = new HumanSymbol(radius, points[0]);
        symbol.draw(g);
    }

}
