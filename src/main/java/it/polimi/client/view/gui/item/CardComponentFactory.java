/**
 *
 */
package it.polimi.client.view.gui.item;

import it.polimi.client.view.exception.ResourceNotFoundException;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class CardComponentFactory {

    private CardComponentFactory() {
    }

    public static CardComponent factory(String input){
        String card = input.split("\\s")[0].toUpperCase();

        switch(card){
            case "DEFAULT":
                return new DefaultComponent(input,"/image/item/default.png");
            case "ATTACK":
                return new AttackComponent(input,"/image/item/attack.png");
            case "ADRENALINE":
                return new CardComponent(input,"/image/item/adrenaline.png");
            case "DEFENSE":
                return new DefenseComponent(input,"/image/item/defense.png");
            case "SEDATIVES":
                return new CardComponent(input,"/image/item/sedatives.png");
            case "SPOTLIGHT":
                return new SpotlightComponent(input,"/image/item/spotlight.png");
            case "TELEPORT":
                return new CardComponent(input,"/image/item/teleport.png");
            default:
                throw new ResourceNotFoundException("This card doesn't exists! Can't factory its behaviour");
        }
    }
}
