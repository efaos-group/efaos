/**
 *
 */
package it.polimi.client.view.gui.bar;

import it.polimi.client.view.gui.listener.ColorListener;

import java.awt.Color;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ButtonGroup;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class WindowBar extends JMenuBar implements ItemListener {
    private static final long serialVersionUID = -3261514040342579705L;

    private transient ColorListener actionToCall;

    private final JRadioButtonMenuItem normalColorMn;
    private final JRadioButtonMenuItem greenColorMn;
    private final JRadioButtonMenuItem blueColorMn;
    private final JRadioButtonMenuItem redColorMn;

    private final transient Map<JRadioButtonMenuItem, Color> colorMap;


    public WindowBar(ColorListener listenerToCall){
        super();
        this.actionToCall = listenerToCall;

        colorMap = new HashMap<JRadioButtonMenuItem, Color>();

        normalColorMn = new JRadioButtonMenuItem("Normal");
        greenColorMn = new JRadioButtonMenuItem("Green");
        blueColorMn = new JRadioButtonMenuItem("Blue");
        redColorMn = new JRadioButtonMenuItem("Red");

        buildMap();
    }

    private void buildMap(){

        JMenu mFile = new JMenu("Map");

        JMenu colorMenu = new JMenu("Change Map's Color :");

        buildColorGroup();
        populateColorMap();

        colorMenu.add(normalColorMn);
        colorMenu.add(greenColorMn);
        colorMenu.add(blueColorMn);
        colorMenu.add(redColorMn);


        mFile.add(colorMenu);
        mFile.addSeparator();

        add(mFile);

    }

    private void buildColorGroup(){
        ButtonGroup group = new ButtonGroup();

        normalColorMn.setSelected(true);
        normalColorMn.setMnemonic(KeyEvent.VK_N);
        normalColorMn.addItemListener(this);
        group.add(normalColorMn);

        greenColorMn.setSelected(false);
        greenColorMn.setMnemonic(KeyEvent.VK_G);
        greenColorMn.addItemListener(this);
        group.add(greenColorMn);

        blueColorMn.setSelected(false);
        blueColorMn.setMnemonic(KeyEvent.VK_B);
        blueColorMn.addItemListener(this);
        group.add(blueColorMn);

        redColorMn.setSelected(false);
        redColorMn.setMnemonic(KeyEvent.VK_R);
        redColorMn.addItemListener(this);
        group.add(redColorMn);

    }

    private void populateColorMap() {
        colorMap.put(normalColorMn, UIManager.getColor("Panel.background"));
        colorMap.put(greenColorMn, Color.GREEN);
        colorMap.put(blueColorMn, Color.BLUE);
        colorMap.put(redColorMn, Color.RED);
    }

    @Override
    public void itemStateChanged(final ItemEvent evt) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                actionToCall.colorChanged(colorMap.get(evt.getSource()));
            }
        });

    }

}
