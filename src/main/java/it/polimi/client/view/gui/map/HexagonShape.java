/**
 *
 */
package it.polimi.client.view.gui.map;

import it.polimi.client.view.exception.BadPositionException;
import it.polimi.client.view.gui.listener.MouseClickListener;
import it.polimi.server.model.sector.Coordinate;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public abstract class HexagonShape extends Path2D.Double implements MouseListener, ActionListener {
    private static final long serialVersionUID = -4534631445540654072L;

    public static final int SIDES = 6;
    protected static final int ANGLE = 60;
    protected static final float BORDER = 1.0f;

    private final Point2D.Double start;
    protected final int radius;
    protected final Point2D.Double[] points;
    private final Point2D.Double[] backupPoints;
    protected Color color;
    protected Color beforeNoise;

    protected final Coordinate position;
    protected MouseClickListener listener;

    public HexagonShape(int radius, Coordinate position){
        this(new Point2D.Double(0, 0), radius, position);
    }

    public HexagonShape(double x, double y, int radius, Coordinate position){
        this(new Point2D.Double(x, y), radius, position);
    }

    public HexagonShape(Point2D.Double start, int radius, Coordinate position){
        super();

        this.start = start;
        this.radius = radius;
        this.points = new Point2D.Double[HexagonShape.SIDES];
        this.backupPoints = new Point2D.Double[HexagonShape.SIDES];

        this.position = position;
        this.color = Color.BLACK;

        calculateVertex();
    }

    public Point2D.Double getStart() {
        return start;
    }

    public Character getCol() {
        return position.getCol();
    }

    public Integer getRow() {
        return position.getRow();
    }

    public int getRadius() {
        return radius;
    }

    protected void calculateVertex(){
        this.reset();

        this.moveTo(start.x, start.y);
        points[0] = start;

        for (int i=1; i<SIDES; i++){

            points[i] = new Point2D.Double(
                    points[i-1].x + radius * Math.cos(Math.toRadians( (double)(i-1) * ANGLE )),
                    points[i-1].y + radius * Math.sin(Math.toRadians( (double)(i-1) * ANGLE ))
                    );
            this.lineTo(points[i].x, points[i].y);
        }
        this.closePath();
        savePointToBackup();
    }

    private void savePointToBackup() {
        for (int i=0; i<SIDES; i++){
            backupPoints[i] = new Point2D.Double(points[i].x, points[i].y);
        }
    }

    public void restorePointsFromBackup() {
        for (int i=0; i<SIDES; i++){
            points[i] = new Point2D.Double(backupPoints[i].x, backupPoints[i].y);
        }
    }

    private void updateShape(){
        this.reset();
        this.moveTo(points[0].x, points[0].y);

        for (int i=1; i<SIDES; i++){
            this.lineTo(points[i].x, points[i].y);
        }
        this.closePath();
    }

    public void horizontalShift(double shift){
        for(Point2D.Double pt : points) {
            pt.x += shift;
        }
        updateShape();
    }

    public void verticalShift(double shift){
        for(Point2D.Double pt : points) {
            pt.y += shift;
        }
        updateShape();
    }

    public void draw(Graphics2D g) {
        Color oldColor = g.getColor();
        Stroke oldStroke = g.getStroke();

        g.setColor(color);
        g.setStroke(new BasicStroke(BORDER));
        g.draw(this);
        g.fill(this);

        resetGraphicsPreferences(g, oldColor, oldStroke);
    }

    protected void resetGraphicsPreferences(Graphics2D g, Color oldColor, Stroke oldStroke) {
        g.setColor(oldColor);
        g.setStroke(oldStroke);
    }

    public void setClickListener(MouseClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void mouseClicked(MouseEvent evt) {
        if(this.contains(evt.getPoint())) {
            listener.mouseClicked(position);
        }
    }

    @Override
    public void mouseEntered(MouseEvent evt) {
        //Do nothing because the hexagon as default is unclickable
    }

    @Override
    public void mouseExited(MouseEvent evt) {
        //Do nothing because the hexagon as default is unclickable
    }

    @Override
    public void mousePressed(MouseEvent evt) {
        //Do nothing because the hexagon as default is unclickable
    }

    @Override
    public void mouseReleased(MouseEvent evt) {
        //Do nothing because the hexagon as default is unclickable
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        movedAway();
    }

    public void movedOver() {
        color = Color.GREEN;
    }

    public void movedAway() {
        color = beforeNoise;
    }

    public void makeNoise() {
        beforeNoise = color;
        color = Color.BLUE;
    }

    public void block() {
        throw new BadPositionException("This sector can't be blocked");
    }

}