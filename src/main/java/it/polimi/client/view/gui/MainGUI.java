/**
 *
 */
package it.polimi.client.view.gui;

import it.polimi.client.remotecontroller.RemoteControllerInterface;
import it.polimi.client.view.gui.dialog.MapChooseDialog;
import it.polimi.client.view.gui.dialog.MessageDialog;
import it.polimi.client.view.gui.dialog.PictureDialog;
import it.polimi.client.view.gui.map.MapPane;
import it.polimi.client.view.gui.map.MapStatusPane;
import it.polimi.common.map.MapType;
import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.Observable;
import it.polimi.common.observer.View;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.swing.SwingUtilities;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class MainGUI implements View, ActionListener {

    private static final String COLOR_ERROR_MESSAGE = "red";
    private static final String COLOR_END_GAME_MESSAGE = "green";
    private static final String COLOR_CARD_MESSAGE = "blue";

    private final RemoteControllerInterface controller;
    private String username;
    private CommandExecuter executor;

    private MapChooseDialog chooseDialog;
    private final Map<MapType, MapPane> mapPaneList;

    private FrameGUI windowFrame;
    private MapStatusPane mapStatusPane;
    private static final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

    public MainGUI(RemoteControllerInterface controller) {
        this.controller = controller;

        showDialog();

        mapPaneList = new HashMap<MapType, MapPane>();

        executor = new CommandExecuter(this, controller);
        initAllMaps();
    }

    private void showDialog() {
        chooseDialog = new MapChooseDialog(this);
        chooseDialog.setResizable(false);

        setLocationToCenter(chooseDialog);
    }

    private void initAllMaps() {
        for(MapType map: MapType.values()) {
            mapPaneList.put(map, new MapPane(map, executor));
        }
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        final String[] choice = (String[]) event.getSource();
        username = choice[1];
        controller.sendPlayerChoice(choice[1], choice[0]);

        startGUI(choice[0]);
    }

    private void startGUI(final String mapString) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MapType map = MapType.parseInput(mapString);
                mapStatusPane = new MapStatusPane(mapPaneList.get(map));
                windowFrame = new FrameGUI(mapStatusPane, executor);
            }
        });
    }

    @Override
    public void choiceAccepted(String msg) {
        chooseDialog.dispose();
        windowFrame.pack();
        setLocationToCenter(windowFrame);
        windowFrame.setVisible(true);
        mapStatusPane.disableClick();
    }

    @Override
    public void askForPlayerID() {
        chooseDialog.blockMap();
        chooseDialog.changeUsername();
        chooseDialog.pack();
    }

    @Override
    public void askForMap() {
        chooseDialog.blockUsername();
    }

    @Override
    public void sendPlayerChoice() {
        //Do nothing, this function is useful only in the cli version
    }

    @Override
    public void showStartTurnDialog(final String msg, final String playerID) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                windowFrame.setSelectedPlayer(playerID);
                MessageDialog msgDialog = new MessageDialog(msg);
                setLocationToCenter(msgDialog);
                windowFrame.publishMessage(msg);
            }
        });
    }

    @Override
    public void showEventDialog(final String msg) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                windowFrame.publishMessage(msg);
            }
        });
    }

    @Override
    public void showCardDialog(final String msg) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MessageDialog msgDialog = new MessageDialog(msg, COLOR_CARD_MESSAGE);
                setLocationToCenter(msgDialog);
                windowFrame.publishMessage(msg);
            }
        });
    }


    @Override
    public void showErrorDialog(final String msg) {
        if(windowFrame != null) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    MessageDialog msgDialog = new MessageDialog(msg, COLOR_ERROR_MESSAGE);
                    setLocationToCenter(msgDialog);
                    windowFrame.publishMessage(msg);
                }
            });
        }
    }

    @Override
    public void showNoiseInSectorDialog(String msg) {
        mapStatusPane.makeNoise(msg);
        windowFrame.publishMessage(msg);
        repaintAll();
    }

    @Override
    public void showEndGameDialog(final String msg) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MessageDialog msgDialog = new MessageDialog(msg, COLOR_END_GAME_MESSAGE);
                setLocationToCenter(msgDialog);
                windowFrame.publishMessage(msg);
            }
        });
    }

    @Override
    public void showPictureDialog(final String msg, final String card) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                PictureDialog picDialog = new PictureDialog(msg, card);
                setLocationToCenter(picDialog);
                windowFrame.publishMessage(msg);
            }
        });
    }

    @Override
    public void showStartGameEvent(final String msg, final String character, final Collection<String> players) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                windowFrame.setPlayers(players);
                windowFrame.setCharacter(username,character);
                windowFrame.publishMessage(msg);
            }
        });
    }

    @Override
    public void disableAll() {
        windowFrame.disableAll();
    }

    @Override
    public void enableMovement() {
        mapStatusPane.enableClick();
    }

    @Override
    public void enableItemUsage() {
        windowFrame.enableItemUsage();
        windowFrame.disableAttackCard();
    }

    @Override
    public void enableAttack() {
        windowFrame.enableAttack();
    }

    @Override
    public void enableDrawSectorCard() {
        windowFrame.enableDrawSectorCard();
    }

    @Override
    public void enableDiscard() {
        windowFrame.enableDiscard();
    }

    @Override
    public void enableEndTurn() {
        windowFrame.enableEndTurn();
    }

    @Override
    public void enableAttackCard() {
        windowFrame.enableAttackCard();
    }

    @Override
    public void enableSelectSector() {
        mapStatusPane.enableNoise();
    }

    @Override
    public void enableDrawItemCard() {
        controller.drawItemCard(username);
    }

    @Override
    public void askForCommand() {
        //Do nothing, this function is useful only in the cli version
    }

    @Override
    public void setCharacterBehaviour(final String character) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                windowFrame.setBehavior(character);
            }
        });
    }

    @Override
    public void setPosition(final String position) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                mapStatusPane.setPosition(position);
            }
        });
    }

    @Override
    public void addNewItemCard(final String card) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                windowFrame.addNewItemCard(card);
                repaintAll();
            }
        });
    }

    @Override
    public void removeUsedItemCard(String card) {
        windowFrame.removeUsedItemCard(card);
    }

    @Override
    public String getPlayerID() {
        return username;
    }

    @Override
    public void notify(Observable source, ModelEvent gameEvent) {
        gameEvent.reactToView(this);
    }

    @Override
    public void blockEscapeSector(String position) {
        mapStatusPane.blockSector(position);
    }

    protected void repaintAll() {
        windowFrame.invalidate();
        windowFrame.validate();
        windowFrame.repaint();
    }

    private void setLocationToCenter(Component component){
        component.setLocation(screen.width/2-component.getSize().width/2, screen.height/2-component.getSize().height/2);
    }
}
