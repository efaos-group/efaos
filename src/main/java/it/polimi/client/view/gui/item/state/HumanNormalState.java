/**
 *
 */
package it.polimi.client.view.gui.item.state;

import it.polimi.client.view.gui.listener.GameActionListener;

import javax.swing.JButton;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class HumanNormalState extends HumanState {
    private static final long serialVersionUID = -41711502379747853L;

    public HumanNormalState(GameActionListener listener, JButton attack, JButton use, JButton discard, JButton end, JButton draw) {
        super(listener, attack, use, discard, end, draw);

        attack.setEnabled(false);
        use.setEnabled(false);
        discard.setEnabled(false);
        end.setEnabled(false);
        draw.setEnabled(false);
    }

    @Override
    public ItemState disableAll() {
        return this;
    }

}
