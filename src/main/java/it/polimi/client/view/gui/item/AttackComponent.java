/**
 *
 */
package it.polimi.client.view.gui.item;


/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class AttackComponent extends CardComponent {
    private static final long serialVersionUID = 926206651198286608L;

    public AttackComponent(String name, String path) {
        super(name, path);
    }

    @Override
    protected void setClickable(boolean enabled) {
        //Do nothing here, only in its father
    }

    @Override
    public void setAttackClickable(boolean clickable){
        this.clickable = clickable;
    }

    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof AttackComponent)) {
            return false;
        }
        if (obj == this) {
            return true;
        }

        AttackComponent c = (AttackComponent) obj;
        return c.getPath().equals(super.getPath()) && c.toString().equals(super.toString());
    }

    @Override
    public int hashCode() {
        return super.getPath().hashCode() + super.toString().hashCode();
    }
}
