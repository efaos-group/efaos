/**
 *
 */
package it.polimi.client.view.gui.informationboard;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Collection;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class PlayerListPane extends JPanel {
    private static final long serialVersionUID = -7660439571596603882L;

    private static final Integer HEIGHT = 150;
    private static final Integer WIDTH = 450;

    private final JScrollPane pane;
    private final JList<String> playerList;
    private final DefaultListModel<String> playerModel;

    public PlayerListPane() {
        super();
        setLayout(new BorderLayout());

        playerModel = new DefaultListModel<String>();
        playerList = new JList<String>(playerModel);

        playerList.setBackground(Color.LIGHT_GRAY);
        pane = new JScrollPane(playerList);
        pane.setPreferredSize(new Dimension(WIDTH, HEIGHT));

        JLabel title = new JLabel("Players");

        JPanel titleContainer = new JPanel();
        titleContainer.setBackground(Color.LIGHT_GRAY);
        titleContainer.add(title);

        super.add(titleContainer, BorderLayout.NORTH);
        super.add(pane, BorderLayout.SOUTH);
        super.setBackground(Color.LIGHT_GRAY);
        super.setBorder(BorderFactory.createEtchedBorder());
    }

    public void setPlayer(Collection<String> players) {

        for(String player : players) {
            playerModel.addElement(player);
        }
        playerList.setModel(playerModel);
    }

    public void setSelectedPlayer(String player){
        playerList.setSelectedValue(player, true);
        playerList.setEnabled(false);
    }

}
