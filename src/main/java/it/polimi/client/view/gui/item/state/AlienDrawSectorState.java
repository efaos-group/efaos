/**
 *
 */
package it.polimi.client.view.gui.item.state;

import it.polimi.client.view.gui.listener.GameActionListener;

import javax.swing.JButton;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class AlienDrawSectorState extends AlienState {
    private static final long serialVersionUID = -3482554084108764155L;

    public AlienDrawSectorState(GameActionListener listener, JButton attack, JButton use, JButton discard, JButton end, JButton draw) {
        super(listener, attack, use, discard, end, draw);

        use.setEnabled(false);
        draw.setEnabled(true);
    }

    @Override
    public ItemState enableDrawSector() {
        return this;
    }

}
