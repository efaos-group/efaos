/**
 *
 */
package it.polimi.client.view.gui.informationboard;

import it.polimi.client.view.gui.dialog.PictureDialog;
import it.polimi.client.view.gui.dialog.picture.Picture;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class PlayerPanel extends JPanel {
    private static final long serialVersionUID = -855286100953585896L;

    private static final Dimension SIZE = new Dimension(63,96);
    private static final Dimension PANEL_SIZE = new Dimension(72, 99);
    private String character;
    private String name;
    private transient MouseListener listener;

    public PlayerPanel() {
        setLayout(new FlowLayout());
        super.setSize(PANEL_SIZE);
        setTitledBorder();
    }

    private void setTitledBorder() {
        TitledBorder border = BorderFactory.createTitledBorder(BorderFactory.createLoweredSoftBevelBorder());
        border.setTitle("Character");
        border.setTitleJustification(TitledBorder.CENTER);
        super.setBorder(border);
    }

    public void setCharacter(String playerID, String character){
        this.character = character;
        this.name = playerID;
        listener = new ImageListener();

        JComponent pic = new Picture(CharacterImageType.parseInput(character), SIZE);
        pic.addMouseListener(listener);

        JLabel nameLabel = new JLabel("You are: " + playerID + " [" + character + "]");

        add(pic);
        add(nameLabel);
        revalidate();
    }

    private class ImageListener extends MouseAdapter{

        @Override
        public void mouseClicked(MouseEvent e) {
            String message = name + ": " + character;
            new PictureDialog(message, character).pack();
        }
    }

}
