/**
 *
 */
package it.polimi.client.view.gui.item.state;

import it.polimi.client.view.gui.listener.GameActionListener;

import javax.swing.JButton;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class AlienAttackState extends AlienState {
    private static final long serialVersionUID = 2925113153066940801L;

    public AlienAttackState(GameActionListener listener, JButton attack, JButton use, JButton discard, JButton end, JButton draw) {
        super(listener, attack, use, discard, end, draw);

        attack.setEnabled(true);
        use.setEnabled(false);
    }

    @Override
    public ItemState enableAttack() {
        return this;
    }

}
