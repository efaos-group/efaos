/**
 * 
 */
package it.polimi.client.view.gui.dialog.map;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public enum MapImageType {
    GALILEI("/image/map/galilei.jpg"),
    GALVANI("/image/map/galvani.jpg"),
    FERMI("/image/map/fermi.jpg");
    
    private final String path;
    
    private MapImageType(String path) {
        this.path = path;
    }
    
    public String getPath(){
        return path;
    }
}
