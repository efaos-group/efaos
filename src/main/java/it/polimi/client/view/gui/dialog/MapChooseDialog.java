/**
 *
 */
package it.polimi.client.view.gui.dialog;

import it.polimi.client.view.gui.custom.JLetterField;
import it.polimi.client.view.gui.dialog.map.MapImageType;
import it.polimi.client.view.gui.dialog.map.MapPanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class MapChooseDialog extends JDialog implements ActionListener {
    private static final long serialVersionUID = -8880626267694946869L;

    private final transient ActionListener chooseListener;

    private final JPanel mapChoosingPane;
    private final JComboBox<String> mapComboBox;

    private final JPanel usernamePane;
    private final JLabel usernameLabel;
    private final JLetterField usernameField;

    private final JButton buttonChoosingMap;

    public MapChooseDialog(ActionListener chooseListener) {
        super();
        super.setTitle("Game Preferences");
        super.setAlwaysOnTop(true);

        this.chooseListener = chooseListener;

        mapChoosingPane = new JPanel(new BorderLayout());
        mapComboBox = new JComboBox<String>();

        usernamePane = new JPanel();
        usernameLabel = new JLabel("Username:");
        usernameField = new JLetterField(10);

        JPanel buttonPane = new JPanel();
        buttonChoosingMap = new JButton("GO!");

        initMapChoosePane();
        initUsernamePane();
        buttonPane.add(buttonChoosingMap);

        super.add(mapChoosingPane, BorderLayout.NORTH);
        super.add(usernamePane, BorderLayout.CENTER);
        super.add(buttonPane, BorderLayout.SOUTH);
        setVisible(true);
        pack();
    }

    private void initMapChoosePane() {

        mapChoosingPane.add(new MapPanel(),BorderLayout.NORTH);

        JPanel comboContainer = new JPanel(new FlowLayout());
        JLabel label = new JLabel("Map:");

        populateMapComboBox();

        comboContainer.add(label);
        comboContainer.add(mapComboBox);
        mapChoosingPane.add(comboContainer, BorderLayout.SOUTH);

        buttonChoosingMap.addActionListener(this);
    }

    private void populateMapComboBox() {
        for (MapImageType type : MapImageType.values()){
            mapComboBox.addItem(type.toString());
        }
    }

    private void initUsernamePane() {
        usernamePane.add(usernameLabel);
        usernamePane.add(usernameField);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        if (!usernameField.getText().isEmpty()) {
            String[] source = new String[] { (String)mapComboBox.getSelectedItem(), usernameField.getText()};
            evt.setSource(source);
            chooseListener.actionPerformed(evt);
        }
    }

    public void blockMap() {
        mapComboBox.setEnabled(false);
    }

    public void setUSername(String username) {
        usernameField.setText(username);
    }

    public void blockUsername() {
        usernameField.setEnabled(false);
    }

    public void changeUsername() {
        usernameField.setBackground(Color.RED);
        usernameLabel.setText("Username already taken:");
    }

}
