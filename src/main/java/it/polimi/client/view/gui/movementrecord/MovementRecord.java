/**
 *
 */
package it.polimi.client.view.gui.movementrecord;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class MovementRecord extends JPanel {
    private static final long serialVersionUID = -2556807246719708778L;

    private static final int COLS = 13;
    private static final int ROWS = 3;
    private static final int INITIAL_TURN = 0;
    private static final int TURNS = 39;
    private static final String VOID_SECTOR = " [  ,  ]";

    private Integer turn = -1;
    private transient List<JLabel> register;

    public MovementRecord() {
        setLayout(new GridLayout(ROWS,COLS,20,10));
        initRegisterList();
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
    }

    private void initRegisterList(){
        register = new ArrayList<JLabel>();
        for (int i = 1; i <= TURNS; i++){
            JLabel label = new JLabel( buildNextMsg(i) + VOID_SECTOR);
            super.add(label);
            register.add(label);
        }
    }

    private String buildNextMsg(int i) {
        return (i < 10)? "0" + i : "" + i;
    }

    public void recordMovement(String position){
        turn += 1;
        if (turn > INITIAL_TURN){
            String []movement = position.split("\\s");
            addMovement(movement[0]);
            revalidate();
        }
    }

    private void addMovement(String pos){
        JLabel label = register.get(turn - 1);
        updateLabel(pos, label);
    }

    private void updateLabel(String pos, JLabel label) {
        String []text = label.getText().split("\\s");
        label.setText(text[0] + " " + pos);
    }
}
