/**
 *
 */
package it.polimi.client.view.gui.map;

import it.polimi.client.view.gui.listener.ColorListener;
import it.polimi.client.view.gui.movementrecord.MovementRecord;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class MapStatusPane extends JPanel implements ColorListener {

    private static final long serialVersionUID = 3597217066207513331L;

    private final MapPane mapPane;
    private final MovementRecord movementRecord;

    public MapStatusPane(MapPane mapPane) {
        this.mapPane = mapPane;
        this.movementRecord = new MovementRecord();

        super.setLayout(new BorderLayout());
        super.add(movementRecord, BorderLayout.NORTH);
        super.add(mapPane,BorderLayout.SOUTH);
    }

    public void disableClick() {
        mapPane.disableClick();
    }

    public void enableClick() {
        mapPane.enableClick();
    }

    public void enableNoise() {
        mapPane.enableNoise();
    }

    public void makeNoise(String msg){
        mapPane.makeNoise(msg);
    }

    public void blockSector(String position){
        mapPane.blockSector(position);
    }

    public void setPosition(String position) {
        mapPane.setPosition(position);
        movementRecord.recordMovement(position);
    }

    @Override
    public void colorChanged(Color newColor) {
        mapPane.colorChanged(newColor);
    }
}
