/**
 *
 */
package it.polimi.client.view.gui.item;

import it.polimi.client.view.exception.ResourceNotFoundException;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class CardComponent extends JComponent {
    private static final long serialVersionUID = 4588455412420750724L;

    private static final Dimension NORMAL_SIZE = new Dimension(98,150);
    private static final Dimension SELECTED_SIZE = new Dimension(118,180);

    private transient BufferedImage image;
    private boolean selected;
    protected boolean clickable;
    private final String name;
    private final String path;

    public CardComponent(String name, String path) {
        this.clickable = false;
        this.name = name;
        this.path = path;
        this.selected = false;

        setPreferredSize(NORMAL_SIZE);
        loadImage();
        setClickable(true);
    }

    private void loadImage() {
        try {
            URL url = this.getClass().getResource(path);
            image = ImageIO.read(url);
        } catch (IOException e){
            throw new ResourceNotFoundException("Couldn't load the card image! " + e);
        }
    }

    @Override
    public void paint(Graphics g) {
        Dimension size = getSize();
        g.drawImage(image, 0, 0,size.width, size.height, 0, 0, image.getWidth(), image.getHeight(), null);
    }

    public void setSelected(boolean selected){
        this.selected = selected;
        if (selected) {
            setPreferredSize(SELECTED_SIZE);
        } else {
            setPreferredSize(NORMAL_SIZE);
        }
        update();
    }

    protected boolean isSelected(){
        return selected;
    }

    protected void setClickable(boolean enabled){
        this.clickable = enabled;
    }

    protected void setAttackClickable(boolean enabled){
        //Do Nothing here, only in attack component
    }

    protected void setDefenseCardClickable(boolean enabled){
        //Do Nothing here, only in defense component
    }

    protected boolean isClickable(){
        return clickable;
    }

    public boolean needSectorChoice(){
        return false;
    }

    public boolean isDefault(){
        return false;
    }

    public String getPath() {
        return this.path;
    }

    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof CardComponent)) {
            return false;
        }
        if (obj == this) {
            return true;
        }

        CardComponent c = (CardComponent) obj;
        return c.getPath().equals(this.path) && c.toString().equals(this.name);
    }

    @Override
    public int hashCode() {
        return path.hashCode()+name.hashCode();
    }

    private void update() {
        revalidate();
        repaint();
    }

    @Override
    public String toString(){
        return name;
    }
}
