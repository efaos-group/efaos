/**
 *
 */
package it.polimi.client.view.gui.map.state;

import it.polimi.client.view.gui.listener.MouseClickListener;
import it.polimi.server.model.sector.Coordinate;

import java.io.Serializable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public abstract class ClickState implements Serializable {
    private static final long serialVersionUID = 4624957311754097360L;

    protected final MouseClickListener listener;

    public ClickState(MouseClickListener listener) {
        this.listener = listener;
    }

    public abstract void notifyClick(Coordinate clickPosition);

    public ClickState disableClick() {
        return new UnclickableState(listener);
    }

    public ClickState enableClick() {
        return new ClickableState(listener);
    }

    public ClickState enableNoise() {
        return new NoiseState(listener);
    }

}
