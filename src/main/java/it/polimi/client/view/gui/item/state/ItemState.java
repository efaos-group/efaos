/**
 *
 */
package it.polimi.client.view.gui.item.state;

import it.polimi.client.view.gui.listener.GameActionListener;

import java.io.Serializable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public abstract class ItemState implements Serializable {
    private static final long serialVersionUID = 1360836123667094706L;

    protected final GameActionListener listener;

    public ItemState(GameActionListener listener) {
        this.listener = listener;
    }

    public void endTurn() {
        listener.passTurn();
    }

    public void useCard() {
        listener.useCard(null);
    }

    public void discardCard() {
        listener.discardCard(null);
    }

    public void attack() {
        listener.attack();
    }

    public void drawSector() {
        listener.drawSector();
    }

    public abstract ItemState disableAll();

    public abstract ItemState enableDrawSector();

    public abstract ItemState enableAttack();

    public abstract ItemState enableUseCard();

    public abstract ItemState enableDiscardCard();

    public abstract ItemState enableEndTurn();
}
