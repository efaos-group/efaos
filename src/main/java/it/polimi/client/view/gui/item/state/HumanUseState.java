/**
 *
 */
package it.polimi.client.view.gui.item.state;

import it.polimi.client.view.gui.listener.GameActionListener;

import javax.swing.JButton;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class HumanUseState extends HumanState {
    private static final long serialVersionUID = -3362655701646629144L;

    public HumanUseState(GameActionListener listener, JButton attack, JButton use, JButton discard, JButton end, JButton draw) {
        super(listener, attack, use, discard, end, draw);

        attack.setEnabled(false);
        use.setEnabled(true);
    }

    @Override
    public ItemState enableUseCard() {
        return this;
    }

}
