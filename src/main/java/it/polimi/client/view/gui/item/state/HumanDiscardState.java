/**
 *
 */
package it.polimi.client.view.gui.item.state;

import it.polimi.client.view.gui.listener.GameActionListener;

import javax.swing.JButton;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class HumanDiscardState extends HumanState {
    private static final long serialVersionUID = -4204284976659825028L;

    public HumanDiscardState(GameActionListener listener, JButton attack, JButton use, JButton discard, JButton end, JButton draw) {
        super(listener, attack, use, discard, end, draw);

        attack.setEnabled(false);
        discard.setEnabled(true);
    }

    @Override
    public ItemState enableDiscardCard() {
        return this;
    }

}
