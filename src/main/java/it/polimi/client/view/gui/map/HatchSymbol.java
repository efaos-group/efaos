/**
 *
 */
package it.polimi.client.view.gui.map;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class HatchSymbol extends Symbol {

    private final Integer number;

    public HatchSymbol(Integer radius, Point2D.Double leftCorner,  Integer number) {
        super(radius, leftCorner);
        this.number = number;
    }

    @Override
    protected void changeGraphicsPreferences(Graphics2D g) {
        super.changeGraphicsPreferences(g);
        Font font = new Font("Serif", Font.BOLD, 28);
        g.setFont(font);
    }

    @Override
    protected Line2D.Double[] getSymbolLines() {
        Line2D.Double[] lines = new Line2D.Double[4];

        Point2D.Double point = new Point2D.Double(leftCorner.x, leftCorner.y);

        lines[0] = new Line2D.Double(point.x - oneSixthRadius, point.y + 3 * oneQuartersHeight,
                                                    point.x + oneEighthRadius, point.y + oneQuartersHeight);
        lines[1] = new Line2D.Double(point.x + oneEighthRadius, point.y + oneQuartersHeight,
                                                    point.x + halfRadius, point.y + oneQuartersHeight);

        point.x += radius;
        point.y += height;
        lines[2] = new Line2D.Double(point.x + oneSixthRadius, point.y + oneQuartersHeight,
                                                        point.x - oneEighthRadius, point.y + 3 * oneQuartersHeight);

        point.y += 3 * oneQuartersHeight;
        lines[3] = new Line2D.Double(point.x - oneEighthRadius, point.y, point.x - halfRadius, point.y);

        return lines;
    }

    @Override
    protected void drawNumber(Graphics2D g) {

        float xString = (float) (leftCorner.x + 2 * oneEighthRadius);
        float yString = (float) (leftCorner.y + height + 3 * oneEighthHeight);
        g.drawString("" + number, xString, yString);
    }


}
