/**
 *
 */
package it.polimi.client.view.gui.item.state;

import it.polimi.client.view.gui.listener.GameActionListener;

import javax.swing.JButton;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class HumanState extends ItemState {
    private static final long serialVersionUID = -1965692349274104102L;

    protected final JButton attack, use, discard, end, draw;

    public HumanState(GameActionListener listener, JButton attack, JButton use, JButton discard, JButton end, JButton draw) {
        super(listener);

        this.attack = attack;
        this.use = use;
        this.discard = discard;
        this.end = end;
        this.draw = draw;
    }

    @Override
    public final void attack() {
        // Do nothing because human can never attack
    }

    @Override
    public ItemState disableAll() {
        return new HumanNormalState(listener, attack, use, discard, end, draw);
    }

    @Override
    public final ItemState enableAttack() {
        return this;
    }

    @Override
    public ItemState enableUseCard() {
        return new HumanUseState(listener, attack, use, discard, end, draw);
    }

    @Override
    public ItemState enableDiscardCard() {
        return new HumanDiscardState(listener, attack, use, discard, end, draw);
    }

    @Override
    public ItemState enableEndTurn() {
        return new HumanEndState(listener, attack, use, discard, end, draw);
    }

    @Override
    public ItemState enableDrawSector() {
        return new HumanDrawSectorState(listener, attack, use, discard, end, draw);
    }

}
