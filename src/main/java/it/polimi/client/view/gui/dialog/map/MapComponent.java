/**
 *
 */
package it.polimi.client.view.gui.dialog.map;

import it.polimi.client.view.exception.ResourceNotFoundException;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class MapComponent extends JComponent {
    private static final long serialVersionUID = -399510085052370365L;

    private static final Dimension NORMAL_SIZE = new Dimension(279,271);
    private transient BufferedImage image;
    private MapImageType type;

    public MapComponent(MapImageType type) {
        this.type = type;
        setImage();
        this.setPreferredSize(NORMAL_SIZE);
    }

    private void setImage() {
        URL url = this.getClass().getResource(type.getPath());
        try {
            this.image = ImageIO.read(url);
        } catch (IOException e) {
            throw new ResourceNotFoundException("Impossibile to find map image " + e);
        }
    }

    @Override
    public void paint(Graphics g) {
        Dimension size = getSize();
        g.drawImage(image, 0, 0,size.width, size.height, 0, 0, image.getWidth(), image.getHeight(), null);
    }

    @Override
    public String toString(){
        return type.toString();
    }

}
