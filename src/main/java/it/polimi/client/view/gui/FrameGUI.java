/**
 *
 */
package it.polimi.client.view.gui;

import it.polimi.client.view.gui.bar.WindowBar;
import it.polimi.client.view.gui.dialog.MessageDialog;
import it.polimi.client.view.gui.informationboard.InformationPane;
import it.polimi.client.view.gui.item.CardComponent;
import it.polimi.client.view.gui.item.ItemsPanel;
import it.polimi.client.view.gui.item.action.ItemButtonPane;
import it.polimi.client.view.gui.listener.GameActionListener;
import it.polimi.client.view.gui.map.MapStatusPane;

import java.awt.BorderLayout;
import java.util.Collection;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class FrameGUI extends JFrame implements GameActionListener {
    private static final long serialVersionUID = 9152929403246099382L;

    private JPanel eastPane;
    private ItemsPanel itemsPane;
    private ItemButtonPane itemButtonPane;
    private InformationPane infoPane;

    private final GameActionListener listener;

    public FrameGUI(MapStatusPane mapStatusPane, GameActionListener listener) {
        super();
        super.setTitle("EFAOS");
        super.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.listener = listener;

        super.setJMenuBar(new WindowBar(mapStatusPane));

        itemsPane = new ItemsPanel();
        infoPane = new InformationPane();
        itemButtonPane = new ItemButtonPane();
        buildEastPane();

        super.add(new JScrollPane(mapStatusPane), BorderLayout.CENTER);
        super.add(eastPane, BorderLayout.EAST);

        super.setExtendedState(JFrame.MAXIMIZED_BOTH);
        super.setLocationRelativeTo(null);
    }

    private void buildEastPane() {
        eastPane = new JPanel();
        eastPane.setLayout(new BorderLayout());

        eastPane.add(itemsPane, BorderLayout.NORTH);
        eastPane.add(itemButtonPane, BorderLayout.CENTER);
        eastPane.add(infoPane, BorderLayout.SOUTH);
    }

    public void addNewItemCard(String card) {
        itemsPane.addNewItemCard(card);
    }

    public void removeUsedItemCard(String card) {
        itemsPane.removeUsedItemCard(card);
    }

    public void setBehavior(String behavior) {
        itemButtonPane.setBehavoir(behavior, this);
    }

    public void disableAll() {
        itemButtonPane.disableAll();
        itemsPane.disableItemUsage();
    }

    public void setPlayers(Collection<String> players){
        infoPane.setPlayers(players);
    }

    public void setCharacter(String player,String pg){
        infoPane.setCharacter(player,pg);
    }

    public void publishMessage(String message) {
        infoPane.publishMessage(message);
    }

    public void setSelectedPlayer(String pg) {
        infoPane.setSelectedPlayer(pg);
    }

    @Override
    public void passTurn() {
        if(itemsPane.countItems() <= ItemsPanel.MAX_ITEMS) {
            listener.passTurn();
        } else {
            new MessageDialog("You can't end your turn with four cards!").pack();
        }
    }

    @Override
    public void attack() {
        listener.attack();
    }

    @Override
    public void useCard(CardComponent card) {
        CardComponent selectedCard = itemsPane.getSelectedCard();
        if(selectedCard != null) {
            listener.useCard(selectedCard);
        } else {
            createNoCardSelectedDialog();
        }
    }

    private void createNoCardSelectedDialog() {
        MessageDialog cardsDialog = new MessageDialog("No card selected!");
        cardsDialog.setVisible(true);
        cardsDialog.pack();
    }

    @Override
    public void discardCard(String card) {
        CardComponent selectedCard = itemsPane.getSelectedCard();
        if(selectedCard != null) {
            listener.discardCard(selectedCard.toString());
        } else {
            createNoCardSelectedDialog();
        }
    }

    @Override
    public void drawSector() {
        listener.drawSector();
    }

    public void enableAttack() {
        itemButtonPane.enableAttack();
    }

    public void enableAttackCard() {
        itemsPane.enableAttackCard();
        itemButtonPane.enableUseCard();
    }

    public void disableAttackCard() {
        itemsPane.disableAttackCard();
    }

    public void enableItemUsage() {
        itemButtonPane.enableUseCard();
        itemsPane.enableItemUsage();
    }

    public void enableDiscard() {
        itemsPane.enableDiscardCard();
        itemButtonPane.enableDiscardCard();
    }

    public void enableEndTurn() {
        itemButtonPane.enableEndTurn();
    }

    public void enableDrawSectorCard() {
        itemButtonPane.enableDrawSectorCard();
    }

}
