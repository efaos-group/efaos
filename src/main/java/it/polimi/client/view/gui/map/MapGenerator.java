/**
 *
 */
package it.polimi.client.view.gui.map;

import it.polimi.client.view.exception.MapNotFoundException;
import it.polimi.common.map.MapType;
import it.polimi.server.model.SectorType;
import it.polimi.server.model.sector.Coordinate;

import java.io.IOException;
import java.net.URL;
import java.util.StringTokenizer;
import java.util.logging.Logger;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class MapGenerator {

    private static final Logger LOGGER = Logger.getLogger(MapGenerator.class.getName());

    public static final Integer ROW = 14;
    public static final Integer COLUMN = 23;
    public static final Integer RADIUS = 30;

    private Integer hatchNumber;

    private final HexagonShape[][] map;

    public MapGenerator(MapType map) {
        this.map = new HexagonShape[ROW][COLUMN];
        hatchNumber = 1;

        try {
            fillMap(map.getPath());
        } catch (IOException e) {
            LOGGER.severe("Cannot create the graphic map! " + e);
        }
    }

    public HexagonShape[][] getHexagonMap() {
        return map;
    }

    /**
     * Load the Config file and transfer it into an HashMap.
     * Each line should follow the below schema:
     * COLUMN ROW SECTORTYPE
     * i.e.
     * A 1 INVISIBLE
     *
     * @param mapLocation
     * @throws IOException
     */
    protected void fillMap(String mapLocation) throws IOException {

        URL url = this.getClass().getResource(mapLocation);
        String text = Resources.toString(url, Charsets.UTF_8);

        String[] lines = text.split("\n");

        for (int i = 0; i < lines.length; i++) {
            addSectorFromLine(lines[i]);
        }
    }

    private void addSectorFromLine(String line) {
        StringTokenizer tokens = new StringTokenizer(line);

        Character col = tokens.nextToken().charAt(0);
        Integer row = Integer.parseInt(tokens.nextToken());
        SectorType type = SectorType.parseInput(tokens.nextToken());

        map[row-1][col-'A'] = factory(type, col, row);

    }

    private HexagonShape factory(SectorType type, Character col, Integer row) {
        switch(type) {
            case ALIEN:
                return new AlienHexagon(RADIUS, new Coordinate(col, row));
            case DANGEROUS:
                return new DangerousHexagon(RADIUS, new Coordinate(col, row));
            case ESCAPE:
                return new EscapeHexagon(RADIUS, new Coordinate(col, row), hatchNumber++);
            case HUMAN:
                return new HumanHexagon(RADIUS, new Coordinate(col, row));
            case INVISIBLE:
                return new InvisibleHexagon(RADIUS, new Coordinate(col, row));
            case SECURE:
                return new SecureHexagon(RADIUS, new Coordinate(col, row));
            default:
                break;
        }

        throw new MapNotFoundException("This kind of sector doesn't exists");
    }

}
