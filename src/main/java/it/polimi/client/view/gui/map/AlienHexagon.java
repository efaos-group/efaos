/**
 *
 */
package it.polimi.client.view.gui.map;

import it.polimi.server.model.sector.Coordinate;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class AlienHexagon extends HexagonShape {
    private static final long serialVersionUID = 9057445273313183418L;

    public AlienHexagon(int radius, Coordinate position) {
        super(radius, position);
        color = Color.BLACK;
    }

    public AlienHexagon(double x, double y, int radius, Coordinate position) {
        super(x, y, radius, position);
        color = Color.BLACK;
    }

    public AlienHexagon(java.awt.geom.Point2D.Double start, int radius, Coordinate position) {
        super(start, radius, position);
        color = Color.BLACK;
    }

    @Override
    public void draw(Graphics2D g) {
        super.draw(g);

        Symbol symbol = new AlienSymbol(radius, points[0]);
        symbol.draw(g);
    }

}
