/**
 *
 */
package it.polimi.client.view.gui.informationboard;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.Collection;

import javax.swing.JPanel;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class InformationPane extends JPanel {
    private static final long serialVersionUID = -4626808161642708098L;

    private MessageBoard board;
    private PlayerListPane playerPane;
    private PlayerPanel character;

    public InformationPane() {
        super();
        super.setLayout(new BorderLayout());

        character = new PlayerPanel();
        board = new MessageBoard();
        playerPane = new PlayerListPane();

        add(character, BorderLayout.NORTH);
        add(playerPane, BorderLayout.CENTER);
        add(board, BorderLayout.SOUTH);
        setBackground(Color.LIGHT_GRAY);
    }

    public void publishMessage(String message) {
        board.showMessage(message);
    }

    public void setPlayers(Collection<String> players){
        playerPane.setPlayer(players);
    }

    public void setCharacter(String player, String pg){
        character.setCharacter(player, pg);
    }

    public void setSelectedPlayer(String pg) {
        playerPane.setSelectedPlayer(pg);
    }

}
