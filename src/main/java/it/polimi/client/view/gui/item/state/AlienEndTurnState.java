/**
 *
 */
package it.polimi.client.view.gui.item.state;

import it.polimi.client.view.gui.listener.GameActionListener;

import javax.swing.JButton;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class AlienEndTurnState extends AlienState {
    private static final long serialVersionUID = -5502042549040417630L;

    public AlienEndTurnState(GameActionListener listener, JButton attack, JButton use, JButton discard, JButton end, JButton draw) {
        super(listener, attack, use, discard, end, draw);

        end.setEnabled(true);
        use.setEnabled(false);
    }

    @Override
    public ItemState enableEndTurn() {
        return this;
    }

}
