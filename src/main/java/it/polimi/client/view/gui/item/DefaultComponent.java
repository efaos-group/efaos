/**
 *
 */
package it.polimi.client.view.gui.item;


/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class DefaultComponent extends CardComponent {
    private static final long serialVersionUID = 2919755327255474504L;

    public DefaultComponent(String name, String path) {
        super(name,path);
    }

    @Override
    public void setClickable(boolean clickable){
        this.clickable = false;
    }

    @Override
    public boolean isDefault(){
        return true;
    }

    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof DefaultComponent)) {
            return false;
        }
        if (obj == this) {
            return true;
        }

        DefaultComponent c = (DefaultComponent) obj;
        return c.getPath().equals(super.getPath()) && c.toString().equals(super.toString());
    }

    @Override
    public int hashCode() {
        return super.getPath().hashCode() + super.toString().hashCode();
    }


}
