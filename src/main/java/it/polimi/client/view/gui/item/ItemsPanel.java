/**
 *
 */
package it.polimi.client.view.gui.item;

import it.polimi.server.model.exception.BadCardException;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class ItemsPanel extends JPanel {
    private static final long serialVersionUID = 6010050670786122388L;

    private static final Integer HEIGHT = 210;
    private static final Integer WIDHT = 450;
    private static final String DEFAULT = "default card";

    public static final int MAX_ITEMS = 3;

    private final transient List<CardComponent> items;

    public ItemsPanel() {
        super();

        super.setPreferredSize(new Dimension(WIDHT, HEIGHT));
        super.setLayout(new FlowLayout(FlowLayout.LEADING));
        setTitledBorder();
        this.items = new LinkedList<CardComponent>();
        addDefaultCards();
    }

    private void setTitledBorder() {
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        TitledBorder title = BorderFactory.createTitledBorder(border, "Cards");
        title.setTitleJustification(TitledBorder.CENTER);
        setBorder(title);
    }

    private void addDefaultCards(){
        for (int i = 0; i < MAX_ITEMS; i++) {
            CardComponent card = CardComponentFactory.factory(DEFAULT);
            setUpCard(card);
        }
    }

    private void setUpCard(CardComponent card) {
        items.add(card);
        card.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e) {
                CardComponent source = (CardComponent) e.getSource();
                if (source.isClickable()){
                    removeSelection();
                    source.setSelected(true);
                }
            }
        });
        super.add(card);
    }

    private void removeSelection(){
        for (CardComponent card: items){
            card.setSelected(false);
        }
    }

    public void addNewItemCard(String newCard) {

        if (countItems() < MAX_ITEMS) {
            CardComponent defaultItem = getFreeCardComponent();
            CardComponent newItem = CardComponentFactory.factory(newCard);
            swapCards(defaultItem, newItem);
        } else {
            CardComponent card = CardComponentFactory.factory(newCard);
            setUpCard(card);
        }
        super.revalidate();
    }

    private void swapCards(CardComponent oldCard, CardComponent newCard) {
        removeCard(oldCard);
        setUpCard(newCard);
    }

    private void removeCard(CardComponent card) {
        items.remove(card);
        super.remove(card);
    }

    public void removeUsedItemCard(String input) {
        CardComponent card = findComponent(input);

        if ( countItems() > MAX_ITEMS ) {
            removeCard(card);
        } else {
            CardComponent defaultCard = CardComponentFactory.factory(DEFAULT);
            swapCards(card, defaultCard);
        }
        super.revalidate();
        super.repaint();
    }

    private CardComponent findComponent(String name){
        CardComponent card = CardComponentFactory.factory(name);

        for (CardComponent c: items){
            if (c.equals(card)){
                return c;
            }
        }

        throw new BadCardException("item is not in the list of ItemsPanel");
    }

    public int countItems() {
        int counter = 0;
        for (CardComponent card : items) {
            if (!card.isDefault()) {
                counter++;
            }
        }
        return counter;
    }

    private CardComponent getFreeCardComponent(){
        for (CardComponent card : items) {
            if (card.isDefault()) {
                return card;
            }
        }
        throw new BadCardException("There aren't free item slot!");
    }

    public CardComponent getSelectedCard(){
        for (CardComponent card : items){
            if(card.isSelected()){
                removeSelection();
                return card;
            }
        }
        return null;
    }

    public void enableItemUsage(){
        enableAllCard();
        disableDefenseCard();
    }

    public void enableAttackCard(){
        for (CardComponent card: items){
            card.setAttackClickable(true);
        }
    }

    public void disableItemUsage(){
        for (CardComponent card: items){
            card.setClickable(false);
            card.setAttackClickable(false);
            card.setDefenseCardClickable(false);
            card.setSelected(false);
        }
    }

    public void disableAttackCard(){
        for (CardComponent card: items){
            card.setAttackClickable(false);
            card.setSelected(false);
        }
    }

    private void disableDefenseCard(){
        for (CardComponent card: items){
            card.setDefenseCardClickable(false);
            card.setSelected(false);
        }
    }

    private void enableAllCard(){
        for (CardComponent card: items){
            card.setClickable(true);
            card.setAttackClickable(true);
            card.setDefenseCardClickable(true);
        }
    }

    public void enableDiscardCard(){
        enableAllCard();
    }

}
