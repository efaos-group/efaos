/**
 *
 */
package it.polimi.client.view.gui.map;

import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class AlienSymbol extends Symbol {

    public AlienSymbol(Integer radius, Point2D.Double leftCorner) {
        super(radius, leftCorner);
    }

    @Override
    protected Line2D.Double[] getSymbolLines() {
        Line2D.Double[] lines = new Line2D.Double[6];

        Point2D.Double point = new Point2D.Double(leftCorner.x, leftCorner.y);
        point.y += height;

        lines[0] = new Line2D.Double(point.x, point.y, point.x, point.y + 3 * oneQuartersHeight);
        point.y += 3 * oneQuartersHeight;

        lines[1] = new Line2D.Double(point.x, point.y, point.x + halfRadius, point.y - halfRadius);
        lines[2] = new Line2D.Double(point.x + halfRadius, point.y - halfRadius, point.x + radius, point.y);
        point.x += radius;

        lines[3] = new Line2D.Double(point.x, point.y, point.x, point.y - 3 * oneQuartersHeight);
        point.y -= 3 * oneQuartersHeight;

        lines[4] = new Line2D.Double(point.x, point.y, point.x - radius, point.y - 3 * oneQuartersHeight);
        lines[5] = new Line2D.Double(point.x, point.y - 3 * oneQuartersHeight, point.x - radius, point.y);

        return lines;
    }

    @Override
    public void drawNumber(Graphics2D g) {
        //Alien symbol doesn't have a number
    }

}
