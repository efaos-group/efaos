/**
 *
 */
package it.polimi.client.view.gui.map;

import it.polimi.server.model.sector.Coordinate;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class EscapeHexagon extends HexagonShape {
    private static final long serialVersionUID = -5770501191187444278L;

    private Integer number;

    public EscapeHexagon(int radius, Coordinate position, Integer number) {
        super(radius, position);
        this.number = number;
        color = Color.BLACK;
    }

    public EscapeHexagon(double x, double y, int radius, Coordinate position, Integer number) {
        super(x, y, radius, position);
        this.number = number;
        color = Color.BLACK;
    }

    public EscapeHexagon(Point2D.Double start, int radius, Coordinate position, Integer number) {
        super(start, radius, position);
        this.number = number;
        color = Color.BLACK;
    }

    @Override
    public void draw(Graphics2D g) {
        super.draw(g);

        Symbol symbol = new HatchSymbol(radius, points[0], number);
        symbol.draw(g);
    }

    @Override
    public void movedOver() {
        color = Color.YELLOW;
        beforeNoise = Color.RED;
    }

    @Override
    public void block() {
        color = Color.RED;
    }

    @Override
    public void movedAway() {
        if(beforeNoise != null) {
            color = Color.RED;
        } else {
            color = Color.BLACK;
        }
    }

}
