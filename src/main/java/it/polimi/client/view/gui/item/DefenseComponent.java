/**
 * 
 */
package it.polimi.client.view.gui.item;


/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class DefenseComponent extends CardComponent {
    private static final long serialVersionUID = -2692079715571831556L;

    public DefenseComponent(String name, String path) {
        super(name, path);
    }
    
    @Override
    public void setClickable(boolean clickable){
        //Do nothing here, only in its father
    }
      
    @Override
    protected void setDefenseCardClickable(boolean enabled) {
        this.clickable = enabled;
    }

    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof DefenseComponent)) {
            return false;
        }
        if (obj == this) {
            return true;
        }

        DefenseComponent c = (DefenseComponent) obj;
        return c.getPath().equals(super.getPath()) && c.toString().equals(super.toString());
    }

    @Override
    public int hashCode() {
        return super.getPath().hashCode() + super.toString().hashCode();
    }

}
