/**
* A Simple JButton that shows border only on click
*
*/
package it.polimi.client.view.gui.custom;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */

public class PlainButton extends JButton implements MouseListener {
   private static final long serialVersionUID = 5244190816256523933L;

   public PlainButton(){
       super();
       setPlainStyle();
   }

   public PlainButton(String title){
       super(title);
       setPlainStyle();
   }

   private void setPlainStyle(){
       this.setContentAreaFilled(false);
       this.setBorderPainted(true);
       this.addMouseListener(this);
   }

   @Override
   public void mousePressed(MouseEvent e) {
       this.setBorderPainted(false);
   }

   @Override
   public void mouseReleased(MouseEvent e) {
       this.setBorderPainted(true);
   }

   @Override
   public void mouseClicked(MouseEvent e) {
       //Do nothing
   }

   @Override
   public void mouseEntered(MouseEvent e) {
       //Do nothing
   }

   @Override
   public void mouseExited(MouseEvent e) {
       //Do nothing
   }


}
