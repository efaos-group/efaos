/**
 *
 */
package it.polimi.client.view.gui.map;

import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class HumanSymbol extends Symbol {

    public HumanSymbol(Integer radius, Point2D.Double leftCorner) {
        super(radius, leftCorner);
    }

    @Override
    protected Line2D.Double[] getSymbolLines() {
        Line2D.Double[] lines = new Line2D.Double[10];


        Point2D.Double point = new Point2D.Double(leftCorner.x, leftCorner.y);
        point.y += height;

        lines[0] = new Line2D.Double(point.x, point.y, point.x, point.y + halfHeight);
        point.y += halfHeight;

        lines[1] = new Line2D.Double(point.x, point.y, point.x + halfRadius, point.y + oneQuartersHeight);
        lines[2] = new Line2D.Double(point.x + halfRadius, point.y + oneQuartersRadius, point.x + radius, point.y);
        point.x += radius;

        lines[3] = new Line2D.Double(point.x, point.y, point.x, point.y - halfHeight);
        point.y -= halfHeight;

        lines[4] = new Line2D.Double(point.x, point.y, point.x - halfRadius, point.y + oneQuartersHeight);
        lines[5] = new Line2D.Double(point.x - halfRadius, point.y + oneQuartersHeight, point.x - radius, point.y);
        point.x -= radius;

        lines[6] = new Line2D.Double(point.x, point.y, point.x, point.y - halfHeight);
        lines[7] = new Line2D.Double(point.x + radius, point.y, point.x + radius, point.y - halfHeight);
        point.y -= halfHeight;

        lines[8] = new Line2D.Double(point.x, point.y, point.x + oneQuartersRadius, point.y - oneEighthHeight);
        lines[9] = new Line2D.Double(point.x + radius, point.y, point.x + radius - oneQuartersRadius, point.y - oneEighthHeight);

        return lines;
    }

    @Override
    public void drawNumber(Graphics2D g) {
        //Human symbol doesn't have a number
    }

}
