/**
 *
 */
package it.polimi.client.view.gui.informationboard;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;
import javax.swing.text.DefaultCaret;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class MessageBoard extends JPanel {
    private static final long serialVersionUID = -8261448809806036731L;

    private static final Integer HEIGHT = 200;
    private static final Integer WIDTH = 420;

    private final JTextArea board;
    private final JScrollPane scroller;

    public MessageBoard() {
        super();
        super.setLayout(new FlowLayout(FlowLayout.LEFT));

        board = new JTextArea();
        scroller = new JScrollPane(board);

        setUpBoard();

        add(scroller);
        setTitledBorder();
    }

    private void setTitledBorder() {
        TitledBorder border = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK));
        border.setTitle("Message Board");
        border.setTitleJustification(TitledBorder.CENTER);
        super.setBorder(border);
    }

    private void setUpBoard() {

        board.setEditable(false);
        board.setFont(new Font("Serif", Font.ITALIC, 14));

        scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scroller.setPreferredSize(new Dimension(WIDTH, HEIGHT));

        DefaultCaret caret = (DefaultCaret) board.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
    }

    public void showMessage(String message) {
        board.append(message + "\n");
    }

}
