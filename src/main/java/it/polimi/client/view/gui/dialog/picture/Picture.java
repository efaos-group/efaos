/**
 *
 */
package it.polimi.client.view.gui.dialog.picture;

import it.polimi.client.view.exception.ResourceNotFoundException;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class Picture extends JComponent {
    private static final long serialVersionUID = 3815576438337989818L;

    private transient BufferedImage image;

    public Picture(String path, Dimension size) {
        URL url = this.getClass().getResource(path);
        try {
            image = ImageIO.read(url);
        } catch (IOException e) {
            throw new ResourceNotFoundException("Couldn't load the card image! " + e);
        }
        setPreferredSize(size);
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        Dimension size = getSize();
        g.drawImage(image, 0, 0,size.width, size.height, 0, 0, image.getWidth(), image.getHeight(), null);
    }

}
