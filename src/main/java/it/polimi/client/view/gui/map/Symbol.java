/**
 *
 */
package it.polimi.client.view.gui.map;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public abstract class Symbol {

    private static final BasicStroke BORDER_WIDTH = new BasicStroke(3.0f);

    protected Integer radius;
    protected Point2D.Double leftCorner;

    protected double height;
    protected double halfHeight;
    protected double oneQuartersHeight;
    protected double oneEighthHeight;
    protected double oneEighthRadius;
    protected double oneSixthRadius;
    protected double halfRadius;
    protected double oneQuartersRadius;

    public Symbol(Integer radius, Point2D.Double leftCorner) {
        this.radius = radius;
        this.leftCorner = new Point2D.Double(leftCorner.x, leftCorner.y);

        initValues();
    }

    public void setRadius(int radius) {
        this.radius = radius;
        initValues();
    }

    public void setLeftCorner(Point2D.Double leftCorner) {
        this.leftCorner = new Point2D.Double(leftCorner.x, leftCorner.y);
    }

    private void initValues() {
        this.height = Math.sqrt(3) * radius / 2;
        this.halfHeight = height / 2;
        this.oneQuartersHeight = height / 4;
        this.oneEighthHeight = height / 8;
        this.oneEighthRadius = (double)radius / 8;
        this.oneSixthRadius = (double)radius / 6;
        this.halfRadius = (double)radius / 2;
        this.oneQuartersRadius = (double)radius / 4;
    }

    protected void resetGraphicsPreferences(Graphics2D g, Color oldColor, Stroke oldStroke, Font oldFont) {
        g.setColor(oldColor);
        g.setStroke(oldStroke);
        g.setFont(oldFont);
    }

    public void draw(Graphics2D g) {
        Color oldColor = g.getColor();
        Stroke oldStroke = g.getStroke();
        Font oldFont = g.getFont();

        changeGraphicsPreferences(g);

        Line2D.Double[] lines = getSymbolLines();
        drawLines(g, lines);
        drawNumber(g);

        resetGraphicsPreferences(g, oldColor, oldStroke, oldFont);
    }

    protected void drawLines(Graphics2D g, Line2D.Double[] lines) {
        for(int i=0; i<lines.length; i++) {
            g.draw(lines[i]);
        }
    }

    protected void changeGraphicsPreferences(Graphics2D g) {
        g.setStroke(BORDER_WIDTH);
        g.setColor(Color.WHITE);
    }

    protected abstract Line2D.Double[] getSymbolLines();

    protected abstract void drawNumber(Graphics2D g);
}
