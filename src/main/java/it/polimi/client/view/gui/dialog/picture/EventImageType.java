package it.polimi.client.view.gui.dialog.picture;

import it.polimi.client.view.exception.ResourceNotFoundException;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public enum EventImageType {
    NOISE_IN_ANY_SECTOR("NOISE_IN_ANY_SECTOR","/image/event/sector/noiseany.png"),
    NOISE_IN_ANY_SECTOR_ITEM("NOISE_IN_ANY_SECTOR_ITEM","/image/event/sector/noiseanyitem.png"),
    NOISE_IN_YOUR_SECTOR("NOISE_IN_YOUR_SECTOR","/image/event/sector/noiseyour.png"),
    NOISE_IN_YOUR_SECTOR_ITEM("NOISE_IN_YOUR_SECTOR_ITEM","/image/event/sector/noiseyouritem.png"),
    SILENCE("SILENCE","/image/event/sector/silence.png"),
    ATTACK("Attack card","/image/event/item/attack.png"),
    SPOTLIGHT("Spotlight card","/image/event/item/spotlight.png"),
    DEFENSE("Defense card","/image/event/item/defense.png"),
    TELEPORT("Teleport card", "/image/event/item/teleport.png"),
    SEDATIVES("Sedatives card", "/image/event/item/sedatives.png"),
    ADRENALINE("Adrenaline card", "/image/event/item/adrenaline.png"),
    RED("RED", "/image/event/escape/red.png"),
    GREEN("GREEN", "/image/event/escape/green.png"),
    ALIEN("ALIEN", "/image/event/victory/alien.png"),
    HUMAN("HUMAN", "/image/event/victory/human.png"),
    CAPTAIN("The Captain Ennio Maria Dominoni","/image/event/character/captain.png"),
    PILOT("The Pilot Julia Niguloti a.k.a. “Cabal”","/image/event/character/pilot.png"),
    PSYCHOLOGIST("The Psychologist Silvano Porpora","/image/event/character/psychologist.png"),
    SOLDIER("The Soldier Tuccio Brendon a.k.a. “Piri”","/image/event/character/soldier.png"),
    PIERO("Piero Ceccarella","/image/event/character/piero.png"),
    VITTORIO("Vittorio Martana","/image/event/character/vittorio.png"),
    MARIA("Maria Galbani","/image/event/character/maria.png"),
    PAOLO("Paolo Landon","/image/event/character/paolo.png");

    private final String path;
    private final String name;

    private EventImageType(String name,String path) {
        this.path = path;
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public String getPath(){
        return path;
    }

    public static String parseInput(String name){
        for (EventImageType type : EventImageType.values()) {
            if (name.equalsIgnoreCase(type.getName())){
                return type.getPath();
            }
        }

        throw new ResourceNotFoundException("Couldn't find the image");
    }

}
