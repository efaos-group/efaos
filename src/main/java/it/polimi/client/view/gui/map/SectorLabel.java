/**
 *
 */
package it.polimi.client.view.gui.map;

import it.polimi.server.model.sector.Coordinate;

import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class SectorLabel extends Symbol {

    private final Coordinate label;

    public SectorLabel(Integer radius, Point2D.Double leftCorner, Coordinate label) {
        super(radius, leftCorner);
        this.label = label;
    }

    @Override
    protected Line2D.Double[] getSymbolLines() {
        return new Line2D.Double[0];
    }

    @Override
    protected void drawNumber(Graphics2D g) {
        float xString = (float) (leftCorner.x + oneEighthRadius);
        float yString = (float) (leftCorner.y + height + 2 * oneEighthHeight);

        String text = "" + this.label.getCol();
        text += (this.label.getRow() > 9) ? this.label.getRow() : "0"+this.label.getRow() ;

        g.drawString(text, xString, yString);

    }

}
