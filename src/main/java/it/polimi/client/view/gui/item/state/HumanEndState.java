/**
 *
 */
package it.polimi.client.view.gui.item.state;

import it.polimi.client.view.gui.listener.GameActionListener;

import javax.swing.JButton;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class HumanEndState extends HumanState {
    private static final long serialVersionUID = -8408954786592464209L;

    public HumanEndState(GameActionListener listener, JButton attack, JButton use, JButton discard, JButton end, JButton draw) {
        super(listener, attack, use, discard, end, draw);

        attack.setEnabled(false);
        end.setEnabled(true);
    }

    @Override
    public ItemState enableEndTurn() {
        return this;
    }

}
