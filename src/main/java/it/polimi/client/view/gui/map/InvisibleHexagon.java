/**
 *
 */
package it.polimi.client.view.gui.map;

import it.polimi.client.view.exception.BadPositionException;
import it.polimi.server.model.sector.Coordinate;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class InvisibleHexagon extends HexagonShape {
    private static final long serialVersionUID = -3983510445838540125L;

    public InvisibleHexagon(int radius, Coordinate position) {
        super(radius, position);
    }

    public InvisibleHexagon(double x, double y, int radius, Coordinate position) {
        super(x, y, radius, position);
    }

    public InvisibleHexagon(Point2D.Double start, int radius, Coordinate position) {
        super(start, radius, position);
    }

    @Override
    public void draw(Graphics2D g) {
        //Draw nothing because it' not clickable and useless
    }

    @Override
    public void mouseClicked(MouseEvent evt) {
        //Do nothing because this hexagon should be unclickable
    }

    @Override
    public void movedOver() {
        throw new BadPositionException("This sector can't be walked");

    }

    @Override
    public void movedAway() {
        //Do nothing because no-one can move over this kind of sector
    }

    @Override
    public void block() {
        throw new BadPositionException("This sector can't be blocked");

    }


}
