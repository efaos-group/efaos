/**
 *
 */
package it.polimi.client.view.gui.map.state;

import it.polimi.client.view.gui.listener.MouseClickListener;
import it.polimi.server.model.sector.Coordinate;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class UnclickableState extends ClickState {
    private static final long serialVersionUID = 4376536705820134079L;

    public UnclickableState(MouseClickListener listener) {
        super(listener);
    }

    @Override
    public void notifyClick(Coordinate clickPosition) {
        //Do nothing because the component is not clickable or at least should not fire event
    }

}
