/**
 *
 */
package it.polimi.client.view.gui.listener;

import it.polimi.client.view.gui.item.CardComponent;

import java.io.Serializable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public interface GameActionListener extends Serializable {

    public void passTurn();

    public void attack();

    public void useCard(CardComponent card);

    public void discardCard(String card);

    public void drawSector();

}
