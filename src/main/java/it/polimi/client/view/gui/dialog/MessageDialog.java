/**
 *
 */
package it.polimi.client.view.gui.dialog;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.beans.EventHandler;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class MessageDialog extends JDialog {
    private static final long serialVersionUID = 7531707033959209252L;

    private static final String DEFAULT_COLOR = "black";

    public MessageDialog(String message) {
        this(message, DEFAULT_COLOR);
    }

    public MessageDialog(String message, String color) {
        super();
        super.setTitle("Event!");
        super.setAlwaysOnTop(true);

        JPanel messagePane = new JPanel();
        buildMessagePane(messagePane, message, color);
        JPanel buttonPane = buildButtonPane();

        super.add(messagePane, BorderLayout.CENTER);
        super.add(buttonPane, BorderLayout.SOUTH);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void buildMessagePane(JPanel pane, String message, String color) {
        String formattedMessage = "<html><font color=" + color + ">"  + message.replaceAll("\\n", "<br/>") + "</font></html>";
        formattedMessage = formattedMessage.replaceAll("\\t", "&nbsp;&nbsp;&nbsp;&nbsp;");

        JLabel messageLabel = new JLabel(formattedMessage);
        pane.add(messageLabel);
    }

    private JPanel buildButtonPane() {
        JPanel pane = new JPanel();

        JButton confirmButton = new JButton("Ok");
        confirmButton.addActionListener(EventHandler.create(ActionListener.class, this, "dispose"));
        pane.add(confirmButton);

        return pane;
    }
}
