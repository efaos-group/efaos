/**
 *
 */
package it.polimi.client.view.gui.map.state;

import it.polimi.client.view.gui.listener.MouseClickListener;
import it.polimi.server.model.sector.Coordinate;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class ClickableState extends ClickState {
    private static final long serialVersionUID = -8840945745300999704L;

    public ClickableState(MouseClickListener listener) {
        super(listener);
    }

    @Override
    public void notifyClick(Coordinate clickPosition) {
        listener.mouseClicked(clickPosition);
    }

}
