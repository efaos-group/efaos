
package it.polimi.client.view.gui.informationboard;

import it.polimi.client.view.exception.ResourceNotFoundException;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public enum CharacterImageType {
    CAPTAIN("The Captain Ennio Maria Dominoni","/image/character/captain.png"),
    PILOT("The Pilot Julia Niguloti a.k.a. “Cabal”","/image/character/pilot.png"),
    PSYCHOLOGIST("The Psychologist Silvano Porpora","/image/character/psychologist.png"),
    SOLDIER("The Soldier Tuccio Brendon a.k.a. “Piri”","/image/character/soldier.png"),
    PIERO("Piero Ceccarella","/image/character/piero.png"),
    VITTORIO("Vittorio Martana","/image/character/vittorio.png"),
    MARIA("Maria Galbani","/image/character/maria.png"),
    PAOLO("Paolo Landon","/image/character/paolo.png");

    private final String name;
    private final String path;

    private CharacterImageType(String name,String path){
        this.path = path;
        this.name = name;
    }

    public String getPath(){
        return path;
    }

    public String getName(){
        return name;
    }

    public static String parseInput(String input){

        for (CharacterImageType type : CharacterImageType.values()){
            if (input.equalsIgnoreCase(type.getName())) {
                return type.getPath();
            }
        }
        throw new ResourceNotFoundException("Couldn't find the character image");
    }
}
