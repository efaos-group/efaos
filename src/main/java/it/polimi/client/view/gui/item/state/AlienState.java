/**
 *
 */
package it.polimi.client.view.gui.item.state;

import it.polimi.client.view.gui.listener.GameActionListener;

import javax.swing.JButton;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class AlienState extends ItemState {
    private static final long serialVersionUID = 2539344779831713258L;

    protected final JButton attack, use, discard, end, draw;

    public AlienState(GameActionListener listener, JButton attack, JButton use, JButton discard, JButton end, JButton draw) {
        super(listener);

        this.attack = attack;
        this.use = use;
        this.discard = discard;
        this.end = end;
        this.draw = draw;
    }

    @Override
    public void useCard() {
        //Do nothing because alien can never use a card
    }

    @Override
    public ItemState enableAttack() {
        return new AlienAttackState(listener, attack, use, discard, end, draw);
    }

    @Override
    public final ItemState enableUseCard() {
        return this;
    }

    @Override
    public ItemState enableDiscardCard() {
        return new AlienDiscardState(listener, attack, use, discard, end, draw);
    }

    @Override
    public ItemState enableEndTurn() {
        return new AlienEndTurnState(listener, attack, use, discard, end, draw);
    }

    @Override
    public ItemState disableAll() {
        return new AlienNormalState(listener, attack, use, discard, end, draw);
    }

    @Override
    public ItemState enableDrawSector() {
        return new AlienDrawSectorState(listener, attack, use, discard, end, draw);
    }

}
