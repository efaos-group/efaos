/**
 *
 */
package it.polimi.client.view.exception;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class BadPositionException extends RuntimeException {
    private static final long serialVersionUID = -5661289644382225031L;

    public BadPositionException(String message) {
        super(message);
    }

}
