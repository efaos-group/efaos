/**
 *
 */
package it.polimi.client.view.exception;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class MapNotFoundException extends RuntimeException {
    private static final long serialVersionUID = -9102346078133680916L;

    public MapNotFoundException(String message) {
        super(message);
    }
}
