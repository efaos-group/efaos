/**
 *
 */
package it.polimi.client.view.exception;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class ItemNotFoundException extends RuntimeException {
    private static final long serialVersionUID = -4884812573718016800L;

    public ItemNotFoundException(String msg) {
        super(msg);
    }

}
