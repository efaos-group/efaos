/**
 *
 */
package it.polimi.client.view.exception;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class ResourceNotFoundException extends RuntimeException {
    private static final long serialVersionUID = -4151985113089568927L;

    public ResourceNotFoundException(String message) {
        super(message);
    }

}
