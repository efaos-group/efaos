/**
 *
 */
package it.polimi.client.view.exception;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class CommandNotFoundException extends RuntimeException {
    private static final long serialVersionUID = -7446081867382903122L;

    public CommandNotFoundException(String msg) {
        super(msg);
    }

}
