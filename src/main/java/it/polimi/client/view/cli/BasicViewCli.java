/**
 *
 */
package it.polimi.client.view.cli;

import it.polimi.client.remotecontroller.RemoteControllerInterface;
import it.polimi.client.view.cli.behaviour.BehaviourFactory;
import it.polimi.client.view.cli.behaviour.CharacterBehaviour;
import it.polimi.client.view.cli.behaviour.CommandExecuter;
import it.polimi.client.view.cli.questioner.Console;
import it.polimi.client.view.cli.questioner.Questioner;
import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.Observable;
import it.polimi.common.observer.View;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public abstract class BasicViewCli implements View {
    protected final List<String> items;
    protected String playerID;
    protected String map;
    protected String position;
    protected CharacterBehaviour character;
    protected Questioner questioner;

    protected RemoteControllerInterface controller;

    public BasicViewCli(RemoteControllerInterface controller) {
        this.controller = controller;
        this.items = new LinkedList<String>();
        this.questioner = new Questioner(new Console());
    }

    @Override
    public void showCardDialog(String msg) {
        questioner.write(msg);
    }

    @Override
    public void showEventDialog(String msg) {
        questioner.write(msg);
    }

    @Override
    public void showErrorDialog(String msg) {
        questioner.write(msg);
    }

    @Override
    public void showNoiseInSectorDialog(String msg) {
        questioner.write(msg);
    }

    @Override
    public void choiceAccepted(String msg) {
        questioner.write(msg);
    }

    @Override
    public void showEndGameDialog(String msg) {
        questioner.write(msg);
    }
    @Override
    public void enableMovement() {
        character.enableMovement();
    }
    @Override
    public void enableItemUsage() {
        character.enableItemUsage();
    }
    @Override
    public void enableAttack() {
        character.enableAttack();
    }
    @Override
    public void enableEndTurn() {
        character.enableEndTurn();
    }
    @Override
    public void enableDrawSectorCard() {
        character.enableDrawSectorCard();
    }
    @Override
    public void enableDiscard() {
        character.enableDiscard();
    }
    @Override
    public void enableAttackCard() {
        character.enableAttackCard();
    }
    @Override
    public void enableSelectSector() {
        character.enableSelectSector();
    }
    @Override
    public void enableDrawItemCard() {
        character.enableDrawItemCard();
    }
    @Override
    public void disableAll() {
        character.disableAttack();
        character.disableDiscard();
        character.disableDrawSectorCard();
        character.disableEndTurn();
        character.disableItemUsage();
        character.disableMovement();
        character.disableAttackCard();
        character.disableSelectSector();
        character.disableDrawItemCard();
    }

    @Override
    public void setCharacterBehaviour(String character) {
        CommandExecuter executer = new CommandExecuter(playerID, questioner, items, controller);
        this.character = BehaviourFactory.factory(character,executer);
    }

    @Override
    public void removeUsedItemCard(String card) {
        items.remove(card);
    }

    @Override
    public void addNewItemCard(String card) {
        items.add(card);
    }

    @Override
    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String getPlayerID() {
        return playerID;
    }

    @Override
    public void notify(Observable source, ModelEvent gameEvent) {
        gameEvent.reactToView(this);
    }
}
