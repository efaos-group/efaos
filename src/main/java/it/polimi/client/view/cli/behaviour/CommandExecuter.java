/**
 *
 */
package it.polimi.client.view.cli.behaviour;

import it.polimi.client.view.cli.command.AttackCardCommand;
import it.polimi.client.view.cli.command.AttackCommand;
import it.polimi.client.view.cli.command.Command;
import it.polimi.client.view.cli.command.DiscardCommand;
import it.polimi.client.view.cli.command.DrawItemCommand;
import it.polimi.client.view.cli.command.DrawSectorCardCommand;
import it.polimi.client.view.cli.command.MoveCommand;
import it.polimi.client.view.cli.command.PassCommand;
import it.polimi.client.view.cli.command.SelectSectorCommand;
import it.polimi.client.view.cli.command.UseItemCommand;
import it.polimi.client.view.cli.questioner.Questioner;
import it.polimi.server.controller.Controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class CommandExecuter {

    private final Map <String, Command> commands;
    private final String playerID;
    private final Questioner questioner;
    private final List<String> items;
    private final Controller controller;

    public CommandExecuter(String playerID, Questioner questioner, List<String> items, Controller controller){
        commands = new HashMap<String,Command>();
        this.playerID = playerID;
        this.questioner = questioner;
        this.items = items;
        this.controller = controller;

        initializeCommands();
    }

    private void initializeCommands(){
        commands.put(CommandMatcher.ATTACK, new AttackCommand(playerID, controller));
        commands.put(CommandMatcher.ATTACK_CARD, new AttackCardCommand(playerID, controller, questioner,items));
        commands.put(CommandMatcher.DISCARD, new DiscardCommand(playerID, controller, questioner,items));
        commands.put(CommandMatcher.DRAW_ITEM, new DrawItemCommand(playerID, controller));
        commands.put(CommandMatcher.DRAW_SECTOR, new DrawSectorCardCommand(playerID, controller));
        commands.put(CommandMatcher.ITEM_COMMAND, new UseItemCommand(playerID, controller, questioner,items));
        commands.put(CommandMatcher.MOVE_COMMAND, new MoveCommand(playerID,controller));
        commands.put(CommandMatcher.PASS_TURN, new PassCommand(playerID, controller));
        commands.put(CommandMatcher.SELECT_SECTOR, new SelectSectorCommand(playerID, controller));
    }

    public void execute(String command, String answer){
        commands.get(command).execute(answer);
    }

}
