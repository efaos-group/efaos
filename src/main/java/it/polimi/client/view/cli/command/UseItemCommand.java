/**
 * 
 */
package it.polimi.client.view.cli.command;

import it.polimi.client.view.cli.questioner.Console;
import it.polimi.client.view.cli.questioner.Questioner;
import it.polimi.client.view.exception.ItemNotFoundException;
import it.polimi.server.controller.Controller;

import java.util.List;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class UseItemCommand extends ItemCommand {
    
    public UseItemCommand(String playerID, Controller controller, Questioner questioner, List<String> items) {
        super(playerID, controller, questioner, items);
    }

    @Override
    public void execute(String answer) {
        if(!items.isEmpty()) {
            showItem();
            manageItemChoice();
        } else {
            questioner.write(Console.ITEM_USAGE_ERROR);
            throw new ItemNotFoundException(Console.ITEM_USAGE_ERROR);
        }
    }
    
    private void manageItemChoice() {
        String item = questioner.askForWhichItemToUse(items);
        if(item.equals(Questioner.SPOTLIGHT_CARD)) {
            String sector = questioner.askForWhichSector();
            controller.useCard(playerID, item, parseCol(sector), parseRow(sector));
        } else {
            controller.useCard(playerID, item, " ", " ");
        }
    }

}
