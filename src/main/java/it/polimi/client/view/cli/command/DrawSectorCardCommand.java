
package it.polimi.client.view.cli.command;

import it.polimi.server.controller.Controller;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class DrawSectorCardCommand extends Command{

    public DrawSectorCardCommand(String playerID, Controller controller) {
        super(playerID, controller);
    }

    @Override
    public void execute(String answer){
        controller.drawSectorCard(playerID);
    }
}
