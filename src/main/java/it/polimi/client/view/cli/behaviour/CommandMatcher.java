/**
 *
 */
package it.polimi.client.view.cli.behaviour;

import java.util.regex.Pattern;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public final class CommandMatcher {

    public static final String MOVE_COMMAND = "sendMoveCommand";
    public static final String ITEM_COMMAND = "sendUseItemCommand";
    public static final String ATTACK = "sendAttackCommand";
    public static final String PASS_TURN = "sendPassTurnCommand";
    public static final String DISCARD = "sendDiscardCommand";
    public static final String DRAW_SECTOR = "sendDrawSectorCardCommand";
    public static final String SELECT_SECTOR = "sendSelectSectorCommand";
    public static final String ATTACK_CARD = "sendAttackCardCommand";
    public static final String DRAW_ITEM = "sendDrawItemCardCommand";

    public static final Pattern moveCommand = Pattern.compile("[m][o][v][e]\\s[A-W]\\s([1-9]|[1][0-4])");
    public static final Pattern itemCommand = Pattern.compile("[i][t][e][m]");
    public static final Pattern attackCommand = Pattern.compile("[a][t][t][a][c][k]");
    public static final Pattern endTurnCommand = Pattern.compile("[e][n][d]");
    public static final Pattern drawSectorCardCommand = Pattern.compile("[d][r][a][w]\\s[s][e][c][t][o][r]\\s[c][a][r][d]");
    public static final Pattern discardCommand = Pattern.compile("[d][i][s][c][a][r][d]");
    public static final Pattern attackCardCommand = Pattern.compile("[u][s][e]\\s[a][t][t][a][c][k]\\s[c][a][r][d]");
    public static final Pattern selectSectorCommand = Pattern.compile("[s][e][l][e][c][t]\\s[A-W]\\s([1-9]|[1][0-4])");
    public static final Pattern drawItemCardCommand = Pattern.compile("[d][r][a][w]\\s[i][t][e][m]\\s[c][a][r][d]");

    public static final String MOVE_DESCRIPTION = "Write 'move X Y' to move.\nFor example 'move A 1'";
    public static final String ITEM_DESCRIPTION = "Write 'item' to use an item.";
    public static final String ATTACK_DESCRIPTION = "Write 'attack' to attack.";
    public static final String PASS_TURN_DESCRIPTION = "Write 'end' to end your turn.";
    public static final String DRAW_SECTOR_DESCRIPTION = "Write 'draw sector card' to draw a sector card";
    public static final String DISCARD_DESCRIPTION = "Write 'discard' to discard a card";
    public static final String DRAW_ITEM_DESCRIPTION = "Write 'draw item card' to draw an item";
    public static final String ATTACK_CARD_DESCRIPTION = "Write 'use attack card' to use an attack card";
    public static final String SELECT_SECTOR_DESCRIPTION = "Write 'select X Y' to select a sector.\nFor example 'select B 2'.";

    private CommandMatcher(){
    }
}
