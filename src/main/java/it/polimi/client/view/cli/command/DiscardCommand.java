/**
 * 
 */
package it.polimi.client.view.cli.command;

import it.polimi.client.view.cli.questioner.Questioner;
import it.polimi.server.controller.Controller;

import java.util.List;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class DiscardCommand extends ItemCommand {
    
    private Questioner questioner;

    public DiscardCommand(String playerID, Controller controller, Questioner questioner, List<String> items) {
        super(playerID, controller, questioner, items);
        this.questioner = questioner;
    }

    @Override
    public void execute(String answer) {
        showItem();
        String result = questioner.askForWhichItemToDiscard(items);
        controller.discardCard(playerID, result);
    }

}
