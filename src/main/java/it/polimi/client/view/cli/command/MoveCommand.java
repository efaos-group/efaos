package it.polimi.client.view.cli.command;

import it.polimi.server.controller.Controller;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class MoveCommand extends Command {

    public MoveCommand(String playerID, Controller controller) {
        super(playerID, controller);
    }

    @Override
    public void execute(String answer) {
        controller.move(playerID, parseCol(answer), parseRow(answer));
    }

}
