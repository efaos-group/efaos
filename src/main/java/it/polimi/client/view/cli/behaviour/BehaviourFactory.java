/**
 *
 */
package it.polimi.client.view.cli.behaviour;

import it.polimi.server.model.exception.BadCharacterException;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class BehaviourFactory {

    private BehaviourFactory(){
    }

    public static CharacterBehaviour factory(String type, CommandExecuter executer) {

        if("HUMAN".equals(type)) {
            return new HumanBehaviour(executer);
        } else if ("ALIEN".equals(type)) {
            return new AlienBehaviour(executer);
        } else {
            throw new BadCharacterException("Error in creating a Character Behaviour for the view");
        }
    }

}
