/**
 *
 */
package it.polimi.client.view.cli.questioner;

import it.polimi.client.view.exception.ItemNotFoundException;
import it.polimi.server.model.card.item.AttackCard;
import it.polimi.server.model.card.item.DefenseCard;
import it.polimi.server.model.card.item.SpotlightCard;

import java.util.List;
import java.util.regex.Matcher;
/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class Questioner {
    private final Console console;
    private Matcher matcher;

    public static final String ATTACK_CARD = new AttackCard(null).toString();
    public static final String DEFENSE_CARD = new DefenseCard(null).toString();
    public static final String SPOTLIGHT_CARD = new SpotlightCard(null).toString();

    public Questioner(Console console) {
        this.console = console;
    }

    public String askForPlayerID() {
        console.write(Console.USERNAME_QUESTION);
        String answer = "";
        boolean answerIsValid = false;

        while(!answerIsValid) {
            answer = console.read();
            matcher = Console.usernameCommand.matcher(answer);
            if (matcher.matches()) {
                answerIsValid = true;
            } else {
                console.write(Console.USERNAME_ERROR);
            }
        }
        return answer;
    }

    public String askForMap() {
        console.write(Console.MAP_QUESTION);
        String answer = "";
        boolean answerIsValid = false;

        while(!answerIsValid) {
            answer = console.read();
            matcher = Console.mapCommand.matcher(answer);
            if (matcher.matches()) {
                answerIsValid = true;
            } else {
                console.write(Console.MAP_ERROR);
            }
        }
        return answer;
    }

    public String askForWhichSector() {
        console.write(Console.SECTOR_QUESTION);
        String answer = "";
        boolean answerIsValid = false;

        while (!answerIsValid) {
            answer = console.read();
            matcher = Console.sectorCommand.matcher(answer);
            if (matcher.matches()) {
                answerIsValid = true;
            } else {
                console.write(Console.SECTOR_CHOICE_ERROR);
            }
        }
        return answer;
    }

    public String askForWhichItemToDiscard(List<String> items) {
        String answer = "";
        boolean answerIsValid = false;

        while(!answerIsValid) {
            console.write(Console.DISCARD_QUESTION);
            answer = console.read();
            if(haveItem(answer,items)) {
                answerIsValid = true;
            } else {
                console.write(Console.DISCARD_ERROR);
            }
        }
        return answer;
    }

    public String askForWhichItemToUse(List<String> items) {
        console.write(Console.ITEM_USAGE_QUESTION);
        String answer = console.read();

        if(canUse(answer, items)) {
            return answer;
        } else {
            console.write(Console.INVALID_ITEM_ERROR);
            throw new ItemNotFoundException("Cannot use that item right now");
        }
    }

    private boolean haveItem(String answer, List<String> items){
        return items.contains(answer);
    }

    private boolean canUse(String answer, List<String> items){
        boolean isAttackCard = ATTACK_CARD.equalsIgnoreCase(answer);
        boolean isDefenseCard = DEFENSE_CARD.equalsIgnoreCase(answer);
        return haveItem(answer, items) && !isAttackCard && !isDefenseCard;
    }

    public void write(String message){
        console.write(message);
    }

    public String read(){
        return console.read();
    }
}
