/**
 *
 */
package it.polimi.client.view.cli.behaviour;

import it.polimi.client.view.exception.CommandNotFoundException;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public abstract class CharacterBehaviour {

    protected final Map<Pattern,String> actions;
    protected final Map<Pattern,String> descriptions;
    private Matcher matcher;
    private CommandExecuter executer;

    public CharacterBehaviour (CommandExecuter executer) {
        actions = new HashMap<Pattern,String>();
        descriptions = new HashMap<Pattern,String>();
        initializeDescriptions();
        this.executer = executer;
    }

    private void initializeDescriptions(){
        descriptions.put(CommandMatcher.attackCardCommand, CommandMatcher.ATTACK_CARD_DESCRIPTION);
        descriptions.put(CommandMatcher.attackCommand, CommandMatcher.ATTACK_DESCRIPTION);
        descriptions.put(CommandMatcher.discardCommand, CommandMatcher.DISCARD_DESCRIPTION);
        descriptions.put(CommandMatcher.drawItemCardCommand, CommandMatcher.DRAW_ITEM_DESCRIPTION);
        descriptions.put(CommandMatcher.drawSectorCardCommand, CommandMatcher.DRAW_SECTOR_DESCRIPTION);
        descriptions.put(CommandMatcher.endTurnCommand, CommandMatcher.PASS_TURN_DESCRIPTION);
        descriptions.put(CommandMatcher.moveCommand, CommandMatcher.MOVE_DESCRIPTION);
        descriptions.put(CommandMatcher.selectSectorCommand, CommandMatcher.SELECT_SECTOR_DESCRIPTION);
        descriptions.put(CommandMatcher.itemCommand, CommandMatcher.ITEM_DESCRIPTION);
    }

    public void enableMovement() {
        actions.put(CommandMatcher.moveCommand,CommandMatcher.MOVE_COMMAND);
    }
    public void disableMovement() {
        actions.remove(CommandMatcher.moveCommand);
    }
    public void enableItemUsage() {
        actions.put(CommandMatcher.itemCommand,CommandMatcher.ITEM_COMMAND);
    }
    public void disableItemUsage() {
        actions.remove(CommandMatcher.itemCommand);
    }
    public void enableAttack() {
        actions.put(CommandMatcher.attackCommand, CommandMatcher.ATTACK);
    }
    public void disableAttack() {
        actions.remove(CommandMatcher.attackCommand);
    }
    public void enableEndTurn() {
        actions.put(CommandMatcher.endTurnCommand, CommandMatcher.PASS_TURN);
    }
    public void disableEndTurn() {
        actions.remove(CommandMatcher.endTurnCommand);
    }
    public void enableDrawSectorCard() {
        actions.put(CommandMatcher.drawSectorCardCommand, CommandMatcher.DRAW_SECTOR);
    }
    public void disableDrawSectorCard() {
        actions.remove(CommandMatcher.drawSectorCardCommand);
    }
    public void enableDiscard() {
        actions.put(CommandMatcher.discardCommand, CommandMatcher.DISCARD);
    }
    public void disableDiscard() {
        actions.remove(CommandMatcher.discardCommand);
    }
    public void enableAttackCard() {
        actions.put(CommandMatcher.attackCardCommand, CommandMatcher.ATTACK_CARD);
    }
    public void disableAttackCard() {
        actions.remove(CommandMatcher.attackCardCommand);
    }
    public void enableSelectSector() {
        actions.put(CommandMatcher.selectSectorCommand, CommandMatcher.SELECT_SECTOR);
    }
    public void disableSelectSector() {
        actions.remove(CommandMatcher.selectSectorCommand);
    }
    public void enableDrawItemCard() {
        actions.put(CommandMatcher.drawItemCardCommand, CommandMatcher.DRAW_ITEM);
    }
    public void disableDrawItemCard() {
        actions.remove(CommandMatcher.drawItemCardCommand);
    }

    public void executeCommand(String answer) {
        for (Pattern pattern : actions.keySet()) {
            matcher = pattern.matcher(answer);

            if (matcher.matches()) {
                executer.execute(actions.get(pattern), answer);
                return;
            }
        }
        throw new CommandNotFoundException("Command not Found, try again!");
    }

    public String getAvailableActions(){
        String message = "";
        for (Pattern action: actions.keySet()) {
            message = message + "\n" + descriptions.get(action);
        }
        return message;
    }
}
