/**
 *
 */
package it.polimi.client.view.cli.behaviour;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class AlienBehaviour extends CharacterBehaviour {
    public AlienBehaviour(CommandExecuter executer) {
        super(executer);
    }

    @Override
    public void enableItemUsage() {
        //Do nothing because alien cannot use item
    }

    @Override
    public void enableAttackCard() {
        //Do nothing because alien cannot use attack card
    }

    @Override
    public String toString(){
        return "Alien";
    }
}
