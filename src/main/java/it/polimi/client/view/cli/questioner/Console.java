/**
 *
 */
package it.polimi.client.view.cli.questioner;

import java.io.PrintWriter;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class Console {
    public static final Pattern mapCommand = Pattern.compile("([g][a][l][i][l][e][i])|([g][a][l][v][a][n][i])|([f][e][r][m][i])");
    public static final String MAP_QUESTION = "Choose a map: galvani, galilei or fermi";
    public static final String MAP_ERROR = "Map doesn't exist, try again!";
    public static final Pattern sectorCommand = Pattern.compile("[s][e][l][e][c][t]\\s[A-W]\\s([1-9]|[1][1-4])");
    public static final String SECTOR_QUESTION = "\nWrite 'select X Y'.\nFor example 'select B 2'.";
    public static final Pattern usernameCommand = Pattern.compile("[a-zA-Z0-9]+");
    public static final String USERNAME_QUESTION = "Insert your username";
    public static final String USERNAME_ERROR = "Username not valid, try again!";
    public static final String COMMAND_ERROR = "Command not Found, try again!";
    public static final String COMMAND_QUESTION = "\nWhat do you want to do?";
    public static final String ATTACK_CARD_ERROR = "You don't have Attack Card, try a different command!";
    public static final String DISCARD_QUESTION = "Write the name of the item you want to discard";
    public static final String DISCARD_ERROR = "You cannot discard that item, try again!";
    public static final String ITEM_USAGE_ERROR = "You don't have items, try a different command!";
    public static final String INVALID_ITEM_ERROR = "You can't use that item now!";
    public static final String SHOW_ITEM_MESSAGE = "You have the following item card:";
    public static final String SECTOR_CHOICE_ERROR = "You cannot select that sector";
    public static final String ITEM_USAGE_QUESTION = "Write the name of the item you want to use";

    private final Scanner scanner;
    private final PrintWriter printer;

    public Console() {
        this.scanner = new Scanner(System.in);
        this.printer = new PrintWriter(System.out, true);
    }

    public void write(String msg){
        printer.println(msg);
    }

    public String read(){
        return scanner.nextLine();
    }

    public void close(){
        scanner.close();
        printer.close();
    }


}
