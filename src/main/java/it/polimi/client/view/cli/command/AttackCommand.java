/**
 * 
 */
package it.polimi.client.view.cli.command;

import it.polimi.server.controller.Controller;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class AttackCommand extends Command {

    public AttackCommand(String playerID, Controller controller) {
        super(playerID, controller);
    }

    @Override
    public void execute(String answer) {
        controller.attack(playerID);
    }

}
