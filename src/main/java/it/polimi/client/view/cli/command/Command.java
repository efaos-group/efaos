/**
 *
 */
package it.polimi.client.view.cli.command;

import it.polimi.server.controller.Controller;

import java.util.StringTokenizer;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public abstract class Command {
    protected final String playerID;
    protected final Controller controller;

    public Command(String playerID, Controller controller){
        this.playerID = playerID;
        this.controller = controller;
    }

    protected String parseCol(String answer){
        StringTokenizer tokens = new StringTokenizer(answer);
        tokens.nextToken();
        return tokens.nextToken();
    }

    protected String parseRow(String answer){
        StringTokenizer tokens = new StringTokenizer(answer);
        tokens.nextToken();
        tokens.nextToken();
        return tokens.nextToken();
    }

    public abstract void execute(String answer);

}

