/**
 * 
 */
package it.polimi.client.view.cli.command;

import it.polimi.server.controller.Controller;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class SelectSectorCommand extends Command {

    public SelectSectorCommand(String playerID, Controller controller) {
        super(playerID, controller);
    }

    @Override
    public void execute(String answer) {
        controller.selectSectorForNoise(playerID, parseCol(answer), parseRow(answer));
    }

}
