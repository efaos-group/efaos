/**
 *
 */
package it.polimi.client.view.cli;

import it.polimi.client.remotecontroller.RemoteControllerInterface;
import it.polimi.client.view.cli.questioner.Console;
import it.polimi.client.view.exception.CommandNotFoundException;
import it.polimi.client.view.exception.ItemNotFoundException;

import java.util.Collection;
import java.util.logging.Logger;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class MainCLI extends BasicViewCli {

    private static final Logger LOGGER = Logger.getLogger(MainCLI.class.getName());

    public MainCLI(RemoteControllerInterface controller){
        super(controller);
        init();
    }

    private void init() {
        askForMap();
        askForPlayerID();
        sendPlayerChoice();
    }

    @Override
    public void askForPlayerID() {
        playerID = questioner.askForPlayerID();
    }

    @Override
    public void askForMap(){
        map = questioner.askForMap();
    }

    @Override
    public void sendPlayerChoice() {
        controller.sendPlayerChoice(playerID, map);
    }

    @Override
    public void askForCommand(){
        boolean answerIsValid = false;
        showAvailableAction();

        while(!answerIsValid) {
            String answer = questioner.read();
            try {
                character.executeCommand(answer);
                answerIsValid = true;
            } catch (CommandNotFoundException e) {
                questioner.write(Console.COMMAND_ERROR);
                LOGGER.fine("The command typed can't be found" + e);
            } catch (ItemNotFoundException e){
                LOGGER.fine("The item typed can't be found" + e);
            }
        }
    }

    private void showAvailableAction() {
        questioner.write(Console.COMMAND_QUESTION);
        String message = character.getAvailableActions();
        questioner.write(message);
    }

    @Override
    public void showStartTurnDialog(String msg, String playerID) {
        questioner.write(msg);
        if (playerID.equals(this.playerID)) {
            questioner.write("You are " + character.toString() + ".");
            printPosition();
        }
    }

    private void printPosition() {
        if ( position != null) {
            questioner.write("Your position is " + position + ".");
        }
    }

    @Override
    public void showPictureDialog(String msg, String card) {
        questioner.write(msg);
    }

    @Override
    public void blockEscapeSector(String message) {
        questioner.write(message);
    }

    @Override
    public void showStartGameEvent(String msg, String character, Collection<String> players) {
        questioner.write(msg);
        questioner.write("There are the following players:");
        printPlayers(players);
        questioner.write("You are: " + character);
    }

    private void printPlayers(Collection<String> players) {
        for(String player : players) {
            questioner.write(player);
        }
    }
}
