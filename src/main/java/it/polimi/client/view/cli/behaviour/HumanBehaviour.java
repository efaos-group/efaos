/**
 *
 */
package it.polimi.client.view.cli.behaviour;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class HumanBehaviour extends CharacterBehaviour {

    public HumanBehaviour(CommandExecuter executer) {
        super(executer);
    }

    @Override
    public void enableAttack() {
        //Do nothing because human cannot attack
    }

    @Override
    public String toString() {
        return "Human";
    }
}
