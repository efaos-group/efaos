/**
 *
 */
package it.polimi.client.view.cli.command;

import it.polimi.client.view.cli.questioner.Console;
import it.polimi.client.view.cli.questioner.Questioner;
import it.polimi.server.controller.Controller;

import java.util.List;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public abstract class ItemCommand extends Command {

    protected final List<String> items;
    protected final Questioner questioner;

    public ItemCommand(String playerID, Controller controller, Questioner questioner, List<String> items) {
        super(playerID, controller);
        this.questioner = questioner;
        this.items = items;
    }

    protected void showItem() {
        questioner.write(Console.SHOW_ITEM_MESSAGE);

        String result = buildItemMessage();
        questioner.write(result);
    }

    private String buildItemMessage() {
        String result = "";
        for (String item : items) {
            result = result + item + "\n";
        }
        return result;
    }
}
