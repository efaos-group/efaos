/**
 * 
 */
package it.polimi.client.view.cli.command;

import it.polimi.client.view.cli.questioner.Console;
import it.polimi.client.view.cli.questioner.Questioner;
import it.polimi.client.view.exception.ItemNotFoundException;
import it.polimi.server.controller.Controller;

import java.util.List;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class AttackCardCommand extends Command {

    private Questioner questioner;
    private List<String> items;
    
    public AttackCardCommand(String playerID, Controller controller, Questioner questioner, List<String> items) {
        super(playerID, controller);
        this.items = items;
        this.questioner = questioner;
    }

    @Override
    public void execute(String answer) {
        if (items.contains(Questioner.ATTACK_CARD)) {
            controller.useCard(playerID, Questioner.ATTACK_CARD," "," ");
        } else {
            questioner.write(Console.ATTACK_CARD_ERROR);
            throw new ItemNotFoundException(Console.ATTACK_CARD_ERROR);
        }
    }

}
