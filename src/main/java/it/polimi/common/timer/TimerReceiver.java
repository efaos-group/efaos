package it.polimi.common.timer;

public interface TimerReceiver {

    void endGameTimer();
}
