/**
 *
 */
package it.polimi.common.timer;

import java.util.Timer;
import java.util.TimerTask;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 *
 */
public class GameTimer {

    private final TimerReceiver receiver;
    private final Timer timer;

    public GameTimer(TimerReceiver receiver) {
        this.timer = new Timer();
        this.receiver = receiver;
    }

    public void stop(){
        timer.cancel();
        timer.purge();
    }

    public void startGameCountDown(Integer delay){
        timer.schedule(new RemindGameStartTask(), delay);
    }

    private class RemindGameStartTask extends TimerTask {
        @Override
        public void run() {
            receiver.endGameTimer();
            timer.cancel();
        }
    }

}
