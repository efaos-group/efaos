/**
 *
 */
package it.polimi.common.map;


/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public enum MapType {
    GALVANI("/map/GalvaniMap.conf"),
    GALILEI("/map/GalileiMap.conf"),
    FERMI("/map/FermiMap.conf");

    private final String path;

    private MapType(String path) {
        this.path = path;
    }

    public String getPath() {
        return this.path;
    }

    public static MapType parseInput(String input) {
        return Enum.valueOf(MapType.class, input.toUpperCase());
    }
}
