/**
 *
 */
package it.polimi.common.observer;

/**
 * The Interface Observer.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public interface Observer {

    /**
     * Notify.
     *
     * @param source        the source
     * @param gameEvent     the game event
     */
    void notify(Observable source, ModelEvent gameEvent);
}
