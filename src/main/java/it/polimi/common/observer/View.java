/**
 *
 */
package it.polimi.common.observer;

import java.util.Collection;






/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public interface View extends Observer {

    public void choiceAccepted(String msg);

    public void showCardDialog(String msg);

    public void showEventDialog(String msg);

    public void showStartTurnDialog(String msg, String playerID);

    public void showStartGameEvent(String msg, String character, Collection<String> players);
    
    public void showNoiseInSectorDialog(String msg);

    public void showErrorDialog(String msg);

    public void showEndGameDialog(String msg);
    
    public void showPictureDialog(String msg, String card);

    public void enableMovement();

    public void enableItemUsage();

    public void enableAttack();

    public void enableDrawSectorCard();

    public void enableDiscard();

    public void enableEndTurn();

    public void enableAttackCard();

    public void enableSelectSector();

    public void enableDrawItemCard();

    public void askForCommand();

    public void askForPlayerID();

    public void setCharacterBehaviour(String character);

    public void setPosition(String position);

    public void addNewItemCard(String card);

    public void removeUsedItemCard(String card);

    public void blockEscapeSector(String position);

    public void disableAll();

    public String getPlayerID();

    public void askForMap();

    public void sendPlayerChoice();

}
