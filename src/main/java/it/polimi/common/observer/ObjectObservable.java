/**
 *
 */
package it.polimi.common.observer;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 *
 */
public class ObjectObservable implements Observable {
    private final Set<Observer> observers;
    private boolean changed;

    public ObjectObservable() {
        this.observers = new HashSet<Observer>();
        this.changed = false;
    }
    
    @Override
    public void register(Observer obs) {
        observers.add(obs);
    }

    protected final void notify(ModelEvent gameEvent) {
        for (Observer obs : observers) {
            obs.notify(this, gameEvent);
        }
    }

    public int countObservers() {
        return observers.size();
    }

    public void dispatchObserver(Observer victim) {
        observers.remove(victim);
    }

    public void deleteObservers() {
        observers.clear();
    }

    public boolean hasChanged() {
        return changed;
    }

    protected void setChanged() {
        changed = true;
    }

    protected void clearChanged() {
        changed = false;
    }

}
