package it.polimi.common.observer;

import java.io.Serializable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public abstract class ModelEvent implements Serializable {
    private static final long serialVersionUID = 2649541886640035264L;

    private final String message;
    private final String playerID;

    public ModelEvent(String message, String playerID) {
        this.message = message;
        this.playerID = playerID;
    }

    public String getPlayerID() {
        return playerID;
    }

    public String getMessage() {
        return message;
    }

    public abstract void reactToView(View view);

}
