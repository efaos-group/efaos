/**
 * 
 */
package it.polimi.common.observer;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 * 
 */
public interface Observable {
    void register(Observer obs);
}