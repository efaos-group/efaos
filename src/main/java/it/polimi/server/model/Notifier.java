/**
 *
 */
package it.polimi.server.model;

import it.polimi.common.observer.ObjectObservable;

import java.util.Collection;

/**
 * The Class Notifier.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public abstract class Notifier extends ObjectObservable implements CardListener, MapTableListener, PlayerListener{

    protected Player gamer;

    /**
     * Notifies a dangerous movement.
     *
     * @param sector the sector
     */
    public abstract void notifyDangerousMovement(Sector sector);

    /**
     * Notifies a secure movement.
     *
     * @param sector the sector
     */
    public abstract void notifySecureMovement(Sector sector);

    /**
     * Notifies the starting of the game.
     *
     * @param players the players
     */
    public abstract void notifyStartGame(Collection<Player> players);

    /**
     * Notifies the human win.
     */
    public abstract void notifyHumanWin();

    /**
     * Notifies the alien win.
     */
    public abstract void notifyAlienWin();

    /**
     * Notifies a game over.
     */
    public abstract void notifyGameOver();

    /**
     * Notifies disconnection of a player.
     *
     * @param playerID the player id
     */
    public abstract void notifyDisconnection(String playerID);
}
