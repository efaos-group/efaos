package it.polimi.server.model;

import java.util.Collection;

/**
 * The listener interface for receiving character events.
 * The class that is interested in processing a character
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addCharacterListener<code> method. When
 * the character event occurs, that object's appropriate
 * method is invoked.
 *
 * @see CharacterEvent
 */
public interface CharacterListener {

    /**
     * Notifies death of the character.
     */
    public void notifyDeath();

    /**
     * Notifies the usage of the defense card.
     */
    public void usedDefense();

    /**
     * Notifies of an attack action.
     *
     * @param characterAttacked the character attacked
     */
    public void notifyAttack(Collection<Player> characterAttacked);
}
