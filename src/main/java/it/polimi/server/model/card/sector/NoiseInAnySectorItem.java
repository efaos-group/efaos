/**
 *
 */
package it.polimi.server.model.card.sector;

import it.polimi.server.model.GameState;
import it.polimi.server.model.Player;
import it.polimi.server.model.SectorDeck;
import it.polimi.server.model.game.state.NoiseInSectorItemState;

/**
 * The Class NoiseInAnySectorItem.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class NoiseInAnySectorItem extends NoiseInAnySectorCard {

    /**
     * Instantiates a new noise in any sector item.
     *
     * @param sectorDeck the sector deck
     */
    public NoiseInAnySectorItem(SectorDeck sectorDeck) {
        super(sectorDeck);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState use(Player gamer) {
        discard();
        fireChooseNoiseSectorEvent();
        return new NoiseInSectorItemState(gamer, this);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public String toString() {
        return "NOISE_IN_ANY_SECTOR_ITEM";
    }

}
