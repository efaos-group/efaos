/**
 *
 */
package it.polimi.server.model.card.item;

import it.polimi.server.model.ItemCard;
import it.polimi.server.model.ItemDeck;
import it.polimi.server.model.MapTable;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.event.card.item.SpotlightCardEvent;
import it.polimi.server.model.event.player.DiscardedItemCardEvent;

/**
 * The Class SpotlightCard.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class SpotlightCard extends ItemCard {

    /**
     * Instantiates a new spotlight card.
     *
     * @param deck the deck
     */
    public SpotlightCard(ItemDeck deck) {
        super(deck);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean isSpotlightCard() {
        return true;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void use(Player player, Sector sector) {
        MapTable map = player.getMap();

        fireCardEvent(new SpotlightCardEvent(player, map.spotlight(sector), this));
        parentDeck.discarded(this);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void discard(String playerID) {
        parentDeck.discarded(this);
        fireCardEvent(new DiscardedItemCardEvent(playerID, this));
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public String toString() {
        return "Spotlight Card";
    }
}
