/**
 *
 */
package it.polimi.server.model.card.item;

import it.polimi.server.model.ItemCard;
import it.polimi.server.model.ItemDeck;
import it.polimi.server.model.MapTable;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.event.card.item.TeleportCardEvent;
import it.polimi.server.model.event.player.DiscardedItemCardEvent;

/**
 * The Class TeleportCard.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class TeleportCard extends ItemCard {

    /**
     * Instantiates a new teleport card.
     *
     * @param deck the deck
     */
    public TeleportCard(ItemDeck deck) {
        super(deck);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void use(Player player, Sector sector) {
        MapTable map = player.getMap();

        map.teleport(player);
        Sector humanSector = map.getHumanSector();

        fireCardEvent(new TeleportCardEvent(player,this,humanSector));
        parentDeck.discarded(this);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void discard(String playerID) {
        parentDeck.discarded(this);
        fireCardEvent(new DiscardedItemCardEvent(playerID, this));
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public String toString() {
        return "Teleport Card";
    }
}
