/**
 *
 */
package it.polimi.server.model.card.sector;

import it.polimi.server.model.SectorCard;
import it.polimi.server.model.SectorDeck;
import it.polimi.server.model.SectorDeck.SectorCardType;
import it.polimi.server.model.exception.BadCardException;

/**
 * A factory for creating SectorCard objects.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class SectorCardFactory {

    private SectorCardFactory() {
    }

    /**
     * Factory.
     *
     * @param type          the type of the card
     * @param sectorDeck    the sector deck where will be placed the card
     * @return the sector card
     */
    public static SectorCard factory(SectorCardType type, SectorDeck sectorDeck) {

        switch(type) {
            case NOISE_IN_ANY_SECTOR:
                return new NoiseInAnySectorCard(sectorDeck);
            case NOISE_IN_YOUR_SECTOR:
                return new NoiseInYourSectorCard(sectorDeck);
            case SILENCE:
                return new SilenceCard(sectorDeck);
            case NOISE_IN_ANY_SECTOR_ITEM:
                return new NoiseInAnySectorItem(sectorDeck);
            case NOISE_IN_YOUR_SECTOR_ITEM:
                return new NoiseInYourSectorItem(sectorDeck);
            default:
                break;
        }

        throw new BadCardException("couldn't create sector card");
    }
}
