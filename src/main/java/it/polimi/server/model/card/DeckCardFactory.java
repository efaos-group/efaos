package it.polimi.server.model.card;

import it.polimi.server.model.DeckFactory;
import it.polimi.server.model.EscapeDeck;
import it.polimi.server.model.ItemDeck;
import it.polimi.server.model.SectorDeck;
import it.polimi.server.model.card.escape.BaseEscapeDeck;
import it.polimi.server.model.card.item.BaseItemDeck;
import it.polimi.server.model.card.sector.BaseSectorDeck;

public class DeckCardFactory implements DeckFactory {

    @Override
    public EscapeDeck getEscapeDeckInstance() {
            return new BaseEscapeDeck();
    }

    @Override
    public ItemDeck getItemDeckInstance() {
        return new BaseItemDeck();
    }
    
    @Override
    public SectorDeck getSectorDeckInstance() {
        return new BaseSectorDeck();
    }
}
