/**
 *
 */
package it.polimi.server.model.card.item;

import it.polimi.server.model.ItemCard;
import it.polimi.server.model.ItemDeck;
import it.polimi.server.model.exception.BadCardException;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * The Class BaseItemDeck.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class BaseItemDeck implements ItemDeck {

    /** The deck. */
    private final List<ItemCard> deck;

    /** The discard pile. */
    private final List<ItemCard> discarded;

    /** The Constant MAX_ITEMS for each type. */
    private static final int MAX_ITEMS = 2;

    /**
     * Instantiates a new base item deck.
     */
    public BaseItemDeck() {

        this.deck = new LinkedList<ItemCard>();
        this.discarded = new LinkedList<ItemCard>();
        initializeDeck();
    }

    /**
     * Initializes the deck.
     */
    private void initializeDeck() {
        populateDiscardedDeck();
        changeCurrentDeck();
    }

    /**
     * Populates discard deck.
     */
    private void populateDiscardedDeck(){
        for (ItemType item: ItemType.values()) {
            for (int i = 0; i < MAX_ITEMS; i++) {
                discarded.add(ItemFactory.factory(item, this));
            }
        }
    }

    /**
     * Draws a random Card IF there are some available in the Deck.
     *
     * @return ItemCard
     *
     * @throws BadItemException if there aren't cards in the deck
     */
    @Override
    public ItemCard draw() {

        if ( deck.isEmpty() ) {
            changeCurrentDeck();
            if ( deck.isEmpty()) {
                throw new BadCardException("The ItemDeck is empty");
            }
        }

        return deck.remove(0);
    }

    private synchronized void changeCurrentDeck() {
        deck.addAll(discarded);
        discarded.clear();
        Collections.shuffle(deck);
    }

    /**
     * Checks if there are already too many item card likes {card} in the deck.
     *
     * @param card the card
     * @return true, if successful
     */
    private boolean canAdd(ItemCard card) {
        int cardCounter = 0;

        for(ItemCard itemCard: deck) {
            if(itemCard.equals(card)) {
                cardCounter++;
            }
        }

        return cardCounter < MAX_ITEMS;
    }

    /**
     * Adds the discarded card to the Deck.
     *
     * @param card the card
     */
    @Override
    public void discarded(ItemCard card) {

        if(canAdd(card)) {
            discarded.add(card);
            return;
        }
        throw new BadCardException("Cannot add the card to Deck");
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public ItemCard generateCardFromString(String cardType) {
        return ItemFactory.factory(ItemType.parseInput(cardType), this);
    }

}
