/**
 *
 */
package it.polimi.server.model.card.item;

import it.polimi.server.model.ItemCard;
import it.polimi.server.model.ItemDeck;
import it.polimi.server.model.ItemDeck.ItemType;
import it.polimi.server.model.exception.BadCardException;

/**
 * A factory for creating Item objects.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public final class ItemFactory {

    /**
     * Instantiates a new item factory.
     */
    private ItemFactory(){
    }

    /**
     * Creates a new ItemCard from the given {type}.
     *
     * @param type the type
     * @param deck the deck
     * @return the item card
     */
    public static ItemCard factory(ItemType type, ItemDeck deck) {

        switch(type) {
            case ADRENALINE:
                return new AdrenalineCard(deck);
            case ATTACK:
                return new AttackCard(deck);
            case DEFENSE:
                return new DefenseCard(deck);
            case SEDATIVES:
                return new SedativesCard(deck);
            case SPOTLIGHT:
                return new SpotlightCard(deck);
            case TELEPORT:
                return new TeleportCard(deck);
            default:
                break;
        }

        throw new BadCardException("Couldn't create Item");
    }

}
