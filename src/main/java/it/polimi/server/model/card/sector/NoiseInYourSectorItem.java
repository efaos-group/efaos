/**
 *
 */
package it.polimi.server.model.card.sector;

import it.polimi.server.model.GameState;
import it.polimi.server.model.Player;
import it.polimi.server.model.SectorDeck;
import it.polimi.server.model.game.state.NoiseInSectorItemState;

/**
 * The Class NoiseInYourSectorItem.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class NoiseInYourSectorItem extends NoiseInYourSectorCard {

    /**
     * Instantiates a new noise in your sector item.
     *
     * @param sectorDeck the sector deck
     */
    public NoiseInYourSectorItem(SectorDeck sectorDeck) {
        super(sectorDeck);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState use(Player gamer) {
        discard();
        GameState state = new NoiseInSectorItemState(gamer, this);
        return state.notifyNoise(gamer.getCharacterPosition());
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public String toString() {
        return "NOISE_IN_YOUR_SECTOR_ITEM";
    }
}
