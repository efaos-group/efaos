/**
 *
 */
package it.polimi.server.model.card.escape;

import it.polimi.server.model.EscapeCard;
import it.polimi.server.model.EscapeDeck;
import it.polimi.server.model.exception.BadCardException;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * The Class BaseEscapeDeck.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class BaseEscapeDeck implements EscapeDeck {

    private static final Integer MAX_ESCAPE_CARDS = 3;
    private List<EscapeType> deck;

    /**
     * Instantiates a new base escape deck.
     */
    public BaseEscapeDeck() {
        this.deck = new LinkedList<EscapeType>();
        initializeDeck();
    }

    private void initializeDeck() {

        for (EscapeType card: EscapeType.values()) {
            for (int i = 0; i < MAX_ESCAPE_CARDS; i++) {
                deck.add(card);
            }
        }
        Collections.shuffle(deck);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public EscapeCard draw() {

        if ( deck.isEmpty() ) {
            throw new BadCardException("The EscapeDeck is empty");
        }
        EscapeType type = deck.remove(0);

        return EscapeFactory.factory(type);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public EscapeCard generateRedCard() {
        return EscapeFactory.factory(EscapeType.RED);
    }

}
