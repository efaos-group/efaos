/**
 *
 */
package it.polimi.server.model.card.item;

import it.polimi.server.model.ItemCard;
import it.polimi.server.model.ItemDeck;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.event.card.item.ItemUsedEvent;
import it.polimi.server.model.event.player.DiscardedItemCardEvent;

/**
 * The Class SedativesCard.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class SedativesCard extends ItemCard {

    /**
     * Instantiates a new sedatives card.
     *
     * @param deck the deck
     */
    public SedativesCard(ItemDeck deck) {
        super(deck);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void use(Player player, Sector sector) {
        player.sedate();

        fireCardEvent(new ItemUsedEvent(player, this));
        parentDeck.discarded(this);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void discard(String playerID) {
        parentDeck.discarded(this);
        fireCardEvent(new DiscardedItemCardEvent(playerID, this));
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public String toString() {
        return "Sedatives Card";
    }
}
