/**
 *
 */
package it.polimi.server.model.card.sector;

import it.polimi.server.model.GameState;
import it.polimi.server.model.Player;
import it.polimi.server.model.SectorCard;
import it.polimi.server.model.SectorDeck;
import it.polimi.server.model.game.state.AfterMovementState;

/**
 * The Class SilenceCard.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class SilenceCard extends SectorCard {

    /**
     * Instantiates a new silence card.
     *
     * @param sectorDeck the sector deck
     */
    public SilenceCard(SectorDeck sectorDeck) {
        super(sectorDeck);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState use(Player gamer) {
        discard();
        fireSectorSilenceEvent();
        return new AfterMovementState(gamer);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public String toString() {
        return "SILENCE";
    }

}
