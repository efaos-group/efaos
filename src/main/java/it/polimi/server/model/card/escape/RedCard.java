/**
 *
 */
package it.polimi.server.model.card.escape;

import it.polimi.server.model.EscapeCard;

/**
 * The Class RedCard.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class RedCard extends EscapeCard {

    /**
     * Instantiates a new red card.
     */
    public RedCard() {
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean canEscape() {
        return false;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && obj instanceof RedCard;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public int hashCode() {
        return super.hashCode() + "REDCARD".hashCode();
    }

}
