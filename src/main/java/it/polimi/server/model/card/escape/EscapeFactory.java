/**
 *
 */
package it.polimi.server.model.card.escape;

import it.polimi.server.model.EscapeCard;
import it.polimi.server.model.EscapeDeck.EscapeType;
import it.polimi.server.model.exception.BadCardException;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class EscapeFactory {

    private EscapeFactory() {
    }

    /**
     * Creates an escape card from the given {type}.
     *
     * @param type the type
     * @return the escape card
     *
     * @throws BadCardException if the given type can't be recognized.
     */
    public static EscapeCard factory(EscapeType type) {

        switch(type) {
            case RED:
                return new RedCard();
            case GREEN:
                return new GreenCard();
            default:
                break;
        }

        throw new BadCardException("couldn't create escape card");
    }
}
