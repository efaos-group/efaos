/**
 *
 */
package it.polimi.server.model.card.item;

import it.polimi.server.model.BaseCharacter;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.ItemDeck;
import it.polimi.server.model.MapTable;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.event.card.item.AttackCardEvent;
import it.polimi.server.model.event.player.DiscardedItemCardEvent;

import java.util.Collection;
import java.util.LinkedList;

/**
 * The Class AttackCard.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class AttackCard extends ItemCard {

    /**
     * Instantiates a new attack card.
     *
     * @param deck the deck
     */
    public AttackCard(ItemDeck deck) {
        super(deck);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean isAttackCard() {
        return true;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void use(Player player, Sector sector) {
        Collection<Player> players = AttackCard.use(player.getCharacter(), player.getMap());

        fireCardEvent(new AttackCardEvent(player, players, player.getCharacterPosition(), this));
        parentDeck.discarded(this);
    }

    /**
     * Performs an attack in the {owner}'s position.
     * This method is static so it could be used also by AlienCharacter.
     *
     * @param owner the owner
     * @param map the map
     * @return the collection
     */
    public static Collection<Player> use(BaseCharacter owner, MapTable map){

        Collection<Player> players = map.getCharacters(owner.getPosition());
        players = clone(players);
        removePlayer(owner, players);
        killAll(players);
        return players;
    }

    private static Collection<Player> clone(Collection<Player> p) {
        Collection<Player> players = new LinkedList<Player>();
        players.addAll(p);
        return players;
    }

    /**
     * Kills all characters in the list.
     *
     * @param players the players
     */
    private static void killAll(Collection<Player> players){

        BaseCharacter character;
        for (Player player : players) {
            character = player.getCharacter();
            character.die();
        }
    }

    /**
     * Removes the player from the list.
     *
     * @param owner the owner
     * @param players the players
     */
    private static void removePlayer(BaseCharacter owner, Collection<Player> players) {
        BaseCharacter character;
        for (Player player : players) {
            character = player.getCharacter();
            if(character.equals(owner)){
                players.remove(player);
                return;
            }
        }
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void discard(String playerID) {
        parentDeck.discarded(this);
        fireCardEvent(new DiscardedItemCardEvent(playerID, this));
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public String toString() {
        return "Attack Card";
    }

}
