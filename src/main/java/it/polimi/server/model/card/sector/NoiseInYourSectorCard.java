/**
 *
 */
package it.polimi.server.model.card.sector;

import it.polimi.server.model.GameState;
import it.polimi.server.model.Player;
import it.polimi.server.model.SectorCard;
import it.polimi.server.model.SectorDeck;
import it.polimi.server.model.game.state.NoiseInSectorState;

/**
 * The Class NoiseInYourSectorCard.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class NoiseInYourSectorCard extends SectorCard {

    /**
     * Instantiates a new noise in your sector card.
     *
     * @param sectorDeck the sector deck
     */
    public NoiseInYourSectorCard(SectorDeck sectorDeck) {
        super(sectorDeck);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState use(Player gamer) {
        discard();
        GameState state = new NoiseInSectorState(gamer,this);
        return state.notifyNoise(gamer.getCharacterPosition());
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public String toString() {
        return "NOISE_IN_YOUR_SECTOR";
    }
}
