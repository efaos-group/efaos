/**
 *
 */
package it.polimi.server.model.card.escape;

import it.polimi.server.model.EscapeCard;

/**
 * The Class GreenCard.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class GreenCard extends EscapeCard {

    /**
     * Instantiates a new green card.
     */
    public GreenCard() {

    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean canEscape() {
        return true;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && obj instanceof GreenCard;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public int hashCode() {
        return super.hashCode() + "GREENCARD".hashCode();
    }
}
