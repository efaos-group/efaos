/**
 *
 */
package it.polimi.server.model.card.item;

import it.polimi.server.model.ItemCard;
import it.polimi.server.model.ItemDeck;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.event.card.item.DefenseCardEvent;
import it.polimi.server.model.event.player.DiscardedItemCardEvent;

/**
 * The Class DefenseCard.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class DefenseCard extends ItemCard {

    /**
     * Instantiates a new defense card.
     *
     * @param deck the deck
     */
    public DefenseCard(ItemDeck deck) {
        super(deck);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void use(Player player, Sector sector) {

        fireCardEvent( new DefenseCardEvent(player, this));
        parentDeck.discarded(this);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void discard(String playerID) {
        parentDeck.discarded(this);
        fireCardEvent(new DiscardedItemCardEvent(playerID, this));
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean isDefenseCard() {
        return true;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public String toString() {
        return "Defense Card";
    }
}
