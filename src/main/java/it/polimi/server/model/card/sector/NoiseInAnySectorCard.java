/**
 *
 */
package it.polimi.server.model.card.sector;

import it.polimi.server.model.GameState;
import it.polimi.server.model.Player;
import it.polimi.server.model.SectorCard;
import it.polimi.server.model.SectorDeck;
import it.polimi.server.model.game.state.NoiseInSectorState;

/**
 * The Class NoiseInAnySectorCard.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class NoiseInAnySectorCard extends SectorCard {

    /**
     * Instantiates a new noise in any sector card.
     *
     * @param sectorDeck the sector deck
     */
    public NoiseInAnySectorCard(SectorDeck sectorDeck) {
        super(sectorDeck);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState use(Player gamer) {
        discard();
        fireChooseNoiseSectorEvent();
        return new NoiseInSectorState(gamer, this);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public String toString() {
        return "NOISE_IN_ANY_SECTOR";
    }
}
