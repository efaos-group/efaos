/**
 *
 */
package it.polimi.server.model.card.sector;

import it.polimi.server.model.SectorCard;
import it.polimi.server.model.SectorDeck;
import it.polimi.server.model.exception.BadCardException;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * The Class BaseSectorDeck.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class BaseSectorDeck implements SectorDeck {

    /** The deck. */
    private final List<SectorCard> deck;

    /** The discarded card. */
    private final List<SectorCard> discarded;

    /**
     * Instantiates a new base sector deck.
     */
    public BaseSectorDeck() {
        this.deck = new LinkedList<SectorCard>();
        this.discarded = new LinkedList<SectorCard>();

        initializeDeck();
    }

    /**
     * Initializes the deck.
     */
    private void initializeDeck() {
        populateDiscardedDeck();
        changeCurrentDeck();
    }

    /**
     * Populates discard deck.
     */
    private void populateDiscardedDeck(){
        for (SectorCardType card: SectorCardType.values()) {
            for (int i = 0; i < card.getMaxCard(); i++) {
                discarded.add(SectorCardFactory.factory(card, this));
            }
        }
    }

    /**
     * Swaps current deck with the discard deck.
     */
    private synchronized void changeCurrentDeck() {
        deck.addAll(discarded);
        discarded.clear();
        Collections.shuffle(deck);
    }

    /**
     * Evaluates if a card can be added.
     *
     * @param card the card
     * @return true, if successful
     */
    private boolean canAdd(SectorCard card) {
        int cardCounter = 0;

        for(SectorCard sectorCard: deck) {
            if(sectorCard.getClass() == card.getClass()) {
                cardCounter++;
            }
        }

        return cardCounter < SectorCardType.parseInput(card.toString()).getMaxCard();
    }

    /**
      * {@inheritDoc}
      *
      * @throws BadCardException if both decks are empty
      */
    @Override
    public SectorCard draw() {

        if ( deck.isEmpty() ) {
            changeCurrentDeck();
            if ( deck.isEmpty()) {
                throw new BadCardException("The ItemDeck is empty");
            }
        }

        return deck.remove(0);
    }

    /**
      * {@inheritDoc}
      *
      * @throws BadCardException if there are already enough cards like {card} in the discard deck
      */
    @Override
    public void discarded(SectorCard card) {

        if(canAdd(card)) {
            discarded.add(card);
            return;
        }
        throw new BadCardException("Cannot add the card to sector Deck");
    }

}
