/**
 *
 */
package it.polimi.server.model;

/**
 * The Class CharacterMovementState.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class CharacterMovementState {
    private final MapTable map;

    /**
     * Instantiates a new character movement state.
     *
     * @param map the map
     */
    public CharacterMovementState(MapTable map) {
        this.map = map;
    }

    public MapTable getMap() {
        return map;
    }

    /**
     * Check if this character can move to {to}.
     *
     * @param to the to
     * @return true, if successful
     */
    public boolean canMove(Sector position, Sector to) {

        if (!to.isWalkable()) {
            return false;
        } else if (position.areAdjacents(to)) {
            return true;
        }
        return false;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public int hashCode() {
        return 31;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof CharacterMovementState)) {
            return false;
        }

        CharacterMovementState other = (CharacterMovementState) obj;
        if (map == null) {
            return other.map == null;
        } else {
            return map.equals(other.map);
        }
    }
}
