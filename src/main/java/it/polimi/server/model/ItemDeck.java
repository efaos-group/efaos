/**
 *
 */
package it.polimi.server.model;

import java.util.StringTokenizer;

/**
 * The Interface ItemDeck.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public interface ItemDeck {

    /**
     * The Enum ItemType.
     */
    public static enum ItemType {
        ATTACK("ATTACK CARD"), TELEPORT("TELEPORT CARD"), SEDATIVES("SEDATIVES CARD"),
        SPOTLIGHT("SPOTLIGHT CARD"), DEFENSE("DEFENSE CARD"), ADRENALINE("ADRENALINE CARD");

        private final String type;
        private ItemType(String type){
            this.type = type;
        }

        /**
          * {@inheritDoc}
          *
          */
        @Override
        public String toString() {
            return type;
        }

        /**
         * Parses the input.
         *
         * @param input the input
         * @return the item type
         */
        public static ItemType parseInput(String input) {
            StringTokenizer value = new StringTokenizer(input);
            return Enum.valueOf(ItemType.class, value.nextToken().toUpperCase());
        }
    }

    /**
     * Draws an item card.
     *
     * @return the item card
     */
    public ItemCard draw();

    /**
     * Notifies that an item card has been discarded and adds it to the discard deck.
     *
     * @param card the card
     */
    public void discarded(ItemCard card);

    /**
     * Generates card from string.
     *
     * @param cardType the card type
     * @return the item card
     */
    public ItemCard generateCardFromString(String cardType);
}
