/**
 *
 */
package it.polimi.server.model;

import it.polimi.server.model.exception.BadGameTransactionException;

import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.logging.Logger;

/**
 * The Class GameLogic.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public abstract class GameLogic implements SectorListener, GameClearer {

    private static final Logger LOGGER = Logger.getLogger(GameLogic.class.getName());

    /** The Constant ERROR_MESSAGE. */
    protected static final String ERROR_MESSAGE = " tried to perform an illegal action ";

    /** The Constant MAX_TURN. */
    protected final Integer maxTurn;

    /** The turn order. */
    protected final Queue<Player> turn;

    /** The players. */
    protected final Map<String, Player> players;

    /** The map. */
    protected final MapTable map;

    /** The rounds count. */
    protected Integer rounds;

    /** The notifier. */
    protected final Notifier notifier;

    /** The current player. */
    protected Player gamer;

    /** The state of the game. */
    protected GameState state;

    /**
     * Instantiates a new game logic.
     *
     * @param map the map
     * @param players the players
     * @param notifier the notifier
     */
    public GameLogic(MapTable map, Map<String, Player> players, Notifier notifier) {

        this.map = map;

        this.players = players;
        this.maxTurn = players.size() * 39;
        this.turn = new LinkedList<Player>();

        this.notifier = notifier;

        initGame();
    }

    /**
     * Initializes the game.
     */
    private void initGame(){
        map.setCharacters(players.values());
        turn.addAll(players.values());

        notifier.notifyStartGame(players.values());
        setListeners();

        this.rounds = 0;
    }

    /**
     * Sets the listeners.
     */
    private void setListeners() {
        for(Player pg: players.values()) {
            pg.setPlayerListeners(notifier, this);
        }

        map.setMapTableListener(notifier);
        map.setSectorListener(this);
    }

    /**
     * Starts a new turn.
     */
    protected void startTurn(){
        rounds++;
        if(rounds < maxTurn && !state.isGameEnded()) {
            gamer = turn.poll();
            state = state.startTurn(gamer);
        } else {
            state = state.endGame();
            notifier.notifyGameOver();
        }
    }

    /**
     * Passes turn.
     */
    public void passTurn(){
        state = state.endTurn();

        turn.add(gamer);
        startTurn();
    }

    /**
     * Moves the {player}.
     *
     * @param to the to
     */
    public void move(Sector to){
        try {
            state.move(to, map);
        } catch(BadGameTransactionException e) {
            LOGGER.fine(gamer.getUsername() + ERROR_MESSAGE + e);
        }
    }

    /**
     * Uses the given card.
     *
     * @param card the card
     * @param sector the sector
     */
    public void useCard(ItemCard card, Sector sector) {
        try {
            card.addCardListener(notifier);
            state = state.useCard(card, sector);
        } catch(BadGameTransactionException e) {
            LOGGER.fine(gamer.getUsername() + ERROR_MESSAGE + e);
        }
    }

    /**
     * Performs an attack action.
     */
    public void attack() {
        try {
            state = state.attack();
        } catch(BadGameTransactionException e) {
            LOGGER.fine(gamer.getUsername() + ERROR_MESSAGE + e);
        }
    }

    /**
     * Performs a noise in the selected sector.
     *
     * @param sector the sector
     */
    public void selectSectorForNoise(Sector sector){
        try {
            state = state.notifyNoise(sector);
        } catch(BadGameTransactionException e) {
            LOGGER.fine(gamer.getUsername() + ERROR_MESSAGE + e);
        }
    }

    /**
     * Gets the currently player's identifier.
     *
     * @return the player identifier
     */
    public String getGamerIdentifier() {
        return gamer.getUsername();
    }

    /**
     * Gets the sector.
     *
     * @param col the col
     * @param row the row
     * @return the sector
     */
    public Sector getSector(Character col, Integer row) {
        return map.getSector(col, row);
    }

    /**
     * Gets the item card.
     *
     * @param cardType the card type
     * @return the item card
     */
    public abstract ItemCard getItemCard(String cardType);

    /**
     * Makes the player draw a sector card.
     */
    public abstract void drawSector();

    /**
     * Makes the player draw an item card.
     */
    public abstract void drawItemCard();

    /**
     * Discards this card.
     *
     * @param card the card
     */
    public abstract void discardCard(ItemCard card);

    /**
     * Removes the disconnected player from game.
     *
     * @param playerID the player id
     */
    public abstract void removeDisconnectedPlayerFromGame(String playerID);
}
