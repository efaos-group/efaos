/**
 *
 */
package it.polimi.server.model;

/**
 * The Class Player.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public abstract class Player implements CharacterListener{

    /** The Constant MAX_ITEMS. */
    public static final int MAX_ITEMS = 3;

    /** The username. */
    protected final String username;

    /** The character. */
    protected final BaseCharacter character;

    /** The map. */
    protected final MapTable map;

    /** The listener. */
    protected PlayerListener listener;

    /** The game clearer. */
    protected GameClearer gameClearer;


    /**
     * Instantiates a new player.
     *
     * @param username the player's username
     * @param character the character
     * @param map the map
     */
    public Player(String username, BaseCharacter character,  MapTable map){
        this.username = username;
        this.character = character;
        this.character.setParent(this);
        this.map = map;
    }

    /**
     * Moves character to {to}.
     *
     * @param to the to
     */
    public void moveCharacterTo(Sector to) {
        character.moveTo(to);
    }

    /**
     * Makes the character escape.
     */
    public void escape() {
        map.removeEscapedCharacter(character);
        listener.notifyHumanEscaped(character.getPosition());
        gameClearer.deleteEscapedPlayer(username);
    }

    /**
     * Makes the character fail to escape.
     */
    public void notEscape() {
        listener.notifyHumanNotEscaped(getCharacterPosition());
    }

    /**
     * Checks if the character has been sedated.
     *
     * @return true, if it is sedated
     */
    public boolean isSedated() {
        return character.isSedated();
    }

    /**
     * Sedates the character.
     */
    public void sedate() {
        character.sedate();
    }

    /**
     * Attacks.
     */
    public void attack(){
        character.attack();
    }

    /**
     * Resets character status.
     */
    protected void resetCharacterStatus() {
        character.resetStatus();
    }

    /**
     * Starts a new turn.
     */
    public void startTurn() {
        listener.notifyStartTurn(this);
    }

    /**
     * Ends player's turn.
     */
    public void endTurn() {
        resetCharacterStatus();
        listener.notifyEndTurn();
    }

    /**
     * Gets the username.
     *
     * @return the username
     */
    public String getUsername(){
        return username;
    }

    /**
     * Gets the character.
     *
     * @return the character
     */
    public BaseCharacter getCharacter() {
        return character;
    }

    /**
     * Gets the map.
     *
     * @return the map
     */
    public MapTable getMap(){
        return map;
    }

    /**
     * Gets the character position.
     *
     * @return the character position
     */
    public Sector getCharacterPosition(){
        return character.getPosition();
    }

    /**
     * Sets the player listeners.
     *
     * @param listener the listener
     * @param gameClearer the game clearer
     */
    public void setPlayerListeners(PlayerListener listener, GameClearer gameClearer){
        this.listener = listener;
        this.gameClearer = gameClearer;
    }

    /**
     * Gets the items size.
     *
     * @return the items size
     */
    public abstract int getItemsSize();

    /**
     * Draws sector card.
     *
     * @param card the card
     */
    public abstract void drawSectorCard(SectorCard card);

    /**
     * Draws item card.
     *
     * @param card the card
     */
    public abstract void drawItemCard(ItemCard card);

    /**
     * Discards {card}.
     *
     * @param card the card
     */
    public abstract void discard(ItemCard card);

    /**
     * Discards a random card.
     * Called only if the timer triggers.
     */
    public abstract void randomDiscard();

    /**
     * Uses the given item card.
     *
     * @param card the card
     * @param sector the sector
     */
    public abstract void useCard(ItemCard card, Sector sector);

    /**
     * Fires illegal action event.
     */
    public abstract void fireIllegalActionEvent();

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + character.hashCode();
        result = prime * result + username.hashCode();
        return result;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Player)) {
            return false;
        }

        Player other = (Player) obj;

        return character.equals(other.character) && username.equals(other.username);
    }

}
