/**
 *
 */
package it.polimi.server.model.notifier;

import it.polimi.common.observer.ModelEvent;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.Notifier;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.SectorCard;
import it.polimi.server.model.event.card.sector.ChooseNoiseSectorEvent;
import it.polimi.server.model.event.card.sector.NoiseInSectorEvent;
import it.polimi.server.model.event.card.sector.NoiseInSectorItemEvent;
import it.polimi.server.model.event.card.sector.SilenceEvent;
import it.polimi.server.model.event.game.AlienWinningEvent;
import it.polimi.server.model.event.game.DisconnectionEvent;
import it.polimi.server.model.event.game.GameOverEvent;
import it.polimi.server.model.event.game.HumanEscapedEvent;
import it.polimi.server.model.event.game.HumanWinningEvent;
import it.polimi.server.model.event.game.StartGameEvent;
import it.polimi.server.model.event.movement.DangerousMovementEvent;
import it.polimi.server.model.event.movement.IllegalMovementEvent;
import it.polimi.server.model.event.movement.NotEscapedEvent;
import it.polimi.server.model.event.movement.SecureMovementEvent;
import it.polimi.server.model.event.player.DiscardedItemCardEvent;
import it.polimi.server.model.event.player.DrawnItemCardEvent;
import it.polimi.server.model.event.player.DrawnSectorCardEvent;
import it.polimi.server.model.event.player.DrawnTooManyItemCardEvent;
import it.polimi.server.model.event.player.IllegalPlayerActionEvent;
import it.polimi.server.model.event.turn.EndTurnEvent;
import it.polimi.server.model.event.turn.StartTurnEvent;

import java.util.Collection;

/**
 * The Class NotificationHandler.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class NotificationHandler extends Notifier {

    /**
     * General game event notification.
     */

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public void notifyStartGame(Collection<Player> players) {
        super.notify(new StartGameEvent(players));
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void notifyStartTurn(Player gamer) {
        this.gamer = gamer;
        super.notify(new StartTurnEvent(gamer.getUsername()));
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void notifyEndTurn() {
        super.notify(new EndTurnEvent(gamer.getUsername()));
    }

    /**
     * moved into sector notification.
     */

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public void notifyDangerousMovement(Sector sector) {
        super.notify(new DangerousMovementEvent(gamer.getUsername(), sector));
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void notifySecureMovement(Sector sector) {
        super.notify( new SecureMovementEvent(gamer.getUsername(), sector));
    }

    /**
     * Escaping event notification.
     */

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public void notifyHumanNotEscaped(Sector sector) {
        super.notify(new NotEscapedEvent(gamer.getUsername(), sector));
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void notifyHumanEscaped(Sector sector) {
        super.notify(new HumanEscapedEvent(gamer.getUsername(), sector));
    }

    /**
     * ItemCard usage notification.
     */

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public void notifyCardUsed(ModelEvent event) {
        super.notify(event);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void notifyItemCardUsedAfterMove(ModelEvent event) {
        super.notify(event);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void notifyDrawnItemCard(ItemCard card) {
        super.notify(new DrawnItemCardEvent(gamer.getUsername(), card));
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void notifyDrawnTooManyItemCard(ItemCard card) {
        super.notify(new DrawnTooManyItemCardEvent(gamer.getUsername(), card));
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void notifyDiscardedCardEvent(ItemCard card) {
        super.notify(new DiscardedItemCardEvent(gamer.getUsername(), card));
    }

    /**
     * SectorCard drawn notification.
     */

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public void notifyDrawnSectorCard(SectorCard sector) {
        super.notify(new DrawnSectorCardEvent(gamer.getUsername(), sector));
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void notifyChooseSector() {
        super.notify(new ChooseNoiseSectorEvent(gamer.getUsername()));
    }

    /**
     * Noise notification.
     */

    /**
     * {@inheritDoc}
     */
    @Override
    public void notifyNoiseInSector(Sector sector) {
        super.notify(new NoiseInSectorEvent(gamer.getUsername(), sector));
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void notifyNoiseInSectorItem(Sector sector) {
        super.notify(new NoiseInSectorItemEvent(gamer.getUsername(), sector));
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void notifySilence() {
        super.notify(new SilenceEvent(gamer.getUsername()));
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void notifyCharacterEvent(ModelEvent event) {
        super.notify(event);
    }

    /**
     * Winning condition notification.
     */

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public void notifyHumanWin() {
        super.notify(new HumanWinningEvent(gamer.getUsername()));
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void notifyAlienWin() {
        super.notify(new AlienWinningEvent(gamer.getUsername()));
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void notifyGameOver() {
        super.notify(new GameOverEvent());
    }

    /**
     * Notification of error in movement or action.
     */

    /**
     * {@inheritDoc}
     */
    @Override
    public void notifyException(String message) {
        super.notify(new IllegalPlayerActionEvent(gamer.getUsername(), message));
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void notifyIllegalMovementGamer() {
        super.notify(new IllegalMovementEvent(gamer.getUsername()));
    }

    /**
     * Notification of player's disconnection.
     */

    /**
     * {@inheritDoc}
     */
    @Override
    public void notifyDisconnection(String playerID) {
        super.notify(new DisconnectionEvent(playerID));
    }

}
