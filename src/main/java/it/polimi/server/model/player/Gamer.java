/**
 *
 */
package it.polimi.server.model.player;

import it.polimi.server.model.BaseCharacter;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.MapTable;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.SectorCard;
import it.polimi.server.model.event.character.AttackEvent;
import it.polimi.server.model.event.character.CharacterDeathEvent;
import it.polimi.server.model.event.player.IllegalPlayerActionEvent;
import it.polimi.server.model.exception.BadCharacterException;
import it.polimi.server.model.exception.PlayerException;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * The Class Gamer.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class Gamer extends Player {

    /** The item cards' list. */
    protected final List<ItemCard> items;

    /** Random number generator. */
    protected final Random rand = new Random();

    /**
     * Instantiates a new gamer.
     *
     * @param username the username
     * @param character the character
     * @param map the map
     */
    public Gamer(String username, BaseCharacter character, MapTable map) {
        super(username, character, map);
        this.items = new LinkedList<ItemCard>();
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public int getItemsSize() {
        return items.size();
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void notifyDeath() {
        listener.notifyCharacterEvent(new CharacterDeathEvent(username, character.getName()));
        gameClearer.deleteDeadPlayer(username);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void notifyAttack(Collection<Player> characterAttacked) {
        AttackEvent evt = new AttackEvent(username, character.getPosition(), characterAttacked);
        listener.notifyCharacterEvent(evt);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void drawSectorCard(SectorCard card) {
        listener.notifyDrawnSectorCard(card);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void drawItemCard(ItemCard card) {
        items.add(card);
        setDefenseCharacter();

        if(items.size() > MAX_ITEMS){
            listener.notifyDrawnTooManyItemCard(card);
        } else {
            listener.notifyDrawnItemCard(card);
        }
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void discard(ItemCard card) {

        if(items.contains(card)){
            items.remove(card);
            card.discard(username);
            checkDefenseCharacterStatus(card);
            listener.notifyDiscardedCardEvent(card);
        } else {
            throwIllegalAction();
        }
    }

    private void checkDefenseCharacterStatus(ItemCard card) {
        if(card.isDefenseCard()) {
            character.setDefense(false);
            setDefenseCharacter();
        }
    }


    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void randomDiscard() {
        if (items.size() > MAX_ITEMS) {
            int index = rand.nextInt(items.size());
            ItemCard card = items.remove(index);
            card.discard(username);
            listener.notifyDiscardedCardEvent(card);
        }
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void useCard(ItemCard card, Sector sector) {
        if (!character.canUseCard()){
            throw new BadCharacterException("Alien character can't use card!");
        } else if(items.contains(card)){
            items.remove(card);
            card.use(this, sector);
        } else {
            throwIllegalAction();
        }
    }

    /**
     * Throws illegal action.
     */
    private void throwIllegalAction() {
        listener.notifyException("Impossible use a card not owned");
        throw new PlayerException("Impossible use a card not owned");
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void usedDefense() {
        removeDefenseCard();
        setDefenseCharacter();
    }

    /**
     * Removes a defense card from the list.
     */
    private void removeDefenseCard(){
        for(ItemCard c: items) {
            if(c.isDefenseCard()) {
                items.remove(c);
                c.use(this, null);
                return;
            }
        }
    }

    /**
     * Sets up the defense of character if a defense card is in the list.
     */
    private void setDefenseCharacter() {
        for(ItemCard c: items) {
            if(c.isDefenseCard()) {
                character.setDefense(true);
            }
        }
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void fireIllegalActionEvent() {
        listener.notifyCharacterEvent(new IllegalPlayerActionEvent(username, "You can't use this card in this moment"));
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + items.hashCode();
        return result;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof Gamer)) {
            return false;
        }
        Gamer other = (Gamer) obj;
        return items.equals(other.items);
    }

}
