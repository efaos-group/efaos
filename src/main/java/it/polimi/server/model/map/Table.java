/**
 *
 */
package it.polimi.server.model.map;

import it.polimi.server.model.BaseCharacter;
import it.polimi.server.model.MapLoader;
import it.polimi.server.model.MapTable;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

/**
 * The Class Table.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class Table extends MapTable {

    /**
     * Instantiates a new table.
     *
     * @param map the map
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public Table(MapLoader map) throws IOException {
        super(map);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void teleport(Player player) {
        Sector from = player.getCharacterPosition();
        Sector to = constrains.getHumanSector();

        if (map.containsKey(from)) {
            Collection<Player> playersInSector = map.get(from);

            if (playersInSector.contains(player)) {
                swapCharacterPosition(player, to);
                player.getCharacter().teleportTo(to);
            }
        } else {
            notifier.notifyIllegalMovementGamer();
        }
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void blockSector(Sector sector) {
        constrains.blockSector(sector);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean isBlocked(Sector sector) {
        return constrains.isBlocked(sector);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public Multimap<Sector, Player> spotlight(Sector sector) {
        Queue<Sector> sectorSpotted = new LinkedList<Sector>();

        sectorSpotted.add(sector);
        enqueueAllAdjacents(sector,sectorSpotted);

        return spotAllCharacter(sectorSpotted);
    }

    /**
     * Collects all character.
     *
     * @param sectorSpotted the sector spotted
     * @return the multimap
     */
    private Multimap <Sector, Player> spotAllCharacter(Queue<Sector> sectorSpotted) {
        Multimap <Sector, Player> result = ArrayListMultimap.create();
        Sector head;

        while(!sectorSpotted.isEmpty()) {
            head = sectorSpotted.poll();
            result.putAll(head, map.get(head));
        }

        return result;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void removeDeadPlayers(){
        Collection<Player> players = map.values();
        for(Player pg : players) {
            if(!pg.getCharacter().isAlive()){
                map.remove(pg.getCharacterPosition(), pg);
            }
        }
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void removeEscapedCharacter(BaseCharacter character){
        map.remove(character.getPosition(), character);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public Collection<Player> getCharacters(Sector sec) {
        if (map.containsKey(sec)) {
            return map.get(sec);
        } else {
            return new LinkedList<Player>();
        }
    }
}
