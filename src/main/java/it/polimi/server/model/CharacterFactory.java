package it.polimi.server.model;

/**
 * A factory for creating Character objects.
 */
public interface CharacterFactory {

    /**
     * The Enum CharacterType.
     */
    public static enum CharacterType{
        ALIEN, HUMAN
    }

    /**
     * Creates a character from the given {choice}.
     *
     * @param choice the choice
     * @param map the map
     * @return the base character
     */
    public BaseCharacter factory(CharacterType choice, MapTable map);
}
