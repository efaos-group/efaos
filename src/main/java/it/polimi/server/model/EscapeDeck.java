/**
 *
 */
package it.polimi.server.model;

/**
 * The Interface EscapeDeck.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public interface EscapeDeck {

    /**
     * The Enum EscapeType.
     */
    public enum EscapeType {
        RED, GREEN
    }

    /**
     * Draws an escape card from the deck.
     *
     * @return the escape card
     */
    public EscapeCard draw();

    /**
     * Generates a red card not related with the deck.
     *
     * @return the escape card
     */
    public EscapeCard generateRedCard();
}
