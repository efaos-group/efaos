package it.polimi.server.model;


/**
 * The Interface MapLoader.
 */
public interface MapLoader {

    /**
     * Gets the sector from specified {col} and {row}.
     *
     * @param col the column
     * @param row the row
     * @return the sector
     */
    public Sector getSector(Character col, Integer row);

    /**
     * Gets the human sector.
     *
     * @return the human sector
     */
    public Sector getHumanSector();

    /**
     * Gets the alien sector.
     *
     * @return the alien sector
     */
    public Sector getAlienSector();

    /**
     * Sets the sector listener.
     *
     * @param listener the new sector listener
     */
    public void setSectorListener(SectorListener listener);

    /**
     * Stores the {sector} given as blocked.
     *
     * @param sector the sector
     */
    public void blockSector(Sector sector);

    /**
     * Checks if the given {sector} is blocked.
     *
     * @param sector the sector
     * @return true, if is blocked
     */
    public boolean isBlocked(Sector sector);
}