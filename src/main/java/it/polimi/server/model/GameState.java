/**
 *
 */
package it.polimi.server.model;

import it.polimi.server.model.exception.BadGameTransactionException;

/**
 * The Class GameState.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public abstract class GameState {

    /** The ERROR MESSAGE. */
    private static final String ERROR_MESSAGE = "You can't do this action in this State!";

    /** The player who is currently playing. */
    protected final Player gamer;

    /**
     * Instantiates a new game state.
     *
     * @param gamer the player
     */
    public GameState(Player gamer){
        this.gamer = gamer;
    }

    /**
     * Starts a new turn.
     *
     * @param gamer the player
     * @return the game state
     */
    public GameState startTurn(Player gamer) {
        throw new BadGameTransactionException(ERROR_MESSAGE);
    }

    /**
     * Ends this turn.
     *
     * @return the game state
     */
    public GameState endTurn() {
        throw new BadGameTransactionException(ERROR_MESSAGE);
    }

    /**
     * Uses the given card.
     *
     * @param card the card
     * @param to the to
     * @return the game state
     */
    public GameState useCard(ItemCard card, Sector to) {
        throw new BadGameTransactionException(ERROR_MESSAGE);
    }

    /**
     * Moves the current player.
     *
     * @param to the to
     * @param map the map
     */
    public void move(Sector to, MapTable map) {
        throw new BadGameTransactionException(ERROR_MESSAGE);
    }

    /**
     * Discards this card.
     *
     * @param card the card
     * @return the game state
     */
    public GameState discardCard(ItemCard card) {
        throw new BadGameTransactionException(ERROR_MESSAGE);
    }

    /**
     * Performs an attack.
     * This action won't be triggered by using an attack card.
     *
     * @return the game state
     */
    public GameState attack() {
        throw new BadGameTransactionException(ERROR_MESSAGE);
    }

    /**
     * Makes the player draw an item card.
     *
     * @param card the card
     * @return the game state
     */
    public GameState drawItemCard(ItemCard card) {
        throw new BadGameTransactionException(ERROR_MESSAGE);
    }

    /**
     * Makes the player draw a sector card.
     *
     * @param sectorCard the sector card
     * @return the game state
     */
    public GameState drawSectorCard(SectorCard sectorCard) {
        throw new BadGameTransactionException(ERROR_MESSAGE);
    }

    /**
     * Sets the movement as dangerous.
     *
     * @return the game state
     */
    public GameState setDangerousSectorState() {
        throw new BadGameTransactionException(ERROR_MESSAGE);
    }

    /**
     * Sets the movement into an escape sector.
     *
     * @param card the card
     * @param map the map
     * @return the game state
     */
    public GameState setEscapeSectorState(EscapeCard card, MapTable map) {
        throw new BadGameTransactionException(ERROR_MESSAGE);
    }

    /**
     * Sets the movement into a secure sector.
     *
     * @return the game state
     */
    public GameState setSecureSectorState() {
        throw new BadGameTransactionException(ERROR_MESSAGE);
    }

    /**
     * Notifies the noise in this {sector}.
     *
     * @param sector the sector
     * @return the game state
     */
    public GameState notifyNoise(Sector sector) {
        throw new BadGameTransactionException(ERROR_MESSAGE);
    }

    /**
     * Notifies the end of the game.
     *
     * @return the game state
     */
    public GameState endGame() {
        throw new BadGameTransactionException(ERROR_MESSAGE);
    }

    protected void notUseAttackCard(ItemCard card, Sector sector) {
        if(!card.isAttackCard()){
            gamer.useCard(card, sector);
        } else {
            gamer.fireIllegalActionEvent();
            throw new BadGameTransactionException(ERROR_MESSAGE);
        }
    }

    protected void useOnlyAttackCard(ItemCard card, Sector sector) {
        if(card.isAttackCard()){
            gamer.useCard(card, sector);
        } else {
            gamer.fireIllegalActionEvent();
            throw new BadGameTransactionException(ERROR_MESSAGE);
        }

    }

    /**
     * Checks if game is ended.
     *
     * @return true, if game is ended
     */
    public boolean isGameEnded() {
        return false;
    }

}
