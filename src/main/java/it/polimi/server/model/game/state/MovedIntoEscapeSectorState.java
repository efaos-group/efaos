/**
 *
 */
package it.polimi.server.model.game.state;

import it.polimi.server.model.GameState;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;

/**
 * The Class MovedOnEscapeSectorState.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class MovedIntoEscapeSectorState extends GameState {

    /**
     * Instantiates a new moved on escape sector state.
     *
     * @param gamer the player
     */
    public MovedIntoEscapeSectorState(Player gamer) {
        super(gamer);
        gamer.notEscape();
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState endTurn() {
        gamer.endTurn();
        return new EndTurnState(gamer);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState useCard(ItemCard card, Sector sector) {
        card.use(gamer, sector);
        return new AfterMovementState(gamer);
    }

}
