/**
 *
 */
package it.polimi.server.model.game.state;

import it.polimi.server.model.GameState;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.SectorCard;

/**
 * The Class NoiseInSectorState.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class NoiseInSectorState extends GameState {

    /** The card. */
    private final SectorCard card;

    /**
     * Instantiates a new noise in sector state.
     *
     * @param gamer the player
     * @param card the card
     */
    public NoiseInSectorState(Player gamer, SectorCard card) {
        super(gamer);
        this.card = card;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState notifyNoise(Sector sector) {
        card.fireSectorNoiseEvent(sector);
        return new AfterMovementState(gamer);
    }

}
