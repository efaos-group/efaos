/**
 *
 */
package it.polimi.server.model.game.state;

import it.polimi.server.model.GameState;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.SectorCard;

/**
 * The Class MovedOnDangerousSectorState.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class MovedIntoDangerousSectorState extends GameState {

    /**
     * Instantiates a new moved on dangerous sector state.
     *
     * @param gamer the player
     */
    public MovedIntoDangerousSectorState(Player gamer) {
        super(gamer);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState useCard(ItemCard card, Sector sector) {
        useOnlyAttackCard(card, sector);
        return new AfterMovementState(gamer);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState attack() {
        gamer.attack();
        return new AfterMovementState(gamer);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState drawSectorCard(SectorCard card) {
        gamer.drawSectorCard(card);
        return card.use(gamer);
    }

}
