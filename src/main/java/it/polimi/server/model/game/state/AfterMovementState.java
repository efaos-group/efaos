/**
 *
 */
package it.polimi.server.model.game.state;

import it.polimi.server.model.GameState;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;

/**
 * The Class AfterMovementState.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class AfterMovementState extends GameState {

    /**
     * Instantiates a new after movement state.
     *
     * @param gamer the player
     */
    public AfterMovementState(Player gamer) {
        super(gamer);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState useCard(ItemCard card, Sector sector) {
        notUseAttackCard(card, sector);
        return this;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState endTurn() {
        gamer.endTurn();
        return new EndTurnState(gamer);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState endGame() {
        return new EndGameState(gamer);
    }
}
