/**
 *
 */
package it.polimi.server.model.game.state;

import it.polimi.server.model.GameState;
import it.polimi.server.model.Player;

/**
 * The Class EndTurnState.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class EndTurnState extends GameState {

    /**
     * Instantiates a new end turn state.
     *
     * @param gamer the player
     */
    public EndTurnState(Player gamer) {
        super(gamer);
        if (gamer.getItemsSize() > Player.MAX_ITEMS) {
            gamer.randomDiscard();
        }
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState startTurn(Player player){
        player.startTurn();
        return new InitialState(player);
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public GameState endGame() {
        return new EndGameState(gamer);
    }
}
