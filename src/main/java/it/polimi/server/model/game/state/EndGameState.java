/**
 *
 */
package it.polimi.server.model.game.state;

import it.polimi.server.model.GameState;
import it.polimi.server.model.Player;

/**
 * The Class EndGameState.
 * No action is allowd in this state.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class EndGameState extends GameState {

    /**
     * Instantiates a new end game state.
     *
     * @param gamer the player
     */
    public EndGameState(Player gamer) {
        super(gamer);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean isGameEnded() {
        return true;
    }

}
