/**
 *
 */
package it.polimi.server.model.game.state;

import it.polimi.server.model.GameState;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.Player;

/**
 * The Class DrawItemState.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class DrawItemState extends GameState {

    /**
     * Instantiates a new draw item state.
     *
     * @param gamer the player
     */
    public DrawItemState(Player gamer) {
        super(gamer);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState drawItemCard(ItemCard card) {
        gamer.drawItemCard(card);
        if(gamer.getItemsSize() > Player.MAX_ITEMS) {
            return new TooManyCardState(gamer);
        } else {
            return new AfterMovementState(gamer);
        }
    }

}
