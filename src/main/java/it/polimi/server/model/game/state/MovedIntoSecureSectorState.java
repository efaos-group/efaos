/**
 *
 */
package it.polimi.server.model.game.state;

import it.polimi.server.model.GameState;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;

/**
 * The Class MovedOnSecureSectorState.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class MovedIntoSecureSectorState extends GameState {

    /**
     * Instantiates a new moved on secure sector state.
     *
     * @param gamer the gamer
     */
    public MovedIntoSecureSectorState(Player gamer) {
        super(gamer);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState attack() {
        gamer.attack();
        return new AfterMovementState(gamer);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState useCard(ItemCard card, Sector sector) {
        gamer.useCard(card, sector);
        return new AfterMovementState(gamer);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState endTurn() {
        gamer.endTurn();
        return new EndTurnState(gamer);
    }
}
