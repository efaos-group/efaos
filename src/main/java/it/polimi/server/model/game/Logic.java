/**
 *
 */
package it.polimi.server.model.game;

import it.polimi.common.timer.GameTimer;
import it.polimi.common.timer.TimerReceiver;
import it.polimi.server.model.BaseCharacter;
import it.polimi.server.model.DeckFactory;
import it.polimi.server.model.EscapeCard;
import it.polimi.server.model.EscapeDeck;
import it.polimi.server.model.GameLogic;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.ItemDeck;
import it.polimi.server.model.MapTable;
import it.polimi.server.model.Notifier;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.SectorCard;
import it.polimi.server.model.SectorDeck;
import it.polimi.server.model.exception.BadGameTransactionException;
import it.polimi.server.model.game.state.AfterMovementState;
import it.polimi.server.model.game.state.EndTurnState;

import java.util.Map;
import java.util.logging.Logger;

/**
 * The Class Logic.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class Logic extends GameLogic implements TimerReceiver {

    private static final Logger LOGGER = Logger.getLogger(Logic.class.getName());

    /** The time available to a player for performing an action. */
    private static final Integer TURN_TIME = 10 * 60 * 1000;

    /** The escape deck. */
    protected final EscapeDeck escapeDeck;

    /** The sector deck. */
    protected final SectorDeck sectorDeck;

    /** The item deck. */
    protected final ItemDeck itemDeck;

    /** The timer. */
    private GameTimer timer;


    /**
     * Instantiates a new logic.
     *
     * @param map the map
     * @param players the players
     * @param notifier the notifier
     * @param deckFactory the deck factory
     */
    public Logic(MapTable map, Map<String, Player> players, Notifier notifier, DeckFactory deckFactory) {
        super(map, players, notifier);

        this.escapeDeck = deckFactory.getEscapeDeckInstance();
        this.sectorDeck = deckFactory.getSectorDeckInstance();
        this.itemDeck = deckFactory.getItemDeckInstance();

        gamer = turn.peek();
        this.state = new EndTurnState(gamer);
        timer = new GameTimer(this);
        timer.startGameCountDown(TURN_TIME);

        startTurn();
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    protected void startTurn() {
        super.startTurn();
        if(turn.isEmpty() && gamer == null) {
            endGame();
        } else if(rounds < maxTurn) {
            timer.stop();
            timer = new GameTimer(this);
            timer.startGameCountDown(TURN_TIME);
        }
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public void movedIntoSecureSector(Sector sector) {
        state = state.setSecureSectorState();
        notifier.notifySecureMovement(sector);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void movedIntoDangerousSector(Sector sector) {
        state = state.setDangerousSectorState();
        if (!gamer.isSedated()){
            notifier.notifyDangerousMovement(sector);
        } else {
            notifier.notifySecureMovement(sector);
        }
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void movedIntoEscapeSector(Sector sector) {
        EscapeCard card;

        card = checkBlockedSector(sector);
        state = state.setEscapeSectorState(card, map);
    }

    /**
     * Checks blocked sectors.
     *
     * @param sector the sector
     * @return the escape card
     */
    private EscapeCard checkBlockedSector(Sector sector) {
        if(map.isBlocked(sector)) {
            return escapeDeck.generateRedCard();
        } else {
            map.blockSector(sector);
            return escapeDeck.draw();
        }
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public void drawSector(){
        try {
            SectorCard card = sectorDeck.draw();
            card.setCardListener(notifier);
            state = state.drawSectorCard(card);
        } catch(BadGameTransactionException e) {
            LOGGER.fine(gamer.getUsername() + ERROR_MESSAGE + e);
        }
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void drawItemCard() {
        try {
            ItemCard card = itemDeck.draw();
            card.addCardListener(notifier);
            state = state.drawItemCard(card);
        } catch(BadGameTransactionException e) {
            LOGGER.fine(gamer.getUsername() + ERROR_MESSAGE + e);
        }
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void discardCard(ItemCard card) {
        try {
            state = state.discardCard(card);
        } catch(BadGameTransactionException e) {
            LOGGER.fine(gamer.getUsername() + " tried to perform an illegal action" + e);
        }
    }

    /**
     * Erases no more playing players.
     */

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public void deleteDeadPlayer(String playerID) {
        ereasePlayer(playerID);

        if(!areHumanRemaining()) {
            notifier.notifyAlienWin();
            state = state.endGame();
        }
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void deleteEscapedPlayer(String playerID) {

        BaseCharacter character = players.get(playerID).getCharacter();
        ereasePlayer(playerID);

        if(!areHumanRemaining()) {
            notifier.notifyHumanWin();
            state = state.endGame();
            map.removeEscapedCharacter(character);
        } else {
            notifier.notifyHumanEscaped(character.getPosition());
            super.passTurn();
        }

    }

    /**
     * Checks if there are some humans remaining in game.
     *
     * @return true, if successful
     */
    private boolean areHumanRemaining(){
        for(Player pg: players.values()) {
            BaseCharacter character = pg.getCharacter();
            if(character.canUseCard()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Erases safely the given player from the game.
     *
     * @param playerID the player id
     */
    protected void ereasePlayer(String playerID) {
        Player deadPlayer = players.remove(playerID);
        turn.remove(deadPlayer);
    }

    /**
     * Returns a new card to controller.
     *
     * @param cardType the card type
     * @return the item card
     */
    @Override
    public ItemCard getItemCard(String cardType) {
        return itemDeck.generateCardFromString(cardType);
    }

    /**
     * Removes disconnected player.
     *
     * @param playerID the player id
     */
    @Override
    public void removeDisconnectedPlayerFromGame(String playerID) {

        notifier.notifyDisconnection(playerID);

        if(gamer.getUsername().equals(playerID)) {
            endGameTimer();
        }
        ereasePlayer(playerID);
    }

    /**
     * Triggered by the end of the timer.
     * Terminates the turn for the playing player.
     */
    @Override
    public void endGameTimer() {
        if(turn.isEmpty()) {
            turn.add(gamer);
            endGame();
        } else {
            forceEndTurn();
        }
    }

    /**
     * Forces turn to end.
     */
    protected void forceEndTurn() {
        try {
            passTurn();
        } catch(BadGameTransactionException e) {
            LOGGER.info("Time for this turn expired, new turn will begin soon!" + e);
            state = new AfterMovementState(gamer);
            passTurn();
        }
    }

    /**
     * Ends the game.
     */
    protected void endGame() {
        gamer = null;
        timer.stop();
        notifier.notifyGameOver();
    }

}
