/**
 *
 */
package it.polimi.server.model.game.state;

import it.polimi.server.model.GameState;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;

/**
 * The Class TooManyCardState.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class TooManyCardState extends GameState {

    /**
     * Instantiates a new too many card state.
     *
     * @param gamer the player
     */
    public TooManyCardState(Player gamer) {
        super(gamer);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState useCard(ItemCard card, Sector sector) {
        gamer.useCard(card, sector);
        return new AfterMovementState(gamer);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState discardCard(ItemCard card) {
        gamer.discard(card);
        return new AfterMovementState(gamer);
    }

}
