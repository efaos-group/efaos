/**
 *
 */
package it.polimi.server.model.game.state;

import it.polimi.server.model.GameState;
import it.polimi.server.model.MapTable;
import it.polimi.server.model.Player;

/**
 * The Class HumanEscapedState.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class HumanEscapedState extends GameState {

    /**
     * Instantiates a new human escaped state.
     *
     * @param gamer the player
     * @param map the map
     */
    public HumanEscapedState(Player gamer, MapTable map) {
        super(gamer);
        gamer.escape();
        map.removeEscapedCharacter(gamer.getCharacter());
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState endTurn() {
        gamer.endTurn();
        return new EndTurnState(gamer);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState endGame() {
        return new EndGameState(gamer);
    }

}
