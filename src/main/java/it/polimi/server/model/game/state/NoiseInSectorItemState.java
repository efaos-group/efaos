/**
 *
 */
package it.polimi.server.model.game.state;

import it.polimi.server.model.GameState;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.SectorCard;

/**
 * The Class NoiseInSectorItemState.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class NoiseInSectorItemState extends GameState {

    /** The sector card drawn. */
    private final SectorCard card;

    /**
     * Instantiates a new noise in sector item state.
     *
     * @param gamer the player
     * @param card the sector card drawn
     */
    public NoiseInSectorItemState(Player gamer, SectorCard card) {
        super(gamer);
        this.card = card;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState notifyNoise(Sector sector) {
        card.fireSectorNoiseItemEvent(sector);
        return new DrawItemState(gamer);
    }

}
