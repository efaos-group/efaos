/**
 *
 */
package it.polimi.server.model.game.state;

import it.polimi.server.model.EscapeCard;
import it.polimi.server.model.GameState;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.MapTable;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;

/**
 * The Class InitialState.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class InitialState extends GameState {

    /**
     * Instantiates a new initial state.
     *
     * @param gamer the player
     */
    public InitialState(Player gamer) {
        super(gamer);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState useCard(ItemCard card, Sector sector) {
        notUseAttackCard(card, sector);
        return this;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void move(Sector sector, MapTable map) {
        map.move(gamer.getCharacter(), sector);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState setDangerousSectorState() {
        if(!gamer.isSedated()) {
            return new MovedIntoDangerousSectorState(gamer);
        } else {
            return new MovedIntoSecureSectorState(gamer);
        }
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState setEscapeSectorState(EscapeCard card, MapTable map) {
        if(card.canEscape()){
            return new HumanEscapedState(gamer, map);
        } else {
            return new MovedIntoEscapeSectorState(gamer);
        }
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState setSecureSectorState() {
        return new MovedIntoSecureSectorState(gamer);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public GameState endGame() {
        return new EndGameState(gamer);
    }

}
