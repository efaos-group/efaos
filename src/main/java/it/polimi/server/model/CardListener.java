/**
 *
 */
package it.polimi.server.model;

import it.polimi.common.observer.ModelEvent;

/**
 * The listener interface for receiving card events.
 * The class that is interested in processing a card
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addCardListener<code> method. When
 * the card event occurs, that object's appropriate
 * method is invoked.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public interface CardListener {

    /**
     * Notifies a card has been used.
     *
     * @param event the event
     */
    public void notifyCardUsed(ModelEvent event);

    /**
     * Notifies a noise in {sector}.
     *
     * @param sector the sector
     */
    public void notifyNoiseInSector(Sector sector);

    /**
     * Notifies the usage of a sector-item-card.
     *
     * @param sector the sector
     */
    public void notifyNoiseInSectorItem(Sector sector);

    /**
     * Notifies silence.
     */
    public void notifySilence();

    /**
     * Notifies the duty to choose a sector.
     */
    public void notifyChooseSector();
}
