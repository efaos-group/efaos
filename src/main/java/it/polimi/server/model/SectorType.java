/**
 *
 */
package it.polimi.server.model;

/**
 * The Enum SectorType.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 *
 */
public enum SectorType {
    HUMAN("HUMAN"), SECURE("SECURE"), ESCAPE("ESCAPE"),
    INVISIBLE("INVISIBLE"), ALIEN("ALIEN"), DANGEROUS("DANGEROUS");

    private String description;

    /**
     * Instantiates a new sector type.
     *
     * @param description the description
     */
    SectorType(String description) {
        this.description = description;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public String toString() {
        return description;
    }

    /**
     * Parses the input.
     *
     * @param input the input
     * @return the sector type
     */
    public static SectorType parseInput(String input) {
        return Enum.valueOf(SectorType.class, input.toUpperCase());
    }
}
