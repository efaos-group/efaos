/**
 *
 */
package it.polimi.server.model.character;

/**
 * The Class SedatedState.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class SedatedState extends SedativeState {

    @Override
    public boolean isSedated() {
        return true;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public int hashCode() {
        final int prime = 31;
        return super.hashCode() * prime;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && (obj instanceof SedatedState);
    }

}
