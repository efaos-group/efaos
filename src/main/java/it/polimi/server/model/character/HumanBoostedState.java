/**
 *
 */
package it.polimi.server.model.character;

import it.polimi.server.model.CharacterMovementState;
import it.polimi.server.model.MapTable;
import it.polimi.server.model.Sector;

/**
 * The Class HumanBoostedState.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class HumanBoostedState extends CharacterMovementState {

    /** The MIN steps allowed. */
    private static final int MIN_STEPS = 0;

    /** The MAX steps allowed. */
    private static final int MAX_STEPS_BOOSTED = 2;


    /**
     * Instantiates a new human boosted state.
     *
     * @param map the map
     */
    public HumanBoostedState(MapTable map) {
        super(map);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean canMove(Sector position, Sector to) {

        int path = getMap().getDirectDistance(position, to);
        return path > MIN_STEPS && path <= MAX_STEPS_BOOSTED;
    }
}
