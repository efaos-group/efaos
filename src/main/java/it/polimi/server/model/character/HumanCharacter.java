/**
 *
 */
package it.polimi.server.model.character;

import it.polimi.server.model.BaseCharacter;
import it.polimi.server.model.MapTable;
import it.polimi.server.model.Sector;
import it.polimi.server.model.exception.BadCharacterException;
import it.polimi.server.model.sector.unwalkable.HumanSector;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class HumanCharacter extends BaseCharacter {

    private SedativeState sedativeState;

    public HumanCharacter(String name, HumanSector position, MapTable map) {
        super(name);
        setPosition(position);
        super.movementState = new HumanNotBoostedState(map);
        sedativeState = new NotSedatedState();
    }

    @Override
    public boolean canMoveTo(Sector to) {
        return movementState.canMove(getPosition(), to);
    }

    @Override
    public void die() {
        if(!canDefend()){
            super.die();
        } else {
            defend();
        }
    }

    private void defend() {
        setDefense(false);
        parent.usedDefense();
    }

    @Override
    public boolean canDefend() {
        return defense;
    }

    @Override
    public void attack() {
        throw new BadCharacterException("Human cannot attack!");
    }

    @Override
    public void teleportTo(Sector to) {
        setPosition(to);
    }

    @Override
    public void boostMovement() {
        movementState = new HumanBoostedState(getMapFromState());
    }

    @Override
    public void sedate() {
        sedativeState = new SedatedState();
    }

    @Override
    public boolean isSedated(){
        return sedativeState.isSedated();
    }

    @Override
    public void resetStatus() {
        sedativeState = new NotSedatedState();
        movementState = new HumanNotBoostedState(getMapFromState());
    }

    @Override
    public boolean canUseCard() {
        return true;
    }

    private MapTable getMapFromState(){
        return movementState.getMap();
    }

    @Override
    public int hashCode() {
        final int prime = 92821;
        return super.hashCode() * prime;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj) || !(obj instanceof HumanCharacter)) {
            return false;
        }

        HumanCharacter other = (HumanCharacter) obj;
        return movementState.equals(other.movementState) && sedativeState.equals(other.sedativeState);
    }

    @Override
    public String toString() {
        return "HUMAN";
    }

}