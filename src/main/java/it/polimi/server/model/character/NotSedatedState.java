/**
 *
 */
package it.polimi.server.model.character;


/**
 * The Class NotSedatedState.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class NotSedatedState extends SedativeState {

    @Override
    public boolean isSedated() {
        return false;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public int hashCode() {
        final int prime = 31;
        return super.hashCode() * prime;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof NotSedatedState)) {
            return false;
        }
        return true;
    }

}
