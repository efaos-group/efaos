/**
 *
 */
package it.polimi.server.model.character;

import it.polimi.server.model.BaseCharacter;
import it.polimi.server.model.CharacterFactory;
import it.polimi.server.model.MapTable;
import it.polimi.server.model.exception.BadCharacterException;
import it.polimi.server.model.exception.PlayerException;
import it.polimi.server.model.sector.unwalkable.AlienSector;
import it.polimi.server.model.sector.unwalkable.HumanSector;

/**
 * A factory for creating BaseCharacter objects.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class BaseCharacterFactory implements CharacterFactory {

    /** An array containing all possibles human names. */
    private static final String[] humanName = {
        "The Captain Ennio Maria Dominoni",
        "The Pilot Julia Niguloti a.k.a. “Cabal”",
        "The Psychologist Silvano Porpora",
        "The Soldier Tuccio Brendon a.k.a. “Piri”"
    };

    /** An array containing all possibles alien names. */
    private static final String[] alienName = {
        "Piero Ceccarella",
        "Vittorio Martana",
        "Maria Galbani",
        "Paolo Landon"
    };

    /** The human index. */
    private int humanIndex;

    /** The alien index. */
    private int alienIndex;

    /**
     * Instantiates a new base character factory.
     */
    public BaseCharacterFactory() {
        this.humanIndex = 0;
        this.alienIndex = 0;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public BaseCharacter factory(CharacterType choice, MapTable map) {

        switch(choice){
            case ALIEN:
                return new AlienCharacter(getNewAlienName(), (AlienSector)map.getAlienSector(), map);
            case HUMAN:
                return new HumanCharacter(getNewHumanName(), (HumanSector)map.getHumanSector(), map);
            default:
                break;
        }

        throw new BadCharacterException("Tried to create a non-existing character");
    }

    protected String getNewHumanName(){
        if(humanName.length <= humanIndex){
            throw new PlayerException("Too many human!");
        } else {
            return humanName[humanIndex++];
        }
    }

    protected String getNewAlienName(){
        if(alienName.length <= alienIndex){
            throw new PlayerException("Too many alien!");
        } else {
            return alienName[alienIndex++];
        }
    }
}
