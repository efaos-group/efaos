/**
 *
 */
package it.polimi.server.model.character;

import it.polimi.server.model.BaseCharacter;
import it.polimi.server.model.CharacterMovementState;
import it.polimi.server.model.MapTable;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;
import it.polimi.server.model.card.item.AttackCard;
import it.polimi.server.model.exception.BadCharacterException;
import it.polimi.server.model.sector.unwalkable.AlienSector;

import java.util.Collection;

/**
 * The Class AlienCharacter.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class AlienCharacter extends BaseCharacter {

    protected CharacterMovementState movementState;

    /**
     * Instantiates a new alien character.
     *
     * @param name the name
     * @param position the position
     * @param map the map
     */
    public AlienCharacter(String name, AlienSector position, MapTable map) {
        super(name);
        setPosition(position);
        this.movementState = new AlienNormalState(map);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean canMoveTo(Sector to) {

        if (!to.isWalkable() || !to.canAlienStay()) {
            return false;
        }
        return movementState.canMove(getPosition(), to);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void attack() {
        MapTable map = movementState.getMap();
        Collection<Player> players = AttackCard.use(this, map);
        checkCharacterDeath(players);
        parent.notifyAttack(players);
    }

    private void checkCharacterDeath(Collection<Player> players) {
        for(Player player: players) {
            if(!player.getCharacter().isAlive() && player.getCharacter().canUseCard()) {
                boostMovement();
            }
        }
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean canDefend() {
        return false;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void teleportTo(Sector to) {
        throw new BadCharacterException("Alien cannot use teleport!");
    }

    @Override
    public void setDefense(boolean defense) {
        this.defense = false;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void boostMovement() {
        movementState = new AlienBoostedState(getMapFromState());
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void sedate(){
        throw new BadCharacterException("Alien cannot use sedatives item!");
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void resetStatus(){
        //Alien does not reset status at the end of the turn
    }

    private MapTable getMapFromState(){
        return movementState.getMap();
    }

    @Override
    public boolean isSedated() {
        return false;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public int hashCode() {
        final int prime = 92821;
        int result = super.hashCode();
        result = prime * result
                + ((movementState == null) ? 0 : movementState.hashCode());
        return result;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj) || !(obj instanceof AlienCharacter)) {
            return false;
        }

        AlienCharacter other = (AlienCharacter) obj;
        return movementState.equals(other.movementState);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public String toString() {
        return "ALIEN";
    }
}
