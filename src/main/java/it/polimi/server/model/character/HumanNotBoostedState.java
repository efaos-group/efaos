/**
 *
 */
package it.polimi.server.model.character;

import it.polimi.server.model.CharacterMovementState;
import it.polimi.server.model.MapTable;

/**
 * The Class HumanNotBoostedState.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class HumanNotBoostedState extends CharacterMovementState {

    /**
     * Instantiates a new human not boosted state.
     *
     * @param map the map
     */
    public HumanNotBoostedState(MapTable map) {
        super(map);
    }
}
