/**
 *
 */
package it.polimi.server.model.character;

/**
 * The Class SedativeState.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public abstract class SedativeState {

    public abstract boolean isSedated();
}
