/**
 *
 */
package it.polimi.server.model.character;

import it.polimi.server.model.CharacterMovementState;
import it.polimi.server.model.MapTable;
import it.polimi.server.model.Sector;

/**
 * The Class AlienNormalState.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class AlienNormalState extends CharacterMovementState {
    private static final int MAX_STEPS_NORMAL = 2;
    private static final int MIN_STEPS = 0;

    /**
     * Instantiates a new alien normal state.
     *
     * @param map the map
     */
    public AlienNormalState(MapTable map) {
        super(map);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean canMove(Sector position, Sector to) {
        if(super.canMove(position, to)) {
            return true;
        }
        int path = getMap().getDirectDistance(position, to);
        return path > MIN_STEPS && path <= MAX_STEPS_NORMAL;
    }
}
