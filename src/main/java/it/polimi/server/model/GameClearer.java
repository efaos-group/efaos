/**
 *
 */
package it.polimi.server.model;

/**
 * The Interface GameClearer.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public interface GameClearer {

    /**
     * Deletes dead player.
     *
     * @param playerID the player id
     */
    public void deleteDeadPlayer(String playerID);

    /**
     * Deletes escaped player.
     *
     * @param playerID the player id
     */
    public void deleteEscapedPlayer(String playerID);

}
