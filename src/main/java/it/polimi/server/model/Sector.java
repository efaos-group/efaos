/**
 *
 */
package it.polimi.server.model;

import it.polimi.server.model.exception.BadSectorException;

/**
 * The Class Sector.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public abstract class Sector {

    /** Bounds of the map. */
    public static final int MIN_ROW = 1;

    /** The Constant MAX_ROW. */
    public static final int MAX_ROW = 14;

    /** The Constant MIN_COL_INT. */
    public static final int MIN_COL_INT = 0;

    /** The Constant MAX_COL_INT. */
    public static final int MAX_COL_INT = 22;

    /** The Constant MIN_COL. */
    public static final Character MIN_COL = 'A' + MIN_COL_INT;

    /** The Constant MAX_COL. */
    public static final Character MAX_COL = 'A' + MAX_COL_INT;


    /** The col. */
    private final Character col;

    /** The row. */
    private final Integer row;

    /**
     * Instantiates a new sector.
     *
     * @param col the col
     * @param row the row
     */
    public Sector(Character col, Integer row) {
        if (!isValid(row, col)) {
            throw new BadSectorException("Sector not available");
        }
        this.row = row;
        this.col = col;
    }

    protected static boolean isValid(Integer row, Character col) {
        return !(row < MIN_ROW || row > MAX_ROW || col < MIN_COL || col > MAX_COL);
    }

    /**
     * Checks if this and {to} sector are adjacent or not.
     *
     * @param to the to
     * @return true, if successful
     */
    public boolean areAdjacents(Sector to) {
        // Same Sector
        if (this.equals(to)) {
            return false;
        }

        if (isInSameCol(this, to)) {
            return true;
        } else {
            return diagonalAdjacent(this, to);
        }
    }

    private boolean diagonalAdjacent(Sector from, Sector to){

        if (columnToInteger(from) % 2 == 1) {
            return isDiagonalOddAdiacent(getRowDistance(from, to),
                        getColDistance(from, to));
        } else {
            return isDiagonalEvenAdiacent(getRowDistance(from, to),
                        getColDistance(from, to));
        }
    }

    private boolean isInSameCol(Sector from, Sector to) {
        return from.getCol() == to.getCol()
                && Math.abs(getRowDistance(from, to)) == 1;
    }

    private boolean isDiagonalOddAdiacent(int deltaRow, int deltaCol) {
        return Math.abs(deltaCol) == 1 && (deltaRow == 0 || deltaRow == 1); // The adjacent can be on row upper or the same
    }

    private boolean isDiagonalEvenAdiacent(int deltaRow, int deltaCol) {
        return Math.abs(deltaCol) == 1 && (deltaRow == 0 || deltaRow == -1); // The adjacent can be on row lower or the same
    }

    /**
     * Gets the row distance between {from} and {to}.
     *
     * @param from the from
     * @param to the to
     * @return the row distance
     */
    protected  Integer getRowDistance(Sector from, Sector to) {
        return to.getRow() - from.getRow();
    }

    /**
     * Gets the column distance between {from} and {to}.
     *
     * @param from the from
     * @param to the to
     * @return the col distance
     */
    protected Integer getColDistance(Sector from, Sector to) {
        return to.getCol() - from.getCol();
    }

    /**
     * Converts the value of the column to an integer.
     *
     * @param s the s
     * @return the integer
     */
    public static Integer columnToInteger(Sector s) {
        return s.getCol() - MIN_COL;
    }

    private boolean areInSamePosition(Sector a, Sector b) {
        return a.getCol() == b.getCol() && a.getRow() == b.getRow();
    }

    /**
     * Tells if an alien can stay.
     *
     * @return true, if successful
     */
    public boolean canAlienStay() {
        return false;
    }

    /**
     * Gets the column.
     *
     * @return the column
     */
    public Character getCol() {
        return col;
    }

    /**
     * Gets the row.
     *
     * @return the row
     */
    public Integer getRow() {
        return row;
    }

    /**
     * Checks if is walkable.
     *
     * @return true, if is walkable
     */
    public abstract boolean isWalkable();

    /**
     * Reacts when someone move into this sector.
     */
    public abstract void movedInto();

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public String toString(){
        return "["+col+","+row+"]";
    }


    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof Sector)) {
            return false;
        }
        if (obj == this) {
            return true;
        }

        Sector s = (Sector) obj;
        return areInSamePosition(this, s) && s.getClass() == getClass();
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public int hashCode() {
        final int prime = 92821;
        int result = 1;
        result = prime * result + col;
        return prime * result + row;
    }

}
