/**
 *
 */
package it.polimi.server.model;


/**
 * The Class SectorCard.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public abstract class SectorCard {

    /** The parent deck. */
    protected final SectorDeck parent;

    /** The listener. */
    private CardListener listener;

    /**
     * Instantiates a new sector card.
     *
     * @param sectorDeck the sector deck
     */
    public SectorCard(SectorDeck sectorDeck) {
        this.parent = sectorDeck;
    }

    /**
     * Sets the card listener.
     *
     * @param listener the new card listener
     */
    public void setCardListener(CardListener listener) {
        this.listener = listener;
    }

    /**
     * Discards this card.
     */
    protected void discard() {
        parent.discarded(this);
    }

    /**
     * Fires a noise event.
     *
     * @param sector the noisy sector
     */
    public void fireSectorNoiseEvent(Sector sector) {
        listener.notifyNoiseInSector(sector);
    }

    /**
     * Fires sector noise event which enables item drawing.
     *
     * @param sector the noisy sector
     */
    public void fireSectorNoiseItemEvent(Sector sector) {
        listener.notifyNoiseInSectorItem(sector);
    }

    /**
     * Fires an event which makes the user choose the noisy sector.
     */
    public void fireChooseNoiseSectorEvent() {
        listener.notifyChooseSector();
    }

    /**
     * Fires sector silence event.
     */
    protected void fireSectorSilenceEvent() {
        listener.notifySilence();
    }

    /**
     * Uses this sector card.
     *
     * @param gamer the player
     * @return the game state
     */
    public abstract GameState use(Player gamer);

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean equals(Object obj){
        if(obj == this){
            return true;
        } else if(!(obj instanceof SectorCard)) {
            return false;
        }
        return ((SectorCard)obj).getClass() == getClass();
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public int hashCode(){
        return "SECTORCARD".hashCode();
    }

}
