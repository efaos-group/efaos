/**
 *
 */
package it.polimi.server.model;

import it.polimi.common.observer.ModelEvent;

/**
 * The listener interface for receiving player events.
 * The class that is interested in processing a player
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>setPlayerListener<code> method. When
 * the player event occurs, that object's appropriate
 * method is invoked.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public interface PlayerListener {

    /**
     * Notifies that a human didn't manage to escape.
     *
     * @param sector the sector
     */
    public void notifyHumanNotEscaped(Sector sector);

    /**
     * Notifies a human managed to escape.
     *
     * @param sector the sector
     */
    public void notifyHumanEscaped(Sector sector);

    /**
     * Notifies a character event.
     *
     * @param event the event
     */
    public void notifyCharacterEvent(ModelEvent event);

    /**
     * Notifies an exception.
     *
     * @param message the message
     */
    public void notifyException(String message);

    /**
     * Notifies a drawn sector card.
     *
     * @param card the card
     */
    public void notifyDrawnSectorCard(SectorCard card);

    /**
     * Notifies a drawn item card.
     *
     * @param card the card
     */
    public void notifyDrawnItemCard(ItemCard card);

    /**
     * Notifies an item card has been used after a movement.
     *
     * @param event the event
     */
    public void notifyItemCardUsedAfterMove(ModelEvent event);

    /**
     * Notifies a discarded card event.
     *
     * @param card the card
     */
    public void notifyDiscardedCardEvent(ItemCard card);

    /**
     * Notifies a new turn started.
     *
     * @param player the player
     */
    public void notifyStartTurn(Player player);

    /**
     * Notifies too many item card have been drawn.
     *
     * @param card the card
     */
    public void notifyDrawnTooManyItemCard(ItemCard card);

    /**
     * Notifies turn's end.
     */
    public void notifyEndTurn();
}
