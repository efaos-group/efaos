package it.polimi.server.model;

import it.polimi.server.model.exception.IllegalMovementException;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.logging.Logger;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

/**
 * The Class MapTable.
 */
public abstract class MapTable {

    private static final Logger LOGGER = Logger.getLogger(MapTable.class.getName());

    /** The map. */
    protected final Multimap<Sector, Player> map;

    /** The constrains. */
    protected MapLoader constrains;

    /** The notifier. */
    protected MapTableListener notifier;

    /**
     * Instantiates a new map table.
     *
     * @param map the map
     * @throws IOException Signals that the resources containing the map can't be found.
     */
    public MapTable(MapLoader map) throws IOException {
        this.map = ArrayListMultimap.create();
        this.constrains = map;
    }

    /**
     * Moves a player {character} from his position to the specified one {to}.
     *
     * @param character the character
     * @param to the to
     */
    public void move(BaseCharacter character, Sector to) {
        Sector from = character.getPosition();
        if (map.containsKey(from) && to.isWalkable()) {
            Collection<Player> player = map.get(from);

            for (Player p : player) {
                if (character.equals(p.getCharacter()) && character.canMoveTo(to)) {
                    moveForward(p, to);
                    return;
                }
            }
        }
        notifier.notifyIllegalMovementGamer();
    }

    private void moveForward(Player pg, Sector to) {
        try{
            swapCharacterPosition(pg, to);
            pg.moveCharacterTo(to);
        } catch (IllegalMovementException exc) {
            notifier.notifyIllegalMovementGamer();
            LOGGER.warning(exc + " Table class in moveForward methods");
        }
    }

    /**
     * Swaps character position.
     *
     * @param pg the player
     * @param to the to
     */
    protected void swapCharacterPosition(Player pg, Sector to){
        map.remove(pg.getCharacterPosition(), pg);
        map.put(to, pg);
    }


    /**
     * Calculates the minimum steps to move from the source {from} to the destination {to} using BFS's algorithm.
     *
     * @param from      the starting sector
     * @param to        the finishing sector
     * @return the minimum distance between {from} and {to}
     *
     * @throws IllegalMovementException if {to} isn't walkable.
     */
    public int getDirectDistance(Sector from, Sector to) {

        if(!to.isWalkable()){
            throw new IllegalMovementException("Impossible to move over " + to);
        }

        int path = 0;
        List<Sector> fire = new LinkedList<Sector>();
        Queue<Sector> queue = new LinkedList<Sector>();
        queue.add(from);
        queue.add(null);

        while(!queue.isEmpty()){
            Sector q = queue.remove();

            if(q == null){
                path++;
                queue.add(null);
            } else if(q.equals(to)){
                break;
            } else {
                enqueueAdjacents(q, queue, fire);
                fire.add(q);
            }
        }

        return path;
    }

    /**
     * Generates and enqueues all sectors adjacent to {q} which are walkable but not in {fire}.
     *
     * @param q         the sector
     * @param queue     the queue of sector to be analyzed
     * @param fire      the sector already analyzed
     */
    protected void enqueueAdjacents(Sector q, Queue<Sector> queue, List<Sector> fire) {

        Queue <Sector> allAdjacents = new LinkedList<>();
        enqueueAllAdjacents(q,allAdjacents);
        Sector temp;

        while (!allAdjacents.isEmpty()) {
            temp = allAdjacents.poll();
            if(temp.isWalkable() && !fire.contains(temp)) {
                queue.add(temp);
            }
        }
    }

    /**
     * Generates and enqueues all sectors adjacent to {q}.
     *
     * @param q the q
     * @param queue the queue
     */
    protected void enqueueAllAdjacents(Sector q, Queue<Sector> queue) {

        int leftBound = Math.max(Sector.columnToInteger(q) - 1, Sector.MIN_COL_INT);
        int rightBound = Math.min(Sector.columnToInteger(q) + 1, Sector.MAX_COL_INT);
        int upperBound = Math.max(q.getRow() - 1, Sector.MIN_ROW);
        int lowerBound = Math.min(q.getRow() + 1, Sector.MAX_ROW);
        Sector temp;

        for (int i = leftBound; i <= rightBound; i++) {
            for (int j = upperBound; j <= lowerBound; j++) {
                int col = i + Sector.MIN_COL;
                temp = constrains.getSector((char)(col), j);
                if(q.areAdjacents(temp)){
                    queue.add(temp);
                }
            }
        }
    }

    /**
     * Sets the map table listener.
     *
     * @param notifier the new map table listener
     */
    public void setMapTableListener(MapTableListener notifier) {
        this.notifier = notifier;
    }

    /**
     * Sets the sector listener.
     *
     * @param listener the new sector listener
     */
    public void setSectorListener(SectorListener listener){
        this.constrains.setSectorListener(listener);
    }

    /**
     * Gets the human sector.
     *
     * @return the human sector
     */
    public Sector getHumanSector() {
        return constrains.getHumanSector();
    }

    /**
     * Gets the alien sector.
     *
     * @return the alien sector
     */
    public Sector getAlienSector() {
        return constrains.getAlienSector();
    }

    /**
     * Gets the sector from {col} and {to}.
     *
     * @param col the column
     * @param row the row
     * @return the sector
     */
    public Sector getSector(Character col, Integer row) {
        return constrains.getSector(col, row);
    }

    /**
     * Sets the characters.
     *
     * @param players the new characters
     */
    public void setCharacters(Collection<Player> players) {
        for(Player pg: players) {
            map.put(pg.getCharacterPosition(), pg);
        }
    }

    /**
     * Gets the characters in {sec}.
     *
     * @param sec the sector
     * @return the characters
     */
    public abstract Collection<Player> getCharacters(Sector sec);

    /**
     * Teleports player to HumanSector.
     *
     * @param player the player
     */
    public abstract void teleport(Player player);

    /**
     * Spots all Player near {sector}.
     *
     * @param sector the sector
     * @return the multimap containing the spotted players
     */
    public abstract Multimap<Sector, Player> spotlight(Sector sector);

    /**
     * Blocks sector.
     *
     * @param sector the sector
     */
    public abstract void blockSector(Sector sector);

    /**
     * Checks if {sector} is blocked.
     *
     * @param sector the sector
     * @return true, if is blocked
     */
    public abstract boolean isBlocked(Sector sector);

    /**
     * Removes the dead players.
     */
    public abstract void removeDeadPlayers();

    /**
     * Removes the escaped character.
     *
     * @param character the character
     */
    public abstract void removeEscapedCharacter(BaseCharacter character);
}
