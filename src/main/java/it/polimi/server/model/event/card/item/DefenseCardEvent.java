/**
 *
 */
package it.polimi.server.model.event.card.item;

import it.polimi.common.observer.View;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.Player;

import java.io.Serializable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class DefenseCardEvent extends ItemUsedEvent implements Serializable {
    private static final long serialVersionUID = -4357926826451802021L;

    public static final String ENDINGMESSAGE = " was attacked. He used a ";
    private final String card;

    public DefenseCardEvent(Player player, ItemCard card) {
        super(player, card);
        this.card = card.toString();
    }

    @Override
    public String getMessage() {
        return getPlayerID() + ENDINGMESSAGE + card;
    }

    @Override
    protected void waitCommand(View view){
        //Do nothing because it's not used in owner's turn
    }

}
