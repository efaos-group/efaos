/**
 * Event fired to make a noise in the given {sector}
 */
package it.polimi.server.model.event.card.sector;

import it.polimi.common.observer.View;
import it.polimi.server.model.Sector;

import java.io.Serializable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class NoiseInSectorEvent extends NoiseEvent implements Serializable {
    private static final long serialVersionUID = 1149118135264499641L;

    private static final String BEGINNINGMESSAGE = "Noise in ";


    public NoiseInSectorEvent(String playerID, Sector sector) {
        super(BEGINNINGMESSAGE + sector.toString(), playerID);
    }
    
    @Override
    protected void enableActions(View view) {
        view.disableAll();
        view.enableEndTurn();
        view.enableItemUsage();
    }
}
