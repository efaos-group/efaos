/**
 *
 */
package it.polimi.server.model.event.game;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.View;
import it.polimi.server.model.Player;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class StartGameEvent extends ModelEvent implements Serializable {
    private static final long serialVersionUID = 2237756643117040240L;

    private final HashMap<String, String> types;
    private final HashMap<String, String> characters;
    private final HashMap<String, String> positions;

    public StartGameEvent(Collection<Player> players) {
        super("The game has just started", "");
        types = new HashMap<String, String>();
        characters = new HashMap<String, String>();
        positions = new HashMap<String, String>();

        addPlayersToHashMap(players);
    }

    private void addPlayersToHashMap(Collection<Player> players) {
        for (Player p: players) {
            types.put(p.getUsername(), p.getCharacter().toString());
            characters.put(p.getUsername(), p.getCharacter().getName());
            positions.put(p.getUsername(), p.getCharacterPosition().toString());
        }
    }

    @Override
    public void reactToView(View view) {

        String viewPlayer = view.getPlayerID();

        view.showStartGameEvent(getMessage(), characters.get(viewPlayer), characters.keySet());
        view.setPosition(positions.get(viewPlayer));

        String playerType = types.get(viewPlayer);
        view.setCharacterBehaviour(playerType);
    }

}
