/**
 * Event fired after an alien's attack
 */
package it.polimi.server.model.event.character;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.View;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class AttackEvent extends ModelEvent implements Serializable {
    private static final long serialVersionUID = 5662199221290241885L;
    private final String attackedCharacter;
    public static final String MESSAGE = " attacked in ";
    
    public AttackEvent(String playerID, Sector position, Collection<Player> characterAttacked) {
        super(playerID + MESSAGE + position.toString(), playerID);
        this.attackedCharacter = createMessage(characterAttacked);
    }

    private String createMessage(Collection<Player> attackedCharacter) {
        String message = getMessage();
        if(attackedCharacter.isEmpty()) {
            message += "\nNobody was attacked!";
        } else {
            message += "\nThere were the following players:";
            for (Player pg: attackedCharacter ) {
                message = message + "\n\t-" + pg.getUsername();
            }
        }
        return message;
    }
    
    @Override
    public void reactToView(View view) {
        view.showEventDialog(attackedCharacter);
        String viewPlayer = view.getPlayerID();
        if (viewPlayer.equals(getPlayerID())) {
            enableActions(view);
            view.askForCommand();   
        }
    }

    private void enableActions(View view) {
        view.disableAll();
        view.enableEndTurn();
    }

}
