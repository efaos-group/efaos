/**
 *
 */
package it.polimi.server.model.event.turn;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.View;

import java.io.Serializable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class StartTurnEvent extends ModelEvent implements Serializable {
    private static final long serialVersionUID = 9208467625640665757L;

    private static final String STARTINGMESSAGE = "It's ";
    private static final String ENDINGMESSAGE = "'s turn.";
    
    public StartTurnEvent(String playerID) {
        super(STARTINGMESSAGE + playerID + ENDINGMESSAGE, playerID);
    }

    @Override
    public void reactToView(View view) {
        view.showStartTurnDialog(getMessage(), getPlayerID());
        String playerID = view.getPlayerID();

        if ( playerID.equals(getPlayerID())) {
            enableActions(view);
            view.askForCommand();
        }
    }

    private void enableActions(View view) {
        view.disableAll();
        view.enableMovement();
        view.enableItemUsage();
    }

}
