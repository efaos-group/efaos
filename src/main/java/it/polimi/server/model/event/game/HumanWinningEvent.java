/**
 *
 */
package it.polimi.server.model.event.game;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.View;

import java.io.Serializable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class HumanWinningEvent extends ModelEvent implements Serializable {
    private static final long serialVersionUID = -9052096571591819508L;

    private static final String STARTINGMESSAGE = "Humans won! Finally";
    private static final String ENDINGMESSAGE = " managed to escape!";

    public HumanWinningEvent(String playerID) {
        super(STARTINGMESSAGE + playerID + ENDINGMESSAGE, playerID);
    }

    @Override
    public void reactToView(View view) {
        view.showPictureDialog(getMessage(), "HUMAN");
        view.disableAll();
    }

}
