/**
 *
 */
package it.polimi.server.model.event.card.sector;

import it.polimi.common.observer.View;
import it.polimi.server.model.Sector;

import java.io.Serializable;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class NoiseInSectorItemEvent extends NoiseEvent implements Serializable {
    private static final long serialVersionUID = -2164483779478094901L;

    private static final String BEGINNINGMESSAGE = "Noise in ";

    public NoiseInSectorItemEvent(String playerID, Sector sector) {
        super(BEGINNINGMESSAGE + sector.toString(), playerID);
    }

    @Override
    protected void enableActions(View view) {
        view.disableAll();
        view.enableDrawItemCard();
    }

}
