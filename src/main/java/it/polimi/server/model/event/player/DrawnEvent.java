/**
 *
 */
package it.polimi.server.model.event.player;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.View;
import it.polimi.server.model.ItemCard;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public abstract class DrawnEvent extends ModelEvent {

    private static final long serialVersionUID = -4814523240249046577L;

    private static final String ENDINGMESSAGE = " drew an item card";
    private final String card;

    public DrawnEvent(String playerID, ItemCard card) {
        super(playerID + ENDINGMESSAGE, playerID);
        this.card = card.toString();
    }

    @Override
    public void reactToView(View view) {
        String player = view.getPlayerID();

        if ( player.equals(getPlayerID()) ) {
            String message = createMessage();
            view.showPictureDialog(message, card);
            view.addNewItemCard(card);
            enableActions(view);
            view.askForCommand();
        } else {
            view.showCardDialog(getMessage());
        }
    }

    private String createMessage() {
        return "You drew " + card.toString();
    }

    protected abstract void enableActions(View view);

}
