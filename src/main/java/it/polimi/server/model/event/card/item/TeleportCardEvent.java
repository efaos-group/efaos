/**
 *
 */
package it.polimi.server.model.event.card.item;

import it.polimi.common.observer.View;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class TeleportCardEvent extends ItemUsedEvent {

    private static final long serialVersionUID = 2779393748487264703L;
    public static final String MESSAGE = " used a ";
    private final String position;

    public TeleportCardEvent(Player player, ItemCard card, Sector sector) {
        super(player, card);
        this.position = sector.toString();
    }

    @Override
    public void enableAction(View view) {
        view.setPosition(position);
        super.enableAction(view);
    }
}

