/**
 *
 */
package it.polimi.server.model.event.game;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.View;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class DisconnectionEvent extends ModelEvent {
    private static final long serialVersionUID = 4929577630394844222L;

    public DisconnectionEvent(String playerID) {
        super(playerID + " disconnected", playerID);
    }

    @Override
    public void reactToView(View view) {
        view.showEventDialog(getMessage());
    }

}
