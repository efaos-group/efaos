/**
 *
 */
package it.polimi.server.model.event.turn;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.View;

import java.io.Serializable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class EndTurnEvent extends ModelEvent implements Serializable {
    private static final long serialVersionUID = 334063466246295543L;

    private static final String ENDINGMESSAGE = "'s turn is over.";

    public EndTurnEvent(String playerID) {
        super(playerID + ENDINGMESSAGE, playerID);
    }

    @Override
    public void reactToView(View view) {
        view.showEventDialog(getMessage());
        if(view.getPlayerID().equals(getPlayerID())) {
            view.disableAll();
        }
    }

}
