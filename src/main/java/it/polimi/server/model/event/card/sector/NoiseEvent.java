/**
 * 
 */
package it.polimi.server.model.event.card.sector;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.View;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public abstract class NoiseEvent extends ModelEvent {

    private static final long serialVersionUID = 3645149678166952780L;

    public NoiseEvent(String message, String playerID) {
        super(message, playerID);
    }
    
    @Override
    public void reactToView(View view) {
        view.showNoiseInSectorDialog(getMessage());
        String player = view.getPlayerID();
        if (player.equals(getPlayerID())) {
            enableActions(view);
            view.askForCommand();
        }
    }

    protected abstract void enableActions(View view);

}
