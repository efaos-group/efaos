/**
 *
 */
package it.polimi.server.model.event.movement;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.View;
import it.polimi.server.model.Sector;

import java.io.Serializable;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class NotEscapedEvent extends ModelEvent implements Serializable {
    private static final long serialVersionUID = -8489402478683950348L;

    private static final String BEGINNINGMESSAGE = "You moved correctly to ";
    private static final String ENDINGMESSAGE = " unfortunately, the Escape Hatch was broken!";
    private final String sector;

    public NotEscapedEvent(String playerID, Sector sector) {
        super(BEGINNINGMESSAGE + sector.toString() + ENDINGMESSAGE, playerID);
        this.sector = sector.toString();
    }

    @Override
    public void reactToView(View view) {
        String playerID = getPlayerID();
        view.blockEscapeSector(sector);

        if (playerID.equals(view.getPlayerID())) {
            view.showPictureDialog(getMessage(), "RED");
            view.setPosition(sector);
            enableActions(view);
            view.askForCommand();
        } else {
            String message = playerID + " moved to " + sector.toString() + " and drew a RED CARD";
            view.showPictureDialog(message, "RED");
        }
    }

    private void enableActions(View view) {
        view.disableAll();
        view.enableItemUsage();
        view.enableEndTurn();
    }
}
