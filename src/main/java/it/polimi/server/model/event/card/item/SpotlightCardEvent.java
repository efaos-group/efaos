/**
 *
 */
package it.polimi.server.model.event.card.item;

import it.polimi.server.model.ItemCard;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;

import java.io.Serializable;
import java.util.Collection;

import com.google.common.collect.Multimap;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class SpotlightCardEvent extends ItemUsedEvent implements Serializable {
    private static final long serialVersionUID = 5247904826435295667L;

    private static final String MESSAGE = " used a Spotlight card";
    private final String characterSpottedMessage;

    public SpotlightCardEvent(Player player, Multimap<Sector, Player> characterSpotted, ItemCard card) {
        super(player, card);
        this.characterSpottedMessage = createMessage(characterSpotted);
    }

    private String createMessage(Multimap<Sector, Player> characterSpotted) {
        String message = getPlayerID() + MESSAGE;
        if(characterSpotted.isEmpty()) {
            message += "\nNo-one has been spotted";
        } else {
            message += fetchSector(characterSpotted);
        }
        return message;
    }

    private String fetchSector(Multimap<Sector, Player> characterSpotted) {
        String message = "";
        for (Sector sec: characterSpotted.keySet()) {
            message += "\nIn Sector " + sec.toString();
            message += messageForSector(characterSpotted.get(sec));
        }
        return message;
    }

    private String messageForSector(Collection<Player> sector) {
        String message = "";
        if(sector.isEmpty()){
            message += "\n\tNobody was spotted.";
        } else {
            for (Player character: sector) {
                message += "\n\t" + character.getUsername();
            }
        }
        return message;
    }

    @Override
    public String getMessage() {
        return characterSpottedMessage;
    }

}
