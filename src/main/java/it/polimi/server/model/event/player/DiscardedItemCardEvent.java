/**
 * Event rised after discarding an item card
 */
package it.polimi.server.model.event.player;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.View;
import it.polimi.server.model.ItemCard;

import java.io.Serializable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class DiscardedItemCardEvent extends ModelEvent implements Serializable {
    private static final long serialVersionUID = 6371278568834743823L;

    private static final String COJUNCTIVEMESSAGE = " discarded a ";
    private final String card;

    public DiscardedItemCardEvent(String playerID, ItemCard card) {
        super(playerID + COJUNCTIVEMESSAGE + card.toString(), playerID);
        this.card = card.toString();
    }

    @Override
    public void reactToView(View view) {
        view.showCardDialog(getMessage());
        String player = view.getPlayerID();
        if ( player.equals(getPlayerID()) ) {
            view.removeUsedItemCard(card);
            enableActions(view);
            view.askForCommand();
        }
    }

    private void enableActions(View view) {
        view.disableAll();
        view.enableItemUsage();
        view.enableEndTurn();
    }
}
