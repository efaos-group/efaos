/**
 *
 */
package it.polimi.server.model.event.game;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.View;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class GameOverEvent extends ModelEvent {
    private static final long serialVersionUID = 5548308022820021994L;


    public GameOverEvent() {
        super("Game Over!", "");
    }


    @Override
    public void reactToView(View view) {
        view.showEndGameDialog(getMessage());
        view.disableAll();
    }

}
