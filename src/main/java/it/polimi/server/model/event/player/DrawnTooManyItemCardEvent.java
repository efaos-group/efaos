/**
 *
 */
package it.polimi.server.model.event.player;

import it.polimi.common.observer.View;
import it.polimi.server.model.ItemCard;

import java.io.Serializable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class DrawnTooManyItemCardEvent extends DrawnEvent implements Serializable {
    private static final long serialVersionUID = 5490105106486905666L;

    public DrawnTooManyItemCardEvent(String playerID, ItemCard card) {
        super(playerID, card);
    }

    @Override
    protected void enableActions(View view) {
        view.disableAll();
        view.enableItemUsage();
        view.enableDiscard();
    }

}
