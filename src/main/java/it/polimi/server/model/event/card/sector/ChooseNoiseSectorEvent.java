/**
 *
 */
package it.polimi.server.model.event.card.sector;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.View;

import java.io.Serializable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class ChooseNoiseSectorEvent extends ModelEvent implements Serializable {
    private static final long serialVersionUID = -5300772385729303750L;

    public ChooseNoiseSectorEvent(String playerID) {
        super(playerID, playerID);
    }

    @Override
    public void reactToView(View view) {
        String player = view.getPlayerID();

        if (player.equals(getPlayerID())) {
            String playerMessage = createMessage();
            view.showEventDialog(playerMessage);
            enableActions(view);
            view.askForCommand();
        }
    }

    private void enableActions(View view) {
        view.disableAll();
        view.enableSelectSector();
    }

    private String createMessage() {
        return "Select the sector where you want to make noise";
    }

}
