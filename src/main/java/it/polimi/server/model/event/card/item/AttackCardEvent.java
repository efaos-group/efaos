/**
 *
 */
package it.polimi.server.model.event.card.item;

import it.polimi.common.observer.View;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.Player;
import it.polimi.server.model.Sector;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class AttackCardEvent extends ItemUsedEvent implements Serializable {
    private static final long serialVersionUID = 2343592971680008883L;

    private static final String MESSAGE = " used AttackCard in sector ";
    private final String attackedCharacter;
    private final String sector;

    public AttackCardEvent(Player player, Collection<Player> attackedCharacter, Sector sector, ItemCard card) {
        super(player, card);
        this.sector = sector.toString();
        this.attackedCharacter = createMessage(attackedCharacter);
    }

    @Override
    public String getMessage() {
        return attackedCharacter;
    }
    
    @Override
    protected void enableAction(View view) {
        view.disableAll();
        view.enableEndTurn();
        view.enableItemUsage();
        super.enableAction(view);
    }

    private String createMessage(Collection<Player> attackedCharacter) {
        String message = getPlayerID() + MESSAGE + sector.toString();
        if(attackedCharacter.isEmpty()) {
            message += "\nNobody was attacked!";
        } else {
            message += "\nThere were the following players:";
            for (Player pg: attackedCharacter ) {
                message = message + "\n\t-" + pg.getUsername();
            }
        }
        return message;
    }
    
}
