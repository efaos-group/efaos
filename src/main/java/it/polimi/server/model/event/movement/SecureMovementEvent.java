/**
 *
 */
package it.polimi.server.model.event.movement;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.View;
import it.polimi.server.model.Sector;

import java.io.Serializable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class SecureMovementEvent extends ModelEvent implements Serializable {
    private static final long serialVersionUID = -2840120397547012763L;

    private static final String MESSAGE = "You moved correctly to ";
    private final String sector;

    public SecureMovementEvent(String playerID, Sector sector) {
        super(MESSAGE + sector.toString(), playerID);
        this.sector = sector.toString();
    }

    @Override
    public void reactToView(View view) {
        String playerID = getPlayerID();

        if (playerID.equals(view.getPlayerID())) {
            view.showEventDialog(getMessage());
            view.setPosition(sector);
            enableActions(view);
            view.askForCommand();
        }
    }

    private void enableActions(View view) {
        view.disableAll();
        view.enableAttack();
        view.enableAttackCard();
        view.enableItemUsage();
        view.enableEndTurn();
    }
}
