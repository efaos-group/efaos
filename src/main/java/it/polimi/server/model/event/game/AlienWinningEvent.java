/**
 *
 */
package it.polimi.server.model.event.game;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.View;

import java.io.Serializable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class AlienWinningEvent extends ModelEvent implements Serializable {
    private static final long serialVersionUID = -7244173414086332487L;

    private static final String STARTINGMESSAGE = "Aliens won! ";
    private static final String ENDINGMESSAGE = " killed the last Human!";

    public AlienWinningEvent(String playerID) {
        super(STARTINGMESSAGE + playerID + ENDINGMESSAGE, playerID);
    }

    @Override
    public void reactToView(View view) {
        view.showPictureDialog(getMessage(), "ALIEN");
        view.disableAll();
    }

}
