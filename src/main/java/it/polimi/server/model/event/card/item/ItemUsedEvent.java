/**
 * Event fired after using an item card
 */
package it.polimi.server.model.event.card.item;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.View;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.Player;

import java.io.Serializable;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class ItemUsedEvent extends ModelEvent implements Serializable {
    private static final long serialVersionUID = 1423315049523644051L;

    private static final Integer MAX_ITEMS = 3;

    public static final String MESSAGE = " used a ";
    private final String card;
    private final int cardsNumber;

    public ItemUsedEvent(Player player, ItemCard card) {
        super(player.getUsername() + MESSAGE + card.toString(), player.getUsername());
        this.card = card.toString();
        this.cardsNumber = player.getItemsSize();
    }

    @Override
    public void reactToView(View view) {
        view.showPictureDialog(getMessage(), card);
        String player = view.getPlayerID();
        if (player.equals(getPlayerID()) ) {
            enableAction(view);
            view.removeUsedItemCard(card);
            waitCommand(view);
        }
    }

    protected void enableAction(View view) {
        if(cardsNumber == MAX_ITEMS) {
            view.disableAll();
            view.enableItemUsage();
            view.enableEndTurn();
        }
    }

    protected void waitCommand(View view){
        view.askForCommand();
    }


}
