/**
 * Event fired to notify to each player that the movement had not generate any noise
 */
package it.polimi.server.model.event.card.sector;

import it.polimi.common.observer.View;

import java.io.Serializable;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public final class SilenceEvent extends NoiseEvent implements Serializable {
    private static final long serialVersionUID = -5660688466759125672L;

    private static final String MESSAGE = "Silence in all sector!";

    public SilenceEvent(String playerID) {
        super(MESSAGE, playerID);
    }
    
    @Override
    protected void enableActions(View view) {
        view.disableAll();
        view.enableEndTurn();
        view.enableItemUsage();
    }
}
