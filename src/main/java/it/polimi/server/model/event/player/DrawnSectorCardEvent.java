/**
 * Event fired after drawing a sector card
 */
package it.polimi.server.model.event.player;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.View;
import it.polimi.server.model.SectorCard;

import java.io.Serializable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class DrawnSectorCardEvent extends ModelEvent implements Serializable {
    private static final long serialVersionUID = -4617544427708744132L;

    private static final String ENDINGMESSAGE = " drew a sector card";
    private final String card;

    public DrawnSectorCardEvent(String playerID, SectorCard card) {
        super(playerID + ENDINGMESSAGE, playerID);
        this.card = card.toString();
    }

    @Override
    public void reactToView(View view) {
        String playerMessage = createMessage();
        String player = view.getPlayerID();

        if (player.equals(getPlayerID())) {
            view.showPictureDialog(playerMessage,card);
        } else {
            view.showCardDialog(getMessage());
        }

    }

    private String createMessage() {
        return "You drew " + card;
    }
}
