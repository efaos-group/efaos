/**
 *
 */
package it.polimi.server.model.event.character;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.View;

import java.io.Serializable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class CharacterDeathEvent extends ModelEvent implements Serializable {
    private static final long serialVersionUID = -5472406661169435220L;

    private static final String ENDINGMESSAGE = " was attacked and died.";
    public CharacterDeathEvent(String playerID, String characterName) {
        super(characterName + ENDINGMESSAGE, playerID);
    }

    @Override
    public void reactToView(View view) {
        view.showErrorDialog(getMessage());
    }
}
