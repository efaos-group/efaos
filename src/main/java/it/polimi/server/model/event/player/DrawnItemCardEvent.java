/**
 * Event rised after drawing an item card
 */
package it.polimi.server.model.event.player;

import it.polimi.common.observer.View;
import it.polimi.server.model.ItemCard;

import java.io.Serializable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class DrawnItemCardEvent extends DrawnEvent implements Serializable {
    private static final long serialVersionUID = -5956486036792344010L;


    public DrawnItemCardEvent(String playerID, ItemCard card) {
        super(playerID, card);
    }
    
    @Override
    protected void enableActions(View view) {
        view.disableAll();
        view.enableItemUsage();
        view.enableEndTurn();
    }

}
