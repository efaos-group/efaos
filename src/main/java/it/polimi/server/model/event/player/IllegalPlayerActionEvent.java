/**
 *
 */
package it.polimi.server.model.event.player;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.View;

import java.io.Serializable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class IllegalPlayerActionEvent extends ModelEvent implements Serializable {
    private static final long serialVersionUID = -4209510864363645394L;

    private static final String MESSAGE = "You can't perform this action!";
    public IllegalPlayerActionEvent(String playerID, String message) {
        super(MESSAGE + message, playerID);
    }

    @Override
    public void reactToView(View view) {
        String playerID = view.getPlayerID();
        if ( playerID.equals(getPlayerID()) ) {
            view.showErrorDialog(MESSAGE);
        }
    }

}
