/**
 *
 */
package it.polimi.server.model.event.movement;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.View;

import java.io.Serializable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class IllegalMovementEvent extends ModelEvent implements Serializable {
    private static final long serialVersionUID = 2307112395746941900L;

    private static final String MESSAGE = "You performed an illegal movement.\nTry a different Sector.";

    public IllegalMovementEvent(String playerID) {
        super(MESSAGE, playerID);
    }

    @Override
    public void reactToView(View view) {
        String playerID = view.getPlayerID();
        if ( playerID.equals(getPlayerID()) ) {
            view.showErrorDialog(MESSAGE);
            enableActions(view);
            view.askForCommand();
        }
    }

    private void enableActions(View view) {
        view.disableAll();
        view.enableMovement();
    }
}
