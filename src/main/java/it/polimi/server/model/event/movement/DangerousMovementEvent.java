/**
 *
 */
package it.polimi.server.model.event.movement;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.View;
import it.polimi.server.model.Sector;

import java.io.Serializable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class DangerousMovementEvent extends ModelEvent implements Serializable {
    private static final long serialVersionUID = -6160140133410467812L;

    private static final String MESSAGE = "You moved on a dangerous Sector:";
    private final String sector;

    public DangerousMovementEvent(String playerID, Sector sector) {
        super(MESSAGE + sector.toString(), playerID);
        this.sector = sector.toString();
    }

    @Override
    public void reactToView(View view) {
        String playerID = view.getPlayerID();
        if ( playerID.equals(getPlayerID())) {
            view.showEventDialog(getMessage());
            view.setPosition(sector);
            enableActions(view);
            view.askForCommand();
        }
    }

    private void enableActions(View view) {
        view.disableAll();
        view.enableAttack();
        view.enableAttackCard();
        view.enableDrawSectorCard();
    }
}
