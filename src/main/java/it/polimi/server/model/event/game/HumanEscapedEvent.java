/**
 *
 */
package it.polimi.server.model.event.game;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.View;
import it.polimi.server.model.Sector;

import java.io.Serializable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class HumanEscapedEvent extends ModelEvent implements Serializable {
    private static final long serialVersionUID = -6711441317507675486L;

    private static final String ENDINGMESSAGE = " succesfully escaped!";

    private final String position;

    public HumanEscapedEvent(String playerID, Sector sector) {
        super(playerID + ENDINGMESSAGE, playerID);
        position = sector.toString();
    }

    @Override
    public void reactToView(View view) {
        view.showPictureDialog(getMessage(), "GREEN");
        view.blockEscapeSector(position);
        String playerID = view.getPlayerID();

        if (playerID.equals(getPlayerID())){
            view.setPosition(position);
            view.disableAll();
        }

    }

}
