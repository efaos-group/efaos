package it.polimi.server.model;

/**
 * The listener interface for receiving mapTable events.
 * The class that is interested in processing a mapTable
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>setMapTableListener<code> method. When
 * the mapTable event occurs, that object's appropriate
 * method is invoked.
 *
 * @see MapTableEvent
 */
public interface MapTableListener {

    /**
     * Notify illegal movement gamer.
     */
    public void notifyIllegalMovementGamer();
}
