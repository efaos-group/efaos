package it.polimi.server.model;

public interface SectorListener {
    void movedIntoEscapeSector(Sector sector);

    void movedIntoDangerousSector(Sector sector);

    void movedIntoSecureSector(Sector sector);
}
