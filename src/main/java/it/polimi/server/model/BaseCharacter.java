/**
 *
 */
package it.polimi.server.model;

import it.polimi.server.model.exception.IllegalMovementException;

/**
 * The Class BaseCharacter.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public abstract class BaseCharacter {

    /** The position of the character. */
    protected Sector position;

    /** The movement state. */
    protected CharacterMovementState movementState;

    /** The alive flag. */
    protected boolean alive;

    /** The defense flag. */
    protected boolean defense;

    /** The name of the character. */
    protected final String name;

    /** The listener. */
    protected CharacterListener parent;


    /**
     * Instantiates a new base character.
     *
     * @param name the name
     */
    public BaseCharacter(String name) {
        this.name = name;
        this.alive = true;
    }

    public Sector getPosition() {
        return position;
    }

    protected void setPosition(Sector position) {
        this.position = position;
    }

    public void setParent(CharacterListener parent){
        this.parent = parent;
    }

    /**
     * Checks if this character can move to {to}.
     *
     * @param to the to
     * @return true, if successful
     */
    public boolean canMoveTo(Sector to) {
        return movementState.canMove(position, to);
    }

    /**
     * Moves to {to}.
     *
     * @param to the to
     */
    public void moveTo(Sector to) {
        if (canMoveTo(to)) {
            setPosition(to);
            to.movedInto();
        } else {
            throw new IllegalMovementException("Sector is not a valid destination");
        }
    }

    /**
     * Checks if the character is alive.
     *
     */
    public boolean isAlive() {
        return alive;
    }

    /**
     * Makes the character die.
     */
    public void die() {
        this.alive = false;
        parent.notifyDeath();
    }

    public String getName() {
        return name;
    }

    public boolean canUseCard() {
        return false;
    }

    public void setDefense(boolean defense) {
        this.defense = defense;
    }

    /**
     * Boosts movement.
     */
    public abstract void boostMovement();

    /**
     * Makes the character attacks.
     */
    public abstract void attack();

    /**
     * Checks if the character can defend itself.
     *
     * @return true, if successful
     */
    public abstract boolean canDefend();

    /**
     * Performs a teleport action changing its position.
     *
     * @param to the to
     */
    public abstract void teleportTo(Sector to);

    public abstract boolean isSedated();

    /**
     * Sedates the character.
     */
    public abstract void sedate();

    /**
     * Clears all state.
     */
    public abstract void resetStatus();

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public int hashCode() {

        final int prime = 92821;
        int hashCode = prime + name.hashCode();
        return prime * hashCode + position.hashCode();
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof BaseCharacter)) {
            return false;
        }
        if (obj == this) {
            return true;
        }

        BaseCharacter baseCharacter = (BaseCharacter) obj;
        return  this.name == baseCharacter.getName() && baseCharacter.canUseCard() == canUseCard();
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public String toString() {
        return "BaseCharacter";
    }


}
