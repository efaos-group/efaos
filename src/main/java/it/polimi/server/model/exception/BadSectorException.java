/**
 *
 */
package it.polimi.server.model.exception;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 *
 */
public class BadSectorException extends RuntimeException {
    private static final long serialVersionUID = -5057200125082943856L;

    public BadSectorException(String message) {
        super(message);
    }

}
