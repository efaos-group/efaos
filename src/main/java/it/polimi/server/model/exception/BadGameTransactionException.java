/**
 *
 */
package it.polimi.server.model.exception;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class BadGameTransactionException extends RuntimeException {
    private static final long serialVersionUID = 2097790364151299889L;

    public BadGameTransactionException(String message) {
        super(message);
    }

}
