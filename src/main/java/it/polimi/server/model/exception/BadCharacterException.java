/**
 *
 */
package it.polimi.server.model.exception;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class BadCharacterException extends RuntimeException {
    private static final long serialVersionUID = 3760942398593386342L;

    public BadCharacterException(String message) {
        super(message);
    }

}
