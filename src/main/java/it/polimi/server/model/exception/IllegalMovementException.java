/**
 *
 */
package it.polimi.server.model.exception;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 *
 */
public class IllegalMovementException extends RuntimeException {
    private static final long serialVersionUID = 2190745529145084435L;

    public IllegalMovementException(String message) {
        super(message);
    }

}
