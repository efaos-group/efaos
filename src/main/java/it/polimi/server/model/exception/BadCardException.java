/**
 *
 */
package it.polimi.server.model.exception;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class BadCardException extends RuntimeException {
    private static final long serialVersionUID = 497146352896915972L;

    public BadCardException(String message) {
        super(message);
    }

}
