package it.polimi.server.model.exception;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 *
 */
public class GameException extends RuntimeException {
    private static final long serialVersionUID = -4917500521225776666L;

    public GameException(String message) {
        super(message);
    }

}
