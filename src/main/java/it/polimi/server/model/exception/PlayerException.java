/**
 *
 */
package it.polimi.server.model.exception;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 *
 */
public class PlayerException extends RuntimeException {
    private static final long serialVersionUID = -5610163615506669909L;

    public PlayerException(String message) {
        super(message);
    }

}
