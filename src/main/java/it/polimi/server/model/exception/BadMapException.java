/**
 *
 */
package it.polimi.server.model.exception;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 *
 */
public class BadMapException extends RuntimeException {
    private static final long serialVersionUID = 2554301115976159767L;

    public BadMapException(String message) {
        super(message);
    }

}
