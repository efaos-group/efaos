/**
 *
 */
package it.polimi.server.model;


/**
 * The Interface SectorDeck.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public interface SectorDeck {

    /**
     * The Enum SectorCardType.
     */
    public static enum SectorCardType {
        NOISE_IN_YOUR_SECTOR(4), NOISE_IN_ANY_SECTOR(4), SILENCE(5),
        NOISE_IN_YOUR_SECTOR_ITEM(4), NOISE_IN_ANY_SECTOR_ITEM(4);

        private final int max;

        private SectorCardType(int max) {
            this.max = max;
        }

        public int getMaxCard(){
            return max;
        }

        /**
         * Parses the input.
         *
         * @param input the input
         * @return the sector card type
         */
        public static SectorCardType parseInput(String input) {
            return Enum.valueOf(SectorCardType.class, input.toUpperCase());
        }
    }

    /**
     * Draws a sector card.
     *
     * @return the sector card
     */
    public SectorCard draw();

    /**
     * Notifies that a sector card has been discarded and adds it to the discard deck.
     *
     * @param card the card
     */
    public void discarded(SectorCard card);
}
