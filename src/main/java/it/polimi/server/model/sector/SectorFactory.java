/**
 *
 */
package it.polimi.server.model.sector;

import it.polimi.server.model.Sector;
import it.polimi.server.model.SectorListener;
import it.polimi.server.model.SectorType;
import it.polimi.server.model.exception.BadSectorException;
import it.polimi.server.model.sector.unwalkable.AlienSector;
import it.polimi.server.model.sector.unwalkable.HumanSector;
import it.polimi.server.model.sector.unwalkable.InvisibleSector;
import it.polimi.server.model.sector.walkable.DangerousSector;
import it.polimi.server.model.sector.walkable.EscapeSector;
import it.polimi.server.model.sector.walkable.SecureSector;

/**
 * A factory for creating Sector objects.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class SectorFactory {

    private SectorFactory() {
    }

    /**
     * Creates a new sector from the type.
     *
     * @param choice the choice
     * @param location the location
     * @param receiver the receiver
     * @return the sector
     *
     * @throws BadSectorException if the sector specified does not exists.
     */
    public static Sector factory(SectorType choice, Coordinate location, SectorListener receiver) {

        switch(choice) {
            case ALIEN:
                return new AlienSector(location.getCol(), location.getRow());
            case DANGEROUS:
                return new DangerousSector(location.getCol(), location.getRow(), receiver);
            case ESCAPE:
                return new EscapeSector(location.getCol(), location.getRow(), receiver);
            case HUMAN:
                return new HumanSector(location.getCol(), location.getRow());
            case INVISIBLE:
                return new InvisibleSector(location.getCol(), location.getRow());
            case SECURE:
                return new SecureSector(location.getCol(), location.getRow(), receiver);
            default:
                break;
        }

        throw new BadSectorException("Not existing class " + choice);
    }
}
