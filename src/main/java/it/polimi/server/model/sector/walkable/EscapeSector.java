/**
 *
 */
package it.polimi.server.model.sector.walkable;

import it.polimi.server.model.Sector;
import it.polimi.server.model.SectorListener;

/**
 * The Class EscapeSector.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class EscapeSector extends Sector {

    /** The parent. */
    private final SectorListener parent;

    /**
     * Instantiates a new escape sector.
     *
     * @param col the column
     * @param row the row
     * @param parent the parent
     */
    public EscapeSector(Character col, Integer row, SectorListener parent) {
        super(col, row);
        this.parent = parent;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean isWalkable() {
        return true;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void movedInto() {
        parent.movedIntoEscapeSector(this);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public int hashCode() {
        final int prime = 92821;
        return super.hashCode() * prime;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj) || !(obj instanceof EscapeSector)) {
            return false;
        }

        EscapeSector other = (EscapeSector) obj;
        if (parent == null) {
            return other.parent == null;
        } else {
            return parent.equals(other.parent);
        }
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public String toString(){
        return "[" + getCol() + "," + getRow() + "]" + " Escape Sector";
    }
}
