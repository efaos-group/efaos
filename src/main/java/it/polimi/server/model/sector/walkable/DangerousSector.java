/**
 *
 */
package it.polimi.server.model.sector.walkable;

import it.polimi.server.model.SectorListener;

/**
 * The Class DangerousSector.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public final class DangerousSector extends WalkableSector {

    /** The parent. */
    private final SectorListener parent;

    /**
     * Instantiates a new dangerous sector.
     *
     * @param col the column
     * @param row the row
     * @param parent the parent
     */
    public DangerousSector(Character col, Integer row, SectorListener parent) {
        super(col, row);
        this.parent = parent;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void movedInto() {
        parent.movedIntoDangerousSector(this);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public int hashCode() {
        final int prime = 92821;
        return super.hashCode() * prime;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj) || !(obj instanceof DangerousSector)) {
            return false;
        }

        DangerousSector other = (DangerousSector) obj;
        if (parent == null) {
            return other.parent == null;
        } else {
            return parent.equals(other.parent);
        }
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public String toString(){
        return "[" + getCol() + "," + getRow() + "]" + " Dangerous Sector";
    }
}
