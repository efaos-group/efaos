/**
 *
 */
package it.polimi.server.model.sector.unwalkable;

/**
 * The Class InvisibleSector.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public final class InvisibleSector extends UnWalkableSector {

    /**
     * Instantiates a new invisible sector.
     *
     * @param col the column
     * @param row the row
     */
    public InvisibleSector(Character col, Integer row) {
        super(col, row);
    }

}
