/**
 *
 */
package it.polimi.server.model.sector.unwalkable;

/**
 * The Class HumanSector.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */

public final class HumanSector extends UnWalkableSector {

    /**
     * Instantiates a new human sector.
     *
     * @param col the column
     * @param row the row
     */
    public HumanSector(Character col, Integer row) {
        super(col, row);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public String toString(){
        return "[" + getCol() + "," + getRow() + "]" + " Human Sector";
    }
}
