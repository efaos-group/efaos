/**
 *
 */
package it.polimi.server.model.sector.walkable;

import it.polimi.server.model.Sector;

/**
 * The Class WalkableSector.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public abstract class WalkableSector extends Sector {

    /**
     * Instantiates a new walkable sector.
     *
     * @param col       the column
     * @param row       the row
     */
    public WalkableSector(Character col, Integer row) {
        super(col, row);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean isWalkable() {
        return true;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean canAlienStay() {
        return true;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public int hashCode() {
        final int prime = 92821;
        return super.hashCode() * prime;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj) || !(obj instanceof WalkableSector)) {
            return false;
        }
        return true;
    }

}
