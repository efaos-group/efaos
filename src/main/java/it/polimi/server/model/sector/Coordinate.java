/**
 *
 */
package it.polimi.server.model.sector;

import java.io.Serializable;

/**
 * The Class Coordinate.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class Coordinate implements Serializable {
    private static final long serialVersionUID = 7996607548543664706L;

    /** The x. */
    private final Integer x;

    /** The y. */
    private final Character y;

    /**
     * Instantiates a new coordinate.
     *
     * @param y the y
     * @param x the x
     */
    public Coordinate(Character y, Integer x) {
        this.y = y;
        this.x = x;
    }

    /**
     * Gets the row.
     *
     * @return the row
     */
    public Integer getRow() {
        return x;
    }

    /**
     * Gets the column.
     *
     * @return the column
     */
    public Character getCol() {
        return y;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public String toString() {
        return "["+ y + ";" + x + "]";
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + y;
        result = prime * result + x;
        return result;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Coordinate){
            return ((Coordinate)obj).getRow() == x && ((Coordinate)obj).getCol() == y;
        }
        return false;
    }
}