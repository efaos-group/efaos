/**
 *
 */
package it.polimi.server.model.sector.unwalkable;

/**
 * The Class AlienSector.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public final class AlienSector extends UnWalkableSector {

    /**
     * Instantiates a new alien sector.
     *
     * @param col the column
     * @param row the row
     */
    public AlienSector(Character col, Integer row) {
        super(col, row);
    }
}
