/**
 *
 */
package it.polimi.server.model.sector.unwalkable;

import it.polimi.server.model.Sector;
import it.polimi.server.model.exception.IllegalMovementException;

/**
 * The Class UnWalkableSector.
 *
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public abstract class UnWalkableSector extends Sector {

    /**
     * Instantiates a new non-walkable sector.
     *
     * @param col the column
     * @param row the row
     */
    public UnWalkableSector(Character col, Integer row) {
        super(col, row);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean isWalkable() {
        return false;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void movedInto() {
        throw new IllegalMovementException("You can't move into an unwalkable sector");
    }

}
