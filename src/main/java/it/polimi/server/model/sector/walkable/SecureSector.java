/**
 *
 */
package it.polimi.server.model.sector.walkable;

import it.polimi.server.model.SectorListener;

/**
 * The Class SecureSector.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public final class SecureSector extends WalkableSector {

    /** The parent. */
    private final SectorListener parent;

    /**
     * Instantiates a new secure sector.
     *
     * @param col the column
     * @param row the row
     * @param parent the parent
     */
    public SecureSector(Character col, Integer row, SectorListener parent) {
        super(col, row);
        this.parent = parent;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void movedInto() {
        parent.movedIntoSecureSector(this);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public int hashCode() {
        final int prime = 92821;
        return super.hashCode() * prime;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof SecureSector)) {
            return false;
        }
        return true;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public String toString(){
        return "[" + getCol() + "," + getRow() + "]" + " Secure Sector";
    }
}
