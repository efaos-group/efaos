package it.polimi.server.model.sector;

import it.polimi.common.map.MapType;
import it.polimi.server.model.MapLoader;
import it.polimi.server.model.Sector;
import it.polimi.server.model.SectorListener;
import it.polimi.server.model.SectorType;
import it.polimi.server.model.exception.BadMapException;
import it.polimi.server.model.sector.unwalkable.AlienSector;
import it.polimi.server.model.sector.unwalkable.HumanSector;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

/**
 * The Class Mapper.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */

public final class Mapper implements MapLoader{

    /** A map containing each type of sector. */
    private final Map<Coordinate, SectorType> constrains;

    /** The set blocked escape sector. */
    private final Set<Coordinate> blockedEscapeSector;

    /** The listener. */
    private SectorListener listener;

    /**
     * Instantiates a new Mapper.
     *
     * @param map the map
     * @throws IOException if the resource containing the map is unreachable.
     */
    public Mapper(MapType map) throws IOException {
        constrains = new HashMap<Coordinate, SectorType>();
        this.blockedEscapeSector = new HashSet<Coordinate>();
        loadConfig(map.getPath());
    }

    /**
     * Loads the configuration file and transfers it into an HashMap.
     * Each line should follow the following schema:
     * COLUMN ROW SECTORTYPE
     * i.e.
     * A 1 INVISIBLE
     *
     * @param mapLocation the map location
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected void loadConfig(String mapLocation) throws IOException {

        URL url = this.getClass().getResource(mapLocation);
        String text = Resources.toString(url, Charsets.UTF_8);

        String[] lines = text.split("\n");

        for (int i = 0; i < lines.length; i++) {
            addSectorFromLine(lines[i]);
        }
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void blockSector(Sector sector) {
        Coordinate coordinate = new Coordinate(sector.getCol(),sector.getRow());
        blockedEscapeSector.add(coordinate);
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean isBlocked(Sector sector) {
        Coordinate position = new Coordinate(sector.getCol(), sector.getRow());
        return blockedEscapeSector.contains(position);
    }

    /**
     * Adds a single SectorType in the hash map parsing the string.
     *
     * @param line the line
     */
    private void addSectorFromLine(String line) {
        StringTokenizer tokens = new StringTokenizer(line);

        Character col = tokens.nextToken().charAt(0);
        Integer row = Integer.parseInt(tokens.nextToken());
        SectorType type = SectorType.parseInput(tokens.nextToken());

        Coordinate sector = new Coordinate(col, row);
        constrains.put(sector, type);
    }

    /**
     * Returns the specified Sector correctly initialized.
     *
     * @param col the column
     * @param row the row
     * @return the sector
     */
    @Override
    public Sector getSector(Character col, Integer row) {
        Coordinate position = new Coordinate(col, row);
        return SectorFactory.factory(constrains.get(position), position, listener);
    }

    /**
     * Returns the only human sector in the map.
     *
     * @return the human sector
     */
    @Override
    public HumanSector getHumanSector() {

        for (Coordinate sector : constrains.keySet()) {
            if (constrains.get(sector).equals(SectorType.HUMAN)) {
                return (HumanSector)getSector(sector.getCol(), sector.getRow());
            }
        }
        throw new BadMapException("Human Sector doesn't exist");
    }

    /**
     * Returns the only alien sector in the map.
     *
     * @return the alien sector
     */
    @Override
    public AlienSector getAlienSector() {

        for (Coordinate sector : constrains.keySet()) {
            if (constrains.get(sector).equals(SectorType.ALIEN)) {
                return (AlienSector)getSector(sector.getCol(), sector.getRow());
            }
        }
        throw new BadMapException("Alien Sector doesn't exist");
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void setSectorListener(SectorListener listener) {
        this.listener = listener;
    }

}
