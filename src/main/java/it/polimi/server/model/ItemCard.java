/**
 *
 */
package it.polimi.server.model;

import it.polimi.common.observer.ModelEvent;

import java.util.HashSet;
import java.util.Set;

/**
 * The Class ItemCard.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public abstract class ItemCard {

    /** The parent deck. */
    protected final ItemDeck parentDeck;

    /** The listeners. */
    protected final Set<CardListener> listeners;

    /**
     * Instantiates a new item card.
     *
     * @param itemDeck the item deck
     */
    public ItemCard(ItemDeck itemDeck) {
        this.parentDeck = itemDeck;
        this.listeners = new HashSet<CardListener>();
    }

    /**
     * Uses this card.
     *
     * @param player the player
     * @param sector the sector
     */
    public abstract void use(Player player, Sector sector);

    /**
     * Discards this card.
     *
     * @param playerID the player id
     */
    public abstract void discard(String playerID);

    /**
     * Checks if this is an attack card.
     *
     * @return true, if is attack card
     */
    public boolean isAttackCard() {
        return false;
    }

    /**
     * Checks if this is a defense card.
     *
     * @return true, if is defense card
     */
    public boolean isDefenseCard() {
        return false;
    }

    /**
     * Checks if this is a spotlight card.
     *
     * @return true, if is spotlight card
     */
    public boolean isSpotlightCard() {
        return false;
    }

    /**
     * Adds the card listener.
     *
     * @param listener the listener
     */
    public void addCardListener(CardListener listener){
        listeners.add(listener);
    }

    /**
     * Fires card event.
     *
     * @param evt the evt
     */
    public void fireCardEvent(ModelEvent evt){
        for(CardListener listener : listeners) {
            listener.notifyCardUsed(evt);
        }
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean equals(Object obj){
        if(obj == this) {
            return true;
        } else if (obj == null) {
            return false;
        }
        return obj.getClass() == getClass();
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public int hashCode(){
        return toString().hashCode();
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public String toString() {
        return "ItemCard";
    }

}
