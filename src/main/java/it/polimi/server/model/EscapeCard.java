/**
 *
 */
package it.polimi.server.model;

/**
 * The Class EscapeCard.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public abstract class EscapeCard {

    /**
     * Checks if this card let the human escape.
     *
     * @return true, if successful
     */
    public abstract boolean canEscape();

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public boolean equals(Object obj){
        return obj instanceof EscapeCard;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public int hashCode(){
        return "ESCAPECARD".hashCode();
    }

}
