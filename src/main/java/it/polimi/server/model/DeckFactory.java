/**
 *
 */
package it.polimi.server.model;

/**
 * A factory for creating Deck objects.
 *
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public interface DeckFactory {
    public EscapeDeck getEscapeDeckInstance();

    public ItemDeck getItemDeckInstance();

    public SectorDeck getSectorDeckInstance();
}
