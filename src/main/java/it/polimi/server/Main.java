/**
 *
 */
package it.polimi.server;

import it.polimi.network.rmi.publisher.PublisherRMI;
import it.polimi.network.rmi.server.RMIServer;
import it.polimi.network.socket.publisher.PublisherSocket;
import it.polimi.network.socket.server.SocketServer;
import it.polimi.server.controller.balancer.LoadBalancer;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class Main {

    private Main() {
    }

    public static void main(String[] args) throws RemoteException {

        LocateRegistry.createRegistry(1099);

        PublisherSocket publisherSocket = new PublisherSocket();
        new Thread(publisherSocket).start();

        PublisherRMI publisherRMI = new PublisherRMI();

        LoadBalancer balancer = new LoadBalancer(publisherSocket, publisherRMI);

        SocketServer serverSocket = new SocketServer(balancer);
        new Thread(serverSocket).start();

        new RMIServer(balancer).bindServer();
    }

}
