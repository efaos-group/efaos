/**
 *
 */
package it.polimi.server.controller.manager;

import it.polimi.server.controller.Controller;
import it.polimi.server.controller.exception.GamerCheatException;
import it.polimi.server.model.GameLogic;
import it.polimi.server.model.ItemCard;
import it.polimi.server.model.Sector;
import it.polimi.server.model.exception.BadSectorException;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class ModelController implements Controller {

    private static final String ERROR_MESSAGE = "This player can't play beacause it's not his/her turn";

    private final GameLogic game;

    public ModelController(GameLogic game) {
        this.game = game;
    }

    @Override
    public void move(String playerID, String col, String rowString) {

        validatePlayer(playerID);
        validateSector(col, rowString);

        Sector to = getSector(col, rowString);
        game.move(to);
    }

    @Override
    public void passTurn(String playerID){
        validatePlayer(playerID);

        game.passTurn();
    }

    @Override
    public void useCard(String playerID, String cardType, String col, String rowString) {
        validatePlayer(playerID);

        ItemCard card = game.getItemCard(cardType);
        Sector sector;

        if(card.isSpotlightCard()) {
            validateSector(col, rowString);
            sector = getSector(col, rowString);
        } else {
            sector = null;
        }

        game.useCard(card, sector);
    }

    @Override
    public void drawItemCard(String playerID) {
        validatePlayer(playerID);

        game.drawItemCard();
    }

    @Override
    public void discardCard(String playerID, String cardType) {
        validatePlayer(playerID);

        ItemCard card = game.getItemCard(cardType);
        game.discardCard(card);
    }

    @Override
    public void drawSectorCard(String playerID) {
        validatePlayer(playerID);

        game.drawSector();
    }

    @Override
    public void attack(String playerID) {
        validatePlayer(playerID);

        game.attack();
    }

    @Override
    public void selectSectorForNoise(String playerID, String col, String rowString) {
        validatePlayer(playerID);
        Sector to = getSector(col, rowString);
        game.selectSectorForNoise(to);
    }

    protected void validatePlayer(String playerID) {
        String gamer = game.getGamerIdentifier();
        if(!gamer.equals(playerID)) {
            throw new GamerCheatException(ERROR_MESSAGE);
        }
    }

    protected void validateSector(String col, String rowString) {
        if(!StringUtils.isNumeric(rowString)) {
            throw new BadSectorException("fromRow must be a number");
        }
        if(StringUtils.isNumeric(col)) {
            throw new BadSectorException("toRow must be a number");
        }
    }

    protected Sector getSector(String col, String rowString) {

        Integer row = Integer.parseInt(rowString);
        return game.getSector(col.charAt(0), row);
    }

    @Override
    public void removeDisconnectedPlayerFromGame(String playerID) {
        game.removeDisconnectedPlayerFromGame(playerID);
    }

}
