/**
 *
 */
package it.polimi.server.controller.manager;

import it.polimi.common.map.MapType;
import it.polimi.common.timer.GameTimer;
import it.polimi.common.timer.TimerReceiver;
import it.polimi.network.Topic;
import it.polimi.network.rmi.publisher.PublisherRMI;
import it.polimi.network.socket.BrokerSocket;
import it.polimi.server.controller.Controller;
import it.polimi.server.controller.Room;
import it.polimi.server.controller.StartGameListener;
import it.polimi.server.controller.exception.GameStartedException;
import it.polimi.server.controller.exception.LoadMapException;
import it.polimi.server.model.CharacterFactory;
import it.polimi.server.model.CharacterFactory.CharacterType;
import it.polimi.server.model.DeckFactory;
import it.polimi.server.model.GameLogic;
import it.polimi.server.model.MapTable;
import it.polimi.server.model.Notifier;
import it.polimi.server.model.Player;
import it.polimi.server.model.card.DeckCardFactory;
import it.polimi.server.model.character.BaseCharacterFactory;
import it.polimi.server.model.game.Logic;
import it.polimi.server.model.map.Table;
import it.polimi.server.model.notifier.NotificationHandler;
import it.polimi.server.model.player.Gamer;
import it.polimi.server.model.sector.Mapper;
import it.polimi.server.view.RemoteView;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.logging.Logger;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 *
 */
public class GameInitializer implements Room, TimerReceiver {

    private static final Logger LOGGER = Logger.getLogger(GameInitializer.class.getName());

    private static final Integer MAXPLAYERS = 8;
    private static final Integer MINPLAYERS = 2;
    private static final Integer WAITPLAYER = 1 * 60 * 1000;  // 1 minute

    private StartGameListener listener;
    private Topic topic;

    private final MapType map;
    private CharacterFactory charFactory;
    private final DeckFactory deckFactory;

    private GameTimer timer;
    private volatile boolean started;

    private Controller controller;

    private final BrokerSocket publisher;
    private final PublisherRMI publisherRMI;

    private Map<String, Player> players;


    public GameInitializer(MapType mapType, BrokerSocket publisher, PublisherRMI publisherRMI) throws IOException {

        this.topic = new Topic(mapType.name());

        this.charFactory = new BaseCharacterFactory();
        this.deckFactory = new DeckCardFactory();

        this.map = mapType;

        this.publisher = publisher;
        this.publisherRMI = publisherRMI;

        this.started = false;
        this.timer = new GameTimer(this);

        this.players = new HashMap<String, Player>();
    }

    @Override
    public synchronized void addPlayer(String username) {

        if(isStarted()) {
            throw new GameStartedException("It's not possible to add more player while a game is running");
        }
        players.put(username, null);

        if(players.size() == MINPLAYERS){
            timer.startGameCountDown(WAITPLAYER);
        } else if(players.size() == MAXPLAYERS) {
            timer.stop();
            setStarted(true);
            startGameInThread();

        }
    }

    private void startGameInThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    LOGGER.warning("Sleep terminated uncorrectly " + e);
                }
                startGame();
            }
        }).start();
    }

    private synchronized void startGame() {

        MapTable table = getNewMapInstance();
        Map<String, Player> newPlayers = intializePlayer(table);
        Notifier notifier = new NotificationHandler();
        RemoteView view = new RemoteView(topic, publisher, publisherRMI);
        notifier.register(view);


        GameLogic game = new Logic(table, newPlayers, notifier, deckFactory);
        controller = new ModelController(game);

        listener.gameStarted(this, topic, controller);
    }

    private MapTable getNewMapInstance() {
        MapTable table = null;
        try {
            table = new Table(new Mapper(this.map));
        } catch(IOException e) {
            throw new LoadMapException("For some weird error the map can't be initialized " + e);
        }
        return table;
    }

    private Map<String, Player> intializePlayer(MapTable table) {
        Random rand = new Random();
        Map<String, Player> newPlayers = new HashMap<String, Player>();
        int human = players.size() / 2;
        int size = players.size();

        for(String key: players.keySet()) {

            if(rand.nextInt(size) < human){
                newPlayers.put(key, playerFactory(key, CharacterType.HUMAN, table));
                human--;
            } else {
                newPlayers.put(key, playerFactory(key, CharacterType.ALIEN, table));
            }
            size--;
        }
        return newPlayers;
    }

    private Player playerFactory(String key, CharacterType type, MapTable table) {
        return new Gamer(key, charFactory.factory(type, table), table);
    }

    @Override
    public boolean isStarted(){
        return started;
    }

    @Override
    public void setListener(StartGameListener listener) {
        this.listener = listener;
    }

    @Override
    public Controller getController() {
        return controller;
    }

    @Override
    public Topic getTopic() {
        return topic;
    }

    protected void setStarted(boolean start){
        this.started = start;
    }

    @Override
    public void endGameTimer() {
        timer.stop();
        startGame();
    }

    @Override
    public synchronized void reset() {
        this.players.clear();
        this.charFactory = new BaseCharacterFactory();

        this.started = false;
        this.timer = new GameTimer(this);

        this.controller = null;
        this.topic = new Topic(map.name());
    }

    @Override
    public synchronized void removeDisconnectedPlayer(String playerID) {
        if(!isStarted()) {
            removeFromRoom(playerID);
        } else {
            throw new GameStartedException("Impossible to remove a player because the game is started!");
        }
    }

    private void removeFromRoom(String playerID) {

        players.remove(playerID);
        if(players.size() < MINPLAYERS) {
            timer.stop();
            timer = new GameTimer(this);
        }
    }
}
