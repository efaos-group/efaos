/**
 *
 */
package it.polimi.server.controller;

import it.polimi.network.Topic;


/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public interface Room {

    public Controller getController();

    public void setListener(StartGameListener listener);

    public Topic getTopic();

    public void addPlayer(String username);

    public void removeDisconnectedPlayer(String playerID);

    public boolean isStarted();

    public void reset();

}
