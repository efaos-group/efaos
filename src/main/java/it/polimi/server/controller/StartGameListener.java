/**
 *
 */
package it.polimi.server.controller;

import it.polimi.network.Topic;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public interface StartGameListener {

    public void gameStarted(Room source, Topic topic, Controller controller);

}
