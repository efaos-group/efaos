/**
 *
 */
package it.polimi.server.controller.balancer;

import it.polimi.common.map.MapType;
import it.polimi.network.Communicator;
import it.polimi.network.CommunicatorListener;
import it.polimi.network.ProtocolParser;
import it.polimi.network.Topic;
import it.polimi.network.rmi.publisher.PublisherRMI;
import it.polimi.network.socket.publisher.PublisherSocket;
import it.polimi.server.controller.Controller;
import it.polimi.server.controller.Room;
import it.polimi.server.controller.StartGameListener;
import it.polimi.server.controller.exception.BadPlayerCommandException;
import it.polimi.server.controller.exception.GameStartedException;
import it.polimi.server.controller.exception.GamerCheatException;
import it.polimi.server.controller.manager.GameInitializer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class LoadBalancer implements CommunicatorListener, StartGameListener {

    private static final Logger LOGGER = Logger.getLogger(LoadBalancer.class.getName());

    private final ProtocolParser parser;

    private final PublisherSocket publisherSocket;
    private final PublisherRMI publisherRMI;

    private final Map<Communicator, String> usernames;
    private final Map<MapType, Room> rooms;

    private final Map<Topic, Controller> games;


    public LoadBalancer(PublisherSocket publisherSocket, PublisherRMI publisherRMI) {
        parser = new ProtocolParser();

        this.publisherSocket = publisherSocket;
        this.publisherRMI = publisherRMI;

        this.usernames = new HashMap<Communicator, String>();
        this.rooms = new HashMap<MapType, Room>();

        this.games = new HashMap<Topic, Controller>();

        initRooms();
    }

    private void initRooms() {
        try{
            for(MapType map : MapType.values()) {
                GameInitializer room = new GameInitializer(map, publisherSocket, publisherRMI);
                room.setListener(this);
                rooms.put(map, room);
            }
        } catch(IOException e) {
            throw new AssertionError("Can't create the rooms", e);
        }
    }

    @Override
    public synchronized void notifyMessageReceived(Communicator socketSource, String msg) {
        parser.parseMessage(msg);

        String topic = parser.getCommands()[0];
        String username = parser.getCommands()[1];
        String command = parser.getCommands()[2];

        if(isGameTopic(topic)) {
            analyzeRequest(topic, username, command);
        } else if(isNewPlayer(topic) && !isUsernameUsed(username)) {
            Topic room = addToRoom(username, command);
            publishTopic(room);
            usernames.put(socketSource, username);
            setAcceptedPlayerMessage(room, username);
        } else {
            setRejectMessage(username);
        }

        sendAnswerMessage(socketSource);
    }

    private boolean isGameTopic(String topic) {
        for(Topic id : games.keySet()) {
            if(topic.equals(id.getMinimalTopicCode())) {
                return true;
            }
        }
        return false;
    }

    private boolean isNewPlayer(String topic) {
        return topic.equals(ProtocolParser.NEW_CONNECTION+"");
    }

    private boolean isUsernameUsed(String username) {
        return usernames.values().contains(username);
    }

    private void analyzeRequest(String topic, String username, String command) {
        Topic topicObject = getTopic(topic);

        try {
            methodFactory(command, games.get(topicObject), username);
        } catch(BadPlayerCommandException | GamerCheatException e) {
            LOGGER.severe(e + "");
        }
    }

    private Topic addToRoom(String username, String map) {
        MapType mapType = MapType.parseInput(map);
        Room room = rooms.get(mapType);
        addPlayer(username, room);
        return room.getTopic();
    }

    private void addPlayer(String username, Room room) {
        try {
            room.addPlayer(username);
        } catch(GameStartedException e) {
            LOGGER.warning("Too many request in the same time" + e);
            waitMillis(500);
            addPlayer(username, room);
        }
    }

    private void publishTopic(Topic topic) {
        publisherSocket.addTopicToBuffer(topic);
        publisherRMI.addTopicToBuffer(topic);
    }

    private void setAcceptedPlayerMessage(Topic topic, String username) {
        publisherSocket.addTopicToBuffer(topic);
        publisherRMI.addTopicToBuffer(topic);

        String[] commands = new String[] {
                ProtocolParser.ACK+"",
                username,
                topic.getMinimalTopicCode()
        };
        parser.setCommands(commands);
    }

    private void setRejectMessage(String username) {

        String[] commands = new String[] {
                ProtocolParser.REJECTED+"",
                username,
                " "
        };
        parser.setCommands(commands);
    }

    private void sendAnswerMessage(Communicator client) {
        client.send(parser.getParsedMessage());
    }

    @Override
    public synchronized void gameStarted(Room source, Topic topic, Controller controller) {
        games.put(topic, controller);
        source.reset();
    }

    private Topic getTopic(String topic) {
        Topic topicObject = null;
        for(Topic id: games.keySet()) {
            if(id.equalsToMinimalCode(topic)) {
                topicObject = id;
            }
        }
        return topicObject;
    }

    protected void methodFactory(String method, Controller controller, String username) {
        String[] args = parser.getMessageArguments();

        switch(method) {
            case ProtocolParser.PASS_TURN:
                controller.passTurn(username);
                break;
            case ProtocolParser.MOVE:
                controller.move(username, args[0], args[1]);
                break;
            case ProtocolParser.USE_CARD:
                controller.useCard(username, args[0], args[1], args[2]);
                break;
            case ProtocolParser.DRAW_ITEM_CARD:
                controller.drawItemCard(username);
                break;
            case ProtocolParser.DISCARD_CARD:
                controller.discardCard(username, args[0]);
                break;
            case ProtocolParser.DRAW_SECTOR_CARD:
                controller.drawSectorCard(username);
                break;
            case ProtocolParser.ATTACK:
                controller.attack(username);
                break;
            case ProtocolParser.SELECT_SECTOR:
                controller.selectSectorForNoise(username, args[0], args[1]);
                break;
            default:
                throw new BadPlayerCommandException("Impossible to find the function specified!");
        }

    }

    @Override
    public void notifyConnectionClosed(Communicator socketSource) {
        String playerID = usernames.get(socketSource);

        removeFromGames(playerID);
        removeFromRooms(playerID);
        usernames.remove(socketSource);
    }

    private void removeFromGames(String playerID) {
        for(Controller game : games.values()) {
            game.removeDisconnectedPlayerFromGame(playerID);
        }
    }

    private void removeFromRooms(String playerID) {
        try {
            for(Room room: rooms.values()) {
                room.removeDisconnectedPlayer(playerID);
            }
        } catch(GameStartedException e) {
            LOGGER.warning("Tried to delete a player while the game was created. " + e);
            waitMillis(5);
            removeFromGames(playerID);
        }
    }

    private void waitMillis(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            LOGGER.severe("Error while waiting " + e);
        }
    }

}
