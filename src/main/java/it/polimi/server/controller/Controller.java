/**
 *
 */
package it.polimi.server.controller;



/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public interface Controller {

    public void passTurn(String playerID);

    public void move(String playerID, String col, String rowString);

    public void useCard(String playerID, String cardType, String col, String rowString);

    public void drawItemCard(String playerID);

    public void discardCard(String playerID, String cardType);

    public void drawSectorCard(String playerID);

    public void attack(String playerID);

    public void selectSectorForNoise(String playerID, String col, String rowString);

    public void removeDisconnectedPlayerFromGame(String playerID);
}
