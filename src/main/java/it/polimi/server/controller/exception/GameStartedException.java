/**
 *
 */
package it.polimi.server.controller.exception;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class GameStartedException extends RuntimeException {
    private static final long serialVersionUID = -361186601824777916L;

    public GameStartedException(String message) {
        super(message);
    }
}
