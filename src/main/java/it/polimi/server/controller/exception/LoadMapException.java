/**
 *
 */
package it.polimi.server.controller.exception;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class LoadMapException extends RuntimeException {
    private static final long serialVersionUID = 6399060780654636750L;

    public LoadMapException(String message) {
        super(message);
    }
}
