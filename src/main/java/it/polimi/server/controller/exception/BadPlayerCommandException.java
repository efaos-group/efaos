/**
 *
 */
package it.polimi.server.controller.exception;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class BadPlayerCommandException extends RuntimeException {
    private static final long serialVersionUID = 7356916001030323052L;

    public BadPlayerCommandException(String message){
        super(message);
    }
}
