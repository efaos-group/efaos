/**
 *
 */
package it.polimi.server.controller.exception;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class GamerCheatException extends RuntimeException {
    private static final long serialVersionUID = 9220911222962389877L;

    public GamerCheatException(String message) {
        super(message);
    }

}
