/**
 *
 */
package it.polimi.server.view;

import it.polimi.common.observer.ModelEvent;
import it.polimi.common.observer.Observable;
import it.polimi.common.observer.Observer;
import it.polimi.network.Topic;
import it.polimi.network.rmi.publisher.PublisherRMI;
import it.polimi.network.socket.BrokerSocket;

/**
 * The Class RemoteView.
 */
public class RemoteView implements Observer {

    private final Topic topic;
    private final BrokerSocket publisher;
    private final PublisherRMI publisherRMI;

    /**
     * Instantiates a new remote view.
     *
     * @param topic         the topic
     * @param publisher     the publisher
     * @param publisherRMI  the publisher RMI
     */
    public RemoteView(Topic topic, BrokerSocket publisher, PublisherRMI publisherRMI) {
        this.topic = topic;
        this.publisher = publisher;
        this.publisherRMI = publisherRMI;
    }

    /**
      * {@inheritDoc}
      *
      */
    @Override
    public void notify(Observable source, ModelEvent gameEvent) {
        this.publisher.publish(topic, gameEvent);
        this.publisherRMI.publish(topic, gameEvent);
    }

}
