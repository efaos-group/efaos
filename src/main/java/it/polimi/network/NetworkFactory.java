/**
 *
 */
package it.polimi.network;


/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public interface NetworkFactory {

    public Subscriber getNewSubscriber(String topic);

    public Communicator getNewDirectConnection();

}
