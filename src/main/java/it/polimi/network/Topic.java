/**
 *
 */
package it.polimi.network;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class Topic {

    private static final Logger LOGGER = Logger.getLogger(Topic.class.getName());
    private static final Integer START_MINIMAL = 0;
    private static final Integer END_MINIMAL = 10;

    private final String mapType;
    private final Long unixTime;
    private final String topicCode;


    public Topic(String mapType) {
        this.mapType = mapType;
        this.unixTime = getUnixTime();
        this.topicCode = generateTopicCode(this.mapType, unixTime);
    }

    private Long getUnixTime() {
        return System.nanoTime();
    }

    private String generateTopicCode(String mapType, Long timestamp) {
        MessageDigest digest = null;
        byte[] hashedBytes = null;

        try {

            digest = MessageDigest.getInstance("SHA-256");
            digest.update((timestamp + "").getBytes("UTF-8"));
            hashedBytes = digest.digest(mapType.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            LOGGER.severe("Impossible to find SHA-256 algorithoms for some weird reason. " + e);
        } catch (UnsupportedEncodingException e) {
            LOGGER.severe("Impossible to encode in utf-8 charset for some weird reason. " + e);
        }

        return convertByteArrayToHexString(hashedBytes);
    }

    private static String convertByteArrayToHexString(byte[] arrayBytes) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < arrayBytes.length; i++) {
            stringBuilder.append(Integer.toString((arrayBytes[i] & 0xff) + 0x100, 16)
                    .substring(1));
        }
        return stringBuilder.toString();
    }

    public String getMinimalTopicCode() {
        return topicCode.substring(START_MINIMAL, END_MINIMAL);
    }

    public String getCompleteTopicCode() {
        return topicCode;
    }

    @Override
    public int hashCode() {
        final int prime = 92821;
        int result = 1;
        result = prime * result + ((mapType == null) ? 0 : mapType.hashCode());
        result = prime * result
                + ((topicCode == null) ? 0 : topicCode.hashCode());
        result = prime * result
                + ((unixTime == null) ? 0 : unixTime.hashCode());
        return result;
    }

    public boolean equalsToMinimalCode(String topic) {
        return topicCode.substring(START_MINIMAL, END_MINIMAL).equals(topic);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Topic) || obj == null) {
            return false;
        }

        Topic other = (Topic) obj;
        return mapType.equals(other.mapType) &&
                unixTime == other.unixTime &&
                topicCode.equals(other.topicCode);
    }

}
