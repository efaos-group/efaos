/**
 *
 */
package it.polimi.network.exception;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class ProtocolCommandsException extends RuntimeException {
    private static final long serialVersionUID = -6650292062964885956L;

    public ProtocolCommandsException(String message) {
        super(message);
    }

}
