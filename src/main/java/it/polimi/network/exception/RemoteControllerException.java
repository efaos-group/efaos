/**
 *
 */
package it.polimi.network.exception;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class RemoteControllerException extends RuntimeException{
    private static final long serialVersionUID = 1955546459010616715L;

    public RemoteControllerException(String msg) {
        super(msg);
    }

}
