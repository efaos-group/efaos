/**
 *
 */
package it.polimi.network;



/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public interface Communicator {

    public void send(final String message);

    public boolean isActive();

    public void close();

    public void setListener(CommunicatorListener parent);
}
