/**
 *
 */
package it.polimi.network.configuration;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class NetworkConfiguration {

    public static final Integer SOCKET_PORT = 1901;
    public static final String SOCKET_HOST = "localhost";
    public static final Integer PUBLISHER_PORT = 1902;

    public static final Integer RMI_PORT = 1099;
    public static final String RMI_SERVER = "Server";
    public static final String RMI_PUBLISHER = "Broker";


    private NetworkConfiguration() {
    }
}
