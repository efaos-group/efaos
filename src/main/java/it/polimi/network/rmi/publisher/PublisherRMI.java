/**
 *
 */
package it.polimi.network.rmi.publisher;

import it.polimi.common.observer.ModelEvent;
import it.polimi.network.Topic;
import it.polimi.network.configuration.NetworkConfiguration;
import it.polimi.network.rmi.BrokerRMI;
import it.polimi.network.rmi.SubscriberRMI;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Logger;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class PublisherRMI extends UnicastRemoteObject implements BrokerRMI {
    private static final long serialVersionUID = 8170128863014860469L;

    private static final Logger LOGGER = Logger.getLogger(PublisherRMI.class.getName());

    private final transient Multimap<Topic, SubscriberRMI> subscriberList;
    private final transient List<Topic> bufferTopic;

    public PublisherRMI() throws RemoteException {
        subscriberList = ArrayListMultimap.create();
        bufferTopic = new LinkedList<Topic>();
        bindPublisher();
    }

    private void bindPublisher() {
        try {
            Registry registry = LocateRegistry.getRegistry(NetworkConfiguration.RMI_PORT);

            registry.rebind(NetworkConfiguration.RMI_PUBLISHER, this);

        } catch (RemoteException | NoSuchElementException e) {
            throw new AssertionError("Some weird error occured while creating the RMI publisher!", e);
        }
    }

    @Override
    public void subscribe(SubscriberRMI sub, String topic) throws RemoteException {

        Topic topicObject = getTopicFromLists(topic);
        bufferTopic.remove(topicObject);
        subscriberList.put(topicObject, sub);
    }

    private Topic getTopicFromLists(String topic) {
        Topic topicObject = null;

        for(Topic topicID : subscriberList.keySet()) {
            if(topicID.equalsToMinimalCode(topic)) {
                topicObject = topicID;
            }
        }

        for(Topic topicID : bufferTopic) {
            if(topicID.getMinimalTopicCode().equals(topic)) {
                topicObject = topicID;
            }
        }

        return topicObject;
    }

    public void publish(Topic topic, ModelEvent event){

        if(!subscriberList.isEmpty()){

            for (SubscriberRMI sub : subscriberList.get(topic)) {
                sendToSubscriber(event, sub);
            }
        }
    }

    private void sendToSubscriber(ModelEvent event, SubscriberRMI sub) {
        try {
            sub.dispatchMessage(event);
        } catch (RemoteException e) {
            LOGGER.severe("Error during notifing the event to the subscribers. " + e);
        }
    }

    public synchronized void addTopicToBuffer(Topic topic) {
        if(!bufferTopic.contains(topic) && !subscriberList.containsKey(topic)) {
            bufferTopic.add(topic);
        }
    }

    public void close() {
        try {
            Registry registry = LocateRegistry.getRegistry(NetworkConfiguration.RMI_PORT);
            registry.unbind("Broker");
        } catch (RemoteException | NotBoundException e) {
            LOGGER.severe("Some weird error occured while closing the RMI publisher! " + e);
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
                + ((bufferTopic == null) ? 0 : bufferTopic.hashCode());
        result = prime * result
                + ((subscriberList == null) ? 0 : subscriberList.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj) || !(obj instanceof PublisherRMI)) {
            return false;
        }

        PublisherRMI other = (PublisherRMI) obj;
        return bufferTopic.equals(other.bufferTopic) && subscriberList.equals(other.subscriberList);
    }

}
