/**
 *
 */
package it.polimi.network.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public interface RMIServerInterface extends Remote {

    public void receive(String message, ClientAdapter client) throws RemoteException;

}
