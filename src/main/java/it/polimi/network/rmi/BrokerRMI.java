/**
 *
 */
package it.polimi.network.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public interface BrokerRMI extends Remote{

    public void subscribe(SubscriberRMI sub, String topic) throws RemoteException;
}
