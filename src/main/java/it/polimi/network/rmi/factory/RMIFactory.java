/**
 *
 */
package it.polimi.network.rmi.factory;

import it.polimi.network.Communicator;
import it.polimi.network.NetworkFactory;
import it.polimi.network.Subscriber;
import it.polimi.network.rmi.client.RMIClient;
import it.polimi.network.rmi.subscriber.NetworkSubscriberRMI;

import java.rmi.RemoteException;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class RMIFactory implements NetworkFactory {

    @Override
    public Subscriber getNewSubscriber(String topic) {
        NetworkSubscriberRMI sub = new NetworkSubscriberRMI(topic);
        new Thread(sub).start();
        return sub;
    }

    @Override
    public Communicator getNewDirectConnection() {
        Communicator communicator = null;
        try {
            communicator = new RMIClient();
        } catch (RemoteException e) {
            throw new AssertionError("Error in generating RMIClient()",e);
        }
        return communicator;
    }

}
