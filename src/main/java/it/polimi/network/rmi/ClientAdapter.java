/**
 *
 */
package it.polimi.network.rmi;

import it.polimi.network.Communicator;
import it.polimi.network.CommunicatorListener;

import java.io.Serializable;
import java.rmi.RemoteException;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class ClientAdapter implements Communicator, Serializable {
    private static final long serialVersionUID = 2724651900535265931L;

    private RMIClientInterface client;

    public ClientAdapter(RMIClientInterface client) {
        this.client = client;
    }

    @Override
    public void send(final String message) {
        new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    client.receive(message);
                } catch (RemoteException e) {
                    throw new AssertionError("Error in sending message with RMI",e);
                }
            }
        }).start();
    }

    @Override
    public boolean isActive() {
        return true;
    }

    @Override
    public void close() {
        //Do nothing because it cannot be closed
    }

    @Override
    public void setListener(CommunicatorListener parent) {
        //Do nothing because it's not used
    }


}
