/**
 *
 */
package it.polimi.network.rmi.server;

import it.polimi.network.CommunicatorListener;
import it.polimi.network.configuration.NetworkConfiguration;
import it.polimi.network.rmi.ClientAdapter;
import it.polimi.network.rmi.RMIServerInterface;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.NoSuchElementException;
import java.util.logging.Logger;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class RMIServer extends UnicastRemoteObject implements RMIServerInterface{
    private static final long serialVersionUID = -4273599356391846345L;
    private static final Logger LOGGER = Logger.getLogger(RMIServer.class.getName());

    private final transient CommunicatorListener parent;

    public RMIServer(CommunicatorListener parent) throws RemoteException {
        this.parent = parent;
    }

    public void bindServer() {
        try {
            Registry registry = LocateRegistry.getRegistry(NetworkConfiguration.RMI_PORT);

            registry.rebind(NetworkConfiguration.RMI_SERVER, this);

        } catch (RemoteException | NoSuchElementException e) {
            throw new AssertionError("Some weird error occured while creating the RMI server!", e);
        }
    }

    @Override
    public void receive(String message, ClientAdapter client) throws RemoteException {
        parent.notifyMessageReceived(client, message);
    }

    public void close() {
        try {
            Registry registry = LocateRegistry.getRegistry(NetworkConfiguration.RMI_PORT);
            registry.unbind("Server");
        } catch (RemoteException | NotBoundException e) {
            LOGGER.severe("Some weird error occured while closing the RMI publisher! " + e);
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        return prime * super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj) || !(obj instanceof RMIServer)) {
            return false;
        }

        RMIServer other = (RMIServer) obj;
        return parent.equals(other.parent);
    }

}
