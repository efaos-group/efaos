/**
 *
 */
package it.polimi.network.rmi.client;

import it.polimi.network.Communicator;
import it.polimi.network.CommunicatorListener;
import it.polimi.network.configuration.NetworkConfiguration;
import it.polimi.network.rmi.ClientAdapter;
import it.polimi.network.rmi.RMIClientInterface;
import it.polimi.network.rmi.RMIServerInterface;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class RMIClient extends UnicastRemoteObject implements Communicator, RMIClientInterface {
    private static final long serialVersionUID = -3130435286538443119L;

    private transient CommunicatorListener parent;
    private ClientAdapter adapter;
    private transient RMIServerInterface server;

    public RMIClient() throws RemoteException {
        super(0);
        this.adapter = new ClientAdapter(this);
        getServer();
    }

    public void getServer() {
        try {

            server = (RMIServerInterface) Naming.lookup("//localhost/" + NetworkConfiguration.RMI_SERVER);

        } catch (NotBoundException | RemoteException | MalformedURLException e) {
            throw new AssertionError("Some weird exception error occured while estabilishing a new connection towards the server.", e);
        }
    }

    @Override
    public void send(final String message) {

        new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    server.receive(message,adapter);
                } catch (RemoteException e) {
                    throw new AssertionError("Error in sending message with RMI",e);
                }
            }
        }).start();
    }

    @Override
    public void close() {
        server = null;
    }

    @Override
    public void setListener(CommunicatorListener parent) {
        this.parent = parent;
    }

    @Override
    public boolean isActive() {
        return true;
    }

    @Override
    public void receive(String message) throws RemoteException {
        parent.notifyMessageReceived(this, message);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((adapter == null) ? 0 : adapter.hashCode());
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        result = prime * result + ((server == null) ? 0 : server.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj) || !(obj instanceof RMIClient)) {
            return false;
        }

        RMIClient other = (RMIClient) obj;
        return adapter.equals(other.adapter) &&
                parent.equals(other.parent) &&
                server.equals(other.server);
    }
}
