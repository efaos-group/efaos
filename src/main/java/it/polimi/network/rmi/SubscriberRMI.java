/**
 *
 */
package it.polimi.network.rmi;

import it.polimi.common.observer.ModelEvent;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public interface SubscriberRMI extends Remote {

    public void dispatchMessage(ModelEvent event) throws RemoteException;

}
