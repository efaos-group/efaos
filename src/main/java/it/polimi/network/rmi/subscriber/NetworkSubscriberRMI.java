/**
 *
 */
package it.polimi.network.rmi.subscriber;

import it.polimi.common.observer.ModelEvent;
import it.polimi.network.Subscriber;
import it.polimi.network.configuration.NetworkConfiguration;
import it.polimi.network.rmi.BrokerRMI;
import it.polimi.network.rmi.SubscriberRMI;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Logger;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class NetworkSubscriberRMI extends Subscriber implements SubscriberRMI {

    private static final Logger LOGGER = Logger.getLogger(NetworkSubscriberRMI.class.getName());

    private final Queue<ModelEvent> buffer;
    private String topic;

    private boolean active;

    public NetworkSubscriberRMI(String topic) {
        super();
        setTopic(topic);
        this.buffer = new ConcurrentLinkedQueue<ModelEvent>();
        active = false;

        subscribe();
    }

    private void subscribe() {
        try {

            BrokerRMI broker = (BrokerRMI) Naming.lookup("//localhost/" + NetworkConfiguration.RMI_PUBLISHER);
            broker.subscribe((SubscriberRMI)UnicastRemoteObject.exportObject(this,0), topic);

        } catch (NotBoundException | RemoteException | MalformedURLException e) {
            throw new AssertionError("Some weird exception error occured while estabilishing a new connection towards the server.", e);
        }
    }


    @Override
    public void dispatchMessage(ModelEvent event) throws RemoteException {
        buffer.add(event);
        synchronized(buffer) {
            buffer.notify();
        }
    }

    @Override
    public void close() {
        active = false;
    }

    @Override
    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Override
    public void run() {
        active = true;
        while(active) {

            ModelEvent evt = buffer.poll();
            if(evt == null){
                try {
                    synchronized(buffer) {
                        buffer.wait();
                    }
                } catch (InterruptedException e) {
                    LOGGER.severe("Thread was unexpected interrupted " + e);
                }
            } else {
                super.notify(evt);
            }
        }
    }
}
