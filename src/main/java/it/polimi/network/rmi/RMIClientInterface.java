/**
 *
 */
package it.polimi.network.rmi;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public interface RMIClientInterface extends Remote, Serializable {
    public void receive(String message) throws RemoteException;
}
