/**
 *
 */
package it.polimi.network;


/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public interface CommunicatorListener {
    public void notifyMessageReceived(Communicator socketSource, String msg);

    public void notifyConnectionClosed(Communicator socketSource);
}
