/**
 *
 */
package it.polimi.network.socket.factory;

import it.polimi.network.Communicator;
import it.polimi.network.NetworkFactory;
import it.polimi.network.Subscriber;
import it.polimi.network.socket.client.ClientSocket;
import it.polimi.network.socket.subscriber.NetworkSubscriberSocket;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class SocketFactory implements NetworkFactory {

    @Override
    public Subscriber getNewSubscriber(String topic) {
        Subscriber sub = new NetworkSubscriberSocket(topic);
        new Thread(sub).start();
        return sub;
    }

    @Override
    public Communicator getNewDirectConnection() {
        ClientSocket com = new ClientSocket();
        new Thread(com).start();
        return com;
    }

}
