/**
 *
 */
package it.polimi.network.socket;

import it.polimi.common.observer.ModelEvent;
import it.polimi.network.Topic;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public interface BrokerSocket extends Runnable {

    public void publish(Topic topic, ModelEvent event);

}
