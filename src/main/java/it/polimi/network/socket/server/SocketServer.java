/**
 *
 */
package it.polimi.network.socket.server;

import it.polimi.network.CommunicatorListener;
import it.polimi.network.configuration.NetworkConfiguration;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class SocketServer implements Runnable{

    private static final Logger LOGGER = Logger.getLogger(SocketServer.class.getName());

    private final CommunicatorListener parent;
    private ServerSocket serverSocket;

    private Set<SocketCommunicator> clientSet;

    private boolean active;

    public SocketServer(CommunicatorListener parent) {
        this.parent = parent;
        active = false;
        clientSet = new HashSet<SocketCommunicator>();
    }

    private void openServerSocket() {
        try {
            serverSocket = new ServerSocket(NetworkConfiguration.SOCKET_PORT);
            active = true;
        } catch (IOException e) {
            throw new AssertionError("Weird errors with I/O occured, please verify environment config", e);
        }
    }


    public boolean isActive() {
        return active;
    }

    public void close() {

        try {
            active = false;
            serverSocket.close();
        } catch (IOException e) {
            LOGGER.warning("An error occure while closing the socket " + e);
        }
    }

    @Override
    public void run() {
        openServerSocket();
        LOGGER.info("Server ready");

        try {
            while (isActive()) {
                Socket socket = serverSocket.accept();
                SocketCommunicator client = new SocketCommunicator(socket);
                client.setListener(parent);
                clientSet.add(client);
                new Thread(client).start();
            }
        } catch (IOException ex) {
            throw new AssertionError("Weird errors with I/O occured, please verify environment config", ex);
        }

        LOGGER.info("Server closed");
    }

}

