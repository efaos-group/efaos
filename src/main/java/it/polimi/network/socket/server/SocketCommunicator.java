/**
 *
 */
package it.polimi.network.socket.server;

import it.polimi.network.Communicator;
import it.polimi.network.CommunicatorListener;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Logger;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class SocketCommunicator implements Communicator, Runnable {

    private static final Logger LOGGER = Logger.getLogger(SocketCommunicator.class.getName());

    private Socket socket;
    private Scanner in;
    private PrintWriter out;

    private CommunicatorListener parent;
    private final ConcurrentLinkedQueue<String> bufferOut;

    private boolean active;

    public SocketCommunicator(Socket s) {
        socket=s;
        bufferOut = new ConcurrentLinkedQueue<String>();
        active = false;

        initStream();
    }

    private void initStream() {
        try {
            out = new PrintWriter(socket.getOutputStream());
            in = new Scanner(socket.getInputStream());

        } catch (IOException ex) {
            throw new AssertionError("some weird configuration problem occured or pebkac and you haven't opened the socket yet", ex);
        }
    }

    @Override
    public void send(String msg){
        bufferOut.add(msg);
        synchronized(bufferOut) {
            bufferOut.notify();
        }
    }

    @Override
    public void close() {
        try {
            socket.close();
            active = false;
        } catch (IOException e) {
            LOGGER.warning("something wrong happened while closing a socket, who cares? I don't need it anymore: " + e);
        } finally {
            socket = null;
        }
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void setListener(CommunicatorListener parent) {
        this.parent = parent;
    }

    @Override
    public void run() {
        active = true;
        while(isActive()) {

            try {
                String response = receive();
                parent.notifyMessageReceived(this, response);
            } catch(NoSuchElementException e) {
                LOGGER.warning(e + "");
                parent.notifyConnectionClosed(this);
                active = false;
            }

            String message  = bufferOut.poll();

            if(message != null) {
                out.println(message);
                out.flush();
            }
        }
    }

    private String receive() {
        String response = null;
        while(response == null) {
            response = in.nextLine();
        }

        return response;
    }

}