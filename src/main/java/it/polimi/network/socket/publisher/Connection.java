/**
 *
 */
package it.polimi.network.socket.publisher;

import it.polimi.common.observer.ModelEvent;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Logger;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class Connection implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(Connection.class.getName());

    private Socket client;
    private ObjectOutputStream out;
    private boolean active;

    private final ConcurrentLinkedQueue<ModelEvent> buffer;

    public Connection(Socket client) {
        this.client = client;
        this.buffer = new ConcurrentLinkedQueue<ModelEvent>();
        active = true;

        initStream();
    }

    private void initStream() {
        try {
            out = new ObjectOutputStream(client.getOutputStream());
            out.flush();
        } catch (IOException e) {
            LOGGER.severe("Impossibile to initialize the output stream! " + e);
        }
    }

    public void dispatchMessage(ModelEvent msg) {
        buffer.add(msg);
        synchronized(buffer) {
            buffer.notify();
        }
    }

    @Override
    public void run() {
        try {
            while(isActive()) {
                ModelEvent msg = buffer.poll();
                if(msg == null){
                    synchronized (buffer) {
                        buffer.wait();
                    }
                } else {
                    send(msg);
                }
            }
        } catch (IOException e) {
            LOGGER.severe("The connection has been closed! " + e);
        } catch (InterruptedException e) {
            LOGGER.severe("For some weir error the thread as been interrupted. " + e);
        }
    }

    protected void send(ModelEvent msg) throws IOException {
        out.writeObject(msg);
        out.flush();
        out.reset();
    }

    public void close() {
        closeOutputStream();
        closeSocket();
    }

    private void closeOutputStream() {
        try{
            out.close();
        } catch(IOException exc){
            LOGGER.warning(exc + "Some weird configuration problem occured and you can't close correctly the stream");
        }

    }

    private void closeSocket() {
        try {
            active = false;
            client.close();
        } catch (IOException e) {
            LOGGER.warning(e + "Some weird configuration problem occured and you can't close correctly the socket");
        }
    }

    public boolean isActive() {
        return active;
    }

}
