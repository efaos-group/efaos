/**
 *
 */
package it.polimi.network.socket.publisher;

import it.polimi.common.observer.ModelEvent;
import it.polimi.network.ProtocolParser;
import it.polimi.network.Topic;
import it.polimi.network.configuration.NetworkConfiguration;
import it.polimi.network.socket.BrokerSocket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class PublisherSocket implements BrokerSocket {

    private static final Logger LOGGER = Logger.getLogger(PublisherSocket.class.getName());

    private static final Integer MAX_CLIENTS = 32;

    private ServerSocket serverSocket;
    private volatile boolean active;

    private ExecutorService threadPool = Executors.newFixedThreadPool(MAX_CLIENTS);

    private final Multimap<Topic, Connection> clientList;
    private final List<Topic> bufferTopic;

    public PublisherSocket() {
        clientList = ArrayListMultimap.create();
        bufferTopic = new LinkedList<Topic>();
        active = false;
    }

    @Override
    public void publish(Topic topic, ModelEvent event) {

        for(Connection subscriber : clientList.get(topic)) {
            subscriber.dispatchMessage(event);
        }
    }

    @Override
    public void run() {
        openServerSocket();

        while(isActive()) {

            Socket clientSocket = getNewClientSocket();
            Connection newbie = new Connection(clientSocket);

            if(clientSocket != null) {
                subscribe(clientSocket, newbie);
                threadPool.execute(newbie);
            }
        }

        threadPool.shutdown();
    }

    private void openServerSocket() {
        try {
            active = true;
            this.serverSocket = new ServerSocket(NetworkConfiguration.PUBLISHER_PORT);
        } catch (IOException exc){
            throw new AssertionError(exc + " The server can't be initialized");
        }
    }

    private Socket getNewClientSocket() {
        Socket clientSocket = null;

        try {
            clientSocket = serverSocket.accept();
        } catch (IOException e) {
            LOGGER.severe("Some wierd error occured while estabilishing new connection with the client." + e);
        }
        return clientSocket;
    }

    protected void subscribe(Socket clientSocket, Connection newbie) {
        try {
            BufferedReader in = getInputStream(clientSocket);

            String topic = null;
            while(topic == null) {
                topic = in.readLine();
            }

            addToList(topic, newbie);
        } catch (IOException e) {
            LOGGER.severe("An error occured while waiting the topic from the subscriber! " + e);
        }
    }

    private void addToList(String topic, Connection newbie) {
        Topic topicObject = getTopicFromLists(topic);
        bufferTopic.remove(topicObject);
        clientList.put(topicObject, newbie);
    }

    private Topic getTopicFromLists(String message) {
        Topic topicObject = null;
        String topic = parseTopic(message);

        for(Topic topicID : clientList.keySet()) {
            if(topicID.equalsToMinimalCode(topic)) {
                topicObject = topicID;
            }
        }

        for(Topic topicID : bufferTopic) {
            if(topicID.equalsToMinimalCode(topic)) {
                topicObject = topicID;
            }
        }

        return topicObject;
    }

    private String parseTopic(String message) {
        ProtocolParser parser = new ProtocolParser();
        parser.parseMessage(message);
        return parser.getCommands()[0];
    }

    private BufferedReader getInputStream(Socket clientSocket) {
        BufferedReader in = null;
        try {
            in = new BufferedReader(
                    new InputStreamReader(clientSocket.getInputStream()));
        } catch (IOException e) {
            LOGGER.severe("Some wierd error occured while trying to get new stream with the client." + e);
        }

        return in;
    }

    public synchronized void addTopicToBuffer(Topic topic) {
        if(!bufferTopic.contains(topic) && !clientList.containsKey(topic)) {
            bufferTopic.add(topic);
        }
    }

    public synchronized void stop() {
        active = false;
        closeAllSubscriber();
        closeServer();
    }

    private synchronized void closeServer() {
        try {
            serverSocket.close();
        } catch (IOException e) {
            LOGGER.warning(" Some weird configuration problem occured and you can't close correctly the server socket" + e);
        }
    }

    private void closeAllSubscriber() {
        for(Connection client: clientList.values()) {
            client.close();
        }
    }

    public boolean isActive() {
        return active;
    }

}
