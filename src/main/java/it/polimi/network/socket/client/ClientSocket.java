/**
 *
 */
package it.polimi.network.socket.client;

import it.polimi.network.Communicator;
import it.polimi.network.CommunicatorListener;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Logger;

/**
 * @author Migliorino Lorenzo
 * @author Morreale Luca
 */
public class ClientSocket implements Communicator, Runnable {

    private static final Logger LOGGER = Logger.getLogger(ClientSocket.class.getName());

    private Socket server;
    private Scanner in;
    private PrintWriter out;

    private CommunicatorListener parent;
    private final ConcurrentLinkedQueue<String> bufferOut;

    private static final String HOST = "localhost";
    private static final Integer PORT = 1901;

    private boolean active;

    public ClientSocket() {
        bufferOut = new ConcurrentLinkedQueue<String>();
        active = false;

        initSocket();
        getStreams();
    }

    private void initSocket() throws AssertionError {
        try {
            this.server = new Socket(HOST,PORT);
        } catch (IOException e) {
            throw new AssertionError(e + " Impossible to connect to the server");
        }
    }

    private void getStreams() {
        try {
            in = new Scanner(server.getInputStream());
            out = new PrintWriter(server.getOutputStream());
        } catch (IOException ex) {
            throw new AssertionError("some weird configuration problem occured or pebkac and you haven't opened the socket yet", ex);
        }
    }
    @Override
    public void send(final String message){
        bufferOut.add(message);
        synchronized(bufferOut) {
            bufferOut.notify();
        }
    }

    @Override
    public void close() {
        try {
            active = false;
            synchronized(bufferOut) {
                bufferOut.notify();
            }
            server.close();
        } catch (IOException e) {
            LOGGER.severe(e+"something wrong happened while closing a socket, who cares? I don't need it anymore: " + e);
        }
    }

    @Override
    public void setListener(CommunicatorListener parent) {
        this.parent = parent;
    }

    @Override
    public void run() {
        active = true;
        while(isActive()) {
            String message  = bufferOut.poll();

            if(message == null) {
                try {
                    synchronized(bufferOut) {
                        bufferOut.wait();
                    }
                } catch (InterruptedException e) {
                    LOGGER.severe("An error occure while waiting for the message to send. " + e);
                }
            } else {
                out.println(message);
                out.flush();
                try {
                    String response = receive();
                    parent.notifyMessageReceived(this, response);
                }  catch(NoSuchElementException e) {
                    throw new AssertionError("Server disconnected", e);
                }
            }
        }
    }

    private String receive() {
        String response = null;
        response = in.nextLine();

        if(response == null){
            throw new AssertionError("The server has been disconnected");
        } else {
            return response;
        }
    }

    @Override
    public boolean isActive() {
        return active;
    }
}
