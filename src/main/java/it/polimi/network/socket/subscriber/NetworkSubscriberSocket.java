/**
 *
 */
package it.polimi.network.socket.subscriber;

import it.polimi.common.observer.ModelEvent;
import it.polimi.network.ProtocolParser;
import it.polimi.network.Subscriber;
import it.polimi.network.configuration.NetworkConfiguration;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public class NetworkSubscriberSocket extends Subscriber {

    private static final Logger LOGGER = Logger.getLogger(NetworkSubscriberSocket.class.getName());

    private Socket serverConnection;
    private String topic;
    private boolean active;

    private ObjectInputStream in;
    private PrintWriter out;

    public NetworkSubscriberSocket(String topic) {
        active = false;
        setTopic(topic);

        setUpConnection();
        setStreams();

        subscribe();
    }

    private void setUpConnection() {
        try {
            serverConnection = new Socket(NetworkConfiguration.SOCKET_HOST, NetworkConfiguration.PUBLISHER_PORT);
        } catch(IOException e) {
            throw new AssertionError(e + " Impossible to connect to the server");
        }
    }

    private void setStreams() {
        try {
            out =  new PrintWriter(serverConnection.getOutputStream());
        } catch (IOException e) {
            throw new AssertionError(e + " Impossible to create a stream to send messages to the server");
        }

        try {
            in = new ObjectInputStream(serverConnection.getInputStream());
        } catch (IOException e) {
            throw new AssertionError(e + " Impossible to create a stream to receive messages from the server");
        }
    }

    private void subscribe() {
        out.println(topic);
        out.flush();
    }

    @Override
    public void run() {
        active = true;

        try {
            ModelEvent message = null;
            while(active) {
                message = receive();

                if(message != null) {
                    super.notify(message);
                } else {
                    throw new AssertionError("The server has been disconnected!");
                }
            }
        } catch(IOException e) {
            throw new AssertionError("The server has been disconnected!", e);
        }
    }

    private ModelEvent receive() throws IOException {
        ModelEvent message = null;
        try {
            message = (ModelEvent) in.readObject();
        } catch (ClassNotFoundException e) {
            LOGGER.severe(e + " some weird error has occured during receiving the message!");
            active = false;
        }
        return message;
    }

    @Override
    public void close() {
        closeInputStream();
        closeSocketConnection();
        active = false;
    }

    private void closeInputStream() {
        try {
            in.close();
        } catch (IOException e) {
            LOGGER.warning(e + " weird error while closing input stream");
        }
    }

    private void closeSocketConnection() {
        try {
            serverConnection.close();
        } catch (IOException e) {
            LOGGER.warning(e + " weird error while closing socket");
        }
    }

    @Override
    public void setTopic(String topic) {
        ProtocolParser parser = new ProtocolParser();
        parser.setCommands(new String[]{topic, topic, topic});
        this.topic = parser.getParsedMessage();
    }

}
