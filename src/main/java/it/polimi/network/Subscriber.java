/**
 *
 */
package it.polimi.network;

import it.polimi.common.observer.ObjectObservable;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public abstract class Subscriber extends ObjectObservable implements Runnable {

    public abstract void setTopic(String topic);

    public abstract void close();

}
