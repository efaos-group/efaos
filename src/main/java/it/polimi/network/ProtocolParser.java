/**
 *
 */
package it.polimi.network;

import it.polimi.network.exception.ProtocolCommandsException;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * @author Morreale Luca
 * @author Migliorino Lorenzo
 */
public final class ProtocolParser {

    private static final Integer MAX_COMMANDS = 3;
    public static final Integer EMPTY_MESSAGE = 9;

    public static final Character NEW_CONNECTION = '$';
    public static final Character NEW_GAME = '£';
    private static final Character COMMAND_DELIMITER = '#';
    private static final Character OPEN_ARGUMENTS_DELIMITER = '[';
    private static final Character CLOSE_ARGUMENTS_DELIMITER = ']';
    public static final String ACK = "OK";

    public static final String PASS_TURN = "pass";
    public static final String MOVE = "move";
    public static final String USE_CARD = "usecard";
    public static final String DRAW_ITEM_CARD = "drawitemcard";
    public static final String DISCARD_CARD = "discardcard";
    public static final String DRAW_SECTOR_CARD = "drawsectorcard";
    public static final String ATTACK = "attack";
    public static final String SELECT_SECTOR = "selectsector";
    public static final String ACCEPTED = "accepted";
    public static final String REJECTED = "rejected";

    private String topic;
    private String playerID;

    private String commands;
    private String[] commandsReceived;
    private String arguments;
    private String[] argumentsReceived;

    public ProtocolParser() {
        resetProtocolSetUp();
    }

    public void resetProtocolSetUp() {
        this.topic = NEW_CONNECTION+"";
        this.playerID = "";
        resetParsedMessage();
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public void setPlayerID(String playerID) {
        this.playerID = playerID;
    }

    public void parseMessage(String message) {
        parseCommands(message);
        parseArguments(message);

    }

    private void parseCommands(String message) {
        StringTokenizer commandParser = new StringTokenizer(message, COMMAND_DELIMITER + "");
        List<String> commandsList = new ArrayList<String>();
        for(int i = 0; i < MAX_COMMANDS; i++) {
            commandsList.add(commandParser.nextToken());
        }
        this.commandsReceived = castListToArray(commandsList);
    }

    private void parseArguments(String message) {
        StringTokenizer argumentParser = new StringTokenizer(message, OPEN_ARGUMENTS_DELIMITER + "");
        argumentParser.nextToken();
        List<String> argumentsList = new ArrayList<String>();

        while(argumentParser.hasMoreTokens()){
            String token = argumentParser.nextToken();
            token = token.split(CLOSE_ARGUMENTS_DELIMITER + "")[0];
            argumentsList.add(token);
        }

        this.argumentsReceived = castListToArray(argumentsList);
    }

    private String[] castListToArray(List<String> list) {
        String[] array = new String[list.size()];

        for(int i=0; i<list.size(); i++) {
            array[i] = list.get(i);
        }

        return array;
    }

    public String[] getMessageArguments() {
        return this.argumentsReceived;
    }

    public String[] getCommands() {
        return this.commandsReceived;
    }

    public void setCommands() {
        String[] commandsArray = new String[]{topic, playerID, NEW_CONNECTION+""};
        setCommands(commandsArray);
    }

    public void setCommands(String command) {
        String[] commandsArray = new String[]{topic, playerID, command};
        setCommands(commandsArray);
    }

    public void setCommands(String[] commandsArray) {
        if(commandsArray.length > MAX_COMMANDS) {
            throw new ProtocolCommandsException("Too many commands are given");
        }
        this.commands = generateStringProtocol(commandsArray, COMMAND_DELIMITER, COMMAND_DELIMITER);
    }

    private String generateStringProtocol(String[] array, Character openingTag, Character closingTag) {
        String generatedString = "";
        for(int i = 0; i < array.length; i++) {
            generatedString += openingTag + array[i] + closingTag;
        }
        return generatedString;
    }

    public void setMessageArguments(String[] argumentsArray) {
        this.arguments = generateStringProtocol(argumentsArray, OPEN_ARGUMENTS_DELIMITER, CLOSE_ARGUMENTS_DELIMITER);
    }

    public String getParsedMessage() {
        String temporaryCommands = this.commands;
        String temporaryArguments = this.arguments;
        resetParsedMessage();
        return temporaryCommands + temporaryArguments;
    }

    private void resetParsedMessage() {
        this.commands = "";
        this.arguments = "";
    }

}
